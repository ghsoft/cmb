LUEG Custom Reports Deployment Instructions:

1. Deploy the latest lueg-backend.war
2. Restart OAS
3. Run this Rest to add the default SL:
	@POST: http://localhost:8080/lueg-backend/rest/caseservicepoint/sl/services
	@Body: {
 		"serviceId": 0,
        "serviceName": "default",
        "f2f": 10,
        "pp": 30,
  		"wt": 15
	}
4. Run lueg-custom-reports-setup.sql
5. Drop folder "lueg-score-reports" in "D:\Qmatic\orchestra\system\pentaho-solutions\qmaticbi"
6. Update reports caches