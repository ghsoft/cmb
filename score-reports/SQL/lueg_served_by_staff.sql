DECLARE @branch NVARCHAR(100) = 'LUEG';

/*
DECLARE @startTime INT = ${startTime};
DECLARE @endTime INT = ${endTime}; 
DECLARE @fromDate DATE = ${FromDate}; 
DECLARE @toDate DATE = ${ToDate};
DECLARE @openTime INT = ${openTime}; 
DECLARE @startLunchTime INT = ${startLunchTime}; 
DECLARE @endLunchTime INT = ${endLunchTime};
DECLARE @serviceType BIT = ${serviceType};
*/

DECLARE @fromDate DATE = '2014-08-01';
DECLARE @toDate DATE = '2014-08-30';
DECLARE @startTime INT = 27000;
DECLARE @endTime INT = 57600;
DECLARE @openTime INT = 28800;
DECLARE @startLunchTime INT = 42300;
DECLARE @endLunchTime INT = 45000;
DECLARE @serviceType BIT = 0;

SELECT	@branch AS BRANCH_NAME,
		lfcs.served_by_user_name AS FULL_NAME,
		LOWER(lfcs.served_by_user_id) AS USER_NAME,
		COUNT(CASE WHEN service_id=65 THEN 1 ELSE NULL END) AS S01,
		COUNT(CASE WHEN service_id=66 THEN 1 ELSE NULL END) AS S02,
		COUNT(CASE WHEN service_id=67 THEN 1 ELSE NULL END) AS S03,
		COUNT(CASE WHEN service_id=68 THEN 1 ELSE NULL END) AS S04,
		COUNT(CASE WHEN service_id=69 THEN 1 ELSE NULL END) AS S05,
		COUNT(CASE WHEN service_id=70 THEN 1 ELSE NULL END) AS S06,
		COUNT(CASE WHEN service_id=71 THEN 1 ELSE NULL END) AS S07,
		COUNT(CASE WHEN service_id=72 THEN 1 ELSE NULL END) AS S08,
		COUNT(CASE WHEN service_id=73 THEN 1 ELSE NULL END) AS S09,
		COUNT(CASE WHEN service_id=74 THEN 1 ELSE NULL END) AS S10,
		COUNT(CASE WHEN service_id=75 THEN 1 ELSE NULL END) AS S11,
		COUNT(CASE WHEN service_id=76 THEN 1 ELSE NULL END) AS S12,
		COUNT(CASE WHEN service_id=77 THEN 1 ELSE NULL END) AS S13,
		COUNT(CASE WHEN service_id=78 THEN 1 ELSE NULL END) AS S14,
		COUNT(CASE WHEN service_id=79 THEN 1 ELSE NULL END) AS S15,
		COUNT(CASE WHEN service_id=80 THEN 1 ELSE NULL END) AS S16,
		COUNT(CASE WHEN service_id=81 THEN 1 ELSE NULL END) AS S17,
		COUNT(CASE WHEN service_id=82 THEN 1 ELSE NULL END) AS S18,
		COUNT(CASE WHEN service_id=83 THEN 1 ELSE NULL END) AS S19,
		COUNT(CASE WHEN service_id=84 THEN 1 ELSE NULL END) AS S20,
		COUNT(CASE WHEN service_id=85 THEN 1 ELSE NULL END) AS S21,
		COUNT(CASE WHEN service_id=86 THEN 1 ELSE NULL END) AS S22,
		COUNT(CASE WHEN service_id=87 THEN 1 ELSE NULL END) AS S23,
		COUNT(CASE WHEN service_id=88 THEN 1 ELSE NULL END) AS S24,
		COUNT(CASE WHEN service_id=89 THEN 1 ELSE NULL END) AS S25,
		COUNT(CASE WHEN service_id=90 THEN 1 ELSE NULL END) AS S26,
		COUNT(CASE WHEN service_id=91 THEN 1 ELSE NULL END) AS S27,
		COUNT(CASE WHEN service_id=92 THEN 1 ELSE NULL END) AS S28,
		COUNT(1) AS S_TOTAL
FROM	stat.lueg_fact_case_services lfcs
WHERE 
		(LOWER(lfcs.served_by_user_id) IN ('asantos2') OR LOWER(lfcs.served_by_user_id) LIKE (CASE WHEN '%' IN ('%') THEN '%' ELSE '' END))
		--(LOWER(lfcs.served_by_user_id) IN ('eshorb') OR LOWER(lfcs.served_by_user_id) LIKE (CASE WHEN '%' IN ('eshorb') THEN '%' ELSE '' END))
		--(LOWER(lfcs.served_by_user_id) IN (${Staff}) OR LOWER(lfcs.served_by_user_id) LIKE (CASE WHEN '%' IN (${Staff}) THEN '%' ELSE '' END))
		AND lfcs.status='FINISHED' 
		AND ISNULL(lfcs.servingTime, 0) > 0
		AND (CASE WHEN @serviceType=1 THEN ISNULL(lfcs.post_processing_time, 0) ELSE 1 END) > 0
		AND lfcs.servingTime>0
		AND lfcs.served_by_user_name <> 'Anonymous User' AND LOWER(lfcs.served_by_user_id) <> 'superadmin'
		AND lfcs.service_name<>'TRASH'
		AND CAST(lfcs.created AS DATE)>=@fromDate
		AND CAST(lfcs.created AS DATE)<=@toDate
		AND DATEDIFF(SECOND, CONVERT(DATE, lfcs.created), lfcs.created) >= @startTime
		AND DATEDIFF(SECOND, CONVERT(DATE, lfcs.created), lfcs.created) <= @endTime
GROUP BY lfcs.served_by_user_name, LOWER(lfcs.served_by_user_id)