DECLARE @branch NVARCHAR(100) = 'LUEG';
/*
DECLARE @startTime INT = ${startTime};
DECLARE @endTime INT = ${endTime};
DECLARE @fromDate DATE = ${FromDate};
DECLARE @toDate DATE = ${ToDate};
DECLARE @openTime INT = ${openTime}; 
DECLARE @startLunchTime INT = ${startLunchTime};
DECLARE @endLunchTime INT = ${endLunchTime};
*/

DECLARE @fromDate DATE = '2014-01-01';
DECLARE @toDate DATE = '2014-12-30';
DECLARE @startTime INT = 27000;
DECLARE @endTime INT = 57600;
DECLARE @openTime INT = 28800;
DECLARE @startLunchTime INT = 42300;
DECLARE @endLunchTime INT = 45000;

SELECT	@branch BRANCH_NAME,
		CONVERT(nvarchar, CAST(lfc.enter_date AS DATE), 101) FULL_DATE,
		COUNT(case_id) CASES_ENTERED,
		COUNT(CASE WHEN lfc.finished > 0 OR lfc.noshow > 0 THEN 1 ELSE NULL END) CASES_CALLED,
		COUNT(CASE WHEN lfc.finished = lfc.added THEN 1 ELSE NULL END) CASES_FINISHED,
		COUNT(CASE WHEN lfc.finished = 0 AND lfc.noshow > 0 THEN 1 ELSE NULL END) CASES_NOSHOW,
		SUM(lfc.finished) SERVICES_FINISHED
FROM	
	(SELECT c.id AS case_id,
			fcs.added,
			fcs.finished AS finished,
			fcs.noshow AS noshow,
			fcs.rest AS rest,
			CAST(c.created AS DATE) AS enter_date,
			DATEDIFF(SECOND, CONVERT(DATE, c.created), c.created) AS enter_time
	FROM	stat.lueg_fact_cases c
	LEFT JOIN
		(SELECT	parent_case_id,
				COUNT(id) AS added,
				ISNULL(COUNT(CASE WHEN status='FINISHED' THEN 1 ELSE NULL END), 0) AS finished,
				ISNULL(COUNT(CASE WHEN status='NOSHOW' THEN 1 ELSE NULL END), 0) AS noshow,
				ISNULL(COUNT(CASE WHEN status<>'FINISHED' AND status<>'NOSHOW' THEN 1 ELSE NULL END), 0) AS rest
		FROM	stat.lueg_fact_case_services cs
		WHERE	service_name <> 'TRASH'
		GROUP BY parent_case_id
		) fcs ON c.id=fcs.parent_case_id
	) lfc
WHERE lfc.enter_date >= @fromDate
AND lfc.enter_date <= @toDate
AND lfc.enter_time >= @startTime
AND lfc.enter_time <= @endTime
GROUP BY lfc.enter_date
ORDER BY lfc.enter_date--, lfc.enter_time