DECLARE @branch NVARCHAR(100) = 'LUEG';
/*
DECLARE @startTime INT = ${startTime};
DECLARE @endTime INT = ${endTime}; 
DECLARE @fromDate DATE = ${FromDate}; 
DECLARE @toDate DATE = ${ToDate};
DECLARE @openTime INT = ${openTime}; 
DECLARE @startLunchTime INT = ${startLunchTime}; 
DECLARE @endLunchTime INT = ${endLunchTime};
*/

DECLARE @fromDate DATE = '2014-09-01';
DECLARE @toDate DATE = '2014-09-30';
DECLARE @startTime INT = 27000;
DECLARE @endTime INT = 57600;
DECLARE @openTime INT = 28800;
DECLARE @startLunchTime INT = 42300;
DECLARE @endLunchTime INT = 45000;

SELECT	@branch AS BRANCH_NAME,
		FULL_NAME,
		USER_NAME,
		SUM(TOTAL_LOGIN_TIME) AS TOTAL_LOGIN_TIME,
		MIN(FIRST_LOGIN) AS FIRST_LOGIN,
		MAX(LAST_LOGOUT) LAST_LOGOUT,
		ISNULL(SUM(fcs.F2F), 0) F2F,
		ISNULL(SUM(fcs.PP), 0) PP,
		ISNULL(SUM(fcs.F2F), 0) TR,
		ISNULL(SUM(fcs.NO_SHOWS), 0) NO_SHOWS,
		ISNULL(SUM(fcs.SUM_F2FT), 0) SUM_F2FT,
		ISNULL(MAX(fcs.MAX_F2FT), 0) MAX_F2FT,
		ISNULL(AVG(fcs.AVG_F2FT), 0) AVG_F2FT,
		ISNULL(SUM(fcs.SUM_PPT), 0) SUM_PPT,
		ISNULL(MAX(fcs.MAX_PPT), 0) MAX_PPT,
		ISNULL(AVG(fcs.AVG_PPT), 0) AVG_PPT,
		ISNULL(SUM(fcs.SUM_TT), 0) SUM_TRT,
		ISNULL(MAX(fcs.MAX_TT), 0) MAX_TT,
		ISNULL(AVG(fcs.AVG_TT), 0) AVG_TRT
FROM	(	
			SELECT  (dst.first_name + ' ' + dst.last_name) AS FULL_NAME,
					LOWER(dst.name) AS USER_NAME,
					SUM(total_time) AS TOTAL_LOGIN_TIME,
    				MIN(time_key) AS FIRST_LOGIN,
					MAX(CASE WHEN (time_key+total_time) > 86340 THEN NULL ELSE (time_key+total_time) END) AS LAST_LOGOUT,
					NULL AS F2F,
					NULL AS PP,
					NULL AS NO_SHOWS,
					NULL AS SUM_F2FT,
					NULL AS MAX_F2FT,
					NULL AS AVG_F2FT,
					NULL AS SUM_PPT,
					NULL AS MAX_PPT,
					NULL AS AVG_PPT,
					NULL AS SUM_TT,
					NULL AS MAX_TT,
					NULL AS AVG_TT
			FROM
					(
						SELECT	branch_key, 
								date_key, 
								time_key, 
								staff_key,
								(CASE WHEN (time_key + total_time) > @endTime THEN (@endTime - time_key) ELSE total_time END) AS total_time
						FROM stat.fact_staff_session
					) AS fss INNER JOIN 
					stat.dim_date dd ON fss.date_key = dd.id INNER JOIN 
					stat.dim_branch db ON fss.branch_key = db.id INNER JOIN 
					(
						SELECT * 
						FROM stat.dim_staff 
						WHERE (LOWER(name) IN ('asantos2') OR LOWER(name) LIKE (CASE WHEN '%' IN ('%') THEN '%' ELSE '' END))
						--WHERE (LOWER(name) IN ('asantos2') OR LOWER(name) LIKE (CASE WHEN '%' IN ('asantos2') THEN '%' ELSE '' END))
						--WHERE (LOWER(name) IN (${Staff}) OR LOWER(name) LIKE (CASE WHEN '%' IN (${Staff}) THEN '%' ELSE '' END))
					) dst ON fss.staff_key = dst.id
			WHERE
					db.name IN (@branch) AND
					full_date >= @fromDate AND
					full_date <= @toDate
			GROUP BY dst.first_name, dst.last_name, dst.name

			UNION ALL
			SELECT  lfcs.served_by_user_name AS FULL_NAME,
					LOWER(lfcs.served_by_user_id) AS USER_NAME,
					NULL AS TOTAL_LOGIN_TIME,
        			NULL AS FIRST_LOGIN,
					NULL AS LAST_LOGIN,
					COUNT(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN 1 ELSE null END) AS F2F,
					COUNT(lfcs.post_processing_time) AS PP,
					COUNT(CASE WHEN lfcs.status='NOSHOW' THEN 1 ELSE null END) AS NO_SHOWS,
					SUM(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN ISNULL(lfcs.servingTime,0) ELSE null END) AS SUM_F2FT,
					MAX(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN ISNULL(lfcs.servingTime,0) ELSE null END) AS MAX_F2FT,
					AVG(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN ISNULL(lfcs.servingTime,0) ELSE null END) AS AVG_F2FT,
					SUM(lfcs.post_processing_time) AS SUM_PPT,
					MAX(lfcs.post_processing_time) AS MAX_PPT,
					AVG(lfcs.post_processing_time) AS AVG_PPT,
					SUM(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN ISNULL(lfcs.servingTime,0)+ISNULL(lfcs.post_processing_time,0) ELSE null END) AS SUM_TT,
					MAX(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN ISNULL(lfcs.servingTime,0)+ISNULL(lfcs.post_processing_time,0) ELSE null END) AS MAX_TT,
					AVG(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN ISNULL(lfcs.servingTime,0)+ISNULL(lfcs.post_processing_time,0) ELSE null END) AS AVG_TT
			FROM
					stat.lueg_fact_case_services lfcs
			WHERE 
					(LOWER(lfcs.served_by_user_id) IN ('asantos2') OR LOWER(lfcs.served_by_user_id) LIKE (CASE WHEN '%' IN ('%') THEN '%' ELSE '' END))
					--(LOWER(lfcs.served_by_user_id) IN ('asantos2') OR LOWER(lfcs.served_by_user_id) LIKE (CASE WHEN '%' IN ('asantos2') THEN '%' ELSE '' END))
					--(LOWER(lfcs.served_by_user_id) IN (${Staff}) OR LOWER(lfcs.served_by_user_id) LIKE (CASE WHEN '%' IN (${Staff}) THEN '%' ELSE '' END))
					AND lfcs.service_name<>'TRASH'
					AND CAST(lfcs.created AS DATE)>=@fromDate
					AND CAST(lfcs.created AS DATE)<=@toDate
					AND DATEDIFF(SECOND, CONVERT(DATE, lfcs.created), lfcs.created) >= @startTime
					AND DATEDIFF(SECOND, CONVERT(DATE, lfcs.created), lfcs.created) <= @endTime
			GROUP BY lfcs.served_by_user_name, lfcs.served_by_user_id
			) AS fcs
WHERE FULL_NAME <> 'Anonymous User' AND USER_NAME <> 'superadmin'
GROUP BY FULL_NAME, USER_NAME
HAVING   (NOT (SUM(fcs.TOTAL_LOGIN_TIME) IS NULL))
ORDER BY FULL_NAME