DECLARE @branch NVARCHAR(100) = 'LUEG';
/*
DECLARE @interval INT = ${interval};
DECLARE @period INT = ${period};
DECLARE @startTime INT = ${startTime};
DECLARE @endTime INT = ${endTime};
DECLARE @fromDate DATE = ${FromDate};
DECLARE @toDate DATE = ${ToDate};
DECLARE @openTime INT = ${openTime}; 
DECLARE @startLunchTime INT = ${startLunchTime};
DECLARE @endLunchTime INT = ${endLunchTime};
*/

DECLARE @interval INT = 5;
DECLARE @period INT = 30;
DECLARE @fromDate DATE = '2014-09-01';
DECLARE @toDate DATE = '2014-09-30';
DECLARE @startTime INT = 27000;
DECLARE @endTime INT = 57600;
DECLARE @openTime INT = 28800;
DECLARE @startLunchTime INT = 42300;
DECLARE @endLunchTime INT = 45000;

SELECT	@branch AS BRANCH_NAME,
		dt.time_slot AS SLOT,
		COUNT(fcs.created) AS WT,
		COUNT(CASE WHEN fcs.actual_waiting_time <= (60 * 1 * @interval) THEN 1 ELSE null END) AS INTERVAL1,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 1 * @interval) AND fcs.actual_waiting_time <= (60 * 2 * @interval) THEN 1 ELSE null END) AS INTERVAL2,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 2 * @interval) AND fcs.actual_waiting_time <= (60 * 3 * @interval) THEN 1 ELSE null END) AS INTERVAL3,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 3 * @interval) AND fcs.actual_waiting_time <= (60 * 4 * @interval) THEN 1 ELSE null END) AS INTERVAL4,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 4 * @interval) AND fcs.actual_waiting_time <= (60 * 5 * @interval) THEN 1 ELSE null END) AS INTERVAL5,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 5 * @interval) AND fcs.actual_waiting_time <= (60 * 6 * @interval) THEN 1 ELSE null END) AS INTERVAL6,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 6 * @interval) AND fcs.actual_waiting_time <= (60 * 7 * @interval) THEN 1 ELSE null END) AS INTERVAL7,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 7 * @interval) AND fcs.actual_waiting_time <= (60 * 8 * @interval) THEN 1 ELSE null END) AS INTERVAL8,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 8 * @interval) AND fcs.actual_waiting_time <= (60 * 9 * @interval) THEN 1 ELSE null END) AS INTERVAL9,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 9 * @interval) THEN 1 ELSE null END) AS REST,
		SUM(CASE WHEN fcs.actual_waiting_time > ISNULL(sl.wt_lvl, default_sl.wt_lvl) THEN 1 ELSE 0 END) AS ABOVE_WTT_SL
FROM
(
	SELECT DISTINCT FLOOR((FLOOR(id - @startTime) / (@period * 60))) AS time_slot
	FROM stat.dim_time
	WHERE id>=@startTime
	AND id<@endTime
) AS dt
INNER JOIN
(	SELECT 	(CASE WHEN enter_time < @openTime THEN /*entered before opening*/
				(CASE WHEN (waiting_time - (@openTime - enter_time)) > 0 THEN waiting_time - (@openTime - enter_time) ELSE 0 END)
			ELSE
				(CASE WHEN enter_time > @startLunchTime AND enter_time < @endLunchTime THEN /*entered during lunch - called during/after lunch*/
					(CASE WHEN (waiting_time - (@endLunchTime - enter_time)) > 0 THEN waiting_time - (@endLunchTime - enter_time) ELSE 0 END)
				ELSE 
					(CASE WHEN enter_time < @startLunchTime AND enter_time + waiting_time > @endLunchTime  THEN  waiting_time - (@endLunchTime - @startLunchTime) /*entered before lunch - called after lunch*/
					ELSE
						(CASE WHEN enter_time < @startLunchTime AND enter_time + waiting_time < @startLunchTime AND enter_time + waiting_time > @endLunchTime  THEN  (@endLunchTime - enter_time) /*entered before lunch - called after lunch*/
						ELSE
							waiting_time 
						END) 
					END) 
				END) 
			END) AS actual_waiting_time,
			*
	FROM 
	(
		SELECT	FLOOR((FLOOR(DATEDIFF(MINUTE, CONVERT(DATE, created), created)) * 60 - @startTime) / (@period * 60)) AS time_slot,
				DATEDIFF(SECOND, CONVERT(DATE, created), created) AS enter_time, 
				*
		FROM stat.lueg_fact_case_services
		WHERE service_name<>'TRASH'
		AND waiting_time>=0
		AND CAST(created AS DATE)>=@fromDate
		AND CAST(created AS DATE)<=@toDate
	) AS temp_fcs
) AS fcs ON dt.time_slot = fcs.time_slot
LEFT JOIN
(
	SELECT *
	FROM stat.lueg_dim_service_level ldsl
) sl ON fcs.service_id=sl.service_id
INNER JOIN
(
	SELECT *
	FROM stat.lueg_dim_service_level ldsl
	WHERE ldsl.service_id=0
) default_sl ON 1=1
WHERE fcs.service_name<>'TRASH'
AND fcs.waiting_time>=0
AND CAST(fcs.created AS DATE)>=@fromDate
AND CAST(fcs.created AS DATE)<=@toDate
AND fcs.enter_time >= @startTime
AND fcs.enter_time <= @endTime
--AND fcs.service_name IN ('Permit Center Screening', 'Pre-Review - Zoning', 'Pre-Review - Engineer', 'Pre-Review - Land Development', 'PDS Bld - Appointments')
--AND fcs.service_name IN (${Service})
GROUP BY dt.time_slot
ORDER BY dt.time_slot