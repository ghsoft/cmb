DECLARE @branch NVARCHAR(100) = 'LUEG';
/*
DECLARE @customerId INT = ${CustomerId};
DECLARE @fromDate DATE = ${FromDate}; 
DECLARE @toDate DATE = ${ToDate};
*/
DECLARE @customerId INT = 34035;
DECLARE @fromDate DATE = '2014-07-22';
DECLARE @toDate DATE = '2014-09-22';


SELECT	@branch AS BRANCH_NAME,
		qp_visits.visit_key AS VISIT_KEY,
		qp_visits.service_key AS SERVICE_KEY,
		CONVERT(nvarchar, CAST(lfc.created AS DATE), 101) AS VISIT_DATE,
		qp_printer.printer_name AS PRINTER,
		lfc.ticket_number AS TICKET_NUMBER,
		lfcs.order_idx AS SERVICE_ORDER,
		lfc.customer_name AS CUSTOMER_NAME,
		lfc.customer_id AS CUSTOMER_ID,
		lfcs.id AS CASE_SERVICE_ID,
		lfcs.service_name AS SERVICE_NAME,
		lfcs.status AS SERVICE_STATUS,
		lfcs.vip AS VIP,
		lfcs.created AS CREATED,
		CONVERT(varchar(11), CAST(lfcs.created AS TIME), 22) AS ENTERED,
		ISNULL(lfcs.waiting_time, 0) AS WAITING_TIME,
		CONVERT(varchar(11), CAST(lfcs.serving_start AS TIME), 22) AS F2F_START,
		ISNULL(lfcs.servingTime, 0) AS F2F_TIME,
		CONVERT(varchar(11), CAST(lfcs.post_processing_start AS TIME), 22) AS PP_START,
		ISNULL(lfcs.post_processing_time, 0) AS PP_TIME,
		(CASE WHEN lfcs.status='FINISHED' THEN CONVERT(varchar(11), CAST(lfcs.updated AS TIME), 22) ELSE NULL END) AS FINISHED,
		lfcs.record_id AS RECORD_ID,
		lfcs.served_by_user_name AS SERVED_BY,
		lfcs.served_by_counter_name AS SERVED_AT
FROM stat.lueg_fact_cases lfc
INNER JOIN stat.lueg_fact_case_services lfcs ON lfc.id=lfcs.parent_case_id
LEFT JOIN (
	SELECT 	dep.name AS printer_name,
			CAST(dd.full_date AS DATE) AS full_date,
			dv.ticket_id AS ticket_number
	FROM	stat.fact_visit_transaction fvt
	INNER JOIN (
		SELECT DISTINCT vt.visit_key, MIN(vt.id) AS min_id
		FROM stat.fact_visit_transaction vt 
		GROUP BY vt.visit_key
	) small_fvt ON small_fvt.visit_key=fvt.visit_key
	INNER JOIN stat.dim_visit dv ON fvt.visit_key=dv.id
	INNER JOIN stat.dim_date dd ON fvt.date_key=dd.id
	INNER JOIN stat.dim_entry_point dep ON dep.id=fvt.entry_point_key
	WHERE fvt.id=small_fvt.min_id
) AS qp_printer ON CAST(lfc.created AS DATE)=qp_printer.full_date AND lfc.ticket_number=qp_printer.ticket_number
LEFT JOIN (
	SELECT 	CAST(dd.full_date AS DATE) AS full_date,
			fvt.visit_key AS visit_key,
			dv.ticket_id AS ticket_number,
			fvt.service_key AS service_key,
			ds.name AS service_name
	FROM	stat.fact_visit_transaction fvt
	INNER JOIN stat.dim_visit dv ON fvt.visit_key=dv.id
	INNER JOIN stat.dim_date dd ON fvt.date_key=dd.id
	INNER JOIN stat.dim_service ds ON fvt.service_key=ds.id
	WHERE fvt.visit_outcome_key=1
) AS qp_visits ON CAST(lfc.created AS DATE)=qp_visits.full_date AND lfc.ticket_number=qp_visits.ticket_number AND lfcs.service_name=qp_visits.service_name
WHERE lfcs.service_name<>'TRASH'
AND CAST(lfc.created AS DATE)>=@fromDate
AND CAST(lfc.created AS DATE)<=@toDate
AND lfc.customer_id=@customerId
ORDER BY lfc.created, TICKET_NUMBER, SERVICE_ORDER                                                                                          