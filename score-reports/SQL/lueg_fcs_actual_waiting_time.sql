/*
DECLARE @interval INT = 5;
DECLARE @period INT = 30;
DECLARE @fromDate DATE = '2014-09-01';
DECLARE @toDate DATE = '2014-09-30';
DECLARE @startTime INT = 27000;
DECLARE @endTime INT = 57600;
DECLARE @openTime INT = 28800;
DECLARE @startLunchTime INT = 42300;
DECLARE @endLunchTime INT = 45000;
*/

DECLARE @interval INT = 5;
DECLARE @period INT = 30;
DECLARE @fromDate DATE = '2014-09-01';
DECLARE @toDate DATE = '2014-09-30';
DECLARE @startTime INT = 27000;
DECLARE @endTime INT = 57600;
DECLARE @openTime INT = 28800;
DECLARE @startLunchTime INT = 42300;
DECLARE @endLunchTime INT = 45000;

SELECT 	(CASE WHEN enter_time < @openTime THEN /*entered before opening*/
			(CASE WHEN (waiting_time - (@openTime - enter_time)) > 0 THEN waiting_time - (@openTime - enter_time) ELSE 0 END)
		ELSE
			(CASE WHEN enter_time > @startLunchTime AND enter_time < @endLunchTime THEN /*entered during lunch - called during/after lunch*/
				(CASE WHEN (waiting_time - (@endLunchTime - enter_time)) > 0 THEN waiting_time - (@endLunchTime - enter_time) ELSE 0 END)
			ELSE 
				(CASE WHEN enter_time < @startLunchTime AND enter_time + waiting_time > @endLunchTime  THEN  waiting_time - (@endLunchTime - @startLunchTime) /*entered before lunch - called after lunch*/
				ELSE
					(CASE WHEN enter_time < @startLunchTime AND enter_time + waiting_time < @startLunchTime AND enter_time + waiting_time > @endLunchTime  THEN  (@endLunchTime - enter_time) /*entered before lunch - called after lunch*/
					ELSE
						waiting_time 
					END) 
				END) 
			END) 
		END) AS actual_waiting_time,
		*
FROM 
(
	SELECT	FLOOR(DATEDIFF(MINUTE, CONVERT(DATE, created), created))*60 AS time_key, 
			DATEDIFF(SECOND, CONVERT(DATE, created), created) AS enter_time, 
			*
	FROM stat.lueg_fact_case_services
	WHERE service_name<>'TRASH'
	AND waiting_time>=0
	AND CAST(created AS DATE)>=@fromDate
	AND CAST(created AS DATE)<=@toDate
	AND FLOOR(DATEDIFF(MINUTE, CONVERT(DATE, created), created))*60 >= @startTime
 	AND FLOOR(DATEDIFF(MINUTE, CONVERT(DATE, created), created))*60 <= @endTime
) AS temp_fcs
--WHERE service_name IN ('Pre-Review - Zoning')
ORDER BY created