DECLARE @branch NVARCHAR(100) = 'LUEG';
/*
DECLARE @startTime INT = ${startTime};
DECLARE @endTime INT = ${endTime}; 
DECLARE @fromDate DATE = ${FromDate}; 
DECLARE @toDate DATE = ${ToDate};
DECLARE @openTime INT = ${openTime}; 
DECLARE @startLunchTime INT = ${startLunchTime}; 
DECLARE @endLunchTime INT = ${endLunchTime};
*/

DECLARE @fromDate DATE = '2014-09-01';
DECLARE @toDate DATE = '2014-09-30';
DECLARE @startTime INT = 27000;
DECLARE @endTime INT = 57600;
DECLARE @openTime INT = 28800;
DECLARE @startLunchTime INT = 42300;
DECLARE @endLunchTime INT = 45000;


SELECT	@branch BRANCH_NAME,
		ISNULL(dim_services.YEAR_INT, 0) YEAR_INT,
		ISNULL(dim_services.MONTH_INT, 0) MONTH_INT,
		ISNULL(dim_services.MONTH_NAME_EN, 0) MONTH_NAME_EN,
		dim_services.SERVICE_ID SERVICE_ID,
		dim_services.SERVICE_NAME SERVICE_NAME,
		ISNULL(SUM(fcs.ARRIVED), 0) ARRIVED,
		ISNULL(SUM(fcs.WAITED), 0) WAITED,
		ISNULL(SUM(fcs.F2F), 0) F2F,
		ISNULL(SUM(fcs.PP), 0) PP,
		ISNULL(SUM(fcs.F2F), 0) TR,
		ISNULL(SUM(fcs.NO_SHOWS), 0) NO_SHOWS,
		ISNULL(SUM(fcs.ABOVE_F2FT_SL), 0) ABOVE_F2FT_SL,
		ISNULL(SUM(fcs.SUM_F2FT), 0) SUM_F2FT,
		ISNULL(MAX(fcs.MAX_F2FT), 0) MAX_F2FT,
		ISNULL(AVG(fcs.AVG_F2FT), 0) AVG_F2FT,
		ISNULL(SUM(fcs.ABOVE_PPT_SL), 0) ABOVE_PPT_SL,
		ISNULL(SUM(fcs.SUM_PPT), 0) SUM_PPT,
		ISNULL(MAX(fcs.MAX_PPT), 0) MAX_PPT,
		ISNULL(AVG(fcs.AVG_PPT), 0) AVG_PPT,
		ISNULL(SUM(fcs.ABOVE_TRT_SL), 0) ABOVE_TRT_SL,
		ISNULL(SUM(fcs.SUM_TT), 0) SUM_TRT,
		ISNULL(AVG(fcs.AVG_TT), 0) AVG_TRT,
		ISNULL(SUM(fcs.ABOVE_WTT_SL), 0) ABOVE_WTT_SL,
		ISNULL(SUM(fcs.SUM_WT), 0) SUM_WT,
		ISNULL(MAX(fcs.MAX_WT), 0) MAX_WT,
		ISNULL(AVG(fcs.AVG_WT), 0) AVG_WT
FROM
(
	SELECT	DISTINCT service_id SERVICE_ID, 
			service_name SERVICE_NAME,
			dim_date.year YEAR_INT,
			dim_date.month MONTH_INT,
			dim_date.month_name_en MONTH_NAME_EN
	FROM	stat.lueg_fact_case_services, stat.dim_date
	WHERE	dim_date.full_date>=@fromDate
	AND		dim_date.full_date<=@toDate
	GROUP	BY service_id, service_name, dim_date.year, dim_date.month, dim_date.month_name_en
) dim_services
LEFT JOIN
(
	SELECT	lfcs.service_id SERVICE_ID,
			lfcs.service_name SERVICE_NAME,
			COUNT(*) ARRIVED,
			COUNT(lfcs.actual_waiting_time) WAITED,
			COUNT(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN 1 ELSE null END) F2F,
			COUNT(lfcs.post_processing_time) AS PP,
			COUNT(CASE WHEN lfcs.status='NOSHOW' THEN 1 ELSE null END) NO_SHOWS,
			SUM(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>ISNULL(sl.f2f_lvl, default_sl.f2f_lvl) THEN 1 ELSE 0 END) ABOVE_F2FT_SL,
			SUM(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN ISNULL(lfcs.servingTime,0) ELSE null END) AS SUM_F2FT,
			MAX(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN ISNULL(lfcs.servingTime,0) ELSE null END) AS MAX_F2FT,
			AVG(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN ISNULL(lfcs.servingTime,0) ELSE null END) AVG_F2FT,
			SUM(CASE WHEN ISNULL(lfcs.post_processing_time,0)>ISNULL(sl.pp_lvl, default_sl.pp_lvl) THEN 1 ELSE 0 END) ABOVE_PPT_SL,
			SUM(lfcs.post_processing_time) AS SUM_PPT,
			MAX(lfcs.post_processing_time) AS MAX_PPT,
			AVG(lfcs.post_processing_time) AS AVG_PPT,
			SUM(CASE WHEN lfcs.status='FINISHED' AND (ISNULL(lfcs.servingTime,0)+ISNULL(lfcs.post_processing_time,0))>ISNULL(sl.tt_lvl, default_sl.tt_lvl) THEN 1 ELSE 0 END) ABOVE_TRT_SL,
			SUM(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN ISNULL(lfcs.servingTime,0)+ISNULL(lfcs.post_processing_time,0) ELSE null END) AS SUM_TT,
			MAX(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN ISNULL(lfcs.servingTime,0)+ISNULL(lfcs.post_processing_time,0) ELSE null END) AS MAX_TT,
			AVG(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN ISNULL(lfcs.servingTime,0)+ISNULL(lfcs.post_processing_time,0) ELSE null END) AS AVG_TT,
			SUM(CASE WHEN lfcs.actual_waiting_time > ISNULL(sl.wt_lvl, default_sl.wt_lvl) THEN 1 ELSE 0 END ) AS ABOVE_WTT_SL,
			SUM(lfcs.actual_waiting_time) AS SUM_WT,
			AVG(lfcs.actual_waiting_time) AS AVG_WT,
			MAX(lfcs.actual_waiting_time) AS MAX_WT,
			YEAR(MAX(lfcs.created)) YEAR_INT,
			MONTH(MAX(lfcs.created)) MONTH_INT,
			DATENAME(MONTH, MAX(lfcs.created)) MONTH_NAME_EN
	FROM 
	(
		SELECT 	(CASE WHEN enter_time < @openTime THEN /*entered before opening*/
				(CASE WHEN (waiting_time - (@openTime - enter_time)) > 0 THEN waiting_time - (@openTime - enter_time) ELSE 0 END)
			ELSE
				(CASE WHEN enter_time > @startLunchTime AND enter_time < @endLunchTime THEN /*entered during lunch - called during/after lunch*/
					(CASE WHEN (waiting_time - (@endLunchTime - enter_time)) > 0 THEN waiting_time - (@endLunchTime - enter_time) ELSE 0 END)
				ELSE 
					(CASE WHEN enter_time < @startLunchTime AND enter_time + waiting_time > @endLunchTime  THEN  waiting_time - (@endLunchTime - @startLunchTime) /*entered before lunch - called after lunch*/
					ELSE
						(CASE WHEN enter_time < @startLunchTime AND enter_time + waiting_time < @startLunchTime AND enter_time + waiting_time > @endLunchTime  THEN  (@endLunchTime - enter_time) /*entered before lunch - called after lunch*/
						ELSE
							waiting_time 
						END) 
					END) 
				END) 
			END) AS actual_waiting_time,
			*
		FROM 
		(
			SELECT	FLOOR(DATEDIFF(MINUTE, CONVERT(DATE, created), created))*60 AS time_key, 
					DATEDIFF(SECOND, CONVERT(DATE, created), created) AS enter_time, 
					*
			FROM stat.lueg_fact_case_services
			WHERE service_name<>'TRASH'
			AND waiting_time>=0
			AND CAST(created AS DATE)>=@fromDate
			AND CAST(created AS DATE)<=@toDate
		) AS temp_lfcs
	) AS lfcs
	LEFT JOIN
	(
		SELECT *
		FROM stat.lueg_dim_service_level ldsl
	) sl ON lfcs.service_id=sl.service_id
	INNER JOIN
	(
		SELECT *
		FROM stat.lueg_dim_service_level ldsl
		WHERE ldsl.service_id=0
	) default_sl ON 1=1
	WHERE CAST(lfcs.created AS DATE)>=@fromDate
	AND CAST(lfcs.created AS DATE)<=@toDate
	AND lfcs.enter_time >= @startTime
	AND lfcs.enter_time <= @endTime
	GROUP BY lfcs.service_name, lfcs.service_id, YEAR(lfcs.created), MONTH(lfcs.created)
) AS fcs ON fcs.SERVICE_ID=dim_services.SERVICE_ID AND fcs.YEAR_INT=dim_services.YEAR_INT AND fcs.MONTH_INT=dim_services.MONTH_INT
--WHERE dim_services.SERVICE_NAME IN (${Service})
--WHERE dim_services.SERVICE_NAME IN ('Permit Center Screening', 'Pre-Review - Zoning', 'Pre-Review - Engineer', 'Pre-Review - Environmental Health', 'Pre-Review - Land Development')
GROUP BY dim_services.SERVICE_ID, dim_services.SERVICE_NAME, dim_services.YEAR_INT, dim_services.MONTH_INT, dim_services.MONTH_NAME_EN
ORDER BY
YEAR_INT ASC,
MONTH_INT ASC,
SERVICE_ID ASC                                                        