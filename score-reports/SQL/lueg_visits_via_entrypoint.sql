DECLARE @branch NVARCHAR(100) = 'LUEG';
/*
DECLARE @startTime INT = ${startTime};
DECLARE @endTime INT = ${endTime}; 
DECLARE @fromDate DATE = ${FromDate}; 
DECLARE @toDate DATE = ${ToDate};
*/

DECLARE @fromDate DATE = '2014-09-01';
DECLARE @toDate DATE = '2014-09-30';
DECLARE @startTime INT = 27000;
DECLARE @endTime INT = 57600;

SELECT	@branch AS BRANCH_NAME,
		qp_printer.printer_name AS PRINTER,
		COUNT(DISTINCT qp_printer.visit_key) AS ARRIVED
FROM stat.lueg_fact_cases lfc
INNER JOIN stat.lueg_fact_case_services lfcs ON lfc.id=lfcs.parent_case_id
INNER JOIN (
	SELECT 	dep.name AS printer_name,
			CAST(dd.full_date AS DATE) AS full_date,
			dv.ticket_id AS ticket_number,
			fvt.visit_key AS visit_key
	FROM	stat.fact_visit_transaction fvt
	INNER JOIN (
		SELECT DISTINCT vt.visit_key, MIN(vt.id) AS min_id
		FROM stat.fact_visit_transaction vt 
		GROUP BY vt.visit_key
	) small_fvt ON small_fvt.visit_key=fvt.visit_key
	INNER JOIN stat.dim_visit dv ON fvt.visit_key=dv.id
	INNER JOIN stat.dim_date dd ON fvt.date_key=dd.id
	INNER JOIN stat.dim_branch db ON fvt.branch_key=db.id
	INNER JOIN stat.dim_entry_point dep ON dep.id=fvt.entry_point_key
	WHERE fvt.id=small_fvt.min_id
	AND db.name = @branch
) AS qp_printer ON CAST(lfc.created AS DATE)=qp_printer.full_date AND lfc.ticket_number=qp_printer.ticket_number
WHERE lfcs.service_name<>'TRASH'
AND CAST(lfc.created AS DATE)>=@fromDate
AND CAST(lfc.created AS DATE)<=@toDate
AND DATEDIFF(SECOND, CONVERT(DATE, lfcs.created), lfcs.created) >= @startTime
AND DATEDIFF(SECOND, CONVERT(DATE, lfcs.created), lfcs.created) <= @endTime
GROUP BY qp_printer.printer_name
ORDER BY PRINTER                                                                  