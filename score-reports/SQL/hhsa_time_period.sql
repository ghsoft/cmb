DECLARE @branch NVARCHAR(100) = 'LUEG';
/*
DECLARE @period INT = ${period};
DECLARE @startTime INT = ${startTime};
DECLARE @endTime INT = ${endTime};
DECLARE @fromDate DATE = ${FromDate};
DECLARE @toDate DATE = ${ToDate};
DECLARE @openTime INT = ${openTime}; 
DECLARE @startLunchTime INT = ${startLunchTime};
DECLARE @endLunchTime INT = ${endLunchTime};
*/

DECLARE @interval INT = 5;
DECLARE @period INT = 30;
DECLARE @fromDate DATE = '2014-09-02';
DECLARE @toDate DATE = '2014-09-02';
DECLARE @startTime INT = 27000;
DECLARE @endTime INT = 57600;
DECLARE @openTime INT = 28800;
DECLARE @startLunchTime INT = 42300;
DECLARE @endLunchTime INT = 45000;

SELECT	dt.time_slot AS SLOT,
		ISNULL(COUNT(CASE WHEN full_fvt.slot_enter_time = dt.time_slot THEN full_fvt.slot_enter_time ELSE null END), 0) AS ARRIVED,
		ISNULL(COUNT(CASE WHEN full_fvt.slot_enter_time = dt.time_slot THEN full_fvt.actual_waiting_time ELSE null END), 0) AS WAITING,
		ISNULL(COUNT(CASE WHEN full_fvt.slot_enter_time = dt.time_slot AND full_fvt.visit_outcome_key IN (1,4,5,6,7) THEN full_fvt.transaction_time ELSE null END), 0) AS CALLED,
		ISNULL(COUNT(CASE WHEN full_fvt.slot_enter_time = dt.time_slot AND full_fvt.visit_outcome_key IN (2) THEN full_fvt.transaction_time ELSE null END), 0) AS NO_SHOWS,
		ISNULL(COUNT(CASE WHEN full_fvt.slot_enter_time = dt.time_slot THEN full_fvt.above_wtt_sl ELSE null END), 0) AS ABOVE_WTT_SL,
		ISNULL(SUM(CASE WHEN full_fvt.slot_enter_time = dt.time_slot THEN full_fvt.actual_waiting_time ELSE null END), 0) AS SUM_WT,
		ISNULL(AVG(CASE WHEN full_fvt.slot_enter_time = dt.time_slot THEN full_fvt.actual_waiting_time ELSE null END), 0) AS AVG_WT,
		ISNULL(MAX(CASE WHEN full_fvt.slot_enter_time = dt.time_slot THEN full_fvt.actual_waiting_time ELSE null END), 0) AS MAX_WT,
		ISNULL(SUM(CASE WHEN full_fvt.slot_enter_time = dt.time_slot THEN full_fvt.transaction_time ELSE null END), 0) AS SUM_TT,
		ISNULL(AVG(CASE WHEN full_fvt.slot_enter_time = dt.time_slot THEN full_fvt.transaction_time ELSE null END), 0) AS AVG_TT,
		ISNULL(MAX(CASE WHEN full_fvt.slot_enter_time = dt.time_slot THEN full_fvt.transaction_time ELSE null END), 0) AS MAX_TT
FROM 
(
	SELECT DISTINCT FLOOR((FLOOR(id - @startTime) / (@period * 60))) AS time_slot
	FROM stat.dim_time
	WHERE id>=@startTime
	AND id<@endTime
) AS dt,
(
	SELECT	fvt.slot_enter_time,
			(CASE WHEN ISNULL(fvt.actual_waiting_time,0) > 0 THEN fvt.actual_waiting_time ELSE null END) AS actual_waiting_time,
			(CASE WHEN fvt.actual_waiting_time > fvt.queue_wt_lvl THEN 1 ELSE null END) AS above_wtt_sl,
			fvt.slot_start_time,
			(CASE WHEN ISNULL(fvt.transaction_time,0) > 0 THEN fvt.transaction_time ELSE null END) AS transaction_time,
			fvt.visit_outcome_key
	FROM 
	(
		SELECT 	(CASE WHEN time_key < @openTime THEN /*entered before opening*/
					(CASE WHEN (waiting_time - (@openTime - time_key)) > 0 THEN waiting_time - (@openTime - time_key) ELSE 0 END)
				ELSE
					(CASE WHEN time_key > @startLunchTime AND time_key < @endLunchTime THEN /*entered during lunch - called during/after lunch*/
						(CASE WHEN (waiting_time - (@endLunchTime - time_key)) > 0 THEN waiting_time - (@endLunchTime - time_key) ELSE 0 END)
					ELSE 
						(CASE WHEN time_key < @startLunchTime AND time_key + waiting_time > @endLunchTime  THEN  waiting_time - (@endLunchTime - @startLunchTime) /*entered before lunch - called after lunch*/
						ELSE
							(CASE WHEN time_key < @startLunchTime AND time_key + waiting_time < @startLunchTime AND time_key + waiting_time > @endLunchTime  THEN  (@endLunchTime - time_key) /*entered before lunch - called after lunch*/
							ELSE
								waiting_time 
							END) 
						END) 
					END) 
				END) AS actual_waiting_time,
				*
		FROM 
		(
			SELECT	FLOOR((time_key - @startTime) / (@period*60)) AS slot_enter_time,
					FLOOR((time_key + waiting_time - @startTime) / (@period*60)) AS slot_start_time,
					t.*,
					dd.full_date,
					db.name AS branch_name,
					dq.name AS queue_name
			FROM stat.fact_visit_transaction t
			INNER JOIN stat.dim_date dd ON t.date_key=dd.id
			INNER JOIN stat.dim_branch db ON t.branch_key=db.id
			INNER JOIN stat.dim_queue dq ON t.queue_key=dq.id
			WHERE full_date>=@fromDate
			AND full_date<=@toDate
			AND db.name IN (${Branch})
			AND dq.name IN (${Queue})
		) AS temp_fvt
	) AS fvt
	WHERE fvt.full_date>=@fromDate
	AND fvt.full_date<=@toDate
	AND fvt.time_key>= @startTime
 	AND fvt.time_key<= @endTime
	AND branch_name IN (${Branch})
	AND queue_name IN (${Queue})
) AS full_fvt
GROUP BY dt.time_slot
ORDER BY SLOT