DECLARE @branch NVARCHAR(100) = 'LUEG';
/*
DECLARE @period INT = ${period};
DECLARE @startTime INT = ${startTime};
DECLARE @endTime INT = ${endTime};
DECLARE @fromDate DATE = ${FromDate};
DECLARE @toDate DATE = ${ToDate};
DECLARE @openTime INT = ${openTime}; 
DECLARE @startLunchTime INT = ${startLunchTime};
DECLARE @endLunchTime INT = ${endLunchTime};
*/

DECLARE @interval INT = 5;
DECLARE @period INT = 30;
DECLARE @fromDate DATE = '2014-01-01';
DECLARE @toDate DATE = '2014-12-30';
DECLARE @startTime INT = 27000;
DECLARE @endTime INT = 57600;
DECLARE @openTime INT = 28800;
DECLARE @startLunchTime INT = 42300;
DECLARE @endLunchTime INT = 45000;

SELECT	dt.time_slot AS SLOT,
		ISNULL(COUNT(DISTINCT CASE WHEN fcs.slot_case_enter_time = dt.time_slot THEN fcs.case_enter_time ELSE null END), 0) AS TICKETS_ENTERED,
		ISNULL(COUNT(CASE WHEN fcs.slot_case_enter_time = dt.time_slot THEN fcs.case_enter_time ELSE null END), 0) AS ENTERED,
		ISNULL(COUNT(CASE WHEN fcs.slot_service_enter_time = dt.time_slot THEN fcs.actual_waiting_time ELSE null END), 0) AS WT,
		ISNULL(SUM(CASE WHEN fcs.slot_service_enter_time = dt.time_slot THEN fcs.actual_waiting_time ELSE null END), 0) AS SUM_WTT,
		ISNULL(AVG(CASE WHEN fcs.slot_service_enter_time = dt.time_slot THEN fcs.actual_waiting_time ELSE null END), 0) AS AVG_WTT,
		ISNULL(MAX(CASE WHEN fcs.slot_service_enter_time = dt.time_slot THEN fcs.actual_waiting_time ELSE null END), 0) AS MAX_WTT,
		ISNULL(COUNT(CASE WHEN fcs.slot_service_enter_time = dt.time_slot THEN fcs.above_wtt_sl ELSE null END), 0) AS ABOVE_WTT_SL,
		ISNULL(COUNT(CASE WHEN fcs.slot_service_start_time = dt.time_slot THEN fcs.serving_time ELSE null END), 0) AS F2F,
		ISNULL(SUM(CASE WHEN fcs.slot_service_start_time = dt.time_slot THEN fcs.serving_time ELSE null END), 0) AS SUM_F2FT,
		ISNULL(AVG(CASE WHEN fcs.slot_service_start_time = dt.time_slot THEN fcs.serving_time ELSE null END), 0) AS AVG_F2FT,
		ISNULL(MAX(CASE WHEN fcs.slot_service_start_time = dt.time_slot THEN fcs.serving_time ELSE null END), 0) AS MAX_F2FT,
		ISNULL(COUNT(CASE WHEN fcs.slot_service_pp_time = dt.time_slot THEN fcs.pp_time ELSE null END), 0) AS PP,
		ISNULL(SUM(CASE WHEN fcs.slot_service_pp_time = dt.time_slot THEN fcs.pp_time ELSE null END), 0) AS SUM_PPT,
		ISNULL(AVG(CASE WHEN fcs.slot_service_pp_time = dt.time_slot THEN fcs.pp_time ELSE null END), 0) AS AVG_PPT,
		ISNULL(MAX(CASE WHEN fcs.slot_service_pp_time = dt.time_slot THEN fcs.pp_time ELSE null END), 0) AS MAX_PPT
FROM 
(
	SELECT DISTINCT FLOOR((FLOOR(id - @startTime) / (@period * 60))) AS time_slot
	FROM stat.dim_time
	WHERE id>=@startTime
	AND id<@endTime
) AS dt,
(
	SELECT	lfc.slot_case_enter_time,
			lfc.created AS case_enter_time,
			lfcs.slot_service_enter_time,
			lfcs.actual_waiting_time AS actual_waiting_time,
			(CASE WHEN lfcs.actual_waiting_time > ISNULL(sl.wt_lvl, default_sl.wt_lvl) THEN 1 ELSE null END) AS above_wtt_sl,
			lfcs.slot_service_start_time,
			(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.servingTime,0)>0 THEN lfcs.servingTime ELSE null END) AS serving_time,
			lfcs.slot_service_pp_time,
			(CASE WHEN lfcs.status='FINISHED' AND ISNULL(lfcs.post_processing_time,0)>0 THEN lfcs.post_processing_time ELSE null END) AS pp_time
	FROM 
	(
		SELECT 	(CASE WHEN service_enter_time < @openTime THEN /*entered before opening*/
					(CASE WHEN (waiting_time - (@openTime - service_enter_time)) > 0 THEN waiting_time - (@openTime - service_enter_time) ELSE 0 END)
				ELSE
					(CASE WHEN service_enter_time > @startLunchTime AND service_enter_time < @endLunchTime THEN /*entered during lunch - called during/after lunch*/
						(CASE WHEN (waiting_time - (@endLunchTime - service_enter_time)) > 0 THEN waiting_time - (@endLunchTime - service_enter_time) ELSE 0 END)
					ELSE 
						(CASE WHEN service_enter_time < @startLunchTime AND service_enter_time + waiting_time > @endLunchTime  THEN  waiting_time - (@endLunchTime - @startLunchTime) /*entered before lunch - called after lunch*/
						ELSE
							(CASE WHEN service_enter_time < @startLunchTime AND service_enter_time + waiting_time < @startLunchTime AND service_enter_time + waiting_time > @endLunchTime  THEN  (@endLunchTime - service_enter_time) /*entered before lunch - called after lunch*/
							ELSE
								waiting_time 
							END) 
						END) 
					END) 
				END) AS actual_waiting_time,
				*
		FROM 
		(
			SELECT	FLOOR((FLOOR(DATEDIFF(MINUTE, CONVERT(DATE, created), created)) * 60 - @startTime) / (@period * 60)) AS slot_service_enter_time,
					DATEDIFF(SECOND, CONVERT(DATE, created), created) AS service_enter_time,
					FLOOR((FLOOR(DATEDIFF(MINUTE, CONVERT(DATE, serving_start), serving_start)) * 60 - @startTime) / (@period * 60)) AS slot_service_start_time,
					FLOOR((FLOOR(DATEDIFF(MINUTE, CONVERT(DATE, post_processing_start), post_processing_start)) * 60 - @startTime) / (@period * 60)) AS slot_service_pp_time,
					*
			FROM stat.lueg_fact_case_services
			WHERE waiting_time>=0
			AND service_name<>'TRASH'
			AND CAST(created AS DATE)>=@fromDate
			AND CAST(created AS DATE)<=@toDate
		) AS temp_fcs
	) AS lfcs
	INNER JOIN 
	(
		SELECT	FLOOR((FLOOR(DATEDIFF(MINUTE, CONVERT(DATE, created), created))*60 - @startTime) / (@period*60)) AS slot_case_enter_time,
				*
		FROM stat.lueg_fact_cases
	) AS lfc ON lfcs.parent_case_id = lfc.id
	LEFT JOIN
	(
		SELECT *
		FROM stat.lueg_dim_service_level ldsl
	) sl ON lfcs.service_id=sl.service_id
	INNER JOIN
	(
		SELECT *
		FROM stat.lueg_dim_service_level ldsl
		WHERE ldsl.service_id=0
	) default_sl ON 1=1
	WHERE lfcs.service_name<>'TRASH'
	AND CAST(lfc.created AS DATE)>=@fromDate
	AND CAST(lfc.created AS DATE)<=@toDate
	AND lfcs.service_enter_time >= @startTime
 	AND lfcs.service_enter_time <= @endTime
	--AND lfcs.service_name IN ('Permit Center Screening', 'Pre-Review - Zoning', 'Pre-Review - Engineer', 'Pre-Review - Land Development', 'PDS Bld - Appointments')
	--AND lfcs.service_name IN (${Service})
) AS fcs
GROUP BY dt.time_slot
ORDER BY SLOT