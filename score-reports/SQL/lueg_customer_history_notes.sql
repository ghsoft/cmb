SELECT	CONVERT(varchar(15), CAST(lfn.created AS TIME), 100) AS NOTE_TIME,
		lfn.created_by AS NOTE_USER,
		lfn.text AS NOTE_TEXT
FROM	stat.lueg_fact_notes lfn
WHERE	lfn.caseservice_id=3778
ORDER BY NOTE_TIME