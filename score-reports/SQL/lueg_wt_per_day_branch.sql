DECLARE @branch NVARCHAR(100) = 'LUEG';
/*
DECLARE @interval INT = ${interval};
DECLARE @startTime INT = ${startTime};
DECLARE @endTime INT = ${endTime};
DECLARE @fromDate DATE = ${FromDate};
DECLARE @toDate DATE = ${ToDate};
DECLARE @openTime INT = ${openTime}; 
DECLARE @startLunchTime INT = ${startLunchTime};
DECLARE @endLunchTime INT = ${endLunchTime};
*/

DECLARE @interval INT = 2;
DECLARE @startTime INT = 27000;
DECLARE @endTime INT = 57600;
DECLARE @fromDate DATE = '2014-09-02';
DECLARE @toDate DATE = '2014-09-02';
DECLARE @openTime INT = 28800;
DECLARE @startLunchTime INT = 42300;
DECLARE @endLunchTime INT = 45000;

SELECT	@branch AS BRANCH_NAME,
		CONVERT(nvarchar, CAST(fcs.created AS DATE), 101) AS FULL_DATE,
		COUNT(fcs.created) AS WT,
		COUNT(CASE WHEN fcs.actual_waiting_time <= (60 * 1 * @interval) THEN 1 ELSE null END) AS INTERVAL1,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 1 * @interval) AND fcs.actual_waiting_time <= (60 * 2 * @interval) THEN 1 ELSE null END) AS INTERVAL2,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 2 * @interval) AND fcs.actual_waiting_time <= (60 * 3 * @interval) THEN 1 ELSE null END) AS INTERVAL3,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 3 * @interval) AND fcs.actual_waiting_time <= (60 * 4 * @interval) THEN 1 ELSE null END) AS INTERVAL4,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 4 * @interval) AND fcs.actual_waiting_time <= (60 * 5 * @interval) THEN 1 ELSE null END) AS INTERVAL5,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 5 * @interval) AND fcs.actual_waiting_time <= (60 * 6 * @interval) THEN 1 ELSE null END) AS INTERVAL6,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 6 * @interval) AND fcs.actual_waiting_time <= (60 * 7 * @interval) THEN 1 ELSE null END) AS INTERVAL7,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 7 * @interval) AND fcs.actual_waiting_time <= (60 * 8 * @interval) THEN 1 ELSE null END) AS INTERVAL8,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 8 * @interval) AND fcs.actual_waiting_time <= (60 * 9 * @interval) THEN 1 ELSE null END) AS INTERVAL9,
		COUNT(CASE WHEN fcs.actual_waiting_time > (60 * 9 * @interval) THEN 1 ELSE null END) AS REST,
		SUM(CASE WHEN fcs.actual_waiting_time > ISNULL(sl.wt_lvl, default_sl.wt_lvl) THEN 1 ELSE 0 END) AS ABOVE_WTT_SL
FROM
(	SELECT 	(CASE WHEN enter_time < @openTime THEN /*entered before opening*/
				(CASE WHEN (waiting_time - (@openTime - enter_time)) > 0 THEN waiting_time - (@openTime - enter_time) ELSE 0 END)
			ELSE
				(CASE WHEN enter_time > @startLunchTime AND enter_time < @endLunchTime THEN /*entered during lunch - called during/after lunch*/
					(CASE WHEN (waiting_time - (@endLunchTime - enter_time)) > 0 THEN waiting_time - (@endLunchTime - enter_time) ELSE 0 END)
				ELSE 
					(CASE WHEN enter_time < @startLunchTime AND enter_time + waiting_time > @endLunchTime  THEN  waiting_time - (@endLunchTime - @startLunchTime) /*entered before lunch - called after lunch*/
					ELSE
						(CASE WHEN enter_time < @startLunchTime AND enter_time + waiting_time < @startLunchTime AND enter_time + waiting_time > @endLunchTime  THEN  (@endLunchTime - enter_time) /*entered before lunch - called after lunch*/
						ELSE
							waiting_time 
						END) 
					END) 
				END) 
			END) AS actual_waiting_time,
			*
	FROM 
	(
		SELECT	FLOOR(DATEDIFF(MINUTE, CONVERT(DATE, created), created))*60 AS time_key, 
				DATEDIFF(SECOND, CONVERT(DATE, created), created) AS enter_time, 
				*
		FROM stat.lueg_fact_case_services
		WHERE service_name<>'TRASH'
		AND waiting_time>=0
		AND CAST(created AS DATE)>=@fromDate
		AND CAST(created AS DATE)<=@toDate
		AND FLOOR(DATEDIFF(MINUTE, CONVERT(DATE, created), created))*60 >= @startTime
 		AND FLOOR(DATEDIFF(MINUTE, CONVERT(DATE, created), created))*60 <= @endTime
	) AS temp_fcs
) AS fcs
LEFT JOIN
(
	SELECT *
	FROM stat.lueg_dim_service_level ldsl
) sl ON fcs.service_id=sl.service_id
INNER JOIN
(
	SELECT *
	FROM stat.lueg_dim_service_level ldsl
	WHERE ldsl.service_id=0
) default_sl ON 1=1
WHERE fcs.service_name<>'TRASH'
AND fcs.waiting_time>=0
AND CAST(fcs.created AS DATE)>=@fromDate
AND CAST(fcs.created AS DATE)<=@toDate
AND fcs.time_key >= @startTime
AND fcs.time_key <= @endTime
--AND fcs.id = 16236
AND fcs.service_name IN ('Pre-Review - Zoning')
--AND fcs.service_name IN ('Permit Center Screening', 'Pre-Review - Zoning', 'Pre-Review - Engineer', 'Pre-Review - Land Development', 'PDS Bld - Appointments')
--AND fcs.service_name IN (${Service})
GROUP BY CAST(fcs.created AS DATE)