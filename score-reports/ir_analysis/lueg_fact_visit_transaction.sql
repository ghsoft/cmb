DROP VIEW stat.lueg_fact_visit_transaction
DROP VIEW stat.lueg_dim_time
GO

CREATE VIEW stat.lueg_dim_time
AS

SELECT	id,
		time,
		hour_24 AS hour_24,
		CONVERT(NVARCHAR(255), REPLACE(STR(hour, 2), SPACE(1), '0') + ':00' + am_pm) AS hour,
		minute,
		am_pm,
		interval_15_min + am_pm AS interval_15,
		interval_30_min + am_pm AS interval_30
FROM	stat.dim_time

GO

CREATE VIEW stat.lueg_fact_visit_transaction
AS

SELECT	id,
		case_id,
		date_key,
		time_key,
		ticket_number,
		customer_id,
		customer_name,
		service_order,
		service_name,
		service_status,
		ISNULL((CASE WHEN time_key < 28800 THEN /*entered before opening*/
				(CASE WHEN (full_waiting_time - (28800 - time_key)) > 0 THEN full_waiting_time - (28800 - time_key) ELSE 0 END)
			ELSE
				(CASE WHEN time_key > 42300 AND time_key < 45000 THEN /*entered during lunch - called during/after lunch*/
					(CASE WHEN (full_waiting_time - (45000 - time_key)) > 0 THEN full_waiting_time - time_key ELSE 0 END)
				ELSE 
					(CASE WHEN time_key < 42300 AND time_key + full_waiting_time > 45000  THEN  full_waiting_time - (45000 - 42300) /*entered before lunch - called after lunch*/
					ELSE
						(CASE WHEN time_key < 42300 AND time_key + full_waiting_time < 42300 AND time_key + full_waiting_time > 45000  THEN  (45000 - time_key) /*entered before lunch - called after lunch*/
						ELSE
							full_waiting_time 
						END) 
					END) 
				END) 
			END), 0) AS waiting_time,
		waiting_time_sl,
		serving_time,
		serving_time_sl,
		post_processing_time,
		post_processing_time_sl,
		served_by_user_name,
		served_by_counter_name,
		service_record,
		service_vip
FROM 
(
	SELECT	lfcs.id AS id,
			lfc.id AS case_id,
			dd.id AS date_key,
			(DATEDIFF(MINUTE, CAST(lfcs.created AS DATE), lfcs.created) * 60) AS time_key,
			lfc.ticket_number,
			lfc.customer_id,
			lfc.customer_name,
			lfcs.order_idx AS service_order,
			lfcs.service_name,
			lfcs.status AS service_status,
			ISNULL(lfcs.waiting_time, 0) AS full_waiting_time,
			ISNULL(ISNULL(sl.wt_lvl, default_sl.wt_lvl), 0) AS waiting_time_sl,
			ISNULL(lfcs.servingTime, 0) AS serving_time,
			ISNULL(ISNULL(sl.f2f_lvl, default_sl.f2f_lvl), 0) AS serving_time_sl,
			ISNULL(lfcs.post_processing_time, 0) AS post_processing_time,
			ISNULL(ISNULL(sl.pp_lvl, default_sl.pp_lvl), 0) AS post_processing_time_sl,
			ISNULL(lfcs.served_by_user_name, '') AS served_by_user_name,
			ISNULL(lfcs.served_by_counter_name, '') AS served_by_counter_name,
			ISNULL(lfcs.record_id, '') AS service_record,
			ISNULL(lfcs.vip, 0) AS service_vip
	FROM	stat.lueg_fact_cases lfc
	INNER JOIN stat.dim_date dd ON CAST(dd.full_date AS DATE) = CAST(lfc.created AS DATE)
	INNER JOIN stat.lueg_fact_case_services lfcs ON lfc.id = lfcs.parent_case_id
	LEFT JOIN
	(
		SELECT *
		FROM stat.lueg_dim_service_level ldsl
	) sl ON lfcs.service_id = sl.service_id
	INNER JOIN
	(
		SELECT *
		FROM stat.lueg_dim_service_level ldsl
		WHERE ldsl.service_id = 0
	) default_sl ON 1 = 1
) AS lfvt
--WHERE lfcs.service_name <> 'TRASH'
--ORDER BY date_key, time_key, ticket_number, service_order

GO