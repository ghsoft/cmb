Setup:
1. Run the SQL script lueg_fact_visit_transaction.sql against statDB
2. Add the contents of file "catalog.xml" to "{orchestra_installation}\pentaho-solutions\system\olap\datasources.xml" under <Catalogs> section
3. Drop the file "lueg.mondrian.xml" in "{orchestra_installation}\pentaho-solutions\qmaticbi\platform"
4. Drop the file "lueg.metadata.xmi" in "{orchestra_installation}\pentaho-solutions\admin\resources\metadata"
3. Score -> Tools -> Refresh ALL