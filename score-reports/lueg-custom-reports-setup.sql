USE qp_sandiego;
GO

UPDATE qp_sandiego.case_services
SET service_name='Pre-Review - Land Development'
WHERE service_name='Pre-Review -  Land Development'
GO

UPDATE qp_sandiego.case_services
SET service_name='DEH - Mobile Home/RV Park'
WHERE service_name='DEH - Mobil Home/RV Park'
GO

EXEC sp_adduser 'stat', 'stat', 'db_datareader';
GO

ALTER USER stat WITH DEFAULT_SCHEMA = qp_sandiego;
GO

USE statdb
GO

CREATE VIEW stat.lueg_fact_case_services
AS
SELECT id, created, order_idx, post_processing_start, post_processing_time, qp_visit_service_id, record_id, recycles_left, served_by_counter_id, served_by_counter_name, 
                  served_by_user_id, served_by_user_name, service_id, service_name, serving_start, servingTime, status, updated, vip, waiting_time, parent_case_id
FROM     qp_sandiego.qp_sandiego.case_services

GO

CREATE VIEW stat.lueg_dim_services AS
SELECT DISTINCT service_id AS id, service_name AS name
FROM     stat.lueg_fact_case_services
WHERE  (service_name <> 'NOSHOW') AND (service_name <> 'SILENTCALL') AND (service_name <> 'ONHOLD') AND (service_name <> 'TRASH') AND (service_name <> '-')

GO

CREATE VIEW stat.lueg_dim_service_level
AS
SELECT id, f2f_lvl, pp_lvl, service_id, service_name, tt_lvl, wt_lvl
FROM     qp_sandiego.qp_sandiego.service_level

GO

CREATE VIEW stat.lueg_fact_cases
AS
SELECT id, branch_id, created, current_record_id, customer_id, customer_name, qp_visit_id, ticket_number, updated, currentNote_id
FROM     qp_sandiego.qp_sandiego.cases

GO

CREATE VIEW stat.lueg_fact_notes
AS
SELECT id, created, created_by, text, caseservice_id
FROM     qp_sandiego.qp_sandiego.notes

GO