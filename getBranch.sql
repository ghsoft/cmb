SELECT	1 AS id
	,	MAX(customersServed) AS customersServed
	,	MAX(servicesServed) AS servicesServed
	,	MAX(customersBeingServed) AS customersBeingServed
	,	MAX(customersWaiting) AS customersWaiting
	,	MAX(servicesWaiting) AS servicesWaiting
	,	MAX(maxWaitingTime) AS maxWaitingTime
	,	MAX(totalWaitingTime) AS totalWaitingTime
	,	MAX(averageWaitingTime) AS averageWaitingTime
FROM	(	
	SELECT	0 AS customersServed
		,	0 AS servicesServed
		,	0 AS customersBeingServed
		,	ISNULL(COUNT(DISTINCT ws.case_id), 0) AS customersWaiting
		,	ISNULL(COUNT(id), 0) AS servicesWaiting
		,	ISNULL(MAX(CASE WHEN cs.status = 'RECYCLED' THEN DATEDIFF(ss, cs.updated, GETDATE()) ELSE DATEDIFF(ss, cs.created, GETDATE())END), 0) AS maxWaitingTime
		,	ISNULL(SUM(CASE WHEN cs.status = 'RECYCLED' THEN DATEDIFF(ss, cs.updated, GETDATE()) ELSE DATEDIFF(ss, cs.created, GETDATE())END), 0) AS totalWaitingTime
		,	0 AS averageWaitingTime
	FROM	qp_conductor.case_services cs
	INNER JOIN (
		SELECT	parent_case_id AS case_id,
				COUNT(DISTINCT CASE WHEN 
							cs.status = 'SERVING' 
						OR	cs.status = 'TRASH' 
						OR	cs.status = 'NOSHOW' 
						OR	cs.status = 'ONHOLD' 
						OR	(cs.status = 'RECYCLED' AND DATEDIFF(ss, cs.updated, GETDATE()) <= 60) 
					THEN parent_case_id ELSE NULL END) AS not_waiting
		FROM qp_conductor.case_services cs
		INNER JOIN qp_conductor.cases c ON cs.parent_case_id = c.id
		WHERE	c.created > '2015-03-20'
		AND		branch_id = 1
		GROUP BY branch_id, parent_case_id
	) ws ON cs.parent_case_id = ws.case_id AND ws.not_waiting = 0
	WHERE	cs.created > '2015-03-20'
	AND		(cs.status = 'WAITING' OR cs.status = 'PENDING' OR (cs.status = 'RECYCLED' AND DATEDIFF(ss, cs.updated, GETDATE()) > 60))
UNION ALL
	SELECT	ISNULL(COUNT(DISTINCT case_id), 0) AS customersServed
		,	ISNULL(COUNT(id), 0) AS servicesServed
		,	0 AS customersBeingServed
		,	0 AS customersWaiting
		,	0 AS servicesWaiting
		,	0 AS maxWaitingTime
		,	0 AS totalWaitingTime
		,	0 AS averageWaitingTime
	FROM	qp_conductor.case_services cs
	INNER JOIN (
		SELECT	id AS case_id,
				branch_id
		FROM	qp_conductor.cases c
		WHERE	created > '2015-03-20'
		AND		branch_id = 1
	) c ON cs.parent_case_id = c.case_id
	WHERE	created > '2015-03-20'
	AND		status = 'FINISHED'
UNION ALL
	SELECT	0 AS customersServed
		,	0 AS servicesServed
		,	ISNULL(COUNT(id), 0) AS customersBeingServed
		,	0 AS customersWaiting
		,	0 AS servicesWaiting
		,	0 AS maxWaitingTime
		,	0 AS totalWaitingTime
		,	0 AS averageWaitingTime
	FROM	qp_conductor.case_services cs
	INNER JOIN (
		SELECT	id AS case_id,
				branch_id
		FROM	qp_conductor.cases c
		WHERE	created > '2015-03-20'
		AND		branch_id = 1
	) c ON cs.parent_case_id = c.case_id
	WHERE	created > '2015-03-20'
	AND		(cs.status = 'SERVING' OR cs.status = 'POST_PROCESSING')
) data