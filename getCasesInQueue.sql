SELECT TOP 10 c.ticket_number AS ticketId
	,	c.qp_visit_id AS visitId
	,	cs.waiting_time AS waitingTime
FROM	qp_conductor.cases c
INNER JOIN (
	SELECT	parent_case_id AS case_id
		,	COUNT(DISTINCT CASE WHEN 
						cs.status = 'SERVING'
					OR	cs.status = 'TRASH'
					OR	cs.status = 'NOSHOW'
					OR	cs.status = 'ONHOLD'
					OR	(cs.status = 'RECYCLED' AND DATEDIFF(ss, cs.updated, GETDATE()) <= 60) 
				THEN parent_case_id ELSE NULL END) AS not_waiting
	FROM qp_conductor.case_services cs
	INNER JOIN qp_conductor.cases c ON cs.parent_case_id = c.id
	WHERE	c.created > '2015-03-26'
	AND		c.branch_id = 1
	GROUP BY branch_id, parent_case_id
) w ON c.id = w.case_id AND w.not_waiting = 0
INNER JOIN (
	SELECT	parent_case_id
		,	MAX(CASE WHEN cs.status = 'RECYCLED' THEN DATEDIFF(ss, cs.updated, GETDATE()) ELSE DATEDIFF(ss, cs.created, GETDATE()) END) AS waiting_time
	FROM	qp_conductor.case_services cs
	WHERE	cs.created > '2015-03-26'
	AND		(cs.status = 'WAITING' OR cs.status = 'PENDING' OR (cs.status = 'RECYCLED' AND DATEDIFF(ss, cs.updated, GETDATE()) > 60))
	AND		cs.service_name = '2.6 Violations'
	GROUP BY parent_case_id
) cs ON c.id = cs.parent_case_id
WHERE	c.created > '2015-03-26'
AND		c.branch_id = 1
ORDER BY waitingTime DESC