var customer = new function() {

    this.openCreateCustomerModal = function(title, onSaveCallback) {
        $('#customer_modal_title').text(title);

        $('#customer_first_name_fld').val('');
        $('#customer_last_name_fld').val('');
        $('#customer_mobile_fld').val('');
        $('#customer_email_fld').val('');
        //$('#customer_vip_fld').prop('checked', false);

        // Bind save function to button
        $('#customer_save_btn').unbind();
        $('#customer_save_btn').click(function() {
			/*
			if ($('#customer_first_name_fld').val().trim() == '' || $('#customer_last_name_fld').val().trim() == '') {
				util.showError("Could not create customer, at least First Name and Last Name must be entered");
				return;
			}
			*/
            var cust = {
                "firstName" : $('#customer_first_name_fld').val().trim(),
                "lastName" : $('#customer_last_name_fld').val().trim(),
                "phoneNumber" : $('#customer_mobile_fld').val(),
                "email" : $('#customer_email_fld').val()
            };
            CaseServicePointService.createCustomer(cust)
                .done(function(createdCustomer){
                    onSaveCallback(createdCustomer);
                })
                .fail(function(err){
                    util.showError("Could not create customer..", err);
                })

            $('#customer_modal').modal('hide');

        });

        $('#customer_modal').modal('show');
    };

    var populateCustomerForm = function(cust) {
        $('#customer_id_lbl').text(cust.id);
        $('#customer_first_name_lbl').text(cust.firstName);
        $('#customer_last_name_lbl').text(cust.lastName);
        $('#customer_mobile_lbl').text(cust.phoneNumber);
        $('#customer_email_lbl').text(cust.email);
		/*
        if(cust.vip==true){
            $('#customer_vip_lbl').removeClass('hidden');
        }else{
            $('#customer_vip_lbl').addClass('hidden');
        }
		*/
        $('#selected_customer_panel').removeClass('hidden');

        // Bind edit
        $('#selected-customer-edit-btn').unbind().click(function() {
            customer.openEditCustomerModal('Edit ' + cust.firstName + ' ' + cust.lastName, cust, onSaveCustomerCallback);
        });
    };

    var populateEditCustomerForm = function(cust) {
        $('#customer_first_name_fld').val(cust.firstName);
        $('#customer_last_name_fld').val(cust.lastName);
        $('#customer_mobile_fld').val(cust.phoneNumber);
        $('#customer_email_fld').val(cust.email);
        //$('#customer_vip_fld').prop('checked', cust.vip);
        $('#selected_customer_panel').removeClass('hidden');
    };

    this.openEditCustomerModal = function(title, cust, onSaveCallback) {
        $('#customer_modal_title').text(title);

        populateEditCustomerForm(cust);

        // Bind save function to button
        $('#customer_save_btn').unbind();
        $('#customer_save_btn').click(function() {
			/*
			if ($('#customer_first_name_fld').val().trim() == '' || $('#customer_last_name_fld').val().trim() == '') {
				util.showError("Could not save customer information, at least First Name and Last Name must be entered");
				return;
			}
			*/
            var updatedCust = {
                "id" : cust.id,
                "firstName" : $('#customer_first_name_fld').val().trim(),
                "lastName" : $('#customer_last_name_fld').val().trim(),
                //"cardNumber" : cust.id,
                "phoneNumber" : $('#customer_mobile_fld').val(),
                "email" : $('#customer_email_fld').val()
            };
            var params = {
                "customerId" : cust.id,
                "$entity" : updatedCust
            };

            CaseServicePointService.updateCustomer(cust.id, updatedCust)
                .done(function(nothing){
                    $('#customer_modal').modal('hide');
                    onSaveCallback(updatedCust);
                })
                .fail(function(err){
                    util.showError("Could not update customer..", err);
                    $('#customer_modal').modal('hide');
                })
        });

        $('#customer_modal').modal('show');
    };


    this.searchCustomers = function() {
		$('#customer_first_name_lbl').text('');
        $('#customer_last_name_lbl').text('');
        $('#customer_mobile_lbl').text('');
        $('#customer_email_lbl').text('');
        var val = $.trim($('#cust_search_input_fld').val());
        if(val.length < 2) {
            util.showMessage("Type at least two characters");
            return;
        }
        
        $('#selected_customer_panel').addClass('hidden');

        var results = {};
		var params = val.split(' ');
		var factor = "or";
		var searchParams = {};
		if (params.length == 1) {
			searchParams = {
				"id": params[0],
				"firstName": '%' + params[0] + '%',
				"lastName": '%' + params[0] + '%',
				"email": '%' + params[0] + '%',
				"phoneNumber": '%' + params[0] + '%'
			};
		}
		else {
			searchParams = {
				"firstName": '%' + params[0] + '%',
				"lastName": '%' + params[1] + '%'
			};
			factor = "and"
		}
		CaseServicePointService.searchCustomers(searchParams, factor)
			.done(function(searchResult){
				results = searchResult;
			})
			.fail(function(err){
				util.showError("Something when wrong when searching for the customer.", err);
			})

        $('#customer_search_results').empty();

        for(var i = 0; i < results.length; i++) {
            var tr = $(document.createElement('tr'));
            
            $(tr).append('<td>' + results[i].id + '</td>');
            $(tr).append('<td>' + results[i].firstName + ' ' + results[i].lastName + '</td>');
            $(tr).append('<td>' + results[i].phoneNumber + '</td>');
            $(tr).append('<td>' + results[i].email + '</td>');
            // Add click handler for select customer
            $(tr).attr('style', 'cursor: pointer');
            (function(_id) {
                $(tr).click(function() {
                    CaseServicePointService.getCustomer(_id)
                        .done(function(cust){
                            onSaveCustomerCallback(cust);
                            populateCustomerForm(cust);
                        })
                        .fail(function(err){
                            util.showError("Could not get customer data.", err);
                        })

                    reception.updateNewVisitButtonStatus();
                    $('#customer_search_results_container').addClass('hidden');

                });
            })(results[i].id);

            $('#customer_search_results').append(tr);
        }

        $('#customer_search_results_container').removeClass('hidden');
    };


    var collaspseAndHide = function(id) {
        $('#' + id).animate()
    }

    this.initCustomerReceptionElements = function() {
        $('#cust_search_input_fld').unbind().keyup(function(e) {
        	if(e.keyCode == 13) {
        		customer.searchCustomers();
        	}
        });

        // Customer search button
        $('#search_customer_btn').unbind();
        $('#search_customer_btn').click(function() {
            customer.searchCustomers();
        });

        $('#create_customer_btn').unbind();
        $('#create_customer_btn').click(function() {
            customer.openCreateCustomerModal('Create customer', onSaveCustomerCallback);
        });

        $('#edit_customer_btn').unbind();
        $('#edit_customer_btn').click(function() {
            customer.openEditCustomerModal('Update customer', sessvars.currentCustomer, onSaveCustomerCallback);
        });
    };

    var onSaveCustomerCallback = function(cust) {
        sessvars.currentCustomer = cust;
        populateCustomerForm(cust);
		reception.updateNewVisitButtonStatus();
    };
    this.populateCustomerDetails = function(customerId, showNotes) {
        if(typeof customerId === 'undefined') {
            util.showError('Cannot populate customer details, customerId is undefined!');
        }
        CaseServicePointService.getCustomer(customerId)
            .done(function(customerObj){
                $('#customer-details-queues').unbind().click(function() {
                    panels.resetRightPanel();
                });
				if (showNotes) {
					$('#visit-details-note').removeClass('hidden');
					$('#notes-record-data').removeClass('hidden');
				}
				else {
					$('#visit-details-note').addClass('hidden');
					$('#notes-record-data').addClass('hidden');
				}
                $('#customer-details-customer-name').text(customerObj.firstName + " " + customerObj.lastName);
                $('#customer-details-subheading-name').text(customerObj.firstName + " " + customerObj.lastName);
				$('#cd_id_lbl').text(customerObj.id);
                $('#cd_mobile_lbl').text(customerObj.phoneNumber);
                /*
				if(customerObj.vip==true){
                    $('#cd_vip_lbl').removeClass('hidden');
                }else{
                    $('#cd_vip_lbl').addClass('hidden');
                }
				*/
                $('#cd_cardnumber_lbl').text(customerObj.email);

                $('#customer-details-edit-customer-btn').unbind().click(function() {
                    customer.openEditCustomerModal('Edit customer', customerObj, function(customerObj) {
                        customer.populateCustomerDetails(customerObj.id, showNotes);
                    });
                });
            })
            .fail(function(err){
                util.showError("Customer object not found.", err);
            })
    };

};