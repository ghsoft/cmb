/**
 * JS functionality specific to the opspanel part should go in here.
 */

var opspanel = new function() {

    var workstationOffline = false;
    var _active = false;

    this.setActive = function(active){
        _active = active;
    }

    this.isActive = function (){
        return _active
    }

    this.initOpspanelElements = function() {
        if(opspanel.isActive()==false) return;
		
		//initEntryPointSelector();
    };
	
	this.initServicepointsTable = function(elementId, rowClickedCallback) {
        var columns = [
			{"bSearchable": false,
                "bVisible": true,
                "mDataProp": "id"
			},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "name",
				"iDataSort": 0
            },
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "staffFullName"
            },
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "serviceName"
			},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "ticketNumber"
			},
			{"bSearchable": true,
                "bVisible": true,
                "mDataProp": "servicePointStatus"
            },
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "transactionTime"
            },
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "customerName"
			},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "workProfileName"
			},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "servedToday"
			}
        ];
		
		/*
		jQuery.fn.dataTableExt.oSort['unitIdSorter-asc'] = function (a, b) {
			var unitIdA = a.split('_');
			var unitIdB = b.split('_');
			if (unitIdA.length == 2 && unitIdB.length == 2) {
				var uIdA = parseInt(unitIdA[1]);
				var uIdB = parseInt(unitIdB[1]);
				return ((uIdA < uIdB) ? -1 : ((uIdA > uIdB) ? 1 : 0));
			}
			else {
				return 0;
			}
		};
		
		jQuery.fn.dataTableExt.oSort['unitIdSorter-desc'] = function (a, b) {
			var unitIdA = a.split('_');
			var unitIdB = b.split('_');
			if (unitIdA.length == 2 && unitIdB.length == 2) {
				var uIdA = parseInt(unitIdA[1]);
				var uIdB = parseInt(unitIdB[1]);
				return ((uIdA > uIdB) ? -1 : ((uIdA < uIdB) ? 1 : 0));
			}
			else {
				return 0;
			}
		};
		*/

        $('#' + elementId).dataTable(
            {
                "aoColumns":columns,
                "bPaginate": false,
                "bLengthChange": false,
                "iDisplayLength ": false,
                "bFilter": false,
                "bInfo": false,
                "fnRowCallback": rowClickedCallback
            }
		).fnSort( [ [1,'asc'] ] );
    }
	
	this.updateBranchInfo = function() {
		if (!$('#opspanel-container').hasClass('hidden')) {
			OpsPanelService.getBranchInfo(sessvars.branchId)
				.done(function(branchInfo){
					$('#customers-waiting').html(branchInfo.customersWaiting);
					$('#services-waiting').html(branchInfo.servicesWaiting);
					$('#max-waiting-time').html(util.secondsToHms(branchInfo.maxWaitingTime));
					$('#avg-waiting-time').html(util.secondsToHms(branchInfo.averageWaitingTime));
					$('#serving-customers').html(branchInfo.customersBeingServed);
					$('#customers-served').html(branchInfo.customersServed);
					$('#services-served').html(branchInfo.servicesServed);
				});
		}
	}
	
	this.updateOpenCounters = function(openCounters) {
		$('#open-servicepoints').html(openCounters);
	}
	
	this.updateServicepointsTable = function(servicepointsTableElementId) {
		
		if (!$('#opspanel-container').hasClass('hidden')) {
			var openCounters = 0;
			OpsPanelService.getServicePoints(sessvars.branchId)
				.done(function(servicepoints){
					$('#' + servicepointsTableElementId).dataTable().fnClearTable();
					$.map(servicepoints, function(servicepoint) {
						if (servicepoint.servicePointStatus == '') {
							servicepoint.servicePointStatus = servicepoint.status;
						}
						if (servicepoint.name != '') {
							$('#' + servicepointsTableElementId).dataTable().fnAddData([servicepoint]);
						}
						if (servicepoint.workProfileId != 0) {
							openCounters++;
						}
					});
					opspanel.updateOpenCounters(openCounters);
				});
		}
	
    };
	/*
	var initEntryPointSelector = function() {
        $('#opspanel_entrypoint_select').empty();

        CaseServicePointService.getEntryPointsByDeviceType(sessvars.branchId, "SW_RECEPTION" )
            .done(function(entrypoints){
                $.map(entrypoints, function(entryPoint) {
                    $('#opspanel_entrypoint_select').append($("<option />").val(entryPoint.id).text(entryPoint.name));
                });
            });
    };
	*/
	this.showChangeProfileWindow = function(workProfileId, workProfileName, staffName) {
		populateChangeWorkProfileSelect(workProfileName);
		
        var apply = function() {
			var params = {};
			params.branchId = sessvars.branchId;
			params.staffName = staffName;
			params.profileNumber = $('#prioChangeListModal').val();
			
			OpsPanelService.setProfile(params);
			opspanel.updateServicepointsTable('servicepoints_table');
			$('#changeProfileWindow').modal('hide');
        };
                                  
        $('#changeProfileWindow').modal('show');
        $('#changeProfileWindow').unbind().keyup(function(event){
            if(event.keyCode == 13){
                apply();
            }
        });
		$('#apply_change_profile_btn').unbind();
		$('#apply_change_profile_btn').click(function() {apply();});
		
		$('#cancel_change_profile_btn').unbind();
		$('#cancel_change_profile_btn').click(function() {$('#changeProfileWindow').modal('hide');});
    };
	
	var populateChangeWorkProfileSelect = function(workProfileName) {
        $('#prioChangeListModal').empty();
        var params = {
            "branchId" : sessvars.branchId
        };
        cServicePointService.getWorkProfiles(params.branchId)
            .done(function(workProfiles){
                $.map(workProfiles, function(workProfile) {
                    var opt = $("<option />").text(workProfile.name).val(workProfile.id);
                    if($(opt).text() == workProfileName) {
                        $(opt).attr('selected','selected');
                    }
                    $('#prioChangeListModal').append(opt);
                });
            })
            .fail(function(err){
                util.showError('Could not get workprofiles from server.', err);
            })
    };

};