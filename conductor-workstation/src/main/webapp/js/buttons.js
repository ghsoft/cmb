/**
 * Add init and enable/disable stuff for global buttons here.
 */
var buttons = new function() {

    this.initGUI = function() {
        // Bind call next btn
        $('#cst-call-next-btn').unbind().click(servicePoint.callNext);

        // TODO Walk Direct button
        $('#cst-walk-direct-btn').unbind().click(function() {
            //end current!
            //if post -> endPost
            //else if igång -> endF2F
			if (servicePoint.isVisitInDisplayQueue()) {
				return;
			}
		
            if(sessvars.state.userState == servicePoint.userState.SERVING) {
                servicePoint.onEndFaceToFaceServing(walkDirect.openWalkDirectModal);

            } 
			else if(sessvars.isPostProcessing == true) {
				CaseServicePointService.endPostProcessing( sessvars.currentCase.id, sessvars.currentCase.ongoingService.qpVisitServiceId)
                    .done(function(userState){
                        sessvars.state = userState;
                        buttons.setCSTStateForNormalOperation();
                        servicePoint.clearOngoingVisit();
                        walkDirect.openWalkDirectModal();
                    });
            } 
			else
                walkDirect.openWalkDirectModal();
        });

        // Bind Close workstation btn
        $('#cst-close-workstation-btn').unbind().click(function() {servicePoint.closeWorkstation()});
    };

    /**
     * Disable the callNext button, then enable it again after 2 seconds. Useful to prevent double-clicks.
     */
    this.sleepCallNextBtn = function() {
        buttons.disableCallNextBtn();
        setTimeout(buttons.enableCallNextBtn, 2000);
    };

    this.enableCallNextBtn = function() {
        $('#cst-call-next-btn').unbind().click(servicePoint.callNext).removeClass('disabled').removeAttr('disabled');
    };
    this.disableCallNextBtn = function() {
        $('#cst-call-next-btn').unbind().addClass('disabled').attr('disabled','disabled');
    };

    this.enableWorkstationCloseBtn = function() {
        $('#cst-close-workstation-btn').removeClass('disabled').removeAttr('disabled').unbind().click(servicePoint.closeWorkstation);
    };
    this.disableWorkstationCloseBtn = function() {
        $('#cst-close-workstation-btn').addClass('disabled').attr('disabled','disabled').unbind();
    };

    this.updateAccordingToServicePointStatus = function() {
        if(sessvars.state.servicePointState == servicePoint.servicePointState.OPEN) {
            buttons.enableWorkstationCloseBtn();
        } else {
            buttons.disableWorkstationCloseBtn();
        }
		
        if(sessvars.isPostProcessing == true) {
            buttons.setCSTStateForPostProcessing();
        } else {
            buttons.setCSTStateForNormalOperation();
        }
    };


    this.setCSTStateForNormalOperation = function() {
        $('#cst_end_f2f_btn').removeClass('disabled').removeAttr('disabled');
        $('#cst_end_f2f_btn').addClass('btn-danger');
		try {
			if (sessvars.state.visit.currentVisitService.serviceExternalName == 'ONHOLD') {
				$('#cst_put_in_pool_btn').addClass('disabled').attr('disabled','disabled');
			}
			else {
				$('#cst_put_in_pool_btn').removeClass('disabled').removeAttr('disabled');
			}
		}
		catch (e) {
			$('#cst_put_in_pool_btn').removeClass('disabled').removeAttr('disabled');
		}
        $('#cst_noshow_btn').removeClass('disabled').removeAttr('disabled');
        $('#visit_delete_btn').removeClass('disabled').removeAttr('disabled');

        // Set 'Start' and bind start post processing function
        $('#cst_post_processing_start_btn').text('Start Post Processing').unbind().click(servicePoint.onStartPostProcessing);
        $('#cst_post_processing_start_btn').removeClass('btn-danger').addClass('btn-info');
    };


    this.setCSTStateForPostProcessing = function() {
        $('#cst_end_f2f_btn').addClass('disabled').attr('disabled','disabled');
        $('#cst_end_f2f_btn').removeClass('btn-danger');
        $('#cst_put_in_pool_btn').addClass('disabled').attr('disabled','disabled');
        $('#cst_noshow_btn').addClass('disabled').attr('disabled','disabled');
        $('#visit_delete_btn').addClass('disabled').attr('disabled','disabled');

        // Set 'End' and bind end post processing function
        $('#cst_post_processing_start_btn').text('End Post Processing').unbind().click(servicePoint.onEndPostProcessing);
        $('#cst_post_processing_start_btn').removeClass('btn-info').addClass('btn-danger');
    };

};