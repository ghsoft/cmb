var walkDirect = new function() {
    var newVisit;

    this.initGUI = function() {
        $('#call_walk_direct_btn').unbind().click(function() {
            if(!$('#call_walk_direct_btn').hasClass('disabled'))
                callConductorWalkDirect();
        });
        $('#call_walk_direct_btn').addClass('disabled');

        $('#walkdirect_search_customer_button').unbind().click(function() {
            walkDirect.searchCustomers($('#walkdirect_search_field').val(), '#walkdirect_search_results');
        });
		
        $('#walkdirect_use_customer_btn').unbind().click(function() {
			/*
			if ($('#walkdirect_customer_first_name_fld').val().trim() == '' || $('#walkdirect_customer_last_name_fld').val().trim() == '') {
				util.showError("Could not create customer, at least First Name and Last Name must be entered");
				return;
			}
			*/
            var cust = {
                "firstName" : $('#walkdirect_customer_first_name_fld').val().trim(),
                "lastName" : $('#walkdirect_customer_last_name_fld').val().trim(),
                "phoneNumber" : $('#walkdirect_customer_mobile_fld').val(),
                //"vip" : $('#walkdirect_customer_vip_fld').is(':checked'),
                "email" : $('#walkdirect_customer_email_fld').val()
            };
            CaseServicePointService.createCustomer(cust)
                .done(function(createdCustomer){
                    $('#walkdirect_new_customer').addClass('hidden');
                    setCustomerSelected(createdCustomer);
                })
                .fail(function(err){
                    util.showError("Could not create customer..", err);
                })
        });

        $('#walkdirect_cancel_customer_btn').unbind().click(function() {
            $('#walkdirect_new_customer').addClass('hidden');
            showCreateAndSearchPanel(true);
        });

        $('#walkdirect_show_create_customer_button').unbind().click(function() {
            clearNewCustomerForm();
            $('#walkdirect_new_customer').removeClass('hidden');
            $('#walkdirect_search_results').empty();
            $('#walkdirect_search_results_container').addClass('hidden');
            showCreateAndSearchPanel(false);
            $('#call_walk_direct_btn').addClass('disabled');
        });
    };

    this.openWalkDirectModal = function() {
		$('#walkdirect_new_customer').addClass('hidden');
        $('#walkdirect_customer_first_name_fld').val('');
        $('#walkdirect_customer_last_name_fld').val('');
        $('#walkdirect_customer_mobile_fld').val('');
        $('#walkdirect_customer_email_fld').val('');
		
		$('#walkdirect_customer_first_name_lbl').html('');
		$('#walkdirect_customer_last_name_lbl').html('');
		$('#walkdirect_customer_mobile_lbl').html('');
		$('#walkdirect_customer_email_lbl').html('');

        $('#walkdirect_customer_mobile_fld').val('');
        //$('#walkdirect_customer_vip_fld').prop('checked', false);
        $('#walkdirect_search_field').val('');
        $('#walkdirect_add_service').addClass('hidden');
        $('#walkdirect_note').addClass('hidden');
        $('#walkdirect_selected_customer_panel').addClass('hidden');
        $('#call_walk_direct_btn').addClass('disabled');
        $('#walkdirect_note_textarea').val('');
        showCreateAndSearchPanel(true);
        clearSearchResults();

        newVisit = {
            "caseServices" : [],
            "ticketNumber" : "",
            "caseCustomer" : {
				"id" : ""
			}
        };

        $('#walkdirect_search_field').unbind().keyup(function(e) {
            if(e.keyCode == 13) {
                if($('#walkdirect_search_field').val().length > 1) {
                    walkDirect.searchCustomers($('#walkdirect_search_field').val(), '#walkdirect_search_results');
                }
            }
        });
        $('#walk_direct_modal').modal('show');
    };

    this.populatePresetNotes = function(note) {
        PresetNotesService.getPresetNotes()
            .done(function(result){
                $('#walkdirect_preset_notes').empty();
                for (var i = 0; i < result.length; i++) {
                    var item = result[i];
                    $('#walkdirect_preset_notes').append($("<option />").val(item.text).text(item.text));
                }
            })
            .fail(function(error){
                util.showMessage("Failed to retrieve Preset Notes.", error);
            });

        if(util.notNull(note) && util.notNull(note.text)) {
            $('#walkdirect_note_textarea').val(note.text);
        }

        $('#walkdirect_select_preset_note_add').unbind();
        $('#walkdirect_select_preset_note_add').click(function() {
            var preset = $('#walkdirect_preset_notes').val();
            $('#walkdirect_note_textarea').val($('#walkdirect_note_textarea').val() + " " + preset);
        });
    };

    var showEnterNote = function() {
        walkDirect.populatePresetNotes();
        $('#walkdirect_note').removeClass('hidden');
    }

    var showSelectService = function() {
		$('#checkbox_walk_direct_service_vip').prop('checked', false);
        $('#walkdirect_add_service_select').empty();
        $('#walkdirect_add_service_note').val('');

        var params = {
            "branchId":sessvars.branchId
        };
        var services = {};
        CaseServicePointService.getServices()
            .done(function(_services){
                services = _services;
            })
            .fail(function(err){
                util.showError("Could not retrieve the list of Service from the server.", err);
            });

        $('#walkdirect_add_service_select').append($("<option />").val(0).text("Select Service"));
        $.map(services, function(item) {
            $('#walkdirect_add_service_select').append($("<option />").val(item.id).text(item.externalName));
        });
        $('#walkdirect_add_service_select').change(function(){
            if($('#walkdirect_add_service_select').val() == 0){
                $('#call_walk_direct_btn').addClass('disabled');
            }else{
                $('#call_walk_direct_btn').removeClass('disabled');
            }
        });
        $('#walkdirect_add_service').removeClass('hidden');
    };

    var callConductorWalkDirect = function() {

        newVisit.branchId = sessvars.branchId;
        newVisit.branchName = sessvars.branchName;

        newVisit.caseServices.push({
                "serviceId":$('#walkdirect_add_service_select').val(),
                "serviceName": $('#walkdirect_add_service_select option:selected').text(),
                "status" : "PENDING",
                "notes" : new Array(),
				"vip" : $('#checkbox_walk_direct_service_vip').prop('checked')
            }
        );

        newVisit.currentNote = {
            "text":$('#walkdirect_note_textarea').val(),
            "createdBy":sessvars.currentUser.fullName, //sessvars.currentUser.userName
			"created" : new Date()
        };

        CaseServicePointService.walkDirect(newVisit)
            .done(function(userStatus){
                sessvars.state = userStatus;
                CaseServicePointService.getCaseByVisitId(sessvars.state.visit.id)
                    .done(function(dbCase){
                        sessvars.currentCase = dbCase;
                        servicePoint.populateCurrentTransaction(sessvars.currentCase);
                        servicePoint.updateServingTimeClock();
                        panels.resetRightPanel();
                        $('#walk_direct_modal').modal('hide');
                    })
                    .fail(function(err){
                        util.showError("Failed to get Visit data after created a walkDirect visit", err);
                    })
            })
            .fail(function(err){
                util.showError("Failed to create a walkDirect visit", err);
            })
    };

    this.searchCustomers = function(search_string, results_table ) {
		$('#walkdirect_customer_first_name_lbl').html('');
		$('#walkdirect_customer_last_name_lbl').html('');
		$('#walkdirect_customer_mobile_lbl').html('');
		$('#walkdirect_customer_email_lbl').html('');
		
		search_string = $.trim(search_string);
        if(search_string.length < 2) {
            util.showMessage("Type at least two characters");
            return;
        }
        var results = {};
		var params = search_string.split(' ');
		var factor = "or";
		var searchParams = {};
		if (params.length == 1) {
			searchParams = {
				"id": params[0],
				"firstName": '%' + params[0] + '%',
				"lastName": '%' + params[0] + '%',
				"email": '%' + params[0] + '%',
				"phoneNumber": '%' + params[0] + '%'
			};
		}
		else {
			searchParams = {
				"firstName": '%' + params[0] + '%',
				"lastName": '%' + params[1] + '%'
			};
			factor = "and"
		}
		CaseServicePointService.searchCustomers(searchParams, factor)
			.done(function(searchResult){
				results = searchResult;
			})
			.fail(function(err){
				util.showError("Something when wrong when searching for the customer.", err);
			})

        $(results_table).empty();

        for(var i = 0; i < results.length; i++) {
            var tr = $(document.createElement('tr'));

            $(tr).append('<td>' + results[i].id + '</td>');
            $(tr).append('<td>' + results[i].firstName + ' ' + results[i].lastName + '</td>');
            $(tr).append('<td>' + results[i].phoneNumber + '</td>');
            $(tr).append('<td>' + results[i].email + '</td>');

            // Add click handler for select customer
            $(tr).attr('style', 'cursor: pointer');
            (function(_id) {
                $(tr).click(function() {
                    //onSelectResult(_id);
                    CaseServicePointService.getCustomer(_id)
                        .done(function(cust){
                            setCustomerSelected(cust);
                        })
                        .fail(function(err){
                            util.showError("Could not get customer data.", err);
                        });
                    clearSearchResults('#walkdirect_search_results');
                    $('#walkdirect_search_results_container').addClass('hidden');
                });
            })(results[i].id);

            $(results_table).append(tr);
        }
        $(results_table).removeClass('hidden');
        $('#walkdirect_search_results_container').removeClass('hidden');
    };

    var setCustomerSelected = function(cust){
        newVisit.caseCustomer.id = cust.id;
        //newVisit.customerName = cust.firstName + " " + cust.lastName;
		$('#walkdirect_customer_id_lbl').text(cust.id);
        $('#walkdirect_customer_first_name_lbl').text(cust.firstName);
        $('#walkdirect_customer_last_name_lbl').text(cust.lastName);
        $('#walkdirect_customer_mobile_lbl').text(cust.phoneNumber);
        /*
		if(cust.vip==true){
            $('#walkdirect_customer_vip_lbl').removeClass('hidden');
        }else{
            $('#walkdirect_customer_vip_lbl').addClass('hidden');
        }
		*/
        $('#walkdirect_customer_email_lbl').text(cust.email);
        $('#walkdirect_selected_customer_panel').removeClass('hidden');
        showCreateAndSearchPanel(false);
        showEnterNote();
        showSelectService();
    }

    var showCreateAndSearchPanel = function(show){
        if(show==true){
            $('#walkdirect_panel').removeClass('hidden');
        }else{
            $('#walkdirect_panel').addClass('hidden');
        }
    }

    var clearSearchResults = function (results_table){
        $('#walkdirect_search_field').val('');
        $(results_table).empty();
        $(results_table).addClass('hidden');
    }

    var clearNewCustomerForm = function(){
        $('#walkdirect_customer_first_name_fld').val('');
        $('#walkdirect_customer_last_name_fld').val('');
        $('#walkdirect_customer_mobile_fld').val('');
        //$('#walkdirect_customer_vip_fld').prop('checked', false);
        $('#walkdirect_customer_email_fld').val('');
    }
};