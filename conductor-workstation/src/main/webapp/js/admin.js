var admin = new function() {
	
	this.initView = function() {
		$("#preset-note-add-btn").click(function() {
			$("#edit-preset-note-id").val("0");
        	$("#edit-preset-note-text").val("");
        	$("#edit-preset-note-modal").modal("show");
        });
		$("#edit-preset-note-save-btn").click(function() {
			_savePresetNote();
		});
		
		var columns = _createTableColumns();
		_createTable(columns);
		admin.updateView();
	}
	
	this.updateView = function() {
		PresetNotesService.getPresetNotes()
	    .done(function(result){
	    	$("#preset-notes-table").dataTable().fnClearTable();
	        $.map(result, function(presetNotes) {
	            $("#preset-notes-table").dataTable().fnAddData([presetNotes]);
	        });
	    })
	    .fail(function(error){
	        util.showMessage("Failed to retrieve Preset Notes.", error);
	    });
	}

	_createTableColumns = function() {
		var columns = [
           {"bSearchable": false,
               "bVisible": true,
               "mDataProp": "text"},
           {"bSearchable": false,
               "bVisible": true,
               "mDataProp": null}
       ];
		return columns;
	}
	
	_createTable = function(columns) {
		$("#preset-notes-table").dataTable({
			"aoColumns":columns,
	        "bPaginate": false,
	        "bLengthChange": false,
	        "iDisplayLength ": false,
	        "bFilter": false,
	        "bInfo": false,
	        "bSort": false,
	        "fnRowCallback" : function(nRow, aData, iDisplayIndex) {

	        	$("td:eq(1)", nRow).empty();
	        	
	        	var editIcon = $(document.createElement('i'));
                $(editIcon).addClass('icon-edit-note');
                $(editIcon).click(function() {
                	$("#edit-preset-note-id").val(aData.id);
                	$("#edit-preset-note-text").val(aData.text);
                	$("#edit-preset-note-modal").modal("show");
                });
	        	
	        	var deleteIcon = $(document.createElement('i'));
                $(deleteIcon).addClass('icon-delete-note');
                $(deleteIcon).click(function() {
                	_deletePresetNote(aData.id);
                });
	        	
                $("td:eq(1)", nRow).append(editIcon);
	        	$("td:eq(1)", nRow).append(deleteIcon);
	        }
		});
	}
	
	_savePresetNote = function() {
		var id = $("#edit-preset-note-id").val();
		var text = $("#edit-preset-note-text").val();
		if(text.length > 0) {
			PresetNotesService.savePresetNote(id, encodeURIComponent(text))
		    .done(function(result) {
		    	$("#edit-preset-note-modal").modal("hide");
		    	admin.updateView();
		    })
		    .fail(function(error) {
		        util.showMessage("Failed to save Preset Notes.", error);
		    });
		} else {
			
		}
	}
	
	_deletePresetNote = function(id) {
		PresetNotesService.deletePresetNote(id)
	    .done(function(result) {
	    	admin.updateView();
	    })
	    .fail(function(error) {
	        util.showMessage("Failed to delete Preset Notes.", error);
	    });
	}
	
}

