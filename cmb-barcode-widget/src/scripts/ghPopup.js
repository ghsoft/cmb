var ghPopup = new function() {

	this.create = function(popup) {
		var style = 'width: ' + popup.width + 'px; height: ' + popup.height + 'px; margin-left: -' + popup.width/2 + 'px; margin-top: -' + popup.height/2 + 'px;';
		var html = '';
		html += '<div class="popup hide" style="' + style + '" id="' + popup.name + '">';
		html += '	<table>';
		if (popup.header) {
			html += '		<tr class="header">';
			html += '			<td>';
			html += 				createHeader(popup);
			html += '			</td>';
			html += '		</tr>';
		}
		html += '		<tr>';
		html += '			<td class="padding">';
		html += 				createBody(popup);
		html += '			</td>';
		html += '		</tr>';
		if (popup.footer) {
			html += '		<tr class="footer">';
			html += '			<td class="paddingAll">';
			html += 				createFooter(popup);
			html += '			</td>';
			html += '		</tr>';
		}
		html += '	</table>';
		html += '</div>';
		return html;
	}
	
	var createHeader = function(popup) {
		var html = '';
		html += '<table>';
		html += '	<tr>';
		html += '		<td>';
		html += '			';
		html += '		</td>';
		if (popup.header) {
			html += '		<td class="percent95"></td>';
			html += '		<td>';
			if (popup.header.close) {
				html += '			<button class="popupClose" onclick="ghPopup.closePopup(\'' + popup.header.close + '\');">&times;</button>';
			}
			html += '		</td>';
		}
		html += '	</tr>';
		html += '</table>';
		return html;
	}
	
	var createBody = function(popup) {
		var html = '';
		html += '<table>';
		html += '	<tr>';
		if (popup.body == 'spinner') {
			html += '		<td id="loader"></td>';
		}
		else {
			html += '		<td>';
			html += '			<div class="message font8" lang="label.' + popup.body + '"' + (popup.langValue ? ' langvalue="' + popup.langValue + '"' : '') + '></div>';
			html += '		</td>';
		}
		html += '	</tr>';
		html += '</table>';
		return html;
	}
	
	var createFooter = function(popup) {
		var html = '';
		html += '<table>';
		if (popup.footer) {
			if (popup.footer.buttons) {
				var elementId = popup.name + 'FooterButton';
				for (b in popup.footer.buttons) {
					if (popup.footer.rows == true || b == 0) {
						html += '	<tr>';
						html += '		<td class="bottom">';
					}
					html += 			ghButton.create(popup.footer.buttons[b], elementId);
					if (popup.footer.rows == true || b == (popup.footer.buttons.size - 1)) {
						html += '		</td>';
						html += '	</tr>';
					}
				}
			}
		}
		html += '</table>';
		return html;
	}
	
	this.show = function(popupId) {
		if (popupId == 'spinner') {
			variables.general.popupTimer = setTimeout(function(){
				ghPopup.hide();
			}, settings.popupTimeout * 1000);
		}
		ghPopup.hide();
		//events.keepWidget();
		/*
		$('.blackout').fadeTo("fast", .8, function() {
			$(this).removeClass('hide');
		});
		*/
		$('.blackout').removeClass('hide');
		$('#' + popupId + 'Popup').removeClass('hide');
	}
	
	this.hide = function() {
		clearTimeout(variables.general.popupTimer);
		$('.popup').addClass('hide');
		$('.blackout').addClass('hide');
		/*
		$('.blackout').fadeTo("fast", 0, function() {
			$(this).addClass('hide');
		});
		*/
	}
	
	this.closePopup = function(page) {
		ghPopup.hide();
		if (page !== undefined && page != null && page != 'none') {
			events.openExternalPage(page);
		}
	}
	
};