var ghPage = new function() {

	this.create = function(page) {
		var html = '';
		html += '<div class="page ' + (page.barcodeEnabled ? 'barcodeEnabled' : '') + '" id="' + page.name + '">';
		html += '	<table>';
		html += '		<tr>';
		html += '			<td class="header">'
		html += '				<div style="color: ' + settings.general.main.header.color + '; ' + ghLib.qmatic.widget.getFontStyle(settings.general.main.header.font) + '">'
		html +=  					settings.general.main.header.text;
		html += '				</div>';
		html += '			</td>';
		html += '		</tr>';
		html += '		<tr>';
		html += '			<td class="body">'
		html += '				<div id="ticketMessage" style="color: ' + settings.general.ticket.message.color + '; ' + ghLib.qmatic.widget.getFontStyle(settings.general.ticket.message.font) + '">'
		html +=  					settings.general.ticket.message.text;
		html += '				</div>';
		html += '			</td>';
		html += '		</tr>';
		html += '	</table>';
		html += '</div>';
		return html;
	}
	
	this.show = function(pageName) {
		pageName += 'Page';
		ghPopup.hide();
		$('.page').addClass('hide');
		$('#' + pageName).removeClass('hide');
	}
	
};