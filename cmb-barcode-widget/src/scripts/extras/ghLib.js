var ghLib = new function() {
	this.about = {
		version: '0.0.1',
		author: 'Ghaith Habash',
		created: 'March 02, 2015',
		updated: 'March 02, 2015'
	};
	
	this.json = (function(jsonStr) {
		return JSON.stringify(jsonStr, null, 4);
	})
	
	this.qmatic = {
		widget: {
			getFontStyle: (function(font) {
				var fontAttributes = font.split(';');
				var fontStyle = '';
				fontStyle += fontAttributes[0] ? ('font-family: ' + fontAttributes[0] + '; ') : '';
				fontStyle += fontAttributes[1] ? ('font-size: ' + fontAttributes[1] + '; ') : '';
				fontStyle += fontAttributes[2] ? ('font-style: ' + fontAttributes[2] + '; ') : '';
				fontStyle += fontAttributes[3] ? ('font-weight: ' + fontAttributes[3] + '; ') : '';
				return fontStyle;
			}),
			
			parseFont: (function(font) {
				var fontAttributes = font.split(';');
				var fontStyle = {};
				fontStyle.family = fontAttributes[0] ? fontAttributes[0] : '';
				fontStyle.size = fontAttributes[1] ? fontAttributes[1] : '';
				fontStyle.style = fontAttributes[2] ? fontAttributes[2] : '';
				fontStyle.weight = fontAttributes[3] ? fontAttributes[3] : '';
				return fontStyle;
			}),
			
			jsonify: (function(attr) {
				var key = attr.key;
				var parts = key.split('.');
				var newObj = {};
				if (parts[1]){
					var jsonAttr = parts[0];
					parts.splice(0,1);
					var newAttr = {};
					newAttr.key = parts.join('.');
					newAttr.value = attr.value;
					newObj[jsonAttr] = this.jsonify(newAttr);
				}
				else {
					newObj[parts[0]] = attr.value ? attr.value : '';
				}
				return newObj;
			}),
			
			parseAttributes: (function(configAttributes) {
				var jsonAttributes = {};
				for(i in configAttributes) {
					$.extend(true, jsonAttributes, this.jsonify(configAttributes[i]));
				}
				return jsonAttributes;
			})
		}
	}
};