var controller = new function() {
		
	this.onLoaded = function(configuration) {
		settings.general = ghLib.qmatic.widget.parseAttributes(configuration.attributes);
		variables.clients.widget = qmatic.webwidget.client;
		variables.clients.connector = qmatic.connector.client;
		
		variables.general.branchId = variables.clients.widget.getBranchId();
		
		ib.buildPages();
		
		$(window).focus();
	}
	
	this.onLoadError = function(message) {
		$('body').append('<p>Widget load error: ' + message + '</p>');
	}
};