var backend = new function() {
	
	this.searchTicket = function() {
		var ticketNumber = variables.general.ticketNumber;
		variables.general.ticketNumber = '';
		$.ajax({
            type: 'GET',
            url: settings.urls.CaseServicePointRestPath + '/branches/' + variables.general.branchId + '/cases/ticketnumber/' + ticketNumber,
            dataType: 'json',
            async: true,
            cache: false
        }).done(function(visits) {
			variables.general.visitParams = {};
			variables.general.visitParams.ticketNumber = ticketNumber;
			if (visits.length == 0) {
				ib.showTicketMessage('notfound');
				return;
			}
			else {
				for (v in visits) {
					if (visits[v].ongoingService != null) {
						variables.general.visitParams.counterName = visits[v].ongoingService.servedByCounterName;
						variables.general.visitParams.serviceName = visits[v].ongoingService.serviceName;
						if (visits[v].ongoingService.status == 'SERVING') {
							ib.showTicketMessage('serving');
						}
						else if (visits[v].ongoingService.status == 'POST_PROCESSING') {
							ib.showTicketMessage('pp');
						}
						return;
					}
					for (cs in visits[v].caseServices) {
						var cService = visits[v].caseServices[cs];
						if (cService.status == 'RECYCLED') {
							ib.showTicketMessage('recycled');
							return;
						}
						if (cService.status == 'ONHOLD') {
							ib.showTicketMessage('onhold');
							return;
						}
						if (cService.status == 'NOSHOW') {
							ib.showTicketMessage('noshow');
							return;
						}
					}
					if (visits[v].inTrash == true) {
						ib.showTicketMessage('finished');
						return;
					}
					if (visits[v].numberOfUnservedServices > 0) {
						variables.general.visitParams.serviceCount = visits[v].numberOfUnservedServices;
						ib.showTicketMessage('waiting');
						return;
					}
				}
				ib.showTicketMessage('finished');
			}
		})
		.fail(function(error) {
			alert(ghLib.json(error));
		});
	}

};