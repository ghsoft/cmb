var variables = new function() {

	this.general = {
		branchId: '',
		ticketNumber: '',
		messageTimeout: null,
		visitParams: {}
	};

	this.clients = {
		widget: null,
		connector: null
	};
	
	this.barcode = {
		buffer: '',
		buttonTimeout: 0,
		processing: false,
	};
	
	this.replace = (function(fullString, replacedString, replaceString) {
		var re = new RegExp('{' + replacedString + '}', 'g');
		return fullString.replace(re, replaceString);
	});
	
	this.message = (function(ticketMessage) {
		var msg = settings.general.ticket.message[ticketMessage];
		msg = msg.replace(/(?:\\[rn])+/g, '<br />');
		for (p in variables.general.visitParams) {
			msg = variables.replace(msg, p, variables.general.visitParams[p]);
		}
		return msg;
	});
	
	this.current = {
		activePage: (function() {
			return $('.page').not('.hide').each(function() {
				return this.id;
			});
		}),
		activePopup: (function() {
			return $('.popup').not('.hide').each(function() {
				return this.id;
			});
		})
	};
	
};