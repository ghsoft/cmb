var settings = new function() {
	this.general = {};
	
	this.urls = {
		CaseServicePointRestPath : '/conductor-backend/rest/caseservicepoint'
	};
	
	this.barcode = {
		readDelay: 1, //seconds
		bufferResetAfter: 5, //seconds will use settings.general.ticket.message.timeout
		length: 4
	};
};