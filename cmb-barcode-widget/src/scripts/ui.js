var ui = new function() {
		
	this.pages = [
		{
			name: 		'mainPage'
		,	body:		{ type: 'custom' }
		,	barcodeEnabled: true
		}
	];
	
	this.popups = [
		{
			name: 		'spinnerPopup'
		,	width:		500
		,	height:		500
		,	body:		'spinner'
		}
	];
	
};