var ib = new function() {

	this.buildPages = function() {
		$('body').empty().append('<div id="pages"></div>');
		$('body').append('<div id="popups"></div>');
		$('body').append('<div class="blackout hide"></div>');
		for (p = 0; p < ui.pages.length; p++) {
			$('#pages').append(ghPage.create(ui.pages[p]));
		}
		for (p = 0; p < ui.popups.length; p++) {
			$('#popups').append(ghPopup.create(ui.popups[p]));
		}
		ghPage.show('main');
		$('#mainPage').css('background-image', 'url(/cms/thumbs' + settings.general.main.background.image + ')');
		$('#mainPage').css('background-size', '1024px 768px');
		$('#mainPage').css('background-repeat', 'no-repeat');
		//Load spin
		var spinner = new Spinner({lines: 12, color: '#111', length: 60, width: 15, radius: 80}).spin();
		$('#loader').html(spinner.el);
		//Listen to barcode
		document.onkeydown = function (e) {
			if (variables.current.activePopup().attr('id') == undefined && variables.current.activePage().hasClass('barcodeEnabled')) {
				variables.barcode.buffer += String.fromCharCode(e.keyCode);
				variables.barcode.buffer = variables.barcode.buffer.replace(/[^0-9]/gi, ''); //(/[^a-z0-9]/gi, '')
				if (variables.barcode.buffer.length > 0) {
					if (variables.barcode.buttonTimeout == 0) {
						variables.barcode.buttonTimeout = setTimeout(function() {
								variables.barcode.buffer = '';
								variables.barcode.buttonTimeout = 0;
							}
							, settings.general.ticket.message.timeout * 1000
						);
					}
					if (variables.barcode.buffer.length >= settings.barcode.length) {
						return false;
					}
				}
				if(!variables.barcode.processing) {
					variables.barcode.processing = true;
					//Delay Read
					setTimeout(
						function() {
							variables.general.ticketNumber = variables.barcode.buffer;
							backend.searchTicket();
							variables.barcode.processing = false;
							//variables.barcode.buffer = '';
						}
						, settings.barcode.readDelay * 1000
					);
				}
			}
			return false;
		};
	}
	
	this.showTicketMessage = function(ticketMessage) {
		$('#ticketMessage').html(variables.message(ticketMessage));
		variables.general.messageTimeout = setTimeout(function() {
			$('#ticketMessage').html(settings.general.ticket.message.text);
		}
		, settings.general.ticket.message.timeout * 1000);
	}
};