var CaseAppointmentService = (function($) {

    var _baseRestPath = '/conductor-backend/rest/caseappointment';

    function _getAppointments(branchId) {
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/branches/' + branchId + '/appointments',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
    
    function _updateAppointmentToArrived(appointmentId, branchId, entryPointId, customerId, customerName) {
        return $.ajax({
            type: 'POST',
            url: _baseRestPath +'/arrived/' + appointmentId + '/branch/' + branchId + '/entryPoint/' + entryPointId + '/customer/' + customerId + '/' + customerName,
            dataType: 'json',
            async: true,
            cache: false
        });
    }

	return {
        getAppointments : function(branchId) {
            return _getAppointments(branchId);
        },
        
        updateAppointmentToArrived : function(appointmentId, branchId, entryPointId, customerId, customerName) {
            return _updateAppointmentToArrived(appointmentId, branchId, entryPointId, customerId, customerName);
        }
    };
    
})(jQuery);