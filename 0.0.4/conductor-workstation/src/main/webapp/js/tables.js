var tables = new function() {

    var hasBeenInitialized = false;

    var ongoingAjax = false;

    var lastSelectedQueueId = null;
    var lastSelectedQueueName = null;
    var lastSelectedCaseId = null;



    this.initGUI = function() {
        initSearchResultsTable();
        initQueueTable();
        initQueuesTable();
		initServicepointsTable();
        initVisitDetailsServicesTable();

        initUserPoolVisitsTable();

        initCustomerHistoryTable('cst_customer_history_table');
        initCustomerHistoryTable('customer-details-history-table');
        
        //appointments.initView(); Appointments Disabled
        hasBeenInitialized = true;
    };

    /** One-stop method to call to reload contents of tables not tied to a specific case or queue.
     *  Useful after setting change or switch between counter/reception
     */
    this.refresh = function() {
        if(!hasBeenInitialized) {
            return;
        }

        tables.populateUserPoolVisitsTable();
        common.updateQueuesTable('queues_table');
    }

    this.reloadVisitDetailsServicesTable = function(pCase) {
        if(util.notNull(pCase)) {
            lastSelectedCaseId = pCase.id;
            tables.populateVisitDetailsServicesTable(pCase);
        } else if(util.notNull(lastSelectedCaseId)) {
            pCase = CaseServicePointService.getCase(lastSelectedCaseId)
                .success(function(dbCase) {
                    tables.populateVisitDetailsServicesTable(dbCase);
                });
        }
    };
	
	this.changeVipStatus = function(_serviceId, checked) {
		CaseServicePointService.vipCaseService(_serviceId, checked)
		.done(function(service){

		})
		.fail(function(err){
			util.showError('Error assigning VIP status to the service. ', err);
		});
	};

    this.populateVisitDetailsServicesTable = function(pCase) {
        if(util.notNull(pCase)) {
            lastSelectedCaseId = pCase.id;
        }
        $('#visit_details_services_today').dataTable().fnClearTable();
		var count = 0;
        for(var i = 0; i < pCase.caseServices.length; i++) {
			if (pCase.caseServices[i].status != settings.overriddenStatus) {
				count++;
				var servedByCounter = util.notNull(pCase.caseServices[i].servedByCounterName) ? pCase.caseServices[i].servedByCounterName : '---';
				var servedByUser = util.notNull(pCase.caseServices[i].servedByUserName) ? pCase.caseServices[i].servedByUserName : '---';

				var postProcessingTime = util.notNull(pCase.caseServices[i].postProcessingTime)?pCase.caseServices[i].postProcessingTime:0;
				var servingStart = pCase.caseServices[i].servingStart;
				var servingTime =  pCase.caseServices[i].servingTime;
				var finishedTime = "---";
				if(util.notNull(servingStart)){
					if(pCase.caseServices[i].status == "FINISHED"){
						//finishedTime = util.timeOfDate(moment(servingStart).add("seconds", servingTime).toDate());// .format(util);
						finishedTime = util.timeOfDate(pCase.caseServices[i].updated); //Consider updated time which is the last time recorded as Finished
					}
					servingStart = util.timeOfDate(pCase.caseServices[i].servingStart);
				}else{
					servingStart = "---";
				}
				var enteredTime = util.timeOfDate(pCase.caseServices[i].created); //pCase.caseServices[i].status == "PENDING" ? "---" : util.timeOfDate(pCase.caseServices[i].created);
				var duration = "---";
				var TTime = util.notNull(servingTime) ? util.secondsToHms(servingTime+postProcessingTime) : "---";
				var WTime = util.secondsToHms(pCase.caseServices[i].waitingTime);
				
				duration = (pCase.caseServices[i].status == "WAITING" || pCase.caseServices[i].status == "PENDING" || pCase.caseServices[i].status == "TRASH" || pCase.caseServices[i].status == "NOSHOW" || pCase.caseServices[i].status == "ONHOLD") ? WTime : TTime;
				
				var vipServiceColumn = '';
				if((pCase.caseServices[i].status == 'PENDING' || pCase.caseServices[i].status == 'WAITING' || pCase.caseServices[i].status == 'ONHOLD' || pCase.caseServices[i].status == 'RECYCLED') && pCase.caseServices.length > 1) {
					var vipCheckbox = '<input id="vip_service_details_checkbox' + pCase.caseServices[i].id + '" ';
					vipCheckbox += 'name="ip_service_details_checkbox' + pCase.caseServices[i].id + '" ';
					vipCheckbox += 'value="vip_service_details_checkbox' + pCase.caseServices[i].id + '" ';
					vipCheckbox += 'type="checkbox" ';
					vipCheckbox += (pCase.caseServices[i].vip == true ? 'checked="checked" ' : '');
					vipCheckbox += 'onclick="javascript:tables.changeVipStatus(' + pCase.caseServices[i].id + ', ' + !pCase.caseServices[i].vip + ');">';

					vipServiceColumn = vipCheckbox;
				}
				else {
					vipServiceColumn = (pCase.caseServices[i].vip == true ? '<i title="This is a VIP service" class="icon-vip-customer"></i>' : '<i title="This is a VIP service" class="icon-vip-customer hidden"></i>');
				}
				
				var data = {
					"idx": count, //(pCase.caseServices[i].idx+1),
					//"order" : '',
					"serviceName": '<div class="service-name">' + pCase.caseServices[i].serviceName + '</div>',
					"vip": vipServiceColumn,
					"status" : pCase.caseServices[i].status,
					"counter" : servedByCounter,
					"entered" : enteredTime,
					"called" : servingStart,
					"finished" : finishedTime,
					"duration" : duration,
					"servedBy" : servedByUser,
					"notes" : pCase.caseServices[i].notes,
					"deliveredServices" : pCase.caseServices[i].caseServiceMarks,
					"serviceId" : pCase.caseServices[i].serviceId,
					"id" : pCase.caseServices[i].id,
					"parentId" : pCase.id,
					"numberOfServices" : pCase.caseServices.length,
					"allowMoveUp" :   allowMove(pCase.caseServices, i, pCase.numberOfPendingServices, true),
					"allowMoveDown" : allowMove(pCase.caseServices, i, pCase.numberOfPendingServices, false),
					"delete" : '',
					"qpVisitServiceId" : pCase.caseServices[i].qpVisitServiceId,
					"inTrash" : pCase.inTrash,
					"inHold" : pCase.inHold,
					"inNoShow" : pCase.inNoShow,
					"numberOfPendingServices" : pCase.numberOfPendingServices,
				};

				$('#visit_details_services_today').dataTable().fnAddData([data]);
			}
        }
    };



    var initVisitDetailsServicesTable = function() {
        var columns = [
            {"bSearchable": false,
                "bVisible": true,
                "mDataProp": "idx"},
            /* {"bSearchable": false,
                "bVisible": true,
                "mDataProp": "order"}, 
			*/
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "serviceName"},
			{"bSearchable": true,
                "bVisible": true,
                "mDataProp": "vip"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "status"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "counter"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "entered"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "called"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "finished"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "duration"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "servedBy"},
			{"bSearchable": true,
                "bVisible": true,
                "mDataProp": "notes"},
            {"bSearchable": false,
                "bVisible": true,
                "mDataProp": "delete"},
			{"bSearchable": false,
                "bVisible": false,
                "mDataProp": "qpVisitServiceId"}
        ];

        $('#visit_details_services_today').dataTable(
            {
                "aoColumns":columns,
                "bPaginate": false,
                "bLengthChange": false,
                "iDisplayLength ": false,
                "bFilter": false,
                "bInfo": false,
                "bSort": false,
                "fnRowCallback" : function(nRow, aData, iDisplayIndex) {
					/*
                	$('td:eq(1)', nRow).empty();
                    if(aData.allowMoveUp) {
                        var upIcon = $(document.createElement('i'));
                        $(upIcon).addClass('icon-chevron-up').click(
                            function() {
                                if(ongoingAjax)
                                    return;
                                ongoingAjax = true;
                                servicePoint.moveServiceUp(aData.parentId, aData.id);
                                getCaseAndPopulateVisitDetailsServicesTable(aData.parentId);
                                ongoingAjax = false;
                            }
                        ).attr('title','Move up');
                        $('td:eq(1)', nRow).append(upIcon);
                    }

                    if(aData.allowMoveDown) {
                        var downIcon = $(document.createElement('i'));
                        $(downIcon).addClass('icon-chevron-down').click(function() {
                                if(ongoingAjax)
                                    return;
                                ongoingAjax = true;
                                servicePoint.moveServiceDown(aData.parentId, aData.id);
                                getCaseAndPopulateVisitDetailsServicesTable(aData.parentId);
                                ongoingAjax = false;
                            }
                        ).attr('title','Move down');
                        $('td:eq(1)', nRow).append(downIcon);
                    }
					*/
					$('td:eq(3)', nRow).empty().append(aData.status == 'PENDING' ? 'WAITING' : aData.status);
					
					var notesMarkup = $(document.createElement('div'));
                    for(var j = 0, k = 0; j < aData.notes.length || k < aData.deliveredServices.length;) {
                        var dbNote = aData.notes[j];
                        var caseServiceMark = aData.deliveredServices[k];
                        if(caseServiceMark == undefined || (dbNote != undefined && dbNote.created < caseServiceMark.created)) {
                        	var noteMarkup = markupBuilder.buildNoteMarkup(dbNote, aData.serviceId, false);
                            $(notesMarkup).append(noteMarkup);
        	                j++;
                        } else {
                        	var deliveredServiceMarkup = markupBuilder.buildMarkMarkup(caseServiceMark, false);
                        	$(notesMarkup).append(deliveredServiceMarkup);
                        	k++;
                        }
                    }
                    $('td:eq(10)', nRow).empty().append(notesMarkup);
					/*
                    var deliveredMarkup = $(document.createElement('div'));
                    for(var k = 0; k < aData.deliveredServices.length;) {
                        var nextDeliveredService = aData.deliveredServices[k];
                        var deliveredServiceMarkup = markupBuilder.buildMarkMarkup(nextDeliveredService, false);
                        $(deliveredMarkup).append(deliveredServiceMarkup);
                        k++;
                    }
                    $('td:eq(10)', nRow).empty().append(deliveredMarkup);

                    var notesMarkup = $(document.createElement('div'));
                    for(var j = 0; j < aData.notes.length;) {
                    	var nextNote = aData.notes[j];
                        var editAllowed = aData.status== "FINISHED" ? false : true;
                        var noteMarkup = markupBuilder.buildNoteMarkup(nextNote, aData.serviceId, false, function() {
                            getCaseAndPopulateVisitDetailsServicesTable(aData.parentId);
                    	});
                        $(notesMarkup).append(noteMarkup);
        	            j++;
                    }
                    $('td:eq(11)', nRow).empty().append(notesMarkup);
                    */
                    // For the icons column, first clear it, then render call/delete buttons
                    // a bit differently depending on state and whether one is in the
                    // workstation or the reception.
                    $('td:eq(11)', nRow).empty();
                    if((((aData.status == 'PENDING' || aData.status == 'WAITING' || aData.status == 'RECYCLED') && !aData.inNoShow && !aData.inTrash && !aData.inHold) || aData.status == 'ONHOLD') && !$('#counter-container').hasClass('hidden')) {
                        var callIcon =  $(document.createElement('i'));
                        $(callIcon).addClass('icon-play-green').click(function() {
                            CaseServicePointService.getCase(aData.parentId)
                                .done(function(pCase) {
                                    var serving = false;									
									for(var i = 0; i < pCase.caseServices.length; i++) {
										if(pCase.caseServices[i].status == "SERVING"){
											serving = true;
											break;
										}
									}
									if (!serving) {
										servicePoint.callServiceFromQueueList(aData.qpVisitServiceId);
									}
									else {
										util.showMessage('Ticket ' + pCase.ticketNumber + ' was already called by another user!');
										panels.resetRightPanel();
									}
                                });
                        }).attr('title','Call Service');
                        $('td:eq(11)', nRow).append(callIcon);
                    }
                    if(aData.status == 'PENDING'  && aData.numberOfServices > 1 && !aData.inTrash && !aData.inNoShow  && !aData.inHold) {
                        var deleteIcon = $(document.createElement('i'));
                        $(deleteIcon).addClass('icon-trash').click(function() {
                            util.openConfirm('Remove service?','Are you sure you want to remove \'' + $(aData.serviceName).text() + '\' from this Visit?', function() {
                                servicePoint.performRemoveService(aData.parentId, aData.id, function() {
                                    getCaseAndPopulateVisitDetailsServicesTable(aData.parentId);
                                });
                            });
                        }).attr('title','Remove Service');
                        $('td:eq(11)', nRow).append(deleteIcon);
                    }
					if((aData.status == 'WAITING' || (aData.status == 'TRASH' && aData.numberOfPendingServices > 0) || (aData.status == 'NOSHOW' && aData.numberOfPendingServices > 0) || aData.status == 'RECYCLED') && !$('#counter-container').hasClass('hidden') && aData.numberOfServices > 1 && (aData.idx < aData.numberOfServices)) {
                        var finishIcon = $(document.createElement('i'));
						var removeButton = 'Remove Service';
						if (aData.status == 'TRASH' || aData.status == 'NOSHOW') {
							var removeButton = 'Return to Queue';
						}
                        $(finishIcon).addClass('icon-trash').click(function() {
							if (sessvars.state.visitState != null || sessvars.isPostProcessing == true) {
								util.showMessage('Cannot Finish this service. Another ticket is already called, End serving the called ticket and try again');
								return;
							}
							util.openConfirm(removeButton + '?','Are you sure you want to remove \'' + $(aData.serviceName).text() + '\' from this Visit?', function() {
								CaseServicePointService.overrideService(aData.parentId)
								.done(function(userStatus){
									sessvars.state = userStatus;
									$('#confirm_modal').modal('hide');
									panels.resetRightPanel();
								})
								.fail(function(err){
									$('#confirm_modal').modal('hide');
									util.showError(err.responseText);
								})
                            });
                        }).attr('title',removeButton);
                        $('td:eq(11)', nRow).append(finishIcon);
                    }
                    $('td:eq(11)', nRow).addClass("delete-row-column");
                }
            }
        );
    };



    this.showQueueTable = function(queueId, queueName) {

        panels.showQueueTablePanel();

        $('#selected-queue-queue-name').text(queueName);
        // Back links..
        $('#selected-queue-home').unbind().click(function() {
            panels.resetRightPanel();
        });
        $('#selected-queue-queues').unbind().click(function() {
            panels.resetRightPanel();
        });
        $('#selected-queue-queue-name').unbind().click(function() {
            tables.showQueueTable(queueId, queueName);
        });

        tables.reloadQueue(queueId, queueName);
    };

    this.reloadQueue = function(queueId, queueName) {

        // This little "hack" is so we can keep track of which queue that was previously selected when
        // the user switches between counter and reception.
        if(util.isNull(queueId) && !util.isNull(lastSelectedQueueId)) {
            queueId = lastSelectedQueueId;
        }
        if(util.isNull(queueName) && !util.isNull(lastSelectedQueueName)) {
            queueName = lastSelectedQueueName;
        }
        if(util.isNull(queueId) || util.isNull(queueName)) {
            return;
        }
        lastSelectedQueueId = queueId;
        lastSelectedQueueName = queueName;

        $('#queue_table').dataTable().fnClearTable();

        CaseServicePointService.getCasesInQueue(queueId)
            .done(function(visits){
                for(var i = 0; i < visits.length; i++) {

                    var lastNote = '';
                    if(typeof visits[i].lastNote !== 'undefined' && visits[i].lastNote != null && visits[i].lastNote.text != '') {
                        lastNote = '<div class="note"><div class="name">' + visits[i].lastNote.text + '</div><div class="text-right user">' + util.timeOfDate(visits[i].lastNote.created) + ' ' + visits[i].lastNote.createdBy + '</div></div>';
                    }

                    $('#queue_table').dataTable().fnAddData([
                        {
                            "ticketNumber" : visits[i].ticketId,
                            "waitingTime" : util.secondsToHms(visits[i].waitingTime),
                            "customerName" : visits[i].customerName,
							"customerId" : visits[i].customerId,
                            "services" : visits[i].services,
							"vip" : visits[i].vip,
                            "lastNote" : lastNote,
                            "nothing" : '',
                            "qpVisitId" : visits[i].qpVisitId,
                            "queueId" : queueId,
                            "queueName" : queueName
                        }
                    ]);
                }
            })
            .fail(function(err){

            })

    };


    var initQueueTable = function() {

        // var url = "/rest/managementinformation/branches/" + sessvars.branchId + "/queues/";
        var columns = [
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "ticketNumber"},
			{"bSearchable": true,
                "bVisible": true,
                "mDataProp": "waitingTime"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "customerName"},
			{"bSearchable": true,
                "bVisible": true,
                "mDataProp": "customerId"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "services"},
			{"bSearchable": true,
                "bVisible": true,
                "mDataProp": "vip"},
			{"bSearchable": true,
                "bVisible": true,
                "mDataProp": "lastNote"},
            {"bSearchable": false,
                "bVisible": true,
                "mDataProp": "nothing"}
        ];

        $('#queue_table').dataTable(
            {
                "aoColumns":columns,
                "bPaginate": true,
                "bLengthChange": false,
                "iDisplayLength ": false,
                "bAutoWidth" : false,
                "bFilter": false,
                "bInfo": false,

                "fnRowCallback":function(nRow, aData, iDisplayIndex) {
                    var anchor = $(document.createElement("a"));
                    anchor.html(aData.ticketNumber);
                    $(anchor).click(function() {
                        common.getCaseByVisitId(aData.qpVisitId, function(pCase) {
                            servicePoint.populateVisitDetails(pCase, aData.queueId, aData.queueName);
                        }, function() {
                            util.showError('Cannot view Visit details, no case is attached to the visit.');
                        });
                    });
                    $('td:eq(0)', nRow).empty().append(anchor);

                    if(util.notNull(aData.services)) {
                        var serviceArray = aData.services.split(', ');
                        $('td:eq(4)', nRow).empty();
                        for(var i=0; i<serviceArray.length; i++ ){
                            $('td:eq(4)', nRow).append(serviceArray[i] + '<i title="This is a VIP service" class="icon-vip-customer hidden"></i>' + "<br />");
                        }
                    }

					if(util.notNull(aData.vip)) {
                        var vipArray = aData.vip.split(', ');
                        $('td:eq(5)', nRow).empty();
                        for(var i=0; i<vipArray.length; i++ ){
                            $('td:eq(5)', nRow).append((vipArray[i] == 'true' ? '<i title="This is a VIP service" class="icon-vip-customer"></i>' : '<i title="This is a VIP service" class="icon-vip-customer hidden"></i>') + "<br />");
                        }
                    }

                    if(!$('#counter-container').hasClass('hidden')) {
                        $('td:eq(8)', nRow).empty().append('<i class="icon-play-green" title="Call service from Queue"></i>').unbind().click(function() {
                            common.getCaseByVisitId(aData.qpVisitId, function(pCase) {
                                servicePoint.callVisit(pCase);
                            },function() {
                                util.showError('Cannot call Visit, no case is attached to the visit.');
                            });
                        });
                    }
                }
            }).fnSort( [ [1,'desc'] ] );
    };




    var initQueuesTable = function() {

        common.initQueuesTable('queues_table', function(nRow, aData, iDisplayIndex) {
            var anchor = $(document.createElement("a"));
            anchor.html(aData.name);
            $(anchor).click(function() {
                tables.showQueueTable(aData.id, aData.name);
            });
            $('td:eq(0)', nRow).empty().append(aData.id).addClass("hidden");
            $('td:eq(1)', nRow).empty().append(anchor);
            $('td:eq(3)', nRow).empty().append(util.secondsToHms(aData.waitingTime));
			if ($('#opspanel-container').hasClass('hidden')) {
				$('#servedHead').addClass("hidden");
				$('td:eq(4)', nRow).addClass("hidden");
			}
			else {
				$('#servedHead').removeClass("hidden");
				$('td:eq(4)', nRow).empty().append(aData.servicesServed).removeClass("hidden");
			}
        });
        // Do an immediate load of data and then schedule a reload every 20 seconds
        common.updateQueuesTable('queues_table');
        sessvars.queueTimer = setInterval(function() {
                common.updateQueuesTable('queues_table');
            }, 20000
        );
    };
	
	var initServicepointsTable = function() {

        opspanel.initServicepointsTable('servicepoints_table', function(nRow, aData, iDisplayIndex) {
			$('td:eq(0)', nRow).empty().append(aData.id).addClass("hidden");
			$('td:eq(6)', nRow).empty().append(aData.transactionTime > 0 ? util.secondsToHms(aData.transactionTime) : '---');
			if (aData.workProfileId != 0) {
				var anchor = $(document.createElement("a"));
				anchor.html(aData.workProfileName);
				$(anchor).click(function() {
					opspanel.showChangeProfileWindow(aData.workProfileId, aData.workProfileName, aData.staffName);
				});
				$('td:eq(8)', nRow).empty().append(anchor);
			}
        });
		
        // Do an immediate load of data and then schedule a reload every 20 seconds
        opspanel.updateServicepointsTable('servicepoints_table');
        sessvars.servicepointsTimer = setInterval(function() {
                opspanel.updateServicepointsTable('servicepoints_table');
            }, 39000
        );
		
    };

    this.loadCustomerHistory = function(customerId, elementId) {
        $('#' + elementId).dataTable().fnClearTable();
        CaseServicePointService.findCasesHistoryByCustomerId(customerId)
            .done(function(cases){
                for(var i = 0; i < cases.length; i++) {
                    var dbCase = cases[i];

                    var displayDate = "";
                    for(var x = 0; x < dbCase.caseServices.length; x++){
                        var caseService = dbCase.caseServices[x];
                        if( caseService.serviceName != "TRASH" && caseService.status != "TRASH"){
                            displayDate = x==0?moment(dbCase.created).format("M/D/YY"):"";
                        var data = {
                                "created": displayDate,
                            "serviceName": '<div class="service-name">' + caseService.serviceName + '</div>',
                            "servedBy" : caseService.servedByUserName,
                            "duration" : util.secondsToHms(caseService.servingTime),
                            "notes" : caseService.notes,
                            "deliveredServices" : caseService.caseServiceMarks
                        };
                        $('#' + elementId).dataTable().fnAddData([data
                        ]);
                    }
                }
                }
            })
    };

    /**
     * @deprecated We can probably remove this as the customer history functionality in unwanted.
     * @param elementId
     */
    var initCustomerHistoryTable = function(elementId) {
        var columns = [
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "created"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "serviceName"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "servedBy"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "duration"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "notes"}
        ];

        $('#' + elementId).dataTable(
            {
                "aoColumns":columns,
                "bPaginate": false,
                "bLengthChange": false,
                "iDisplayLength ": false,
                "bFilter": false,
                "bInfo": false,
				"bSort": false,
                "fnRowCallback" : function(nRow, aData, iDisplayIndex) {

                    var notesMarkup = $(document.createElement('div'));
                    for(var j = 0, k = 0; j < aData.notes.length || k < aData.deliveredServices.length;) {
                        var dbNote = aData.notes[j];
                        var caseServiceMark = aData.deliveredServices[k];
                        if(caseServiceMark == undefined || (dbNote != undefined && dbNote.created < caseServiceMark.created)) {
                        	var noteMarkup = markupBuilder.buildNoteMarkup(dbNote, aData.serviceId, false);
                            $(notesMarkup).append(noteMarkup);
        	                j++;
                        } else {
                        	var deliveredServiceMarkup = markupBuilder.buildMarkMarkup(caseServiceMark, false);
                        	$(notesMarkup).append(deliveredServiceMarkup);
                        	k++;
                        }
                    }
                    $('td:eq(4)', nRow).empty().append(notesMarkup);
                }
            }
        );
    };

     this.emptyUserPool = function() {
         $('#visits-users-pool-table').dataTable().fnClearTable();
     };


    this.populateUserPoolVisitsTable = function() {
        if(servicePoint.USE_HOLDING_QUEUE == true){
            return;
        }
        CaseServicePointService.getVisitsInUserPool()
            .done(function(visits){
            	tables.emptyUserPool();
                for(var i = 0; i < visits.length; i++) {

                    var servicesStr = buildServicesServedCountStr(visits[i]);

                    $('#visits-users-pool-table').dataTable().fnAddData([
                        {
                            "ticketNumber" : visits[i].ticketId,
                            "timeInPool" : util.formatIntoMMSS(visits[i].waitingTime),
                            "customerName" : visits[i].customerName,
                        //  "ongoingService" : visits[i].serviceName,
                        // Ongoing can be several....
                        // Do we need to find a solution for it or is it not needed?
                            "services" : servicesStr,
                            "serviceList" : visits[i].services,
                            "qpVisitId" : visits[i].qpVisitId,
                            "nothing" : ''
                        }
                    ]);
                }
            })
    };

    var initUserPoolVisitsTable = function() {
        var columns = [
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "ticketNumber"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "timeInPool"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "customerName"},
            //{"bSearchable": true,
            //    "bVisible": true,
            //    "mDataProp": "ongoingService"},
            // Ongoing can be several....
            // Do we need to find a solution for it or is it not needed?
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "services"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "serviceList"},
            {"bSearchable": false,
                "bVisible": true,
                "mDataProp": "nothing"}
        ];

        $('#visits-users-pool-table').dataTable(
            {
                "aoColumns":columns,
                "bPaginate": false,
                "bLengthChange": false,
                "iDisplayLength ": false,
                "bFilter": false,
                "bInfo": false,

                "fnRowCallback":function(nRow, aData, iDisplayIndex) {

                    $('td:eq(5)', nRow).empty().append('<i class="icon-play-green" title="Call service from pool"></i>').click(function() {
                        CaseServicePointService.getCaseByVisitId(aData.qpVisitId)
                            .done(function(dbCase){
                                servicePoint.callVisit(dbCase);
                                tables.populateUserPoolVisitsTable();
                            })
                            .fail(function(err){
                                util.showError('Cannot call Visit, no case is attached to the visit.', err);
                            })
                    });
                }
            });

        // Load data, then reload every 30 seconds.
        tables.populateUserPoolVisitsTable();
        setInterval(tables.populateUserPoolVisitsTable, 30000);
    };

    var initSearchResultsTable = function() {
        var columns = [
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "customerName"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "ticketNumber"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "created"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "ongoingServiceName"},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "ongoingServiceStatus"}
        ];
        $('#search_results_div').dataTable(
            {"aoColumns": columns,
                "bPaginate": true,
                "bLengthChange": false,
                "iDisplayLength ": 4,
                "bFilter": false,
                "bInfo": false,
                "fnRowCallback": function(nRow, aData, iDisplayIndex) {

                    var customerAnchor = $(document.createElement("a"));
                    customerAnchor.html(aData.customerName);
                    $(customerAnchor).click(function() {
                        servicePoint.showCustomerDetails(aData.customerId);
                    });
                    $('td:eq(0)', nRow).html(customerAnchor);

                    var anchor = $(document.createElement("a"));
                    anchor.html(aData.ticketNumber);
                    $(anchor).click(function() {
                        CaseServicePointService.getCaseByVisitId(aData.qpVisitId)
                            .done(function(dbCase){
                                servicePoint.populateVisitDetails(dbCase, null, '');
                            })
                    });

                    $('td:eq(1)', nRow).html(anchor);

                    $('td:eq(2)', nRow).html(moment(aData.created).format("M/D/YY hh:mm A"));
					
					$('td:eq(3)', nRow).css('max-width', '400px');
                }
            });

    };

    var getCaseAndPopulateVisitDetailsServicesTable = function(caseId){
        CaseServicePointService.getCase(caseId)
            .done(function(dbCase){
                tables.populateVisitDetailsServicesTable(dbCase);
            })
            .fail(function(err){
                util.showError('Could not get visit details from server.', err);
            })
    }


    /**************** Helper functions ******************/
    var allowMove = function(services, i, numOfPendingServices, directionUp) {

        if(numOfPendingServices < 2 || services[i].status !== 'PENDING') {
            return false;
        }

        var service = services[i];

        // If first service (and there's more than one)
        if(!directionUp && i == 0 && services.length > 1) {
            return true;
        }
        // If not first, and not last element and previous element is in state PENDING
        if(directionUp && i > 0 && services.length > i+1 && services[i-1].status == 'PENDING') {
            return true;
        }
        // If not first, and not last element and next element is in state PENDING
        if(!directionUp && i > 0 && services.length > i+1 && services[i+1].status == 'PENDING') {
            return true;
        }
        // Last one, more than one item, only if previous is in state PENDING
        if(directionUp && services.length > 1 && i == services.length-1 && services[i-1].status == 'PENDING') {
            return true;
        }
        return false;
    };

    var buildServicesServedCountStr = function(visit) {
        return (visit.numberOfServices-visit.numberOfUnservedServices) + '/' + visit.numberOfServices;
    };

};