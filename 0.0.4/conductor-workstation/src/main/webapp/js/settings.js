var settings = new function() {

	this.overriddenStatus = 'OVERRIDDEN';

    this.workstationOffline = true;

    this.initSettings = function() {
        if(!isLoggedInToWorkstation()) {
            $("#userName").html(sessvars.currentUser.fullName); //sessvars.currentUser.userName);
            settings.showSettingsWindow();
        } else {
            servicePoint.updateUI();
            servicePoint.initGUI();
			$("#branch-name-lbl").text(sessvars.branchName);
			if (servicePoint.isActive() == true) {
				settings.workstationOffline = false;
				sessvars.state = servicePoint.getState(sessvars.state);
				$("#servicepoint-name-lbl").text(sessvars.state.servicePointName);
			}
            $("#userName").html(sessvars.currentUser.fullName); //sessvars.currentUser.userName);
            events.initEvents();
        }

        //servicePoint.confirmSettings(false);
    };

    this.hasValidSettings = function(showMessages) {
		if(servicePoint.isActive()==true){
			if(typeof sessvars.branchId === "undefined" || sessvars.branchId == null) {
				if(showMessages) {
					util.showError("No Branch selected");
				}
				return false;
			}
			else if(typeof sessvars.servicePointId === "undefined" || sessvars.servicePointId == null) {
				if(showMessages) {
					util.showError("No Service Point selected");
				}
				return false;
			}
			else if(typeof sessvars.workProfileId === "undefined" || sessvars.workProfileId == null) {
				if(showMessages) {
					util.showError("No Work Profile selected");
				}
				return false;
			}
		}
		else {
			if(typeof sessvars.branchId === "undefined" || sessvars.branchId == null) {
				if(showMessages) {
					util.showError("No Branch selected");
				}
				return false;
			}
		}
       // return !settings.workstationOffline;
        return true;
    };

    /**
     * Disable 'settings' link
     */
    this.lockWorkstationSettings = function() {

    };

    //Update user name, workstation name, branch name and profile drop down box in the UI
    this.updateWorkstationSettings = function() {
        cServicePointService.getCurrentUser()
            .done(function(user){
                $("#userName").text(user.firstName + " " + user.lastName);
            })
            .fail(function(err){
                util.showError("Could not get current User from server.", err);
            })

//        var branch = $("#branch");
//        var workstation = $("#workstation");
//        var prioSel = $("#prioList");
//
//        showProfiles(sessvars.branchId, sessvars.servicePointId, prioSel);
//
//        branch.text(sessvars.branchName);
//        workstation.text(sessvars.servicePointName);
//
//        prioSel.prop('selectedIndex',
//            $("#prioList option[value=" + sessvars.workProfileId + "]").index());
//        prioSel.prop("disabled", false);
    };



    var isLoggedInToWorkstation = function() {
        var isLoggedIn = false;
        if(settings.hasValidSettings(false)) {
			isLoggedIn = true;
			if (servicePoint.isActive() == true) {
				isLoggedIn = !(sessvars.state.userState == servicePoint.userState.NO_STARTED_USER_SESSION);
			}
        }
        return isLoggedIn;
    };

	var applySettings = function() {
		if(servicePoint.isActive() == false) {
			$('#settingsWindow').modal('hide');
			servicePoint.initGUI();
			tables.refresh();
		   //TODO is this needed for reception?? events.initEvents();
		} else {
			settings.confirmSettings();
			// Check if OK to "start" GUI
			if(sessvars.state.servicePointState == servicePoint.servicePointState.OPEN) {
				servicePoint.initGUI();
				settings.workstationOffline = false;
				sessvars.state = servicePoint.getState(sessvars.state);
				events.initEvents();
				tables.refresh();
			}
		}
	};

    var populateWorkProfileSelect = function() {
		util.clearSelect($('#prioListModal'));
        cServicePointService.getWorkProfiles(sessvars.branchId)
            .done(function(workProfiles){
                $.map(workProfiles, function(workProfile) {
                    var opt = $("<option />").text(workProfile.name).val(workProfile.id);
                    if($(opt).val() == sessvars.workProfileId) {
                        $(opt).attr('selected','selected');
                    }
                    $('#prioListModal').append(opt);
                });
            })
            .fail(function(err){
                util.showError('Could not get workprofiles from server.', err);
            })


        $('#prioListModal').change(function() {
            sessvars.workProfileId =  $('#prioListModal').val();
        });
    };

    var populateServicePointsSelect = function() {
		//$('#selectWorkstationModal').show();
		//$('#workstationListModal').show();
		util.clearSelect($('#workstationListModal'));
        cServicePointService.getServicePointsByDeviceType(sessvars.branchId, "SW_SERVICE_POINT")
            .done(function(servicePoints){
                $.map(servicePoints, function(servicePoint) {
					var opt = $("<option />").text(servicePoint.name + (servicePoint.state == 'OPEN' ? ' (In Use)' : '')).val(servicePoint.id);
					if($(opt).val() == sessvars.servicePointId) {
						$(opt).attr('selected','selected');
					}
					$('#workstationListModal').append(opt);
                });
            })
            .fail(function(err){
                util.showError('Could not get servicePoints from server.', err);
            })
        $('#workstationListModal').change(function() {
            sessvars.servicePointId =  $('#workstationListModal').val();
        });
    };

    this.showSettingsWindow = function() {
		if (sessvars.state.userState == servicePoint.userState.CALLED || sessvars.state.userState == servicePoint.userState.SERVING || sessvars.isPostProcessing == true)
			return;
			
		$('#settingsWindow').modal(
			{"backdrop":"static",
				'show' : true,
				'keyboard' : false
			});
							   
		$('#settingsWindow').unbind().keyup(function(event){
			if(event.keyCode == 13){
				applySettings();
			}
		});
		
		$('#apply_settings_btn').unbind().click(function() {applySettings();});
		$('#cancel_settings_btn').unbind().click(function() {$('#settingsWindow').modal('hide');});
		
		$('#branchListModal').unbind().change(function() {
			sessvars.branchId = $('#branchListModal').val();
			sessvars.branchName = $('#branchListModal option:selected').text();
			populateServicePointsSelect();
			populateWorkProfileSelect();
        });
			
		util.clearSelect($('#branchListModal'));
		util.clearSelect($('#workstationListModal'));
		util.clearSelect($('#prioListModal'));
        if(servicePoint.isActive()==false){
            $('#prioListModal').hide();
            $('#selectProfileModal').hide();
			$('#workstationListModal').hide();
			$('#selectWorkstationModal').hide();
        }
		if(servicePoint.isActive() == true && settings.workstationOffline) {
            $('#cancel_settings_btn').attr('disabled','disabled').addClass('disabled');
        } 
		else {
            $('#cancel_settings_btn').removeAttr('disabled').removeClass('disabled');
        }
        CaseServicePointService.getBranches()
            .done(function(branches){
                $.map(branches, function(branch) {
					var opt = $("<option />").text(branch.name).val(branch.id);
					$('#branchListModal').append(opt);
                });
				//alert(JSON.stringify(branches, null, 4));
				//branches.splice(1, 1);
				
				if (typeof sessvars.branchId !== 'undefined' && sessvars.branchId != null && sessvars.branchId > 0) {
					if (branches.length == 1) {
						$('#branchListModal').attr('disabled','disabled').addClass('disabled');
					}
					$('#branchListModal').val(sessvars.branchId).change();
				}
				else {
					if(branches.length == 0) {
						util.showError('You do not have access to Conductor, you will not be able to use this module.', err);
						sessvars.branchId = 0;
						sessvars.branchName = '';
						return;
					}
					else if (branches.length == 1) {
						$('#branchListModal').attr('disabled','disabled').addClass('disabled');
						$('#branchListModal').val(branches[0].id).change();
					}
					else {
						$('#branchListModal').removeAttr('disabled').removeClass('disabled');
					}
				}
            })
            .fail(function(err){
                util.showError('Could not get Branches from server.', err);
            })
		
		$('#settingsWindow').modal('show');
    };

    this.confirmSettings = function(warnUser) {
        var isHijacking = false;
        var branchSel = $("#branchListModal");
        var workstationSel = $("#workstationListModal");
        var profileSel = $("#prioListModal");

        if(hasValidDropboxSettings(branchSel, workstationSel, profileSel)) {
            var settings = getSettings(branchSel, workstationSel, profileSel);
            if(typeof warnUser === "undefined") {
                warnUser = true;
            }
            isHijacking = confirm(warnUser, settings);
        }
        return isHijacking;
    };

    var getSettings = function(branchSel, workstationSel, profileSel) {
        var settings = {};
        settings.branchId = parseInt(branchSel.val());
        settings.branchName = branchSel.children("option").filter(":selected").text();

        settings.servicePointId = parseInt(workstationSel.val());
        settings.servicePointName = workstationSel.children("option").filter(":selected").text().split(' (In Use)')[0];

        settings.workProfileId = parseInt(profileSel.val());
        settings.profileName = profileSel.children("option").filter(":selected").text();
        return settings;
    };

    /*
     * Use sessvars stored settings to create the params object used by the RESTeasy
     * generated JavaScript connector code.
     */
    var hasValidDropboxSettings = function(branchSel, workstationSel, profileSel) {

        if(branchSel.val() == -1) {
            util.showError('Select a Branch');
            return false;
        }
        else if (workstationSel.val() == -1) {
            util.showError("Select a Service Point");
            return false;
        }
        else if (profileSel.val() == -1) {
            util.showError("Select a Profile");
            return false;
        }
        return true;
    };

    var confirm = function(warnUser, settings) {
        var isHijacking = false;
        var wantedWorkstation;
        cServicePointService.getServicePoint(settings)
            .done(function(workstation){
                wantedWorkstation = workstation;
            })
            .fail(function(err){
                util.showError('Could not get servicePoints from server.', err);
            })
        if(typeof wantedWorkstation !== 'undefined' && null != wantedWorkstation) {
            if(warnUser) {
                isHijacking = isHijack(wantedWorkstation, settings);
            }
            if(!isHijacking) {
              // unsubscribe to events for old unit id if changed to avoid being thrown out
                if(typeof sessvars.servicePointUnitId !== 'undefined' &&
                    null != sessvars.servicePointUnitId &&
                    sessvars.servicePointUnitId != wantedWorkstation.unitId) {
                    events.unsubscribeAndDisableQueues();
                }
                if(isApplied(settings)) {
                    servicePoint.storeSettingsInSession(settings);
                    servicePoint.setProfile(servicePoint.createParams());
                    servicePoint.updateUI();
                }
            }
        }
        return isHijacking;
    };

    var isApplied = function(settingsObject) {
        var isApplied = false;
        if(typeof sessvars.currentUser === 'undefined' || sessvars.currentUser == null) {
            cServicePointService.getCurrentUser()
                .done(function(user){
                    sessvars.currentUser = user;
                })
                .fail(function(err){
                    util.showError('Could not get current User from server.', err);
                })
        }
        if(typeof sessvars.currentUser !== 'undefined' && sessvars.currentUser.hasOwnProperty("userName")) {
            settingsObject.userName = sessvars.currentUser.fullName; //sessvars.currentUser.userName;

            //start user session. Set profile and update status if all went well
            if(startUserSession(settingsObject)) {
                $('#settingsWindow').modal('hide');
                //util.hideModal("settingsWindow");
                isApplied = true;
            }
        }
        return isApplied;
    };

    var startUserSession = function(params) {
        var isUserSessionStarted = false;
        try {
			CaseServicePointService.startUserServicePointSession(params)
				.done(function(userStatus) {
					if(userStatus.userState == servicePoint.userState.NO_STARTED_USER_SESSION) {
						util.showError('Error logging in...');
						isUserSessionStarted = false;
					} else {
						isUserSessionStarted = true;
						sessvars.state = userStatus;
						sessvars.statusUpdated = new Date();
						$("#branch-name-lbl").text(sessvars.branchName);
						$("#servicepoint-name-lbl").text(sessvars.state.servicePointName);
						isUserSessionStarted = true;
					}
				}).fail(function(msg) {
						servicePoint.getState(sessvars.state);
						sessvars.statusUpdated = new Date();
						if(msg.status == 409) {
							util.showErrorWithDuration(msg.responseText.substring(5), 10000); //params.servicePointName + " has an ongoing visit, cannot hijack service point.");
						} else {
							util.showError(msg.responseText);
						}
						isUserSessionStarted = false;
				});
        } catch(ex) {
            util.showError('Error no login'); //translate.msg('error.no.login', [ex]));
        }
        return isUserSessionStarted;
    };

    var isHijack = function(wantedWorkstation, params) {
        var isHijacking = false;
        if(null != wantedWorkstation.state && wantedWorkstation.state != servicePoint.servicePointState.CLOSED) {

            var currentUser;
            cServicePointService.getCurrentUser()
                .done(function(user){
                    currentUser = user;
                })
                .fail(function(err){
                    util.showError('Could not get current User from server.', err);
                })

            var usersOnWantedServicePoint = {};
            cServicePointService.getUsersOnServicePoint(params.branchId, params.servicePointId)
                .done(function(users){
                    usersOnWantedServicePoint = users;
                })
                .fail(function(err){
                    util.showError('Could not get users on this ServicePoint, from server.', err);
                })

            if(usersOnWantedServicePoint != null && usersOnWantedServicePoint.length > 0 && usersOnWantedServicePoint[0].id != currentUser.id) {
					//the user wants to login to an occupied counter; display warning message.
					$('#settingsWindow').modal('hide');
					$('#confirmCounterHijackingWindow').modal(
						{"backdrop":"static",
							'show' : true,
							'keyboard' : false
						});
					$('#confirmCounterHijackingWindow').modal('show');
					$('#closeCounterHijackingBtn').unbind().click(
						function() {
							$('#confirmCounterHijackingWindow').modal('hide');
							$('#settingsWindow').modal('show');
						}
					);
					$('#cancelCounterHijackingBtn').unbind().click(
						function() {
							$('#confirmCounterHijackingWindow').modal('hide');
							$('#settingsWindow').modal('show');
						}
					);
					$('#confirmCounterHijackingBtn').unbind().click(
						function() {
							$('#confirmCounterHijackingWindow').modal('hide');
							settings.confirmSettings(false);
							if(sessvars.state.servicePointState == servicePoint.servicePointState.OPEN && sessvars.state.userState == servicePoint.userState.INACTIVE) {
								servicePoint.initGUI();
								settings.workstationOffline = false;
								sessvars.state = servicePoint.getState(sessvars.state);
								events.initEvents();
								tables.refresh();
							}
							else {
								sessvars.$.clearMem();
								settings.workstationOffline = true;
								$('#confirmCounterHijackingWindow').modal('hide');
								$('#settingsWindow').modal('show');
								util.showError("Cannot Login, this service point might be serving a customer");
							}
						}
					);
					//i18n for counter hijacking confirmation window
					$(document).ready(function() {
						document.getElementById("counterHijackingMessage").innerHTML = params.servicePointName + " is currently in use";
						document.getElementById("confirmCounterHijackingBtn").innerHTML = "Proceed Login, " + usersOnWantedServicePoint[0].fullName.toUpperCase() + " will be logged out";
					});
					isHijacking = true;
            }
        }
        return isHijacking;
    };


    this.setWorkstationOffline = function() {
        if (!settings.workstationOffline) {
            var err = jQuery.i18n.prop('error.communication_error');
            util.showPermanentError(err);
            settings.workstationOffline = true;
            // clear any ongoing vist
            // servicePoint.clearOngoingVisit();
            // stop refreshing queues and clear queue list
            clearTimeout(sessvars.queueTimer);
            sessvars.queueTimerOn = false;
			clearTimeout(sessvars.servicepointsTimer);
            sessvars.servicepointsTimerOn = false;
            tables.emptyQueues();
            //servicePointPool.emptyPool();
            tables.emptyUserPool();
            // disable call buttons
            $('#callButtons a').each(function() {
                $(this).addClass("customLinkDisabled").click( function(e) {
                    e.preventDefault();
                });
                $(this).attr({
                    "class" : "customButtonDisabled",
                    "disabled" : true
                });
            });
            // disable visit buttons
            $('#ongoingVisitButtons a').each(function() {
                $(this).addClass("customLinkDisabled").click( function(e) {
                    e.preventDefault();
                });
                $(this).attr({
                    "class" : "customButtonSmallDisabled",
                    "disabled" : true
                });
            });
            // disable home/settings/logout links
            $('.orch-userinfo a').each(function() {
                $(this).prop("disabled", true);
                $(this).toggleClass("linkDisabled", true);
            });
            $('.orch-actions a').each(function() {
                $(this).prop("disabled", true);
                $(this).toggleClass("imgDisabled", true);
            });

//            document.getElementById("createCustomerLink").className = "newCust customLinkDisabled";
//            document.getElementById("createCustomerLink").disabled = true;
//            document.getElementById("editCustomerLink").className = "editCust customLinkDisabled";
//            document.getElementById("editCustomerLink").disabled = true;
//            document.getElementById("linkCustomerLink").className = "linkCust customLinkDisabled";
//            document.getElementById("linkCustomerLink").disabled = true;
//            document.getElementById("deleteCustomerLink").className = "deleteCust customLinkDisabled";
//            document.getElementById("deleteCustomerLink").disabled = true;
            // disable settings / dropdown in menu
            $('#prioList').prop("disabled", true);
        }
    };

    this.setWorkstationOnline = function() {
        if (settings.workstationOffline) {
            util.hideError();
            settings.workstationOffline = false;
            // try one resource, if we get anything but a HTTP code 200-399 here redirect to the login screen
            // this small check is needed in the case our network drops towards orchestra, but our session is still valid
            var div = $("<div/>");
            div.load("css/style.css?breakcache=" + Math.random(), function(response, status, xhr) {
                if (xhr.status > 399 || xhr.status < 200 || xhr.status == 302) {
                    // authentication error, send user to login/start screen...probably not going to end up here
                    // form authentication means browsers will handle the 302 transparently
                    sessvars.$.clearMem();
                    window.location.replace("/login.jsp");
                } else {
                    // this is a huge hack to work around the fact that web browsers handling redirect transparently
                    var isRedirect = false;
                    try {
                        $.each($(xhr.responseText).filter(function(){return this.nodeType == 8;}),
                            function(index, element) {
                                if(typeof element.textContent !== "undefined" && element.textContent != null &&
                                    element.textContent.trim() == "Fixed navbar") {
                                    // found comment in login page, note that if that comment is removed, this hack won't work anymore
                                    isRedirect = true;
                                }
                            }
                        );
                    } catch(ex) {
                        // parsing failed, not a redirect
                    }
                    if(!isRedirect) {
                        servicePoint.updateUI();
                    } else {
                        // not authenticated any longer, send user to login page
                        sessvars.$.clearMem();
                        window.location.replace("/login.jsp");
                    }
                }
            });
        }
    };


    this.getWorkstationOffline = function() {
        return settings.workstationOffline;
    };

};