var appointments = new function() {
	
	this.initView = function() {
		var columns = _createTableColumns();
		_createTable(columns);
		appointments.updateView();
	}
	
	this.updateView = function() {
		CaseAppointmentService.getAppointments(sessvars.branchId)
	    .done(function(result){
	    	$("#appointments-today-table").dataTable().fnClearTable();
	        $.map(result, function(appointments) {
	            $("#appointments-today-table").dataTable().fnAddData([appointments]);
	        });
	    })
	    .fail(function(error){
	        util.showMessage("Failed to retrieve appointments.", error);
	    });
	}

	_createTableColumns = function() {
		var columns = [
			{"bSearchable": false,
               "bVisible": true,
               "mDataProp": "id"},
           {"bSearchable": false,
               "bVisible": true,
               "mDataProp": "startTime"},
           {"bSearchable": false,
               "bVisible": true,
               "mDataProp": "endTime"},
           {"bSearchable": false,
        	   "bVisible": true,
        	   "mDataProp": "customers"},
           {"bSearchable": false,
               "bVisible": true,
               "mDataProp": "services"},
           {"bSearchable": false,
               "bVisible": true,
               "mDataProp": "status"}
       ];
		return columns;
	}
	
	_createTable = function(columns) {
		$("#appointments-today-table").dataTable({
			"aoColumns":columns,
	        "bPaginate": false,
	        "bLengthChange": false,
	        "iDisplayLength ": false,
	        "bFilter": false,
	        "bInfo": false,
	        "bSort": true,
	        "fnRowCallback" : function(nRow, aData, iDisplayIndex) {
				var idColumn = $("td:eq(0)", nRow);
	        	var startTimeColumn = $("td:eq(1)", nRow);
	        	var stopTimeColumn = $("td:eq(2)", nRow);
	        	var customerColumn = $("td:eq(3)", nRow);
	        	var servicesColumn = $("td:eq(4)", nRow);
	        	var actionColumn = $("td:eq(5)", nRow);
	        	
				idColumn.empty();
	        	idColumn.append(aData.id);
				
	        	startTimeColumn.empty();
	        	startTimeColumn.append(util.timeOfDate(aData.startTime));
	        	
				stopTimeColumn.empty();
				stopTimeColumn.append(util.timeOfDate(aData.endTime));

				customerColumn.empty();
	        	for(i=0; i < aData.customers.length; i++) {
	        		customerColumn.append(aData.customers[i].id + " - " + aData.customers[i].firstName + " " + aData.customers[i].lastName);
	        		customerColumn.append("<br />");
	        	}

	        	servicesColumn.empty();
	        	for(i=0; i < aData.services.length; i++) {
	        		servicesColumn.append(aData.services[i].name);
	        		servicesColumn.append("<br />");
	        	}
	        	
	        	actionColumn.empty();
	        	if(aData.status == 'CREATED') {
	        		var arriveButton = $("<button></button>");
	        		arriveButton.addClass("btn btn-mini btn-primary");
	        		arriveButton.html("Arrived");
	        		arriveButton.click(function() {
	        			arriveButton.addClass("hidden");
	        			actionColumn.append("<img src='images/ajax-loader.gif' class='loading' />");
	        			_updateAppointmentToArrived(aData.id, aData.customers);
	        		});
	        		actionColumn.append(arriveButton);
	        	} else {
	        		actionColumn.append(aData.status);
	        	}
	        }
		});
	}
	
	_updateAppointmentToArrived = function(appointmentId, customers) {
		sessvars.entryPointId = $('#entrypoint_select').val();
		var customerId = null;
		var customerName = null;
		if(customers.length > 0) {
			customerId = customers[0].id;
			customerName = customers[0].firstName + " " + customers[0].lastName;
		}
		CaseAppointmentService.updateAppointmentToArrived(appointmentId, sessvars.branchId, sessvars.entryPointId, customerId, customerName)
		.done(function(result) {
			appointments.updateView();
			util.showMessage("Visit created for appointment");
		})
		.fail(function(error) {
	    	$("#appointments-today-table button").removeClass("hidden");
	    	$("#appointments-today-table img.loading").remove();
			util.showMessage("Failed to create visit for appointment.", error);
		});
	}
	
}

