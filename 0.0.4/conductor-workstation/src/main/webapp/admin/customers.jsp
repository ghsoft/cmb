<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Conductor Customers</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../css/dataTable.css"/>
    <link rel="stylesheet" type="text/css" href="../css/conductor.css"/>
</head>

<body>
	<div class="navbar navbar-fixed-top">
	    <div class="navbar-inner">
	        <div class="container-fluid">
	            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </a>
	            <a class="brand" href="#">Conductor Customers</a>
				<div class="nav-collapse collapse">
	                <p class="navbar-text pull-right">
                        <a class="icon-home-black" href="/"></a>
                    </p>
	            </div>
	        </div>
	    </div>
	</div>
	
	<div class="container-fluid">
		<div class="row-fluid">
			<table id="merge-customers-table" class="table">
				<tr>
					<td colspan="2">
						<div style="text-align: center; padding: 10px;">
							<button type="button" id="merge-customers-btn" class="btn btn-big btn-primary">Merge Customers</button>
						</div>
					</td>
				</tr>
				<tr>
					<!-- Source Customer -->
					<td width="50%">
						<div class="row-fluid content-wrapper theme4">
							<div class="content-toolbar">Source Customer</div>
							<div class="content">
								<div class="row-fluid">
									<div class="span4">
										<span id="admin_source_create_customer_btn" style="cursor: pointer; font-size: 16px; margin-left: 20px;"><i class="icon-add" style="margin-top: 1px;" title="Create customer"></i> Create Customer</span>
									</div>
									<div class="span8">
										<input type="text" class="input-medium search-query" style="border-radius: 2px" id="admin_source_cust_search_input_fld"/>
										<button type="button" id="admin_source_search_customer_btn" class="btn btn-small"><i class="icon-search"></i> Find Customer</button>
									</div>
								</div>
							</div>

							<!-- Customer Search Result -->
							<div id="admin_source_customer_search_results_container" class="hidden">
								<div class="content-toolbar mini">Search results</div>
								<div class="content">
									<table class="table table-hover table-striped" style="width:100%;">
										<thead>
											<tr>
												<th>ID</th>
												<th>Customer name</th>
												<th>Mobile Phone</th>
												<th>Email</th>
											</tr>
										</thead>
										<tbody id="admin_source_customer_search_results"></tbody>
									</table>
								</div>
							</div>
										
							<!-- Customer Details -->
							<div id="admin_source_selected_customer_panel" class="hidden">
								<div class="content-toolbar mini">Customer Details</div>
								<div class="content">
									<div class="row-fluid">
										<div class="span2"><strong>ID</strong></div>
										<div class="span4" id="admin_source_customer_id_lbl"></div>
									</div>
									<div class="row-fluid">
										<div class="span2"><strong>First name</strong></div>
										<div class="span4" id="admin_source_customer_first_name_lbl"></div>
									</div>
									<div class="row-fluid">
										<div class="span2"><strong>Last name</strong></div>
										<div class="span4" id="admin_source_customer_last_name_lbl"></div>
									</div>
									<div class="row-fluid">
										<div class="span2"><strong>Mobile Phone</strong></div>
										<div class="span4" id="admin_source_customer_mobile_lbl"></div>
									</div>
									<div class="row-fluid">
										<div class="span2"><strong>Email</strong></div>
										<div class="span4" id="admin_source_customer_email_lbl"></div>
										<div class="span6 text-right">
											<span id="admin_source_selected-customer-edit-btn" style="cursor: pointer;"><i class="icon-edit-user"></i> Edit customer</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</td>
					<!-- Target Customer -->
					<td width="50%">
						<div class="row-fluid content-wrapper theme2">
							<div class="content-toolbar">Target Customer</div>
							<div class="content">
								<div class="row-fluid">
									<div class="span4">
										<span id="admin_target_create_customer_btn" style="cursor: pointer; font-size: 16px; margin-left: 20px;"><i class="icon-add" style="margin-top: 1px;" title="Create customer"></i> Create Customer</span>
									</div>
									<div class="span8">
										<input type="text" class="input-medium search-query" style="border-radius: 2px" id="admin_target_cust_search_input_fld"/>
										<button type="button" id="admin_target_search_customer_btn" class="btn btn-small"><i class="icon-search"></i> Find Customer</button>
									</div>
								</div>
							</div>

							<!-- Customer Search Result -->
							<div id="admin_target_customer_search_results_container" class="hidden">
								<div class="content-toolbar mini">Search results</div>
								<div class="content">
									<table class="table table-hover table-striped" style="width:100%;">
										<thead>
											<tr>
												<th>ID</th>
												<th>Customer name</th>
												<th>Mobile Phone</th>
												<th>Email</th>
											</tr>
										</thead>
										<tbody id="admin_target_customer_search_results"></tbody>
									</table>
								</div>
							</div>
										
							<!-- Customer Details -->
							<div id="admin_target_selected_customer_panel" class="hidden">
								<div class="content-toolbar mini">Customer Details</div>
								<div class="content">
									<div class="row-fluid">
										<div class="span2"><strong>ID</strong></div>
										<div class="span4" id="admin_target_customer_id_lbl"></div>
									</div>
									<div class="row-fluid">
										<div class="span2"><strong>First name</strong></div>
										<div class="span4" id="admin_target_customer_first_name_lbl"></div>
									</div>
									<div class="row-fluid">
										<div class="span2"><strong>Last name</strong></div>
										<div class="span4" id="admin_target_customer_last_name_lbl"></div>
									</div>
									<div class="row-fluid">
										<div class="span2"><strong>Mobile Phone</strong></div>
										<div class="span4" id="admin_target_customer_mobile_lbl"></div>
									</div>
									<div class="row-fluid">
										<div class="span2"><strong>Email</strong></div>
										<div class="span4" id="admin_target_customer_email_lbl"></div>
										<div class="span6 text-right">
											<span id="admin_target_selected-customer-edit-btn" style="cursor: pointer;"><i class="icon-edit-user"></i> Edit customer</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>

	<footer class="navbar-inner" id="footer-customers-admin"></footer>
	
	<!-- Modals -->
	<jsp:include page="dialogs/edit-customer-dialog.html" />
	
	<div id="message-main" class="message-main fade right in" style="width:400px; display: none;">
	    <div class="arrow"></div>
	    <div id="message-title-heading" class="message-title heading"><i id="message-dismiss-btn" class="icon-remove"></i><span id="message-title">Title here</span></div>
	    <div id="message-content" class="message-content">Text here</div>
	</div>

	<!-- Scripts -->
	<script type="text/javascript" src="../js/jquery/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../js/bootstrap/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap/bootstrapx-clickover.js"></script>
	<script type="text/javascript" src="../js/util.js"></script>
	<script type="text/javascript" src="../js/restservices/CaseServicePointService.js"></script>
	<script type="text/javascript" src="../js/restservices/ServicePointService.js"></script>
	<script type="text/javascript" src="../js/workstation.js"></script>
	<script type="text/javascript" src="../js/sessvars.js"></script>
	<script type="text/javascript" src="js/sourceCustomer.js"></script>
	<script type="text/javascript" src="js/targetCustomer.js"></script>
	<script type="text/javascript" src="js/commonCustomer.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			sourceCustomer.initView();
			targetCustomer.initView();
			commonCustomer.initView();
		});
	</script>

</body>

</html>