var commonCustomer = new function() {
	
	this.initView = function() {
		initSessvars();
		
		var conductorBranchFound = false;
			
		$('#merge-customers-btn').unbind().click(function() {
            if ($('#admin_source_customer_id_lbl').text().length == 0 || $('#admin_target_customer_id_lbl').text().length == 0) {
				util.showError("Please select two customers to merge their records");
				return;
			}
			
			if ($('#admin_source_customer_id_lbl').text() == $('#admin_target_customer_id_lbl').text()) {
				util.showError("The selected customers must be different");
				return;
			}
			var sourceCustomerId = $('#admin_source_customer_id_lbl').text();
			var targetCustomerId = $('#admin_target_customer_id_lbl').text();
			CaseServicePointService.mergeCustomersAndUpdateAllCases(sourceCustomerId, targetCustomerId)
				.done(function(casesCount){
					if(casesCount) {
						$('#admin_target_selected_customer_panel').addClass('hidden');
						$('#admin_source_selected_customer_panel').addClass('hidden');
						util.showMessage('The records of the two customers have been merged, (' + casesCount.cases + ') Cases(s) were updated');
					}
				})
				.fail(function(err){
					util.showError("Something went wrong when merging customer.", err);
				})
			$('#admin_source_cust_search_input_fld').val('');
			$('#admin_target_cust_search_input_fld').val('');
		});
	}
	
	var initSessvars = function () {
	
		cServicePointService.getCurrentUser()
			.done(function(user){
				sessvars.currentUser = user;
			})
			.fail(function(err){
				util.showError('Could not get current User from server.', err);
			})

        cServicePointService.getUserStatus()
            .done(function(userState){
                sessvars.state = userState;
            })
            .fail(function(err){
                util.showError("Could not get User status from server.", err);
            })
		
        sessvars.statusUpdated = new Date();
		if (typeof sessvars.state !== 'undefined' && sessvars.state != null &&
			typeof sessvars.state.branchId !== "undefined" && sessvars.state.branchId != null) {
			servicePoint.storeSettingsInSession(sessvars.state);
		} else {
			// something is not valid, everything is invalid. Scenario: New configuration has been published
			resetSettings();
		}
    }
	
	var resetSettings = function() {
        sessvars.branchId = null;
        sessvars.branchName = null;
        sessvars.servicePointId = null;
        sessvars.servicePointUnitId = null;
        sessvars.servicePointName = null;
        sessvars.workProfileId = null;
        sessvars.profileName = null;
        sessvars.singleSettingsOnly = null;
    };
	
}

