package com.qmatic.ps.bp.common;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-02-05
 * Time: 18:19
 * To change this template use File | Settings | File Templates.
 */
public class BPException extends Exception {

    private final int code;

    public BPException(String message, int code) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
