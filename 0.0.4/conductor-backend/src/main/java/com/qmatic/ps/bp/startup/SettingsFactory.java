package com.qmatic.ps.bp.startup;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.common.BPExceptionCode;
import com.qmatic.ps.bp.common.Constants;
import com.qmatic.ps.bp.dao.SettingDao;
import com.qmatic.ps.bp.model.Setting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-05
 * Time: 10:06
 * To change this template use File | Settings | File Templates.
 */
@Component
public class SettingsFactory {

    @Autowired
    SettingDao settingDao;


    public Setting createOrchestraSettings() throws BPException {
        Setting orchestraSettings = settingDao.findByGroup(Constants.ORCHESTRA_SETTINGS_GROUP);

        if(orchestraSettings == null) {
            // Create base object
            orchestraSettings = new Setting();
            orchestraSettings.setGroup(Constants.ORCHESTRA_SETTINGS_GROUP);
            orchestraSettings.setLabel("Orchestra"); // TODO translate?
            HashMap<String,String> credentials = new HashMap<>();
            credentials.put(Constants.ORCHESTRA_URL_KEY, "http://localhost:8080/qsystem/rest/appointment");
            credentials.put(Constants.ORCHESTRA_USERNAME_KEY, "Calendar");
            credentials.put(Constants.ORCHESTRA_PASSWORD_KEY, "Calendar1");
            orchestraSettingToJson(orchestraSettings, credentials);

            settingDao.create(orchestraSettings);
        }
        return orchestraSettings;
    }

    private void orchestraSettingToJson(Setting orchestraSettings, HashMap<String, String> credentials) throws BPException {
        ObjectMapper om = new ObjectMapper();
        try {
            orchestraSettings.setData(om.writeValueAsString(credentials));
        } catch (JsonProcessingException e) {
            throw new BPException("Could not create JSON object from credentials object: " + e.getMessage(), BPExceptionCode.SERIALIZATION_ERROR);
        }
    }

}
