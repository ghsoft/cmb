package com.qmatic.ps.bp.push;

import org.cometd.bayeux.server.BayeuxServer;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import javax.servlet.*;
import java.io.IOException;

/**
 * This Servlet sets the BayeuxServer on the CometDServer and then
 * "registers" the CometDServer on the Spring-singleton {@link CometDServerRegistry}
 *
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-01-19
 * Time: 12:08
 * To change this template use File | Settings | File Templates.
 */
public class Initializer extends GenericServlet {

    public static final String COMET_D_SERVER_REGISTRY_BEAN_NAME = "cometDServerRegistry";

    @Override
    public void init() throws ServletException {
        // Retrieve the CometD service instantiated by AnnotationCometdServlet
        CometDServer service = (CometDServer) getServletContext().getAttribute(CometDServer.class.getName());
        BayeuxServer bayeuxServer = (BayeuxServer) getServletContext().getAttribute(BayeuxServer.ATTRIBUTE);
        service.setBayeuxServer(bayeuxServer);

        // Get the CometDServerRegistry using programmatic spring, the set the CometDServer to the Registry so we
        // can access it using Spring.
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        CometDServerRegistry cometDServerRegistry = (CometDServerRegistry) ctx.getBean(COMET_D_SERVER_REGISTRY_BEAN_NAME);
        cometDServerRegistry.setCometDServer(service);
    }

    @Override
    public void destroy() {

    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        throw new UnavailableException("Initializer");
    }

}
