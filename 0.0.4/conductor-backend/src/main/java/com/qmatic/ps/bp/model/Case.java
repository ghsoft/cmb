package com.qmatic.ps.bp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qmatic.ps.bp.model.enumeration.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-23
 * Time: 15:45
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "cases")
@Cacheable
@XmlRootElement
public class Case implements Serializable {

    private Long id;
    private Long qpVisitId;
    private Integer branchId;
    private String branchName;
    private String ticketNumber;
    private Long entryPointId;
    private String entryPointName;

    private List<CaseService> caseServices = new ArrayList<>();

    private Date created;
    private Date updated;

    private CaseNote currentNote;
	
	private CaseCustomer caseCustomer;

    /**
     * Internal primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "conductor_seq_table")
    @TableGenerator(name = "conductor_seq_table", table = "id_gen", pkColumnName = "id_name", valueColumnName = "id_val", pkColumnValue = "cases", allocationSize = 1)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="qp_visit_id", nullable = false)
    public Long getQpVisitId() {
        return qpVisitId;
    }

    public void setQpVisitId(Long qpVisitId) {
        this.qpVisitId = qpVisitId;
    }

    @Column(name="branch_id")
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }
	
	@Column(name="branch_name")
    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    public CaseCustomer getCaseCustomer() {
        return caseCustomer;
    }

    public void setCaseCustomer(CaseCustomer caseCustomer) {
        this.caseCustomer = caseCustomer;
    }

    @Column(name="ticket_number")
    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    @Column(name="entry_point_id")
    public Long getEntryPointId() {
        return entryPointId;
    }

    public void setEntryPointId(Long entryPointId) {
        this.entryPointId = entryPointId;
    }
	
	@Column(name="entry_point_name")
    public String getEntryPointName() {
        return entryPointName;
    }

    public void setEntryPointName(String entryPointName) {
        this.entryPointName = entryPointName;
    }

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "parent_case_id")
    @OrderBy(value = "idx")
    public List<CaseService> getCaseServices() {
        return caseServices;
    }

    public void setCaseServices(List<CaseService> caseServices) {
        this.caseServices = caseServices;
    }

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name="created")
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name="updated")
    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@JoinColumn(name = "current_note_id")
    public CaseNote getCurrentNote() {
        return currentNote;
    }

    public void setCurrentNote(CaseNote currentNote) {
        this.currentNote = currentNote;
    }

    @PrePersist
    void onCreate() {
        this.setCreated(new Date());
    }

    @PreUpdate
    void onUpdate() {
        this.setUpdated(new Date());
    }

    @Transient
    public CaseService getOngoingService() {
        for(CaseService cs : caseServices) {
            if(cs.getStatus() == Status.SERVING || cs.getStatus() == Status.POST_PROCESSING) {
                return cs;
            }
        }
        return null;
    }

    /**
     * Maybe over-complex. Returns the overall status of the Case based on the states of
     * the participating CaseService subobjects.
     *
     * @return
     */
	/* 
    @Transient
    public CaseStatus getCaseStatus() {
        boolean allFinished = true;
        boolean isOnGoing = false;
        boolean notStarted = true;
        boolean isRemoved = true;
        for(CaseService cs : caseServices) {
            if(!(cs.getStatus() == Status.FINISHED || cs.getStatus() == Status.TRASH)) {
                allFinished = false;
            }
            if(cs.getStatus() != Status.REMOVED) {
                isRemoved = false;
            }
            if(cs.getStatus() == Status.SERVING || cs.getStatus() == Status.POST_PROCESSING) {
                isOnGoing = true;
            }
            if(cs.getStatus() != Status.PENDING && cs.getStatus() != Status.WAITING || cs.getStatus() == Status.RECYCLED) {
                notStarted = false;
            }
        }
        if(allFinished) return CaseStatus.FINISHED;
        if(isRemoved) return CaseStatus.REMOVED;
        if(notStarted) return CaseStatus.NOT_STARTED;
        if(isOnGoing) return CaseStatus.ONGOING;

        return CaseStatus.UNDEFINED;
    } */

    @Transient
    public boolean getInTrash() {
        for(CaseService cs : caseServices) {
            if(cs.getStatus() == Status.TRASH) {
                return true;
            }
        }
        return false;
    }
	
	@Transient
    public boolean getInHold() {
        for(CaseService cs : caseServices) {
            if(cs.getStatus() == Status.ONHOLD) {
                return true;
            }
        }
        return false;
    }
	
	@Transient
    public boolean getInNoShow() {
        for(CaseService cs : caseServices) {
            if(cs.getStatus() == Status.NOSHOW) {
                return true;
            }
        }
        return false;
    }

    /**
     * Ugly iterations-based way to find the newest note from the participating CaseServices.
     * @return
     */
    @Transient
    public CaseNote getLastNote() {
//        Note newest = null;
//        for(CaseService cs : caseServices) {
//            for(Note note : cs.getNotes()) {
//                if(newest == null || newest.getCreated().compareTo(note.getCreated()) < 0) {
//                    newest = note;
//                }
//            }
//        }
//        return newest;
        if(currentNote == null) {
            return null;
        } else {
            return currentNote;
        }

    }

    /**
     * Convenience method, usable for determining whether to show arrows, delete marks etc in the GUI.
     * @return
     */
    @Transient
    public int getNumberOfPendingServices() {
        int cnt = 0;
        for(CaseService cs : caseServices) {
            if(cs.getStatus() == Status.PENDING) {
                cnt++;
            }
        }
        return cnt;
    }

    /**
     * Convenience method, usable for easy display of 3/5 style things in the GUI.
     * @return
     */
    @Transient
    public Integer getNumberOfUnservedServices() {
        int cnt = 0;
        for(CaseService cs : caseServices) {
            if(cs.getStatus() == Status.FINISHED) {
                cnt++;
            }
        }
        return caseServices.size() - cnt;
    }

}
