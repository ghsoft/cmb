package com.qmatic.ps.bp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qmatic.ps.bp.model.enumeration.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-23
 * Time: 15:48
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "case_services")
@Cacheable
@XmlRootElement
public class CaseService implements Serializable {

    private Long id;
    private Integer serviceId;
    private Long qpVisitServiceId;
    private String serviceName;
    private Integer recyclesLeft = 0;
    private Integer waitingTime;
    private Integer servingTime;
    private Integer postProcessingTime;
    private Integer servedByCounterId;
	private String servedByCounterName;
    private String servedByUserName;
	private String servedByUserId;
	private Boolean vip;

    /** Hand-crafted order index */
    private Integer idx;

    private Status status;

	//private Long parentCaseId;
    private Case parentCase;
    private List<Note> notes = new ArrayList<>();
    private List<CaseServiceMark> caseServiceMarks = new ArrayList<>();

    private Date created;
    private Date updated;

    private Date servingStart;
    private Date postProcessingStart;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "conductor_seq_table")
    @TableGenerator(name = "conductor_seq_table", table = "id_gen", pkColumnName = "id_name", valueColumnName = "id_val", pkColumnValue = "case_services", allocationSize = 1)

    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="service_id", nullable = false)
    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    @Column(name="qp_visit_service_id", nullable = false)
    public Long getQpVisitServiceId() {
        return qpVisitServiceId;
    }

    public void setQpVisitServiceId(Long qpVisitServiceId) {
        this.qpVisitServiceId = qpVisitServiceId;
    }

    @Column(name="service_name")
    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    @Column(name="recycles_left")
    public Integer getRecyclesLeft() {
        return recyclesLeft;
    }

    public void setRecyclesLeft(Integer recycleCount) {
        this.recyclesLeft = recycleCount;
    }

    @Column(name="waiting_time")
    public Integer getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(Integer waitingTime) {
        this.waitingTime = waitingTime;
    }
	
	@Column(name="serving_time")
    public Integer getServingTime() {
        return servingTime;
    }

    public void setServingTime(Integer servingTime) {
        this.servingTime = servingTime;
    }

    @Column(name="post_processing_time")
    public Integer getPostProcessingTime() {
        return postProcessingTime;
    }

    public void setPostProcessingTime(Integer postProcessingTime) {
        this.postProcessingTime = postProcessingTime;
    }

    @Column(name="served_by_counter_id")
    public Integer getServedByCounterId() {
        return servedByCounterId;
    }

    public void setServedByCounterId(Integer servedByCounterId) {
        this.servedByCounterId = servedByCounterId;
    }
	
	@Column(name="served_by_counter_name")
    public String getServedByCounterName() {
        return servedByCounterName;
    }

    public void setServedByCounterName(String servedByCounterName) {
        this.servedByCounterName = servedByCounterName;
    }
	
	@Column(name="served_by_user_id")
    public String getServedByUserId() {
        return servedByUserId;
    }

    public void setServedByUserId(String servedByUserId) {
        this.servedByUserId = servedByUserId;
    }

	@Column(name="served_by_user_name")
    public String getServedByUserName() {
        return servedByUserName;
    }

    public void setServedByUserName(String servedByUserName) {
        this.servedByUserName = servedByUserName;
    }
	
	@Column(name="vip")
    public Boolean getVip() {
        return vip;
    }

    public void setVip(Boolean vip) {
        this.vip = vip;
    }

    @Enumerated(EnumType.STRING)
    @Column(name="status")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "caseservice_id")
    @OrderBy("created DESC")
    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }
	
	/* @JsonIgnore
	@Column(name="parent_case_id", insertable=false, updatable=false)
	public Long getParentCaseId() {
        return parentCaseId;
    }
	
	public void setParentCaseId(Long parentCaseId) {
        this.parentCaseId = parentCaseId;
    } */

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "parent_case_id")
    public Case getParentCase() {
        return parentCase;
    }

    public void setParentCase(Case parentCase) {
        this.parentCase = parentCase;
    }

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "caseservice_id")
    public List<CaseServiceMark> getCaseServiceMarks() {
        return caseServiceMarks;
    }

    public void setCaseServiceMarks(List<CaseServiceMark> caseServiceMarks) {
        this.caseServiceMarks = caseServiceMarks;
    }

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name="created")
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name="updated")
    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="post_processing_start")
    public Date getPostProcessingStart() {
        return postProcessingStart;
    }

    public void setPostProcessingStart(Date postProcessingStart) {
        this.postProcessingStart = postProcessingStart;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="serving_start")
    public Date getServingStart() {
        return servingStart;
    }

    public void setServingStart(Date servingStart) {
        this.servingStart = servingStart;
    }

    @Column(name ="order_idx", nullable = false, insertable = true, updatable = true)
    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    @PrePersist
    void onCreate() {
        this.setCreated(new Date());
    }

    @PreUpdate
    void onUpdate() {
        this.setUpdated(new Date());

        // Also, update the update or the parent case.
        this.getParentCase().setUpdated(new Date());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CaseService that = (CaseService) o;

        if (!id.equals(that.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Transient
    public Long getQpVisitId() {
        return this.parentCase.getQpVisitId();
    }

    @Transient
    public String getTicketNumber() {
        return this.parentCase.getTicketNumber();
    }

    @Transient
    public String getCustomerName() {
        return this.parentCase.getCaseCustomer().getFullName();
    }
	
	@Transient
    public Long getCustomerId() {
        return this.parentCase.getCaseCustomer().getId();
    }
	
	@Transient
    public Long getCaseId() {
        return this.parentCase.getId();
    }
}
