package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.common.BPExceptionCode;
import com.qmatic.ps.bp.model.Case;
import com.qmatic.ps.bp.model.enumeration.Status;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Date;

import org.joda.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class CaseJpaDao extends JpaDao<Case> implements CaseDao {

	@Override
    public Case getCase(Integer branchId, Long caseId) throws BPException {
        List<Case> cases = entityManager.createQuery("SELECT c FROM Case c WHERE c.branchId = :branchId AND c.id = :caseId ORDER BY c.created DESC")
				.setParameter("branchId", branchId)
                .setParameter("caseId", caseId)
                .getResultList();
        if(cases.size() == 0) {
            return null;
        } else {
            return cases.get(0);
        }
    }
	
	@Override
    public Case getCaseByVisitId(Integer branchId, Long qpVisitId) throws BPException {
        List<Case> cases = entityManager.createQuery("SELECT c FROM Case c WHERE c.branchId = :branchId AND c.qpVisitId = :qpVisitId ORDER BY c.created DESC")
				.setParameter("branchId", branchId)
                .setParameter("qpVisitId", qpVisitId)
                .getResultList();
        if(cases.size() == 0) {
            return null;
        } else {
            return cases.get(0);
        }
    }

    @Override
    public List<Case> searchCases(Integer branchId, String searchArg, Map<String, List<String>> params) throws BPException {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.MINUTE,0);
        Timestamp startOfToday = new Timestamp(cal.getTime().getTime());

        TypedQuery <Case> query = entityManager.createQuery("SELECT DISTINCT(c) FROM Case c join c.caseCustomer cc WHERE c.branchId = :branchId AND c.created > :startOfToday AND (lower(cc.firstName) like :arg OR lower(cc.lastName) like :arg2 OR lower(c.ticketNumber) = :arg3)", Case.class);
        query.setParameter("branchId", branchId);
        query.setParameter("arg", "%" + searchArg.toLowerCase() + "%");
        query.setParameter("arg2", "%" + searchArg.toLowerCase() + "%");
        query.setParameter("arg3", searchArg.toLowerCase());
        query.setParameter("startOfToday", startOfToday);
        applyPaging(params, query);
        return query.getResultList();
    }
	
	@Override
	public List<Case> findCasesByCustomerId(Integer branchId, Long customerId) throws BPException {

        List<Case> cases = entityManager.createQuery("SELECT c FROM Case c join c.caseCustomer cc WHERE c.branchId = :branchId AND cc.id = :customerId ORDER BY c.created DESC")
                .setParameter("branchId", branchId)
                .setParameter("customerId", customerId)
                .getResultList();

        return cases;
    }
	
	@Override
    public List<Case> findCasesByTicketNumber(Integer branchId, String ticketNumber) throws BPException {
        List<Case> cases = entityManager.createQuery("SELECT c FROM Case c WHERE c.branchId = :branchId AND c.ticketNumber = :ticketNumber ORDER BY c.created DESC")
				.setParameter("branchId", branchId)
				.setParameter("ticketNumber", ticketNumber)
                .getResultList();
        return cases;
    }
	
	@Override
    public List<Case> findCasesByDate(Integer branchId, Date date) throws BPException {
        List<Case> cases = entityManager.createQuery("SELECT c FROM Case c WHERE c.branchId = :branchId AND cast(c.created as date) = :startOfToday")
				.setParameter("branchId", branchId)
                .setParameter("startOfToday", date)
                .getResultList();

        return cases;
    }
	
	@Override
    public List<Case> findCasesHistoryByCustomerId(Long customerId) throws BPException {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.MINUTE,0);
        Timestamp yesterday = new Timestamp(cal.getTime().getTime());

        TypedQuery <Case> query = entityManager.createQuery("SELECT c FROM Case c join c.caseCustomer cc WHERE cc.id = :customerId AND c.created < :yesterday ORDER BY c.created DESC", Case.class);
        query.setParameter("customerId", customerId);
        query.setParameter("yesterday", yesterday);
        List<Case> cases = query.getResultList();
        return cases;
    }
	
	@Override
    public List<Case> findAllCasesByCustomerId(Long customerId) throws BPException {
        TypedQuery <Case> query = entityManager.createQuery("SELECT c FROM Case c join c.caseCustomer cc WHERE cc.id = :customerId ORDER BY c.created DESC", Case.class);
        query.setParameter("customerId", customerId);
        List<Case> cases = query.getResultList();
        return cases;
    }

    @Override
    public List<Case> findOnGoingCasesForBranchAndServicePoint(Integer branchId, Integer servicePointId) throws BPException {
        List<Case> cases = entityManager.createQuery("SELECT c FROM Case c join c.caseServices cs WHERE c.branchId = :branchId " +
                "AND cs.servedByCounterId = :servicePointId AND " +
                "(cs.status = :status1 OR cs.status = :status2) ORDER BY c.created DESC")   //  OR cs.status = :status3
                .setParameter("branchId", branchId)
                .setParameter("servicePointId", servicePointId)
                .setParameter("status1", Status.SERVING)
                .setParameter("status2", Status.POST_PROCESSING)
                //.setParameter("status3", Status.RECYCLED)

                .getResultList();

        return cases;
    }
	
	@Override
    public boolean isCaseBeingServed(Integer branchId, Long caseId) throws BPException {
        List<Case> cases = entityManager.createQuery("SELECT c FROM Case c join c.caseServices cs WHERE c.id = :caseId AND c.branchId = :branchId " +
                "AND cs.status = :status ORDER BY c.created DESC")
                .setParameter("branchId", branchId)
                .setParameter("caseId", caseId)
                .setParameter("status", Status.SERVING)

                .getResultList();

        return cases.size() >= 1;
    }
	
	@Override
    public boolean isCaseLocked(Integer branchId, Long caseId) throws BPException {
        List<Case> cases = entityManager.createQuery("SELECT c FROM Case c join c.caseServices cs WHERE c.id = :caseId AND c.branchId = :branchId " +
                "AND cs.status = :status ORDER BY c.created DESC")
                .setParameter("branchId", branchId)
                .setParameter("caseId", caseId)
                .setParameter("status", Status.LOCKED)

                .getResultList();

        return cases.size() >= 1;
    }
	
    @Override
    public List<Case> findVisitsInTrashQueue(Integer branchId) throws BPException {
        List<Case> cases = entityManager.createQuery("SELECT c FROM CaseService cs join cs.parentCase c WHERE cs.status = :status")
                .setParameter("status", Status.TRASH)
                .getResultList();

        return cases;
    }

    @Override
    public void deleteOngoingCasesForBranch(Integer branchId) throws BPException {
		Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.MINUTE,0);
        Timestamp startOfToday = new Timestamp(cal.getTime().getTime());

        String[] statusList = new String[]{Status.PENDING.name(), Status.SERVING.name(), Status.WAITING.name(), Status.POST_PROCESSING.name(), Status.RECYCLED.name(), Status.TRASH.name()};
        StringBuilder buf = new StringBuilder();
        for(String s : statusList) {
            buf.append("\'").append(s).append("\'").append(",");
        }
        buf.setLength(buf.length() - 1);
        try {
            List<Case> caseList = entityManager.createQuery("SELECT c FROM Case c join c.caseServices cs WHERE c.created > :startOfToday AND c.branchId = :branchId AND cs.status IN (" + buf.toString() + ")")
                    .setParameter("branchId", branchId)
					.setParameter("startOfToday", startOfToday)
            .getResultList();
            // For each case, delete everything!!
            for(Case c : caseList) {
                entityManager.remove(c);
                log.info("Deleted Case for Ticket number: {}", c.getTicketNumber());
            }
        } catch (Exception e) {
            log.error("Caught error finding ongoing cases for branch: {}", e.getMessage());
            e.printStackTrace();
            throw new BPException(e.getMessage(), BPExceptionCode.READ_ERROR);
        }
    }

    @Override
    public List<Case> findCasesInTrashQueues(Integer branchId) throws BPException {
        List<Case> caseList = entityManager.createQuery("SELECT c FROM Case c join c.caseServices cs WHERE c.branchId = :branchId AND cs.status = :status")
                .setParameter("status", Status.TRASH)
                .setParameter("branchId", branchId)
                .getResultList();
        return caseList;
    }

	@Override
    public List<Case> getTodaysCases(Integer branchId) throws BPException {
		Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.MINUTE,0);
        Timestamp startOfToday = new Timestamp(cal.getTime().getTime());
		
		List<Case> todaysCases = entityManager.createQuery("SELECT c FROM Case c WHERE c.created > :startOfToday AND c.branchId = :branchId")
            .setParameter("branchId", branchId)
			.setParameter("startOfToday", startOfToday)
            .getResultList();
			
		return todaysCases;
	}
}
