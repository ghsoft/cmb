package com.qmatic.ps.bp.rest.mapper;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.Case;
import com.qmatic.ps.bp.model.CaseService;
import com.qmatic.ps.bp.model.enumeration.Status;
import com.qmatic.qp.api.connectors.dto.Visit;
import com.qmatic.qp.api.connectors.dto.VisitService;

/**
 * @author Dejan Milojevic (dejmil), @created Nov 5, 2013
 */

@Component
public class CaseBuilder {

	public Case createCase(Visit visit, Integer branchId, Integer customerId, String customerName) throws BPException {
    	Case newCase = new Case();
    	newCase.setQpVisitId(visit.getId());
    	newCase.setBranchId(branchId);
    	newCase.setCreated(new Date(System.currentTimeMillis()));
    	newCase.setTicketNumber(visit.getTicketId());
    	if(customerId != null && customerName != null) {
    		newCase.getCaseCustomer().setId(new Long(customerId));
    	}
    	List<CaseService> caseServices = new ArrayList<>();
    	int index = 0;
    	CaseService caseService = createCaseService(visit.getCurrentVisitService(), index++);
    	caseService.setParentCase(newCase);
		caseServices.add(caseService);
    	for(VisitService visitService :  visit.getUnservedVisitServices()) {
    		caseService = createCaseService(visitService, index++);
    		caseService.setParentCase(newCase);
    		caseServices.add(caseService);
    	}
    	newCase.setCaseServices(caseServices);
    	return newCase;
    }
    
    public CaseService createCaseService(VisitService visitService, int index) {
    	CaseService caseService = new CaseService();
		caseService.setCreated(new Date(System.currentTimeMillis()));
		caseService.setIdx(index);
		caseService.setQpVisitServiceId(visitService.getId());
		caseService.setRecyclesLeft(3);
		caseService.setServiceId(visitService.getServiceId());
		caseService.setServiceName(visitService.getServiceExternalName());
		caseService.setStatus(Status.WAITING);
		return caseService;
    	
    }
    
}
