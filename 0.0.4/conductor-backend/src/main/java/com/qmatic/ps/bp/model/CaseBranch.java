package com.qmatic.ps.bp.model;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-23
 * Time: 15:48
 * To change this template use File | Settings | File Templates.
 */
public class CaseBranch {

    private Integer id;
	private Integer customersServed = 0;
    private Integer servicesServed = 0;
    private Integer customersBeingServed = 0;
    private Integer customersWaiting = 0;
    private Integer servicesWaiting = 0;
    private Integer totalWaitingTime = 0;
    private Integer maxWaitingTime = 0;
    private Integer averageWaitingTime = 0;
	
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	
	public Integer getCustomersServed() {
        return customersServed;
    }

    public void setCustomersServed(Integer customersServed) {
        this.customersServed = customersServed;
    }
	
	public Integer getServicesServed() {
        return servicesServed;
    }

    public void setServicesServed(Integer servicesServed) {
        this.servicesServed = servicesServed;
    }
	
	public Integer getCustomersBeingServed() {
        return customersBeingServed;
    }

    public void setCustomersBeingServed(Integer customersBeingServed) {
        this.customersBeingServed = customersBeingServed;
    }
	
	public Integer getCustomersWaiting() {
        return customersWaiting;
    }

    public void setCustomersWaiting(Integer customersWaiting) {
        this.customersWaiting = customersWaiting;
    }
	
	public Integer getServicesWaiting() {
        return servicesWaiting;
    }

    public void setServicesWaiting(Integer servicesWaiting) {
        this.servicesWaiting = servicesWaiting;
    }
	
	public Integer getTotalWaitingTime() {
        return totalWaitingTime;
    }

    public void setTotalWaitingTime(Integer totalWaitingTime) {
        this.totalWaitingTime = totalWaitingTime;
    }
	
	public Integer getMaxWaitingTime() {
        return maxWaitingTime;
    }

    public void setMaxWaitingTime(Integer maxWaitingTime) {
        this.maxWaitingTime = maxWaitingTime;
    }
	
	public Integer getAverageWaitingTime() {
        return averageWaitingTime;
    }

    public void setAverageWaitingTime(Integer averageWaitingTime) {
        this.averageWaitingTime = averageWaitingTime;
    }
	
	public void calculateAverageWaitingTime() {
		if (this.servicesWaiting > 0) {
			this.averageWaitingTime = Math.round(this.totalWaitingTime/this.servicesWaiting);
		}
	}
}
