package com.qmatic.ps.bp.model;

import com.qmatic.qp.api.connectors.dto.ServicePointData;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-23
 * Time: 15:48
 * To change this template use File | Settings | File Templates.
 */

@Entity
public class CaseServicePoint {

    private Long id;
    private String name;
	private String staffName;
	private String staffFullName;
    private Integer workProfileId;
    private String workProfileName;
    private String status;
    private Integer serviceId;
    private String serviceName;
    private String servicePointStatus;
    private Integer transactionTime;
    private Integer customerId;
    private String customerName;
    private String ticketNumber;
    private Integer servedToday;
	
	public CaseServicePoint() {
	
	}
	
	public CaseServicePoint(ServicePointData servicePointData) {
		this.name = servicePointData.getName();
		if (servicePointData.getWorkProfileId() != null) {
			this.staffName = servicePointData.getStaffName();
			this.staffFullName = servicePointData.getStaffFullName();
			this.workProfileId = servicePointData.getWorkProfileId();
			this.workProfileName = servicePointData.getWorkProfileName();
			this.status = servicePointData.getStatus().toString();
		}
		else {
			this.staffName = "";
			this.staffFullName = "";
			this.workProfileId = 0;
			this.workProfileName = "";
			this.status = "CLOSED";
		}
		this.serviceId = 0;
		this.serviceName = "";
		this.servicePointStatus = "";
		this.transactionTime = 0;
		this.customerId = 0;
		this.customerName = "";
		this.ticketNumber = "";
		this.servedToday = 0;
	}
	
	@Id
    public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getStaffName() {
		return staffName;
	}
	
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	
	public String getStaffFullName() {
		return staffFullName;
	}
	
	public void setStaffFullName(String staffFullName) {
		this.staffFullName = staffFullName;
	}
	
	public Integer getWorkProfileId() {
		return workProfileId;
	}
	
	public void setWorkProfileId(Integer workProfileId) {
		this.workProfileId = workProfileId;
	}
	
	public String getWorkProfileName() {
		return workProfileName;
	}
	
	public void setWorkProfileName(String workProfileName) {
		this.workProfileName = workProfileName;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Integer getServiceId() {
		return serviceId;
	}
	
	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}
	
	public String getServiceName() {
		return serviceName;
	}
	
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	public String getServicePointStatus() {
		return servicePointStatus;
	}
	
	public void setServicePointStatus(String servicePointStatus) {
		this.servicePointStatus = servicePointStatus;
	}
	
	public Integer getTransactionTime() {
		return transactionTime;
	}
	
	public void setTransactionTime(Integer transactionTime) {
		this.transactionTime = transactionTime;
	}
	
	public Integer getCustomerId() {
		return customerId;
	}
	
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public String getTicketNumber() {
		return ticketNumber;
	}
	
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	
	public Integer getServedToday() {
		return servedToday;
	}
	
	public void setServedToday(Integer servedToday) {
		this.servedToday = servedToday;
	}

}
