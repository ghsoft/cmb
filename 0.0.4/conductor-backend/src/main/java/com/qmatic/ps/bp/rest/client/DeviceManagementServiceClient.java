package com.qmatic.ps.bp.rest.client;

/**
 * Created with IntelliJ IDEA.
 * User: Roger
 * Date: 2013-08-13
 * Time: 15:45
 * To change this template use File | Settings | File Templates.
 */
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qmatic.ps.bp.util.CookieHolder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.qmatic.qp.api.connectors.dto.*;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.GenericType;
import org.springframework.stereotype.Component;

@Component
public class DeviceManagementServiceClient {
    protected Client c;
    protected ObjectMapper om = new ObjectMapper();

    @PostConstruct
    public void init() {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put("com.sun.jersey.api.json.POJOMappingFeature", Boolean.TRUE);
        c = Client.create(clientConfig);
        c.setFollowRedirects(true);
        c.setConnectTimeout(5000);
    }

    protected String getRestPath() {
        return getProtocol() + "://" + getIp() + ":" + getPort() + "/" + getBasePath();
    }

    protected String getProtocol() {
        return "http";
    }

    protected String getIp() {
        return "127.0.0.1";
    }

    protected String getPort() {
        //TODO change to config file.
        return "8080";
    }

    protected String getBasePath() {
        return "rest";
    }

    private Cookie getSSOCookie() {
        return CookieHolder.get();
    }

    // start JAX-RS API
    public java.util.List<com.qmatic.qp.api.connectors.dto.TinyBranch> getBranches() {
        java.util.List<com.qmatic.qp.api.connectors.dto.TinyBranch> result = c.resource(getRestPath() + "/dm/branches")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.TinyBranch>>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.TinyUnit> getUnits(Integer branchId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.TinyUnit> result = c.resource(getRestPath() + "/dm/branches/" + branchId + "/units")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.TinyUnit>>() {
                });
        return result;
    }

    public Unit getUnit(Integer branchId, Long unitId) {
        Unit result = c.resource(getRestPath() + "/dm/branches/" + branchId + "/units/" + unitId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<Unit>() {
                });
        return result;
    }
}

