package com.qmatic.ps.bp.dao.admin;

import com.qmatic.ps.bp.dao.Dao;
import com.qmatic.ps.bp.model.admin.PresetNote;


/**
 * @author Dejan Milojevic (dejmil), @created Nov 6, 2013
 */

public interface PresetNoteDao extends Dao<PresetNote> {

}
