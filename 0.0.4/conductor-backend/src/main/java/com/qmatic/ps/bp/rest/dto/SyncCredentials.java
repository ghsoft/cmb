package com.qmatic.ps.bp.rest.dto;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-02-14
 * Time: 12:28
 * To change this template use File | Settings | File Templates.
 */
public class SyncCredentials implements Serializable {

    private String username;
    private String password;
    private String orchestraUrl;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOrchestraUrl() {
        return orchestraUrl;
    }

    public void setOrchestraUrl(String orchestraUrl) {
        this.orchestraUrl = orchestraUrl;
    }
}
