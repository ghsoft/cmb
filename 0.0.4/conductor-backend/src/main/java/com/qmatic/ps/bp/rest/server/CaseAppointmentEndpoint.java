package com.qmatic.ps.bp.rest.server;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.common.BPExceptionCode;
import com.qmatic.ps.bp.dao.CaseDao;
import com.qmatic.ps.bp.model.Case;
import com.qmatic.ps.bp.rest.client.AppointmentServiceClient;
import com.qmatic.ps.bp.rest.client.EntryPointServiceClient;
import com.qmatic.ps.bp.rest.mapper.CaseBuilder;
import com.qmatic.ps.bp.util.CookieHolder;
import com.qmatic.qp.api.appointment.AppointmentStatus;
import com.qmatic.qp.api.connectors.dto.Appointment;
import com.qmatic.qp.api.connectors.dto.TransferSortPolicy;
import com.qmatic.qp.api.connectors.dto.Visit;
import com.qmatic.qp.api.connectors.dto.VisitParameters;

/**
 * @author Dejan Milojevic (dejmil), @created Nov 4, 2013
 */

@Component
@Scope(value = "request")
@Path("/caseappointment")
public class CaseAppointmentEndpoint {
	
	private final Logger log = LoggerFactory.getLogger(CaseAppointmentEndpoint.class);
	
	private HttpHeaders httpHeaders;
    private AppointmentServiceClient appointmentServiceClient;
    private EntryPointServiceClient entryPointServiceClient;
    private CaseBuilder caseBuilder;
    private CaseDao caseDao;

    @GET
    @Path("/branches/{branch}/appointments")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAppointments(@PathParam("branch") Integer branchId) throws BPException {
        try{
        	setSSOCookie();
        	List<Appointment> appointments = appointmentServiceClient.getAppointmentsForBranch(branchId);
        	List<Appointment> appointmentsNotArrived = new ArrayList<>();
        	for(Appointment appointment : appointments) {
        		if(AppointmentStatus.CREATED.equals(appointment.getStatus())) {
        			appointmentsNotArrived.add(appointment);
        		}
        	}
            return Response.ok(appointmentsNotArrived).build();
        } catch(Exception e){
        	log.error("Failed to retrieve appointments: " + e.getMessage());
            throw new BPException("Failed to retrieve appointments: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	@GET
    @Path("/branches/{branch}/appointments/{appointment}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAppointment(@PathParam("branch") Integer branchId, @PathParam("appointment") Integer appointmentId) throws BPException {
        try{
        	setSSOCookie();
        	Appointment appointment = appointmentServiceClient.getAppointmentForBranch(branchId, appointmentId);
        	if(AppointmentStatus.CREATED.equals(appointment.getStatus())) {
				return Response.ok(appointment).build();
			}
			return Response.ok("{}").build();
        } catch(Exception e){
        	log.error("Failed to retrieve appointment: " + e.getMessage());
            return Response.ok("{}").build();
        }
    }

    @POST
	@Path("/arrived/{appointment}/branch/{branch}/entryPoint/{entryPoint}/customer/{customerId}/{customerName}")
    @Produces(value = MediaType.APPLICATION_JSON)
	public Response updateAppointmentToArrived(
			@PathParam("appointment") Integer appointmentId,
			@PathParam("branch") Integer branchId,
			@PathParam("entryPoint") Integer entryPointId,
			@PathParam("customerId") Integer customerId,
			@PathParam("customerName") String customerName) throws BPException {
		try {
			setSSOCookie();
			VisitParameters visitParameters = new VisitParameters();
			visitParameters.setAppointmentId(appointmentId);
			Visit visit = entryPointServiceClient.createVisit(branchId, entryPointId, visitParameters, TransferSortPolicy.SORTED);
			Case newCase = caseBuilder.createCase(visit, branchId, customerId, customerName);
	    	newCase = caseDao.create(newCase);
			return Response.ok(newCase).build();
		} catch(Exception e) {
			log.error("Failed to arrive appointment: " + e.getMessage());
			throw new BPException("Failed to arrive appointment: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
		}
	}
        	
	protected void setSSOCookie() {
        CookieHolder.set(httpHeaders.getCookies().get("SSOcookie"));
    }
	
	@Context
	public void setHttpHeaders(HttpHeaders httpHeaders) {
		this.httpHeaders = httpHeaders;
	}

	@Autowired
	public void setAppointmentServiceClient(AppointmentServiceClient appointmentServiceClient) {
		this.appointmentServiceClient = appointmentServiceClient;
	}

	@Autowired
	public void setEntryPointServiceClient(EntryPointServiceClient entryPointServiceClient) {
		this.entryPointServiceClient = entryPointServiceClient;
	}

	@Autowired
	public void setCaseBuilder(CaseBuilder caseBuilder) {
		this.caseBuilder = caseBuilder;
	}
	
	@Autowired
	public void setCaseDao(CaseDao caseDao) {
		this.caseDao = caseDao;
	}
	
}
