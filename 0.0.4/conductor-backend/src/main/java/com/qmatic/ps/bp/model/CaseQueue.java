package com.qmatic.ps.bp.model;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-23
 * Time: 15:48
 * To change this template use File | Settings | File Templates.
 */
public class CaseQueue {

    private Integer id;
	private String name;
	private Integer servicesWaiting = 0;
	private Integer waitingTime = 0;
	private Integer servicesServed = 0;
	
    public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getServicesWaiting() {
		return servicesWaiting;
	}
	
	public void setServicesWaiting(Integer servicesWaiting) {
		this.servicesWaiting = servicesWaiting;
	}
	
	public Integer getWaitingTime() {
		return waitingTime;
	}
	
	public void setWaitingTime(Integer waitingTime) {
		this.waitingTime = waitingTime;
	}
	
	public Integer getServicesServed() {
		return servicesServed;
	}
	
	public void setServicesServed(Integer servicesServed) {
		this.servicesServed = servicesServed;
	}

}
