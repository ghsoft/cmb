package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.ServiceLevel;

import javax.ws.rs.core.MultivaluedMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
public interface ServiceLevelDao extends Dao<ServiceLevel> {
	
	List<ServiceLevel> getServiceLevels() throws BPException;
	
	ServiceLevel getServiceLevel(Integer serviceId) throws BPException;
	
}
