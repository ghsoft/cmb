SELECT	id
	,	name
	,	MAX(servicesWaiting) AS servicesWaiting
	,	MAX(waitingTime) AS waitingTime
	,	MAX(servicesServed) AS servicesServed
FROM	(	
	SELECT	service_id AS id
		,	service_name AS name
		,	ISNULL(COUNT(id), 0) AS servicesWaiting
		,	ISNULL(MAX(CASE WHEN cs.status = 'RECYCLED' THEN DATEDIFF(ss, cs.updated, GETDATE()) ELSE DATEDIFF(ss, cs.created, GETDATE())END), 0) AS waitingTime
		,	0 AS servicesServed
	FROM	qp_conductor.case_services cs
	INNER JOIN (
		SELECT	parent_case_id AS case_id,
				COUNT(DISTINCT CASE WHEN 
							cs.status = 'SERVING' 
						OR	cs.status = 'TRASH' 
						OR	cs.status = 'NOSHOW' 
						OR	cs.status = 'ONHOLD' 
						OR	(cs.status = 'RECYCLED' AND DATEDIFF(ss, cs.updated, GETDATE()) <= 60) 
					THEN parent_case_id ELSE NULL END) AS not_waiting
		FROM qp_conductor.case_services cs
		INNER JOIN qp_conductor.cases c ON cs.parent_case_id = c.id
		WHERE	c.created > '2015-03-24'
		AND		branch_id = 1
		GROUP BY branch_id, parent_case_id
	) ws ON cs.parent_case_id = ws.case_id AND ws.not_waiting = 0
	WHERE	cs.created > '2015-03-24'
	AND		(cs.status = 'WAITING' OR cs.status = 'PENDING' OR (cs.status = 'RECYCLED' AND DATEDIFF(ss, cs.updated, GETDATE()) > 60))
	GROUP BY service_id, service_name
UNION ALL
	SELECT	service_id AS id
		,	service_name AS name
		,	0 AS servicesWaiting
		,	0 AS waitingTime
		,	ISNULL(COUNT(id), 0) AS servicesServed
	FROM	qp_conductor.case_services cs
	INNER JOIN (
		SELECT	id AS case_id,
				branch_id
		FROM	qp_conductor.cases c
		WHERE	created > '2015-03-24'
		AND		branch_id = 1
	) c ON cs.parent_case_id = c.case_id
	WHERE	created > '2015-03-24'
	AND		status = 'FINISHED'
	GROUP BY service_id, service_name
UNION ALL
	SELECT	service_id AS id
		,	service_name AS name
		,	ISNULL(COUNT(id), 0) AS servicesWaiting
		,	ISNULL(MAX(DATEDIFF(ss, cs.created, GETDATE())), 0) AS waitingTime
		,	0 AS servicesServed
	FROM	qp_conductor.case_services cs
	WHERE	created > '2015-03-24'
	AND		(
				(status = 'TRASH' AND service_name = 'TRASH')
			OR	(status = 'ONHOLD' AND service_name = 'ONHOLD')
			OR	(status = 'NOSHOW' AND service_name = 'NOSHOW')
			)
	GROUP BY service_id, service_name
) data
GROUP BY id, name