package com.qmatic.ps.bp.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * The Setting entity.
 */
@Entity
@Table(name = "settings")
@Cacheable
@XmlRootElement
public class Setting implements Serializable {

    private Long id;
    private String group;
    private String label;
    private String data;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "conductor_seq_table")
    @TableGenerator(name = "conductor_seq_table", table = "id_gen", pkColumnName = "id_name", valueColumnName = "id_val", pkColumnValue = "settings", allocationSize = 1)

    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Key for this setting
     */
    @Column(name="settings_group")
    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    /**
     * Human-readable label for this setting.
     */
    @Column(name="label")
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Typically a JSON string with key-value pairs or similar.
     */
    @Lob
    @Column(name="data")
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
