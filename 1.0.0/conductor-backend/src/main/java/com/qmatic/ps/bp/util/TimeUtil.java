package com.qmatic.ps.bp.util;

import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-02-06
 * Time: 16:38
 * To change this template use File | Settings | File Templates.
 */
public class TimeUtil {

    public static final String DATE_FORMAT_STRING = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String DATE_FORMAT_STRING_WITHOUT_TIMEZONE = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String TIME_FORMAT_STRING = "HH:mm";

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_STRING);
    public static final DateTimeFormatter JODA_DATE_FORMAT = DateTimeFormat.forPattern(DATE_FORMAT_STRING);

    public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat(TIME_FORMAT_STRING);
    public static final DateTimeFormatter JODA_TIME_FORMAT = DateTimeFormat.forPattern(TIME_FORMAT_STRING);

    private static final Logger log = LoggerFactory.getLogger(TimeUtil.class);

    /**
     * Times must be in same timeZone.
     *
     * @param start
     * @param end
     * @return The duration that has passed between start and end.
     */
    public static long calculateDuration(String start, String end) {
        final DateTime dateTimeStart = JODA_DATE_FORMAT.withOffsetParsed().parseDateTime(start);
        final DateTime dateTimeEnd = JODA_DATE_FORMAT.withOffsetParsed().parseDateTime(end);
        return calculateDuration(dateTimeStart, dateTimeEnd);
    }

    /**
     * DateTimes must be in same timezone.
     * @param start
     * @param end
     * @return The duration that has passed between start and end.
     */
    public static long calculateDuration(DateTime start, DateTime end) {
        Duration duration = null;
        try {
            duration = new Interval(start, end).toDuration();
        } catch (IllegalArgumentException e) {
            log.error("Start time : " + (start != null ? start : "null") + ", End time : " + (end != null ? end : "null"));
            throw e;
        }
        return duration.getMillis();
    }

    /**
     * Provide a timezone and this method will provide you with the current time at that particular timezone
     * formatted according to the format specified in QPConstants.
     * @param timeZone
     * @return
     */
    public static String getCurrentTimeInZoneAsString(String timeZone) {
        DateTime now = new DateTime(System.currentTimeMillis(), DateTimeZone.forID(timeZone));
        DateTimeFormatter fmt = DateTimeFormat.forPattern(DATE_FORMAT_STRING);
        return now.toString(fmt);
    }

    /**
     * Returns time in another timezone as a joda DateTime object.
     * @param timeZone
     * @return
     */
    public static DateTime getCurrentTimeInZoneAsDateTime(String timeZone) {
        return new DateTime(System.currentTimeMillis(), DateTimeZone.forID(timeZone));
    }

    /**
     * Returns the local time as an ISO formatted string.
     * @return
     */
    public static String getCurrentLocalTimeAsString() {
        DateTime now = new DateTime();
        return now.toString(JODA_DATE_FORMAT);
    }

    /** Returns the local time as a joda DateTime object.
     * @return
     */
    public static DateTime getCurrentLocalTimeAsDateTime() {
        return new DateTime();
    }

    /**
     * Returns a iso formatted string representation of the DateTime object.
     * @param dateTime
     * @return
     */
    public static String formatDatetimeAsString(DateTime dateTime) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern(DATE_FORMAT_STRING);
        return dateTime.toString(fmt);
    }
    /**
     * Returns an iso formatted string representation of the DateTime object in the specified timezone.
     * @param dateTime
     * @param timeZone
     * @return
     */
    public static String formatDatetimeInOtherZoneAsString(DateTime dateTime, String timeZone) {
        DateTime destDateTime = dateTime.withZone(DateTimeZone.forID(timeZone));
        return formatDatetimeAsString(destDateTime);
    }

    /**
     * Returns a iso formatted string representation of the DateTime object.
     * @param dateTime
     * @return
     */
    public static String formatDatetimeAsStringWithoutTimezone(DateTime dateTime) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern(DATE_FORMAT_STRING_WITHOUT_TIMEZONE);
        return dateTime.toString(fmt);
    }

    /**
     * Provide a timestamp and a timezone and this method will provide you with the time at that particular timezone
     * as a string ISO-formatted.
     * @param timeZone
     * @return
     */
    public static String convertLocalTimeToTimeInOtherZoneAsString(DateTime timestamp, String timeZone) {
        DateTime time = new DateTime(timestamp, DateTimeZone.forID(timeZone));
        DateTimeFormatter fmt = DateTimeFormat.forPattern(DATE_FORMAT_STRING);
        return time.toString(fmt);
    }


    //Methods parsing a String  into Dates, Long, etc.

    /**
     * Parses an ISO formatted string into a joda DateTime object.
     * @param time
     * @return
     */
    public static DateTime parseStringToDateTime(String time) {
        if(time == null) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT_STRING);
        return formatter.parseDateTime(time);
    }

    /**
     * Parses an ISO formatted three-part string (date + time + timezone), e.g. 2013-02-01' '14:00' '+0100'
     * into a joda LocalDateTime object. Any parsed time-zone or offset field is completely ignored.
     * In other words all zone-info is removed but time is kept "as is".
     *
     * @param date
     *          2013-03-05
     * @param time
     *          15:32
     * @param timeZone
     *          +0100
     * @return
     */
    public static LocalDateTime parseStringToLocalDateTime(String date, String time, String timeZone) {
        if(time == null || date == null || timeZone == null) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT_STRING);
        return formatter.parseLocalDateTime(date + "T" + time + ":00.000" + timeZone);
    }

    /**
     * Parses an ISO formatted string into a joda LocalDateTime object. Any parsed time-zone or offset field is completely ignored.
     * In other words all zone-info is removed but time is kept "as is".
     * @param time
     * @return
     */
    public static LocalDateTime parseStringToLocalDateTime(String time) {
        if(time == null) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT_STRING);
        return formatter.parseLocalDateTime(time);
    }

    public static LocalDateTime parseStringWithoutTimezoneToLocalDateTime(String time) {
        if(time == null) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT_STRING_WITHOUT_TIMEZONE);
        return formatter.parseLocalDateTime(time);
    }


    public static DateTime parseStringToDateTimeWithOffsetParsed(String time) {
        if(time == null) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT_STRING);
        return formatter.withOffsetParsed().parseDateTime(time);
    }

    /**
     * Converts minute of day into HH:mm
     * @param minuteOfDay
     * @return
     */
    public static String timeToString(Integer minuteOfDay) {
        String hour = Integer.toString(minuteOfDay / 60);
        String minute = Integer.toString(minuteOfDay % 60);

        return (hour.length() == 2 ? hour : "0" + hour) + ":" + (minute.length() == 2 ? minute : "0" + minute);
    }

    /**
     * Converts HH:mm into minute of day
     * @param time
     * @return
     */
    public static Integer timeToInteger(String time) {
        String[] parts = time.split(":");
        String hourStr = parts[0];
        String minuteStr = parts[1];
        Integer hour = hourStr.charAt(0) == '0' ? Integer.parseInt("" + hourStr.charAt(1)) : Integer.parseInt(hourStr);
        Integer minute = minuteStr.charAt(0) == '0' ? Integer.parseInt("" + minuteStr.charAt(1)) : Integer.parseInt(minuteStr);
        return hour*60 + minute;
    }

    public static String toUTC(String isoDateTime) {
        DateTime dt = parseStringToDateTimeWithOffsetParsed(isoDateTime);
        dt = dt.withZone(DateTimeZone.UTC);
        dt = dt.toDateTimeISO();
        return formatDatetimeAsString(dt);
    }

    public static String toUTC(DateTime isoDateTime) {
        DateTime dt = isoDateTime.toDateTimeISO();
        dt = dt.withZone(DateTimeZone.UTC);
        return formatDatetimeAsString(dt);
    }

    /**
     *
     * @param dateTime
     *      An ISO8601 string with +0000 as time zie
     * @param timeZone
     *      The time zone to "translate" into.
     * @return
     */
    public static DateTime toDateTimeInTimeZone(String dateTime, String timeZone) {
        DateTime dt = parseStringToDateTime(dateTime);
        return dt.withZone(DateTimeZone.forID(timeZone));

    }



    /** Some Calendar helpers */
    public static String fromCalendar(Calendar cal) {
        return DATE_FORMAT.format(cal.getTime());
    }

    public static Calendar toCalendar(String dateTimeStr) {
        try {
            Date d = DATE_FORMAT.parse(dateTimeStr);
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            return c;
        } catch (ParseException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Gets the HH:mm of the supplied dateTime in whatever timezone the object is in.
     * @param dateTime
     * @return
     */
    public static String toTime(DateTime dateTime) {
        return dateTime.toString(JODA_TIME_FORMAT);
    }

    public static String toTime(String dateTime) {
        // Make sure it's parsable first
        JODA_DATE_FORMAT.parseDateTime(dateTime);
        return dateTime.substring(12, 17);
    }

    /**
     * Returns 2013-03-26 if supplied with 2013-03-27 and -1.
     * Returns 2013-03-29 if supplied with 2013-03-27 and 2.
     * @param date
     * @param add
     * @return
     */
    public static String dateOfDayAdded(LocalDateTime date, int add) {
       // DateTime dt = date.toDateTimeAtStartOfDay();
        LocalDateTime dt = date.plusDays(add);
        return dt.toString(JODA_DATE_FORMAT).substring(0, 10);
      //  return toUTC(dt).substring(0, 10);
    }

    public static int getWeekDay(LocalDate date) {
        return date.getDayOfWeek() - 1;
    }

    public static int getWeekDay(DateTime date) {
        return date.getDayOfWeek() - 1;
    }

    public static int getWeekDay(String dateAsIsoString) {
        DateTime date = parseStringToDateTime(dateAsIsoString);
        return date.getDayOfWeek() - 1;
    }

    /**
     * Will return 3 for +0300, will return -11 for -1100 etc...
     * @param timeZone
     * @return
     */
    public static int getOffsetHours(String timeZone) {

        char prefix = timeZone.charAt(0);
        switch(prefix) {
            case 'Z':
                return 0;
            case '+':
                return Integer.parseInt(timeZone.substring(1, 3));
            case '-':
                return -Integer.parseInt(timeZone.substring(1, 3));
            default:
                return Integer.parseInt(timeZone.substring(0, 2));
        }
    }
}
