package com.qmatic.ps.bp.push;

import com.qmatic.ps.bp.push.event.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-02-22
 * Time: 11:23
 * To change this template use File | Settings | File Templates.
 */
@Component
public class EventSender {

    @Autowired
    CometDServerRegistry cometDServerRegistry;

    public void sendEvent(Event event) {
        cometDServerRegistry.getCometDServer().onGenericEvent(event);
    }
}
