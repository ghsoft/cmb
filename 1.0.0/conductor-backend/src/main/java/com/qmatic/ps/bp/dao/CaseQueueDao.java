package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.CaseQueue;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
public interface CaseQueueDao extends Dao<CaseQueue> {

	List<CaseQueue> getQueuesInfo(Integer branchId) throws BPException;
	
}
