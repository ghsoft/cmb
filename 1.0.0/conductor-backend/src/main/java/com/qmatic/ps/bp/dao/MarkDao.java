package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.Mark;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-06-14
 * Time: 11:06
 * To change this template use File | Settings | File Templates.
 */
public interface MarkDao extends Dao<Mark> {
    Mark findMarkByDeliveredServiceId(Integer deliveredServiceId) throws BPException;
}
