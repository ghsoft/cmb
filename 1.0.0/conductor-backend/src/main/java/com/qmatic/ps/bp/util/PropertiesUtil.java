package com.qmatic.ps.bp.util;

import java.util.HashMap;  
import java.util.Map;  
import java.util.Properties; 

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * Bean that should be used instead of the {@link PropertyPlaceholderConfigurer} if you want to have
 * access to the resolved properties not obly from the Spring context. e.g. from JSP or so. More
 * details about usage here http://wiki.sonopia.com/x/GmQ
 * 
 * @author Mykola Palienko
 */
public class PropertiesUtil extends PropertyPlaceholderConfigurer {

	private static Map<String, String> ctxPropertiesMap;  
  
    @Override  
    protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess, Properties props) throws BeansException {  
		super.processProperties(beanFactoryToProcess, props);  
		ctxPropertiesMap = new HashMap<String, String>();  
		for (Object key : props.keySet()) {  
			String keyStr = key.toString();  
			String value = props.getProperty(keyStr);  
			ctxPropertiesMap.put(keyStr, value);  
		}  
	}  
  
	public static String getContextProperty(String name) {  
		return ctxPropertiesMap.get(name);  
	}  

}