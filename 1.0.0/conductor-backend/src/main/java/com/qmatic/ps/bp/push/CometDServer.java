package com.qmatic.ps.bp.push;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.qmatic.ps.bp.push.event.Event;
import org.cometd.annotation.Session;
import org.cometd.bayeux.server.BayeuxServer;
import org.cometd.bayeux.server.ConfigurableServerChannel;
import org.cometd.bayeux.server.LocalSession;
import org.cometd.bayeux.server.ServerChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-02-22
 * Time: 10:57
 * To change this template use File | Settings | File Templates.
 */

public class CometDServer {
    public static final String EVENTS_BASE_CHANNEL = "/events/";

    private static final Logger log = LoggerFactory.getLogger(CometDServer.class);

    private BayeuxServer bayeuxServer;

    @Session
    private LocalSession sender;

    private final ObjectMapper om = new ObjectMapper();

    /**
     * Posts the supplied registration event to subscribers of the channel:
     *
     * /events/[eventId]
     *
     * @param event
     */
    public void onGenericEvent(Event event) {

        // Initialize the channel, making it persistent and lazy
        bayeuxServer.createIfAbsent(EVENTS_BASE_CHANNEL + "generic", new ConfigurableServerChannel.Initializer() {
            public void configureChannel(ConfigurableServerChannel channel) {
                channel.setPersistent(true);
                channel.setLazy(true);
            }
        });


        // Publish to all subscribers
        try {
            ServerChannel channel = bayeuxServer.getChannel(EVENTS_BASE_CHANNEL + "generic");
            channel.publish(sender, om.writeValueAsString(event), null);
        } catch (JsonProcessingException e) {
            log.error("Could not serialize event into JSON");
        }
    }


    public void setBayeuxServer(BayeuxServer bayeuxServer) {
        this.bayeuxServer = bayeuxServer;
    }
}
