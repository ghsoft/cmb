package com.qmatic.ps.bp.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.common.BPExceptionCode;
import com.qmatic.ps.bp.common.Constants;
import com.qmatic.ps.bp.dao.SettingDao;
import com.qmatic.ps.bp.model.Setting;
import com.qmatic.ps.bp.rest.dto.SyncCredentials;
import com.qmatic.ps.bp.startup.SettingsFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-02-18
 * Time: 15:15
 * To change this template use File | Settings | File Templates.
 */

@Component
public class SettingsUtil {

    @Autowired
    SettingDao settingDao;

    @Autowired
    SettingsFactory settingsFactory;

    public SyncCredentials getOrchestraCredentialsFromSettings() {
        try {
            Setting orchestraSettings = readOrchestraSettingGroup();

            HashMap<String, String> map = settingJsonDataToHashMap(orchestraSettings);

            validateOrchestraSettings(map);

            return buildSyncSettings(map);
        } catch (BPException e) {
            return null;
        }
    }



    private Setting readOrchestraSettingGroup() throws BPException {
        Setting orchestraSettings = settingDao.findByGroup(Constants.ORCHESTRA_SETTINGS_GROUP);
        if (orchestraSettings == null || orchestraSettings.getData() == null || orchestraSettings.getData().length() == 0) {
            throw new BPException("Cannot perform sync, no credentials stored in settings", BPExceptionCode.SYNC_ERROR);
        }
        return orchestraSettings;
    }

    private SyncCredentials buildSyncSettings(HashMap<String, String> map) {
        SyncCredentials credentials = new SyncCredentials();
        credentials.setOrchestraUrl(map.get(Constants.ORCHESTRA_URL_KEY));
        credentials.setUsername(map.get(Constants.ORCHESTRA_USERNAME_KEY));
        credentials.setPassword(map.get(Constants.ORCHESTRA_PASSWORD_KEY));
        return credentials;
    }

    private HashMap<String, String> settingJsonDataToHashMap(Setting setting) throws BPException {
        ObjectMapper om = new ObjectMapper();
        HashMap<String, String> map = null;
        try {
            map = om.readValue(setting.getData(), HashMap.class);
        } catch (IOException e) {
            throw new BPException("Could not perform sync with stored credentials: " + e.getMessage(), BPExceptionCode.DESERIALIZATION_ERROR);
        }
        return map;
    }

    private void validateOrchestraSettings(HashMap<String, String> map) throws BPException {
        String[] required = new String[]{Constants.ORCHESTRA_URL_KEY, Constants.ORCHESTRA_USERNAME_KEY, Constants.ORCHESTRA_PASSWORD_KEY};
        for (String key : required) {
            if (!map.containsKey(key)) {
                throw new BPException("Cannot perform sync with stored credentials, the [" + key + "] property is missing", BPExceptionCode.SYNC_ERROR);
            }
        }
    }


}
