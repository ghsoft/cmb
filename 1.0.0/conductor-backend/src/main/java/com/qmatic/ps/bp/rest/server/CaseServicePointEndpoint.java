package com.qmatic.ps.bp.rest.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.common.BPExceptionCode;
import com.qmatic.ps.bp.dao.*;
import com.qmatic.ps.bp.model.*;
import com.qmatic.ps.bp.model.Mark;
import com.qmatic.ps.bp.model.CaseQueue;
import com.qmatic.ps.bp.model.GeneralParams;
import com.qmatic.ps.bp.model.enumeration.Status;
import com.qmatic.ps.bp.rest.client.DeviceManagementServiceClient;
import com.qmatic.ps.bp.rest.client.EntryPointServiceClient;
import com.qmatic.ps.bp.rest.client.ServicePointServiceClient;
import com.qmatic.ps.bp.rest.dto.ExtendedVisitDataDTO;
import com.qmatic.ps.bp.util.CookieHolder;
import com.qmatic.ps.bp.util.HostSettingsUtil;
import com.qmatic.ps.bp.util.SecurityUtil;
import com.qmatic.ps.bp.util.SettingsUtil;
import com.qmatic.qp.api.UnitType;
import com.qmatic.qp.api.connectors.dto.*;
import com.qmatic.qp.exception.QException;
import com.sun.jersey.api.client.UniformInterfaceException;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.*;
import java.util.regex.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;


@Component
@Scope(value = "request")
@Path("/caseservicepoint")
public class CaseServicePointEndpoint {

	private final String inDisplayQueueMessage = "Your ticket is in queue to be called. Please wait for it to be announced.";

    private final Logger log = LoggerFactory.getLogger(CaseServicePointEndpoint.class);

    @Context
    UriInfo uriInfo;

    @Autowired(required = true)
    CaseDao caseDao;

    @Autowired(required = true)
    CaseServiceDao caseServiceDao;
	
	@Autowired(required = true)
    CaseQueueDao caseQueueDao;
	
	@Autowired(required = true)
    ServiceLevelDao serviceLevelDao;

    @Autowired(required = true)
    NoteDao noteDao;

    @Autowired(required = true)
    MarkDao markDao;
	
	@Autowired(required = true)
    CaseCustomerDao caseCustomerDao;

    @Autowired
    SettingsUtil settingsUtil;

    @Context
    protected HttpHeaders headers;

    @Autowired
    EntryPointServiceClient entryPointClient;

    @Autowired
    ServicePointServiceClient servicePointClient;

    @Autowired
    DeviceManagementServiceClient deviceManagementServiceClient;
	
	//****************************************************************************************
	//Customer Functions
	//****************************************************************************************
	
	@POST
    @Path("/customers/{checkExistance}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response createCustomer(@PathParam("checkExistance") String checkExistance, CaseCustomer customer) throws BPException {
        try{
			setSSOCookie();
			boolean exist = false;
			if (checkExistance.equals("true")) {
				if (caseCustomerDao.isCustomerExist(customer)) {
					exist = true;
				}
			}
			if (exist) {
				return Response.serverError().entity("Cannot create customer, customer already exists").build();
			}
			customer = caseCustomerDao.create(customer);
			return Response.ok(customer).build();
        }catch(Exception e){
            log.error("Caught exception in createCustomer: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not create customer : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	/**
     *
     * @param andor : "and": all attributes should match/"or": any attribute should match
	 * @data : 	{
					"id": 0,
					"branchId": 1,
					"firstName":"%gh%",
					"lastName":"%ha%",
					"email":"%",
					"phoneNumber":"%",
					"created": "2015-01-13T00:00:00.000-0500"
				}
     * @return customers
     * @throws BPException
     */
	@PUT
    @Path("/customers/search/{andor}/{limit}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response searchCustomers(@PathParam("andor") String andor, @PathParam("limit") Integer limit, CaseCustomer searchCustomer) throws BPException {
        try{
			setSSOCookie();
			List<CaseCustomer> customers = caseCustomerDao.search(searchCustomer, andor, limit);
            return Response.ok(customers).build();
        }
		catch(Exception e){
            log.error("Caught exception in searchCustomer: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not search customer table : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	@GET
    @Path("/customers/{customerId}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getCustomer(@PathParam("customerId") Long customerId) throws BPException {
        try{
			setSSOCookie();
			CaseCustomer customer = caseCustomerDao.searchById(customerId);
            return Response.ok(customer).build();
        }
		catch(Exception e){
            log.error("Caught exception in getCustomer: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not get customer : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
    /**
     *
     * @param customerId
     * @return
     * @throws BPException
     */
    @PUT
    @Path("/customers/{customerId}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response updateCustomer(@PathParam("customerId") Long customerId, CaseCustomer newCustomer) throws BPException {
        try{
            setSSOCookie();
			CaseCustomer currentCustomer = caseCustomerDao.searchById(customerId);
			
			if (currentCustomer == null) {
				return Response.serverError().entity("Couldn't find specified customer").build();
			}
			
			for (java.lang.reflect.Field newField : newCustomer.getClass().getDeclaredFields()) {
				newField.setAccessible(true);
				if (!newField.getName().equals("id")) {
					for (java.lang.reflect.Field currentField : currentCustomer.getClass().getDeclaredFields()) {
						currentField.setAccessible(true);
						if (newField.get(newCustomer) != null) {
							if (newField.getName().equals(currentField.getName())) {
								currentField.set(currentCustomer, newField.get(newCustomer));
							}
						}
					}
				}
			}
			
			caseCustomerDao.update(currentCustomer);
			
			return Response.ok(currentCustomer).build();
        }catch(Exception e){
            log.error("Caught exception in updateCustomer: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not update customer with id=" + customerId.toString() + " : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	/**
     *
     * @param customerId
     * @return
     * @throws BPException
     */
    @PUT
    @Path("/customers/{currentCustomerId}/merge/{newCustomerId}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response mergeCustomersAndUpdateAllCases(@PathParam("currentCustomerId") Long currentCustomerId, @PathParam("newCustomerId") Long newCustomerId) throws BPException {
        try{
            setSSOCookie();
			
			CaseCustomer currentCustomer = caseCustomerDao.searchById(currentCustomerId);
			CaseCustomer newCustomer = caseCustomerDao.searchById(newCustomerId);
			
			if (currentCustomer == null || newCustomer == null) {
				return Response.serverError().entity("Couldn't find specified customers").build();
			}
			
			List<Case> cases = caseDao.findAllCasesByCustomerId(currentCustomerId);
            for(Case c: cases){
                c.setCaseCustomer(newCustomer);
				caseDao.update(c);
            }
			
			caseCustomerDao.delete(currentCustomer);

			log.info("Executing mergeCustomersAndUpdateAllCases has updated {} Cases to Customer ID {}", cases.size(), newCustomerId);
            return Response.ok("{\"cases\" : \"" + cases.size() + "\"}").build();
        }catch(Exception e){
            log.error("Caught exception in mergeCustomersAndUpdateAllCases: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not update customer with customerId=" + newCustomerId.toString() + " : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	//****************************************************************************************
	//Common Functions
	//****************************************************************************************
	
	@GET
	@Path("/branches")
    @Produces({ MediaType.APPLICATION_JSON })
    @org.codehaus.enunciate.jaxrs.TypeHint(TinyBranch.class)
    public Response getBranches() throws BPException {
        try{
            setSSOCookie();
            List<TinyBranch> allBranches = servicePointClient.getBranches();
            List<TinyBranch> conductorBranches = new ArrayList<TinyBranch>();
            for(TinyBranch b : allBranches){
                BranchInformation branch = servicePointClient.getBranch(b.getId());
				Set<BranchParameter> branchParams = branch.getBranchParameters();
				for(BranchParameter bParam : branchParams){
					if (bParam.getKey().equals("description")) {
						if (bParam.getValue().equals("conductor")) {
							conductorBranches.add(b);
						}
					}
				}
            }
            Collections.sort(conductorBranches, new Comparator<TinyBranch>() {
                @Override
                public int compare(TinyBranch o1, TinyBranch o2) {
                    return o1.getId().compareTo(o2.getId());
                }
            });
            return Response.ok(conductorBranches).build();
        }catch(Exception e){
            log.error("Caught Exception in getBranches: {}, {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not get Branches. : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	//****************************************************************************************
	//Case Functions
	//****************************************************************************************
	
	@POST
	@Path("/branches/{branchId}/cases/entrypoint/{entryPointId}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response createCase(@PathParam("branchId") Integer branchId, @PathParam("entryPointId") Long entryPointId, Case newCase) throws BPException {
        try{
            setSSOCookie();
		
			if (newCase.getCaseCustomer() == null) {
				return Response.serverError().entity("Specify a customer").build();
			}
			
			CaseCustomer caseCustomer = caseCustomerDao.searchById(newCase.getCaseCustomer().getId());
			if (caseCustomer == null) {
				return Response.serverError().entity("Couldn't find specified customer").build();
			}

            VisitParameters visitParams = new VisitParameters();
            //visitParams.addCustomer(newCase.getCaseCustomer().getId());
            int idx = 0;
            for(CaseService cs : newCase.getCaseServices()) {
                visitParams.addServices(cs.getServiceId().intValue());
                cs.setRecyclesLeft(GeneralParams.MAX_RECYCLES);
                cs.setIdx(idx++);
            }
			Visit visit = entryPointClient.createVisit(branchId, entryPointId.intValue(), visitParams, TransferSortPolicy.SORTED);
			
			if (visit == null || visit.getTicketId() == null || visit.getId() == null) {
				return Response.serverError().entity("Couldn't create ticket").build();
			}
            newCase.setTicketNumber(visit.getTicketId());
            newCase.setQpVisitId(visit.getId());

            setQpVisitServiceIdsForCase(newCase, visit);
            for(CaseService cs : newCase.getCaseServices()) {
                Iterator<Note> iterator = cs.getNotes().iterator();
                while(iterator.hasNext()) {
                    Note note = iterator.next();
                    if(note.getText() == null || note.getText().trim().length() == 0) {
                        iterator.remove();
                    }
                }
                cs.setParentCase(newCase);
            }
            // Remove current note if no text content.
            if(!hasCurrentNote(newCase)) {
				newCase.setCurrentNote(null);
            }
			newCase.setCaseCustomer(caseCustomer);

            newCase = caseDao.create(newCase);
			
			Map<String,String> customVisitParams = new HashMap<String, String>();
			customVisitParams.put("custom2", visit.getId().toString());
			customVisitParams.put("custom3", newCase.getId().toString());
			visit = servicePointClient.updateVisitParameters(branchId, visit.getId(), customVisitParams);
			
            return Response.ok(visit).build();
        }catch(Exception e){
            log.error("Caught Exception in createCase: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not create visit: " + e.getMessage(), BPExceptionCode.CREATE_ERROR);
        }

    }
	
	/** Case & Case Services */
    @GET
    @Path("/branches/{branchId}/cases/case/{caseId:[0-9][0-9]*}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getCase(@PathParam("branchId") Integer branchId, @PathParam("caseId") Long caseId) throws BPException {
        try{
            Case entity = caseDao.getCase(branchId, caseId);
            if (entity == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            return Response.ok(entity).build();
        }catch(Exception e){
            log.error("Caught Exception in getCase with id=" + caseId + ": {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not find Case with id " + caseId + " : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	@GET
    @Path("/branches/{branchId}/cases/visit/{visitId:[0-9][0-9]*}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getCaseByVisitId(@PathParam("branchId") Integer branchId, @PathParam("visitId") Long visitId) throws BPException {
        try{
            Case entity = caseDao.getCaseByVisitId(branchId, visitId);
            if (entity == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            for(CaseService cs: entity.getCaseServices()){
                if(cs.getStatus()==Status.WAITING || cs.getStatus()==Status.PENDING || cs.getStatus()==Status.TRASH || cs.getStatus()==Status.NOSHOW || cs.getStatus()==Status.ONHOLD){
                  Long wt = (new Date().getTime() - cs.getCreated().getTime())/1000;
                  cs.setWaitingTime(wt.intValue());
                }
            }
            return Response.ok(entity).build();
        }catch(Exception e){
            log.error("Caught Exception in getCaseByVisitId with visitId=" + visitId + ": {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not find Case with visitId " + visitId + " : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	/**
     * Searches for Case objects matching the supplied search criteria.
     *
     * <li>Search by field: /search?name=John Doe</li>
     * <li>Search by related object: Not applicable to Resources since they don't reference any other entities themselves</li>
     * <li>Partial selection: /search?fields=name,custom</li>
     * <li>Pagination: ?limit=10&offset=1 (return items 10-19</li>
     *
     * @return
     *      A JSON object with the meta, notifications and result elements.
     * @throws BPException
     */
    @GET
    @Path("/branches/{branchId}/cases/search/{searchArg}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response searchCases(@PathParam("branchId") Integer branchId, @PathParam("searchArg") String searchArg) throws BPException {
        try{
            java.util.Map<String,java.util.List<String>> params = uriInfo.getQueryParameters();
            List<Case> all = caseDao.searchCases(branchId, searchArg, params);
            return Response.ok(all).build();
        }catch(Exception e){
            log.error("Caught exception in searchCases: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not search Case: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	/**
     *
     * @param customerId
     * @return
     * @throws BPException
     */
    @GET
    @Path("/branches/{branchId}/cases/customer/{customerId}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response findCasesByCustomerId(@PathParam("branchId") Integer branchId, @PathParam("customerId") Long customerId) throws BPException {
        try{
            List<Case> cases = caseDao.findCasesByCustomerId(branchId, customerId);
            return Response.ok(cases).build();
        }catch(Exception e){
            log.error("Caught exception in findCasesByCustomerId: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not find customer with customerId=" + customerId.toString() + " : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	/**
     * Gets the LATEST case with the specified ticketNumber
     * @param ticketNumber
     * @return
     * @throws BPException
     */
    @GET
    @Path("/branches/{branchId}/cases/ticketnumber/{ticketNumber}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response findCasesByTicketNumber(@PathParam("branchId") Integer branchId, @PathParam("ticketNumber") String ticketNumber) throws BPException {
        try{
            List<Case> cases = caseDao.findCasesByTicketNumber(branchId, ticketNumber);
            return Response.ok(cases).build();
        }catch(Exception e){
            log.error("Caught Exception in findCasesByTicketNumber with ticketNumber=" + ticketNumber + ": {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not find Case with ticketNumber " + ticketNumber + " : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	@GET
    @Path("/branches/{branchId}/cases/date/{date}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response findCasesByDate(@PathParam("branchId") Integer branchId, @PathParam("date") String dateStr) throws BPException {
        try{
			Date date = new java.text.SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
            List<Case> cases = caseDao.findCasesByDate(branchId, date);
            return Response.ok(cases).build();
        }catch(Exception e){
            log.error("Caught exception in findCasesByDate: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not find cases history with date=" + dateStr + " : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	/**
     *
     * @param customerId
     * @return
     * @throws BPException
     */
    @GET
    @Path("/cases/history/customer/{customerId}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response findCasesHistoryByCustomerId(@PathParam("customerId") Long customerId) throws BPException {
        try{
            List<Case> cases = caseDao.findCasesHistoryByCustomerId(customerId);
            return Response.ok(cases).build();
        }catch(Exception e){
            log.error("Caught exception in findCasesHistoryByCustomerId: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not find customer history with customerId=" + customerId.toString() + " : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	@DELETE
    @Path("/branches/{branchId}/cases/case/{caseId:[0-9][0-9]*}")
    public Response deleteCurrentCase(@PathParam("branchId") Integer branchId, @PathParam("caseId") Long caseId) throws BPException {
        try{
            setSSOCookie();
			UserStatus qpUserStatus = servicePointClient.getUserStatus();
			Integer servicePointId = qpUserStatus.getServicePointId();
			
            Case dbCase = caseDao.getCase(branchId, caseId);
            if(dbCase==null){
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            // check that no caseServices are ongoing at any other counter
            for(CaseService cs : dbCase.getCaseServices()){
                if(cs.getServedByCounterId()!= servicePointId && (cs.getStatus()==Status.SERVING || cs.getStatus() == Status.POST_PROCESSING)){
                    return Response.serverError().entity("Cannot Remove this visit. The visit is currently being served").build();
                }
            }
			
			// Prevent deleting a case if a ticket is already called at the counter
			CaseService currentDbCaseService = caseServiceDao.findServingByServicePoint(branchId, servicePointId);

            if(currentDbCaseService!=null){
                return Response.serverError().entity("Cannot Remove this Visit. Another ticket is already called, End serving the called ticket and try again").build();
            }
			// Check if only one remaining service is left then don't add TRASH service, as TRASH will be added anyway after finishing this service.
			if (dbCase.getNumberOfUnservedServices() > 1) {
				CaseService caseService = buildCaseServiceFromName(dbCase, GeneralParams.TRASH_QUEUE);
				addCaseService(dbCase.getBranchId(), dbCase.getId(), caseService);
				moveCaseServiceToTop(caseService);
			}
			// Now override the current service
			CaseService nextDbCaseService = caseServiceDao.getNextCallableCaseService(branchId, caseId);
			overrideCaseService(branchId, caseId, GeneralParams.VISIT_REMOVED_DELIVERED_SERVICE);
			cloneOverriddenService(nextDbCaseService);
			
			updateCaseStatuses(dbCase);
			
			qpUserStatus = servicePointClient.getUserStatus();
            return Response.ok(qpUserStatus).build();

        }catch(Exception e){
            log.error("Caught Exception deleteCurrentCase with caseId=" + caseId + " : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not delete Case: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	//****************************************************************************************
	//Case Service Functions
	//****************************************************************************************
	@POST
	@Path("/branches/{branchId}/cases/case/{caseId:[0-9][0-9]*}/caseservices")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addCaseService(@PathParam("branchId") Integer branchId, @PathParam("caseId") Long caseId, CaseService caseService) throws BPException {
        setSSOCookie();

		Case dbCase = caseDao.getCase(branchId, caseId);	
		UserStatus qpUserStatus = servicePointClient.getUserStatus();
		
		if (qpUserStatus.getVisit() != null && qpUserStatus.getVisit().getId() != null && dbCase.getQpVisitId() != null && qpUserStatus.getVisit().getId().longValue() == dbCase.getQpVisitId().longValue()) { //if the ticket is being served
			if(isVisitInDisplayQueue(qpUserStatus)) {
				throw new BPException(inDisplayQueueMessage, BPExceptionCode.CREATE_ERROR);
			}
		}
		
		try{
			Long visitServiceId = addServiceInQP(dbCase, caseService, qpUserStatus);
			caseService.setQpVisitServiceId(visitServiceId);
            addCaseServiceToCase(dbCase, caseService);
        } catch (BPException e){
            log.error("Caught Exception in addCaseService with caseId=" + caseId + ": {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            removeServiceInQP(caseService, qpUserStatus );
            throw new BPException("Could not add CaseService to caseId: " + caseId + " msg: " + e.getMessage(), BPExceptionCode.CREATE_ERROR);
        }
        return Response.ok(dbCase).build();
    }
	
	/**
     * Returns all case-services on the specified case that has been created since 00:00 today.
     *
     * @param caseId
     * @return
     * @throws BPException
     */
    @GET
    @Path("/branches/{branchId}/cases/case/{caseId:[0-9][0-9]*}/caseservices")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getCaseServices(@PathParam("branchId") Integer branchId, @PathParam("caseId") Long caseId) throws BPException {
        try{
            List<CaseService> caseServices = caseServiceDao.getCaseServices(branchId, caseId);

            return Response.ok(caseServices).build();
        }catch(Exception e){
            log.error("Caught Exception in getCaseServices with caseId=" + caseId + " {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not get Service for today: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	@DELETE
    @Path("/branches/{branchId}/cases/case/{caseId}/caseservices/{caseserviceId}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response removeServiceFromCase(@PathParam("branchId") Integer branchId, @PathParam("caseId") Long caseId, @PathParam("caseserviceId") Long caseserviceId) throws BPException {
        setSSOCookie();
		
		Case dbCase = caseDao.getCase(branchId, caseId);
        
        UserStatus qpUserStatus = servicePointClient.getUserStatus();
		
		if (qpUserStatus.getVisit() != null && qpUserStatus.getVisit().getId() != null && dbCase.getQpVisitId() != null && qpUserStatus.getVisit().getId().longValue() == dbCase.getQpVisitId().longValue()) { //if the ticket is being served
			if(isVisitInDisplayQueue(qpUserStatus)) {
				throw new BPException(inDisplayQueueMessage, BPExceptionCode.CREATE_ERROR);
			}
		}
		
		CaseService caseService = caseServiceDao.findById(caseserviceId);

        if(caseService.getQpVisitServiceId() == null) {
            log.error("Could not remove Service from Case, the caseService had no qpVisitServiceId!!");
            throw new BPException("Could not remove Service from Case. QpVisitServiceId on CaseService: " + caseService.getId() + " is null. ", BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
        removeServiceInQP(caseService, qpUserStatus);

        try{
            removeCaseServiceFromCase(caseService);
        }catch(Exception e){
            log.error("Caught Exception in removeServiceFromCase with caseId=" + caseId + " and caseserviceId=" + caseserviceId + ": {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not remove Service: " + caseService.getServiceId() + " from visit: + " + caseService.getParentCase().getQpVisitId() + " on branch: " + caseService.getParentCase().getBranchId() + " We did not find serviceId on Visit.",  BPExceptionCode.INTERNAL_SERVER_ERROR);

        }
        return Response.ok(qpUserStatus).build();
    }
	
	@PUT
    @Path("/caseservices/{caseserviceId}/move/{direction}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response moveCaseService(@PathParam("caseserviceId") Long caseserviceId, @PathParam("direction") String direction) throws BPException {
        setSSOCookie();
		
		UserStatus qpUserStatus = servicePointClient.getUserStatus();
		if(isVisitInDisplayQueue(qpUserStatus)) {
			return Response.serverError().entity(inDisplayQueueMessage).build();
		}
		
        CaseService caseService = caseServiceDao.findById(caseserviceId);
		
		if (direction.toLowerCase().equals("up")) {
			Case dbCase = moveCaseService(caseService, true);
			return Response.ok(dbCase).build();
		}
		else if (direction.toLowerCase().equals("down")) {
			Case dbCase = moveCaseService(caseService, false);
			return Response.ok(dbCase).build();
		}
		else {
			return Response.serverError().entity("Invalid move").build();
		}
    }
	
	@PUT
    @Path("/caseservices/{caseserviceId}/vip/{addremove}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response vipCaseService(@PathParam("caseserviceId") Long caseserviceId, @PathParam("addremove") String addremove) throws BPException {
        setSSOCookie();
        CaseService caseService = caseServiceDao.findById(caseserviceId);
		
		if (addremove.toLowerCase().equals("add")) {
			caseService.setVip(true);
			return Response.ok(caseService).build();
		}
		else if (addremove.toLowerCase().equals("remove")) {
			caseService.setVip(false);
			return Response.ok(caseService).build();
		}
		else {
			return Response.serverError().entity("Invalid VIP assignment").build();
		}
    }
	
	//****************************************************************************************
	//Delivered Services Functions
	//****************************************************************************************

    @POST
	@Path("/deliveredServices")
    @Produces({ MediaType.APPLICATION_JSON })
    //@org.codehaus.enunciate.jaxrs.TypeHint(DeliveredService.class)
    public Response addDeliveredServices(String deliveredServices) throws BPException {
        try{
            setSSOCookie();
            UserStatus qpUserStatus = servicePointClient.getUserStatus();

            // We must have ongoing visit etc.
            if(qpUserStatus.getVisit() != null && qpUserStatus.getVisit().getCurrentVisitService() != null && deliveredServices.length()!=0){
                CaseService cs = caseServiceDao.findByQpVisitServiceId(qpUserStatus.getVisit().getCurrentVisitService().getId());

                List<String> items = Arrays.asList(deliveredServices.split(","));

                for(String ds : items){
                    Integer deliveredServiceId = Integer.parseInt(ds);
                    qpUserStatus = servicePointClient.addDeliveredService(qpUserStatus.getBranchId(), qpUserStatus.getVisit().getId(), qpUserStatus.getVisit().getCurrentVisitService().getId(), deliveredServiceId);
					//Do NOT add Delivered Services to Conductor where its name is similar to Service name
					List<VisitDeliveredService> visitDeliveredServices = qpUserStatus.getVisit().getCurrentVisitService().getVisitDeliveredServices();
					VisitDeliveredService visitDeliveredService = visitDeliveredServices.get(visitDeliveredServices.size() - 1);
					if (!visitDeliveredService.getDeliveredServiceName().trim().equalsIgnoreCase(cs.getServiceName().trim())) {
						// Add 'Mark' to our case...
						Mark mark = markDao.findMarkByDeliveredServiceId(visitDeliveredService.getDeliveredServiceId());
						if(mark == null) {
							mark = new Mark();
							mark.setName(visitDeliveredService.getDeliveredServiceName());
							//mark.setCode(visitDeliveredService.getVisitOutcome().getOutcomeCode());
							mark.setQpDeliveredServiceId(visitDeliveredService.getDeliveredServiceId());
							mark = markDao.update(mark);
						}

						CaseServiceMark csm = new CaseServiceMark();
						csm.setMark(mark);
						csm.setQpVisitDeliveredServiceId(visitDeliveredService.getId());
						cs.getCaseServiceMarks().add(csm);
					}
                }
                caseServiceDao.update(cs);
            }
            return Response.ok(qpUserStatus).build();
        }catch(Exception e){
            log.error("Caught Exception in addDeliveredServices: {}, {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not Add Delivered Services. : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	/**
     * Retrieves service that are delivered.
     * Ignore Delivered services with NOSHOW name.
     * @param branchId
     *            id of branch
     * @param serviceId
     *            logic id of service.
     * @return List of {@link DeliveredService}. Empty list if none found.
     * @throws QException
     */
	
	@GET
    @Path("/deliveredServices")
    @Produces({ MediaType.APPLICATION_JSON })
    @org.codehaus.enunciate.jaxrs.TypeHint(DeliveredService.class)
    public Response getDeliveredServices() throws BPException {
        try{
            setSSOCookie();
			UserStatus qpUserStatus = servicePointClient.getUserStatus();
            List<DeliveredService> deliveredServices = servicePointClient.getDeliveredServices(qpUserStatus.getBranchId(), qpUserStatus.getVisit().getCurrentVisitService().getServiceId());
            List<DeliveredService> strippedList = new ArrayList<DeliveredService>();
            for(DeliveredService ds : deliveredServices){
                if(!ds.getName().equals(GeneralParams.NOSHOW_DELIVERED_SERVICE) && !ds.getName().equals(GeneralParams.ONHOLD_DELIVERED_SERVICE) && !ds.getName().equals(GeneralParams.QUEUE_CORRECTION_DELIVERED_SERVICE) && !ds.getName().equals(GeneralParams.VISIT_REMOVED_DELIVERED_SERVICE) && !ds.getName().equals(GeneralParams.SERVICE_OVERRIDDEN_DELIVERED_SERVICE)){
                    strippedList.add(ds);
                }
            }
            return Response.ok(strippedList).build();
        }catch(Exception e){
            log.error("Caught Exception in getDeliveredServices: {}, {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not Get Delivered Services. : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }

	@DELETE
    @Path("/deliveredServices/{deliveredServiceId}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response removeDeliveredService(@PathParam("deliveredServiceId") Long deliveredServiceId) throws BPException {
        try{
            setSSOCookie();
            UserStatus qpUserStatus = servicePointClient.getUserStatus();

            // We must have ongoing visit etc.
            if(qpUserStatus.getVisit() != null && qpUserStatus.getVisit().getCurrentVisitService() != null) {
                CaseService cs = caseServiceDao.findByQpVisitServiceId(qpUserStatus.getVisit().getCurrentVisitService().getId());
                qpUserStatus = servicePointClient.removeDeliveredService(qpUserStatus.getBranchId(), qpUserStatus.getVisit().getId(), deliveredServiceId);

                // Remove the delivered Service from our local CaseServiceMark
                caseServiceDao.removeMark(cs.getId(), deliveredServiceId);
                return Response.ok(qpUserStatus).build();
            }
            return Response.serverError().entity("No ongoing visit, cannot remove mark.").build();
        }catch(Exception e){
            log.error("Caught Exception in removeDeliveredService with deliveredServiceId=" + deliveredServiceId + " : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not remove Transaction Type: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	//****************************************************************************************
	//Calling Functions
	//****************************************************************************************

	@POST
    @Path("/branches/{branchId}/servicepoint/{servicePointId}/next")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response callNext(@PathParam("branchId") Integer branchId, @PathParam("servicePointId") Integer servicePointId) throws BPException {
        try {
            setSSOCookie();
            UserStatus qpUserStatus = servicePointClient.getUserStatus();
			if(isVisitInDisplayQueue(qpUserStatus)) {
                return Response.serverError().entity(inDisplayQueueMessage).build();
            }
            // Start userServicePointSession if no exists
            if(qpUserStatus.getUserState() == UserState.NO_STARTED_SERVICE_POINT_SESSION) {
                qpUserStatus = servicePointClient.startUserServicePointSession(branchId, servicePointId, SecurityUtil.getCurrentUserName());
                if(qpUserStatus==null || qpUserStatus.getUserState() == UserState.NO_STARTED_SERVICE_POINT_SESSION ){
                    log.error("Could not start userServicePointSession in callNext");
                    throw new BPException("Could not start userServicePointSession in callNext", BPExceptionCode.VISIT_CALL_FAILED);
                }
            }

            CaseService currentDbCaseService = caseServiceDao.findServingByServicePoint(branchId, servicePointId);

            // End existing Post Processing if any
            if(currentDbCaseService!=null && currentDbCaseService.getStatus()==Status.POST_PROCESSING){
                setCaseServiceStatusToFinished(currentDbCaseService);
            }

            // End existing Face to Face Transaction if any
            if(isOutcomeOrDeliveredServiceNeeded(qpUserStatus)) {
                return Response.serverError().entity("Cannot callNext. Ongoing visit requires Transaction Type or outcome").build();
            }
            if(currentDbCaseService!=null){
                endCurrentCaseServiceVisit(currentDbCaseService, qpUserStatus);
				setEnterTime(currentDbCaseService.getParentCase());
            }

            // Call next visit on Orchestra
            qpUserStatus = servicePointClient.nextVisit(branchId, servicePointId);
            if(qpUserStatus.getVisit() != null) {
                CaseService dbCaseService = caseServiceDao.findByQpVisitServiceId(qpUserStatus.getVisit().getCurrentVisitService().getId());
                if(dbCaseService == null) {
                    log.warn("Could not find an ongoing case service for visit with id {}, has a visit been created outside of the Case workstation/reception?",qpUserStatus.getVisit().getId());
                    return Response.serverError().entity("callNext could not find an ongoing case for the called visit").build();
                }

                // Update our caseService to Serving
                setCaseServiceStatusToServing(dbCaseService, qpUserStatus);
                transferCurrentNoteOntoCaseService(dbCaseService);

            } else {
                //nothing, qpUserStatus will answer that no Visit was called
            }
            return Response.ok(qpUserStatus).build();
        } catch (Exception e) {
            log.error("Caught some kind of exception callNext: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not call visit: " + e.getMessage(), BPExceptionCode.VISIT_CALL_FAILED);
        }
    }
	
    @POST
    @Path("/branches/{branchId}/servicepoint/{servicePointId}/walkdirect")
    @Consumes("application/json")
    @Produces("application/json")
    public Response walkDirect(@PathParam("branchId") Integer branchId, @PathParam("servicePointId") Integer servicePointId, Case wdCase)  throws BPException{
        try{
            setSSOCookie();
            UserStatus qpUserStatus = servicePointClient.getUserStatus();
			if(isVisitInDisplayQueue(qpUserStatus)) {
                return Response.serverError().entity(inDisplayQueueMessage).build();
            }
            CaseService currentDbCaseService = caseServiceDao.findServingByServicePoint(branchId, servicePointId);

            // End existing Post Processing if any
            if(currentDbCaseService!=null && currentDbCaseService.getStatus()==Status.POST_PROCESSING){
                setCaseServiceStatusToFinished(currentDbCaseService);
            }

            // End existing Face to Face Transaction if any
            if(isOutcomeOrDeliveredServiceNeeded(qpUserStatus)) {
                return Response.serverError().entity("Cannot walkDirect. Ongoing visit requires Transaction Type or outcome").build();
            }
            if( currentDbCaseService!=null){
                endCurrentCaseServiceVisit(currentDbCaseService, qpUserStatus);
				setEnterTime(currentDbCaseService.getParentCase());
            }

            // Create walkdirect
            List<Integer> services = new ArrayList<>();
            int idx = 0;
            for(CaseService cs : wdCase.getCaseServices()) {
                removeEmptyNotes(cs);
                services.add(cs.getServiceId());
                cs.setParentCase(wdCase);
                cs.setRecyclesLeft(GeneralParams.MAX_RECYCLES);
                cs.setIdx(idx++);
            }

            VisitParameters entity = new VisitParameters();
            entity.setServices(services);

            qpUserStatus = servicePointClient.createVisitAtServicePoint(branchId,servicePointId,entity);

            setQpVisitServiceIdsForCase(wdCase, qpUserStatus.getVisit());
            wdCase.setTicketNumber(qpUserStatus.getVisit().getTicketId());
            wdCase.setQpVisitId(qpUserStatus.getVisit().getId());
            wdCase.setBranchId(branchId);
            if(hasCurrentNote(wdCase)) {
                Note note = new Note();
                note.setCreatedBy(wdCase.getCurrentNote().getCreatedBy());
                note.setText(wdCase.getCurrentNote().getText());
				note.setCreated(wdCase.getCurrentNote().getCreated());
                wdCase.getCaseServices().get(0).getNotes().add(note);
                //caseServiceDao.update(wdCase.getCaseServices().get(0));
            } else {
                wdCase.setCurrentNote(null);
            }
			
			wdCase.setEntryPointId(new Long(0));
			wdCase.setEntryPointName("Walk Direct");

            wdCase = caseDao.create(wdCase);

            if(qpUserStatus.getVisit()!=null){
                CaseService dbCaseService = wdCase.getCaseServices().get(0);
                //first walkDirect caseService should not be recyclable
                dbCaseService.setRecyclesLeft(0);
                setCaseServiceStatusToServing(dbCaseService, qpUserStatus);
            }

            return Response.ok(qpUserStatus).build();
        }catch(Exception e){
            log.error("Caught Exception in walkDirect: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not do a WalkDirect: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	/**
     * Call a CaseService (e.g. Visit). The actual "call" should already have been performed
     * by the client on the ServicePoint connector (Orchestra).
     *
     * @param caseId
     * @return
     * @throws BPException
     */
    @POST
	@Path("/branches/{branchId}/servicepoint/{servicePointId}/call/case/{caseId}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response callNextOnCase(@PathParam("branchId") Integer branchId, @PathParam("servicePointId") Integer servicePointId, @PathParam("caseId") Long caseId) throws BPException {
        try{
            setSSOCookie();
            UserStatus qpUserStatus = servicePointClient.getUserStatus();
			if(isVisitInDisplayQueue(qpUserStatus)) {
                return Response.serverError().entity(inDisplayQueueMessage).build();
            }
            Case dbCase = caseDao.getCase(branchId, caseId);
			
			if(dbCase!=null && caseDao.isCaseBeingServed(dbCase.getBranchId(), dbCase.getId())){
				return Response.serverError().entity("Ticket " + dbCase.getTicketNumber() + " was already called by another user!").build();
			}
			
            CaseService currentDbCaseService = caseServiceDao.findServingByServicePoint(branchId, servicePointId);

            // End existing Post Processing if any
            if(currentDbCaseService!=null && currentDbCaseService.getStatus()==Status.POST_PROCESSING){
                setCaseServiceStatusToFinished(currentDbCaseService);
            }

            // End existing Face to Face Transaction if any
            if(isOutcomeOrDeliveredServiceNeeded(qpUserStatus)) {
                return Response.serverError().entity("Cannot callNext. Ongoing visit requires Transaction Type").build();
            }
            if(currentDbCaseService!=null){
                endCurrentCaseServiceVisit(currentDbCaseService, qpUserStatus);
				setEnterTime(currentDbCaseService.getParentCase());
            }

            // First, call visit in Orchestra
            qpUserStatus = servicePointClient.callVisit(dbCase.getBranchId(), servicePointId, dbCase.getQpVisitId().intValue());
            if(qpUserStatus.getVisit() == null) {
                // Call Visit failed for some reason
                throw new BPException("Call Visit failed on Orchestra side.", BPExceptionCode.VISIT_CALL_FAILED);
            } else {
                log.info("Call Visit returned VisitState: " + qpUserStatus.getVisitState().toString());
                log.info("Call Visit returned UserState: " + qpUserStatus.getUserState().toString());
            }

            CaseService dbCalledCaseService = caseServiceDao.findByQpVisitServiceId(qpUserStatus.getVisit().getCurrentVisitService().getId());
            if(dbCalledCaseService == null) {
                log.warn("Could not find an ongoing case service for visit with id {}, has a visit been created outside of the Case workstation/reception?",qpUserStatus.getVisit().getId());
                return Response.serverError().entity("callNextOnCase could not find an ongoing case for the called visit").build();
            }

            // Update our called caseService to serving
            setCaseServiceStatusToServing(dbCalledCaseService, qpUserStatus);
            transferCurrentNoteOntoCaseService(dbCalledCaseService);

            return Response.ok(qpUserStatus).build();
        }catch(Exception e){
            log.error("Caught Exception in callNextOnCase: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not call Next service on Visit: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }

	@PUT
	@Path("/branches/{branchId}/servicepoint/{servicePointId}/call/service/{qpVisitServiceId}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response callService(@PathParam("branchId") Integer branchId, @PathParam("servicePointId") Integer servicePointId, @PathParam("qpVisitServiceId") Long qpVisitServiceId) throws BPException {
        try{
			setSSOCookie();
            UserStatus qpUserStatus = servicePointClient.getUserStatus();
			if(isVisitInDisplayQueue(qpUserStatus)) {
                return Response.serverError().entity(inDisplayQueueMessage).build();
            }
            CaseService servingDbCaseService = caseServiceDao.findServingByServicePoint(branchId, servicePointId);
			CaseService newDbCaseService = caseServiceDao.findByQpVisitServiceId(qpVisitServiceId);
			if (newDbCaseService == null) {
				return Response.serverError().entity("Cannot get this service to call it!").build();
			}
			
			// End existing Face to Face Transaction if any
			if(isOutcomeOrDeliveredServiceNeeded(qpUserStatus)) {
				return Response.serverError().entity("Cannot callService. Ongoing visit requires Transaction Type").build();
			}
			
			Case newDbCase = newDbCaseService.getParentCase();
			if (servingDbCaseService != null) { //Service Point is SERVING
				Case servingDbCase = servingDbCaseService.getParentCase();
				//The case from where the new service will be called is equal to the currently being served case, so use native serveService API
				if (servingDbCase.getId() == newDbCase.getId()) {
					moveCaseServiceToTop(newDbCaseService);
					
					// Only end the CaseService, callService needs an ongoing visit to work
					setCaseServiceStatusToFinished(servingDbCaseService);
					caseDao.update(servingDbCaseService.getParentCase());
					setEnterTime(newDbCaseService.getParentCase());
					
					// call the Service in Orchestra
					qpUserStatus = servicePointClient.serveService(qpUserStatus.getBranchId(), newDbCaseService.getParentCase().getQpVisitId(), qpVisitServiceId);
					if(qpUserStatus.getVisit() == null || qpUserStatus.getVisit().getCurrentVisitService() == null) {
						throw new BPException("serveService failed on Orchestra side.", BPExceptionCode.VISIT_CALL_FAILED);
					}

					// Update our called caseService to serving
					setCaseServiceStatusToServing(newDbCaseService, qpUserStatus);
					transferCurrentNoteOntoCaseService(newDbCaseService);
				}
				else {
					if(newDbCase!=null && caseDao.isCaseBeingServed(newDbCase.getBranchId(), newDbCase.getId())){
						return Response.serverError().entity("Ticket " + newDbCase.getTicketNumber() + " was already called by another user!").build();
					}
					endCurrentCaseServiceVisit(servingDbCaseService, qpUserStatus);
					setEnterTime(servingDbCaseService.getParentCase());

					qpUserStatus = callServiceFromQueue(qpUserStatus, branchId, servicePointId, qpVisitServiceId);
				}
			}
			else {
				if(newDbCase!=null && caseDao.isCaseBeingServed(newDbCase.getBranchId(), newDbCase.getId())){
					return Response.serverError().entity("Ticket " + newDbCase.getTicketNumber() + " was already called by another user!").build();
				}
				qpUserStatus = callServiceFromQueue(qpUserStatus, branchId, servicePointId, qpVisitServiceId);
			}
			return Response.ok(qpUserStatus).build();
        }catch(Exception e){
            log.error("Caught Exception in callService with qpVisitServiceId=" + qpVisitServiceId + " : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not Call Service: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	//****************************************************************************************
	//Calling Actions Functions
	//****************************************************************************************
	
	@PUT
	@Path("/branches/{branchId}/cases/visit/{visitId}/hold")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response holdVisit(@PathParam("branchId") Integer branchId, @PathParam("visitId") Long visitId) throws BPException {
        setSSOCookie();
        try{
            UserStatus qpUserStatus = servicePointClient.getUserStatus();
			if(isVisitInDisplayQueue(qpUserStatus)) {
                return Response.serverError().entity(inDisplayQueueMessage).build();
            }

            CaseService dbCurrentCaseService = caseServiceDao.findServingByServicePoint(branchId, qpUserStatus.getServicePointId());
			Case dbCase = dbCurrentCaseService.getParentCase();

            if(qpUserStatus.getVisit() != null && qpUserStatus.getVisit().getCurrentVisitService() != null) {
                qpUserStatus = addDeliveredServiceByName(qpUserStatus, GeneralParams.ONHOLD_DELIVERED_SERVICE);
            }
			else {
				return Response.serverError().entity("This visit is not called").build();
			}
			
			CaseService onholdCaseService = buildCaseServiceFromName(dbCase, GeneralParams.ONHOLD_QUEUE);
			addCaseService(dbCase.getBranchId(), dbCase.getId(), onholdCaseService);
			onholdCaseService.setVip(dbCurrentCaseService.getVip());
			moveCaseServiceToTop(onholdCaseService);
			qpUserStatus = servicePointClient.getUserStatus();
			qpUserStatus = endCurrentCaseServiceVisit(dbCurrentCaseService, qpUserStatus);
			setEnterTime(onholdCaseService.getParentCase());
			updateCaseStatuses(dbCase);
            qpUserStatus = servicePointClient.getUserStatus();
            return Response.ok(qpUserStatus).build();
        }catch(Exception e){
            log.error("Caught Exception in holdVisit with visitId=" + visitId.toString() + " : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not put ONHOLD for visitId=" + visitId.toString() + " : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	@PUT
	@Path("/branches/{branchId}/cases/visit/{visitId}/noshow")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response noShowVisit(@PathParam("branchId") Integer branchId, @PathParam("visitId") Long visitId) throws BPException {
        setSSOCookie();
        try{
            UserStatus qpUserStatus = servicePointClient.getUserStatus();
			if(isVisitInDisplayQueue(qpUserStatus)) {
                return Response.serverError().entity(inDisplayQueueMessage).build();
            }
            CaseService dbCurrentCaseService = caseServiceDao.findServingByServicePoint(branchId, qpUserStatus.getServicePointId());
			Case dbCase = dbCurrentCaseService.getParentCase();
			
			if(qpUserStatus.getVisit() != null && qpUserStatus.getVisit().getCurrentVisitService() != null) {
				if(isOutcomeOrDeliveredServiceNeeded(qpUserStatus)){
					qpUserStatus = addDeliveredServiceByName(qpUserStatus, GeneralParams.NOSHOW_DELIVERED_SERVICE);
				}
				else {
					return Response.serverError().entity("Cannot No Show this service. A Transaction Type was already added").build();
				}
            }
			else {
				return Response.serverError().entity("This visit is not called").build();
			}
			
			CaseService noshowCaseService = buildCaseServiceFromName(dbCase, GeneralParams.NOSHOW_QUEUE);
			addCaseService(dbCase.getBranchId(), dbCase.getId(), noshowCaseService);
			moveCaseServiceToTop(noshowCaseService);
			qpUserStatus = servicePointClient.getUserStatus();
			qpUserStatus = endCurrentCaseServiceVisit(dbCurrentCaseService, qpUserStatus);
			setEnterTime(noshowCaseService.getParentCase());
			updateCaseStatuses(dbCase);
            qpUserStatus = servicePointClient.getUserStatus();
            return Response.ok(qpUserStatus).build();
			
        }catch(Exception e){
            log.error("Caught Exception in noShowVisit with visitId=" + visitId.toString() + " : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not do a NoShow on visitId=" + visitId.toString() + " : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	@PUT
	@Path("/branches/{branchId}/servicePoints/{servicePointId}/visit/recall")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response recallVisit(@PathParam("branchId") Integer branchId, @PathParam("servicePointId") Integer servicePointId) throws BPException {
        try{
            setSSOCookie();
			UserStatus qpUserStatus = servicePointClient.getUserStatus();
			if(isVisitInDisplayQueue(qpUserStatus)) {
                return Response.serverError().entity(inDisplayQueueMessage).build();
            }
            qpUserStatus = servicePointClient.recallVisit(branchId, servicePointId);
            return Response.ok(qpUserStatus).build();
        }catch(Exception e){
            log.error("Caught Exception in recallVisit: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not do a reCall on servicePointId=" + servicePointId + " : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }

	@PUT
    @Path("/branches/{branchId}/servicePoints/{servicePointId}/visit/recycle")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response recycleVisit(@PathParam("branchId") Integer branchId, @PathParam("servicePointId") Integer servicePointId) throws BPException {
        try{
            setSSOCookie();
            UserStatus qpUserStatus = servicePointClient.getUserStatus();
            if(qpUserStatus == null) {
                return Response.serverError().build();
            }
			if(isVisitInDisplayQueue(qpUserStatus)) {
                return Response.serverError().entity(inDisplayQueueMessage).build();
            }
			CaseService dbCurrentCaseService = caseServiceDao.findServingByServicePoint(branchId, servicePointId);
            dbCurrentCaseService.setServingTime(qpUserStatus.getVisit().getTimeSinceCalled());

            if(dbCurrentCaseService.getRecyclesLeft()== 0){
                return noShowVisit(branchId,qpUserStatus.getVisit().getId());
            }
            qpUserStatus = servicePointClient.recycleVisit(branchId, servicePointId);

            setCaseServiceStatusToRecycled(dbCurrentCaseService);

            return Response.ok(qpUserStatus).build();

        }catch(Exception e){
            log.error("Caught Exception in recycleVisit with servicePoint=" + servicePointId + " : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not do a reCycle on servicePointId=" + servicePointId + " : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }

	@POST
    @Path("/branches/{branchId}/cases/case/{caseId}/overrideService")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response overrideCaseService(@PathParam("branchId") Integer branchId, @PathParam("caseId") Long caseId, String deliveredService) throws BPException {
		try{
			// Check if a current service is called
			setSSOCookie();
            UserStatus qpUserStatus = servicePointClient.getUserStatus();
			if(isVisitInDisplayQueue(qpUserStatus)) {
                return Response.serverError().entity(inDisplayQueueMessage).build();
            }
			Case dbCase = caseDao.getCase(branchId, caseId);
			if(dbCase==null){
				return Response.status(Response.Status.NOT_FOUND).build();
			}
			// check that no caseServices are ongoing
			for(CaseService cs : dbCase.getCaseServices()){
				if(cs.getStatus()==Status.SERVING || cs.getStatus() == Status.POST_PROCESSING){
					return Response.serverError().entity("Cannot Override this service. The visit is currently being served").build();
				}
			}
			
			// Prevent finishing a service if a ticket is already called at the counter
			Integer servicePointId = qpUserStatus.getServicePointId();
			CaseService currentDbCaseService = caseServiceDao.findServingByServicePoint(branchId, servicePointId);

            if(currentDbCaseService!=null){
                return Response.serverError().entity("Cannot Override this service. Another ticket is already called, End serving the called ticket and try again").build();
            }

			// First, transfer the visit to the SILENTCALL queue
			TransferParams parameters = new TransferParams();
			parameters.setSortPolicy(TransferSortPolicy.SORTED);
			parameters.setFromBranchId(branchId);
			parameters.setVisitId(dbCase.getQpVisitId());
			parameters.setFromId(servicePointId);
			Integer queueId = getQueueIdFromName(branchId, GeneralParams.SILENTCALL_QUEUE);
			// if queue is not found, they forgot to set one up
			if(queueId!=-1){
				servicePointClient.transferVisitToQueue(branchId, queueId, parameters);
			}else{
				return Response.serverError().entity("Cannot Override this service. " + GeneralParams.SILENTCALL_QUEUE + " Queue does not exist").build();
			}
			
			// call visit in Orchestra
			qpUserStatus = servicePointClient.callVisit(dbCase.getBranchId(), servicePointId, dbCase.getQpVisitId().intValue());
			if(qpUserStatus.getVisit() == null) {
				return Response.serverError().entity("Cannot Override this service. Visit cannot be called").build();
			}

			CaseService dbCalledCaseService = caseServiceDao.findByQpVisitServiceId(qpUserStatus.getVisit().getCurrentVisitService().getId());
			if(dbCalledCaseService == null) {
				return Response.serverError().entity("Cannot Override this service. Cannot find a case for this service").build();
			}

			// Update our called caseService to serving
			setCaseServiceStatusToServing(dbCalledCaseService, qpUserStatus);
			transferCurrentNoteOntoCaseService(dbCalledCaseService);
				
			//Adding Queue Correction/Visit Removed as delivered service
			if (deliveredService.equals(GeneralParams.VISIT_REMOVED_DELIVERED_SERVICE)) {
				qpUserStatus = addDeliveredServiceByName(qpUserStatus, GeneralParams.VISIT_REMOVED_DELIVERED_SERVICE);
			}
			else if (deliveredService.equals(GeneralParams.SERVICE_OVERRIDDEN_DELIVERED_SERVICE)) {
				qpUserStatus = addDeliveredServiceByName(qpUserStatus, GeneralParams.SERVICE_OVERRIDDEN_DELIVERED_SERVICE);
			}
			else {
				qpUserStatus = addDeliveredServiceByName(qpUserStatus, GeneralParams.QUEUE_CORRECTION_DELIVERED_SERVICE);
			}
			
			//End service
			qpUserStatus = endCurrentCaseServiceVisit(dbCalledCaseService, qpUserStatus);
			dbCalledCaseService.setStatus(Status.OVERRIDDEN);
			
			return Response.ok(qpUserStatus).build();
        }catch(Exception e){
            log.error("Caught Exception in overrideCaseService: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not Override service on Visit: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
    
	/**
     * Call this when you want to end a service without performing any post-processing.
     *
     * @param caseId
     * @param qpVisitServiceId
     *      VisitService ID from Orchestra
     * @return
     * @throws BPException
     */
    @PUT
    @Path("/branches/{branchId}/cases/case/{caseId}/caseservice/{qpVisitServiceId}/end")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response endFaceToFaceService(@PathParam("branchId") Integer branchId, @PathParam("caseId") Long caseId, @PathParam("qpVisitServiceId") Long qpVisitServiceId) throws BPException {
        try{
            setSSOCookie();
            UserStatus qpUserStatus= servicePointClient.getUserStatus();
			if(isVisitInDisplayQueue(qpUserStatus)) {
                return Response.serverError().entity(inDisplayQueueMessage).build();
            }
			if(isOutcomeOrDeliveredServiceNeeded(qpUserStatus)) {
				return Response.serverError().entity("Cannot End Service. Ongoing visit requires Transaction Type").build();
			}
            CaseService currentDbCaseService = caseServiceDao.findByQpVisitServiceId(qpVisitServiceId);
            if(currentDbCaseService!=null){
                qpUserStatus = endCurrentCaseServiceVisit(currentDbCaseService, qpUserStatus);
				setEnterTime(currentDbCaseService.getParentCase());
            }else{
                log.error("Caught Exception in endFaceToFaceService, could not find CaseService with qpVisitServiceId: " + qpVisitServiceId );
                return Response.serverError().entity("Cannot endFaceToFaceService on Case, could not find CaseService in Serving status!").build();
            }
            return Response.ok(qpUserStatus).build();
        }catch(Exception e){
            log.error("Caught Exception in endFaceToFaceService: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not end Service transaction. : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @param caseId
     * @param qpVisitServiceId
     *
     * @return
     * @throws BPException
     */
    @PUT
    @Path("/branches/{branchId}/cases/case/{caseId}/caseservice/{qpVisitServiceId}/postProcess/start")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response startPostProcessing(@PathParam("branchId") Integer branchId, @PathParam("caseId") Long caseId, @PathParam("qpVisitServiceId") Long qpVisitServiceId) throws BPException {
        try{
            setSSOCookie();

            UserStatus qpUserStatus = servicePointClient.getUserStatus();
			if(isVisitInDisplayQueue(qpUserStatus)) {
                return Response.serverError().entity(inDisplayQueueMessage).build();
            }
			CaseService dbCaseService = caseServiceDao.findByQpVisitServiceId(qpVisitServiceId);
            // Call endVisit in Orchestra
            if(qpUserStatus != null) {
                qpUserStatus = endCurrentCaseServiceVisit(dbCaseService, qpUserStatus);
				setEnterTime(dbCaseService.getParentCase());

                setCaseServiceStatusToPostProcessing(dbCaseService);
            }
            return Response.ok(qpUserStatus).build();

        }catch(Exception e){
            log.error("Caught Exception in startPostProcessing of qpVisitServiceId=" + qpVisitServiceId + " : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not start post processing of VisitServiceId: " + qpVisitServiceId + "", BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Typically, we won't have a QPVisitData object available on endPostProcessing since the Orchestra
     * visit is already in another queue or similar.
     *
     * @param caseId
     * @param qpVisitServiceId
     * @return
     * @throws BPException
     */
    @PUT
    @Path("/branches/{branchId}/cases/case/{caseId}/caseservice/{qpVisitServiceId}/postProcess/end")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response endPostProcessing(@PathParam("branchId") Integer branchId, @PathParam("caseId") Long caseId, @PathParam("qpVisitServiceId") Long qpVisitServiceId) throws BPException {
        try{
            setSSOCookie();

            CaseService dbCurrentCaseService = caseServiceDao.findByQpVisitServiceId(qpVisitServiceId);
            if(dbCurrentCaseService != null && dbCurrentCaseService.getStatus()==Status.POST_PROCESSING){
                setCaseServiceStatusToFinished(dbCurrentCaseService);
            }
            return Response.ok(servicePointClient.getUserStatus()).build();
        }catch(Exception e){
            log.error("Caught Exception in endPostProcessing of qpVisitServiceId=" + qpVisitServiceId + " {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not end post processing: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	//****************************************************************************************
	//Queues Functions
	//****************************************************************************************
    
	@GET
    @Path("/branches/{branchId}/queues")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getQueues(@PathParam("branchId") Integer branchId) throws BPException {
		try {
			setSSOCookie();
			
			//List<CaseQueue> allCaseQueues = new ArrayList<>();
			List<CaseQueue> allCaseQueues = caseQueueDao.getQueuesInfo(branchId);
			List<Service> services = servicePointClient.getServices(branchId);
			for(Service service : services) {
				Boolean queueFound = false;
				for(CaseQueue queue : allCaseQueues) {
					if(service.getId() == queue.getId()) {
						queueFound = true;
						break;
					}
				}
				if (!queueFound) {
					allCaseQueues.add(new CaseQueue(service.getId(), service.getInternalName()));
				}
			}
			Collections.sort(allCaseQueues, new Comparator<CaseQueue>() {
                @Override
                public int compare(CaseQueue o1, CaseQueue o2) {
                    return o1.getId().compareTo(o2.getId());
                }
            });
			
			return Response.ok(allCaseQueues).build();
		}
		catch(Exception e){
            log.error("Caught Exception getQueues : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not get Queues: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	 
	@GET
    @Path("/branches/{branch}/queues/{queueId}/cases")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getCasesInQueue(@PathParam("branch") Integer branchId, @PathParam("queueId") Integer queueId) throws BPException {
		try {
			setSSOCookie();
			MultivaluedMap<String,String> params = uriInfo.getQueryParameters();
			List<ExtendedVisitDataDTO> allVisits = new ArrayList<ExtendedVisitDataDTO>();
			Service service = servicePointClient.getService(branchId, queueId);
			if (service != null) {
				String serviceStatus = "";
				if (service.getInternalName().equals(GeneralParams.ONHOLD_QUEUE) || service.getInternalName().equals(GeneralParams.NOSHOW_QUEUE) || service.getInternalName().equals(GeneralParams.TRASH_QUEUE)) {
					if (service.getInternalName().equals(GeneralParams.ONHOLD_QUEUE)) {
						serviceStatus = Status.ONHOLD.toString();
					}
					else if (service.getInternalName().equals(GeneralParams.NOSHOW_QUEUE)) {
						serviceStatus = Status.NOSHOW.toString();
					}
					else if (service.getInternalName().equals(GeneralParams.TRASH_QUEUE)) {
						serviceStatus = Status.TRASH.toString();
					}
				}
				allVisits = caseDao.getWaitingCaseVisits(branchId, service.getId(), serviceStatus, params);
			}
			return Response.ok(allVisits).build();
		}
		catch(Exception e){
            log.error("Caught Exception getCasesInQueue : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not get Cases in Queue: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	//****************************************************************************************
	//Orchestra Standard Functions
	//****************************************************************************************
    
	/**
     * Starts a service point session for the currently logged in user (as per the Shiro context)
     *
     * @param branchId
     * @param servicePointId
     * @return
     * @throws BPException
     */
	 
	@PUT
    @Path("/branches/{branchId}/servicePoints/{servicePointId}/start")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response startUserServicePointSession(@PathParam("branchId") Integer branchId, @PathParam("servicePointId") Integer servicePointId) throws BPException {
        setSSOCookie();
        UserStatus qpUserStatus = null;
        try {
            qpUserStatus = servicePointClient.startUserServicePointSession(branchId, servicePointId, SecurityUtil.getCurrentUserName());
        } catch (UniformInterfaceException e) {
            if(e.getResponse().getStatus() == Response.Status.CONFLICT.getStatusCode()) {
                String error_message = e.getResponse().getHeaders().get("ERROR_MESSAGE").get(0);
                return Response.status(Response.Status.CONFLICT).entity(error_message).build();
            }
            log.error(e.getMessage());
            return Response.status(e.getResponse().getStatus()).build();
        }
        return Response.ok(qpUserStatus).build();
    }
	
	/**
     * Mimics the original workstation js terminal closeWorkstation "flow".
     *
     * @return
     */
	 
	@DELETE
    @Path("/branches/{branchId}/servicePoints/{servicePointId}/users/{userName}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response closeWorkstation() throws BPException {
        try{
            setSSOCookie();

            UserStatus qpUserStatus = servicePointClient.getUserStatus();
			if(isVisitInDisplayQueue(qpUserStatus)) {
                return Response.serverError().entity(inDisplayQueueMessage).build();
            }
            CaseService currentDbCaseService = caseServiceDao.findServingByServicePoint(qpUserStatus.getBranchId(), qpUserStatus.getServicePointId());

            // End existing Post Processing if any
            if(currentDbCaseService != null && currentDbCaseService.getStatus()==Status.POST_PROCESSING){
                setCaseServiceStatusToFinished(currentDbCaseService);
            }

            // End existing Face to Face Transaction if any
            if(isOutcomeOrDeliveredServiceNeeded(qpUserStatus)) {
                return Response.serverError().entity("Cannot close Service Point. Ongoing visit requires Transaction Type or outcome").build();
            }

            if(currentDbCaseService!=null){
                endCurrentCaseServiceVisit(currentDbCaseService, qpUserStatus);
				setEnterTime(currentDbCaseService.getParentCase());
            }

            // Close servicePoint
            qpUserStatus = servicePointClient.endUserServicePointSession(qpUserStatus.getBranchId(), qpUserStatus.getServicePointId(), SecurityUtil.getCurrentUserName());
            return Response.ok(qpUserStatus).build();

        }catch(Exception e){
            log.error("Caught Exception in closeWorkstation: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not Close Workstation: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	@GET
	@Path("/branches/{branchId}/entryPoints/deviceTypes/{deviceType}")
    @Produces({ MediaType.APPLICATION_JSON })
    @org.codehaus.enunciate.jaxrs.TypeHint(EntryPoint.class)
    public List<EntryPoint> getEntryPointsByDeviceType(@PathParam("branchId") Integer branchId, @PathParam("deviceType") String deviceType)
            throws BPException {
        try{
            setSSOCookie();
            return entryPointClient.getEntryPointsByDeviceType(branchId, deviceType);
        }catch (Exception e){
            log.error("Caught Exception in getEntryPointsByDeviceType with branchId: "+ branchId + " and deviceType: " + deviceType + " error: " + e.getMessage());
            throw new BPException("Could not get entrypoints with type: " + deviceType + " on branchId: " + branchId, BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }

	@GET
    @Path("/branches/{branchId}/services")
    @Produces({ MediaType.APPLICATION_JSON })
    @org.codehaus.enunciate.jaxrs.TypeHint(Service.class)
    public Response getServices(@PathParam("branchId") Integer branchId ) throws BPException {
        try{
            setSSOCookie();
            List<Service> services = servicePointClient.getServices(branchId);
            List<Service> strippedList = new ArrayList<Service>();
            for(Service s : services){
                if(!s.getInternalName().equals(GeneralParams.TRASH_QUEUE) && !s.getInternalName().equals(GeneralParams.ONHOLD_QUEUE) && !s.getInternalName().equals(GeneralParams.NOSHOW_QUEUE) && !s.getInternalName().equals(GeneralParams.SILENTCALL_QUEUE) && !s.getInternalName().equals(GeneralParams.NEVERCALL_QUEUE)){
                    strippedList.add(s);
                }
            }
            Collections.sort(strippedList, new Comparator<Service>() {
                @Override
                public int compare(Service o1, Service o2) {
                    return o1.getId().compareTo(o2.getId());
                }
            });
            return Response.ok(strippedList).build();
        }catch(Exception e){
            log.error("Caught Exception in getServices: {}, {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not get Services. : " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	//****************************************************************************************
	//Service Level Functions
	//****************************************************************************************
	
	/**
		POST body
		{
			"serviceId": 67, //0 is the default
			"serviceName": null, //this is optional
			"f2f": 20,
			"pp": 30,
			"wt": 15
		}
	*/
	@POST
	@Path("/sl/services")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addServiceLevels(ServiceLevel sl) throws BPException {
		try{
			setSSOCookie();
			
			if (sl.getF2f() == null || sl.getPp() == null || sl.getServiceId() ==  null) {
				return Response.serverError().entity("Please specify all values correctly.").build();
			}
			
			if (sl.getF2f() < 0 || sl.getPp() < 0) {
				return Response.serverError().entity("Please specify positive values").build();
			}

			ServiceLevel serviceLevel = serviceLevelDao.getServiceLevel(sl.getServiceId());
			sl.setTt(sl.getF2f() + sl.getPp());
			
			if (serviceLevel == null) {
				sl = serviceLevelDao.create(sl);
			}
			else {
				sl.setId(serviceLevel.getId());
				sl = serviceLevelDao.update(sl);
			}
			
			return Response.ok(sl).build();
		
        }catch (BPException e){
            log.error("Caught Exception in addServiceLevels : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not add addServiceLevels  msg: " + e.getMessage(), BPExceptionCode.CREATE_ERROR);
        }
    }
	
	@GET
    @Path("/sl/services")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getServicesSL() throws BPException {
		try {
			setSSOCookie();
			List<ServiceLevel> serviceLevels = serviceLevelDao.getServiceLevels();
			
			return Response.ok(serviceLevels).build();
		}
		catch(Exception e){
            log.error("Caught Exception getServicesSL : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not get Service Levels: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	@GET
    @Path("/sl/services/{serviceId}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getServiceSL(@PathParam("serviceId") Integer serviceId) throws BPException {
		try {
			setSSOCookie();
			ServiceLevel serviceLevel = serviceLevelDao.getServiceLevel(serviceId);
			
			return Response.ok(serviceLevel).build();
		}
		catch(Exception e){
            log.error("Caught Exception getServiceSL : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not get Service Level for the specified service: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }

    //****************************************************************************************
	//Notes Functions
	//****************************************************************************************
	
	@POST
    @Path("/notes/case/{caseId}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response createCaseNote(@PathParam("caseId") Long caseId, CaseNote note) throws BPException {
        try{
            Case dbCase = caseDao.findById(caseId);
            dbCase.setCurrentNote(note);
            caseDao.update(dbCase);
            return Response.ok(dbCase.getCurrentNote()).build();
        }catch(Exception e){
            log.error("Caught Exception in createCaseNote on case=" + caseId + " : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not create note: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	/**
     * @deprecated
     *      Shouldn't be used anymore. All creation of notes on CaseService objects should happen internally at call time after
     *      changes 2014-02.
     *
     * @param caseserviceId
     * @param note
     * @return
     * @throws BPException
     */
    @POST
    @Path("/notes/caseservice/{caseserviceId}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response createCaseServiceNote(@PathParam("caseserviceId") Long caseserviceId, Note note) throws BPException {
        try{
            CaseService caseService = caseServiceDao.findById(caseserviceId);
            caseService.getNotes().add(note);
            caseServiceDao.update(caseService);
            return Response.ok(note).build();
        }catch(Exception e){
            log.error("Caught Exception in createCaseServiceNote on caseserviceId=" + caseserviceId + " : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not create note: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	/**
     * Sets the supplied CaseNote as currentNote on the case.
     *
     *
     * 1.	Note entered once when ticket is created, ie not when adding the services
     - KM: Notes are not added to individual services but note can be added at any point after creation as well (example: when waiting in queue)
     2.	The note entered when the ticket is created is saved to the first pending service
     -	KM: The note entered is saved as the active note.  This note can still be edited before the first pending service is called.
     3.	The note is displayed in the GUI with that service and when called on screen 10.
     -	KM: the active note is displayed in the GUI
     4.	The note is saved to the service AS IT IS WHEN THE TICKET/SERVICE IS CALLED.  (this is where we didn’t understand; we understood the edited note saved to this service when it was finished)
     -KM : correct.  The note that exists at the time the service is called is burned in as transactional data for that service.
     5.	Any edits made to the note, either when called or from the queue list, are saved to the NEXT pending/waiting service.
     -	KM: Any edits made are saved to the active noted.  This note can still be edited before the next service is called.
     */
    @PUT
    @Path("/notes/case/{caseId}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateCurrentNoteOnCase(@PathParam("caseId") Long caseId, CaseNote note) throws BPException {
        try{
            note.setCreated(new Date());
            Case dbCase = caseDao.findById(caseId);
            dbCase.setCurrentNote(note);
            caseDao.update(dbCase);
            return Response.ok(dbCase.getCurrentNote()).build();
        }catch(Exception e){
            throw new BPException("Could not update Note: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	/**
      *
      * @deprecated
      *      Shouldn't be used anymore. All creation of notes on CaseService objects should happen internally at call time after
      *      changes 2014-02.
      *
     * @param noteId
     * @param note
     * @return
     * @throws BPException
     */
    @PUT
    @Path("/notes/note/{noteId}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateNote(@PathParam("noteId") Long noteId, Note note) throws BPException {
        try{
            note.setId(noteId);
            note = (Note) noteDao.update(note);
            return Response.ok(note).build();
        }catch(Exception e){
            log.error("Caught Exception in updateNote with noteId=" + noteId + " {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not update Note: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	@GET
    @Path("/notes/caseservice/{caseserviceId}")
    @Produces("application/json")
    public Response getCaseServiceNotes(@PathParam("caseserviceId") Long caseserviceId) throws BPException {
        try{
            MultivaluedMap<String,String> params = uriInfo.getQueryParameters();
            List<Note> notes = caseServiceDao.getNotesOfCaseService(caseserviceId, params);
            return Response.ok(notes).build();
        }catch(Exception e){
            log.error("Caught Exception in getCaseServiceNotes with caseserviceId=" + caseserviceId + " : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not get notes for CaseService: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }

	/**
     * Find and return the entity identified by the supplied primary key ID.
     *
     * @param id
     *      Primary key (numeric) of an entity of the type that this endpoint serves
     * @return
     *      HTTP 200 and the identified entity if found
     *      HTTP 404 if not found
     * @throws BPException
     */
    @GET
    @Path("/notes/note/{id:[0-9][0-9]*}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response findNoteById(@PathParam("id") Long id) throws BPException {
        try{
            Note entity = (Note) noteDao.findById(id);
            if (entity == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            return Response.ok(entity).build();
        }catch(Exception e){
            log.error("Caught Exception in findNoteById : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not find note: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }

    @DELETE
    @Path("/notes/note/{id:[0-9][0-9]*}")
    public Response deleteNote(@PathParam("id") Long id) throws BPException {
        try{
            Note entity = (Note) noteDao.findById(id);
            if (entity == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            noteDao.deleteById(entity.getId());
            return Response.noContent().build();
        }catch(Exception e){
            log.error("Caught Exception in deleteNote with id=" + id + " : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not find note: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }

	//****************************************************************************************
	//Other Functions
	//****************************************************************************************
	
	@PUT
	@Path("/branches/{branchId}/servicepoint/{servicePointId}/trash/empty")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response emptyTrashQueue(@PathParam("branchId") Integer branchId, @PathParam("servicePointId") Integer servicePointId) throws BPException {
        try{
            setSSOCookie();

            if(branchId == null ||servicePointId == null) {
                return Response.serverError().entity("Cannot empty trash, no Service Point specified.").build();
            }

            List<Case> trashCases = caseDao.findCasesInTrashQueues(branchId);
            for(Case dbCase : trashCases) {
                // End visit in Orchestra
                for(CaseService cs : dbCase.getCaseServices()) {
                    if(cs.getStatus() == Status.TRASH) {
                        // Call the visit, then end it directly.
                        servicePointClient.callVisit(dbCase.getBranchId(), servicePointId, dbCase.getQpVisitId().intValue());
                        servicePointClient.endVisit(dbCase.getBranchId(), dbCase.getId());
                        cs.setStatus(Status.FINISHED);
                        caseServiceDao.update(cs);
                        log.info("Successfully ended visit {} from trash queue.", dbCase.getTicketNumber());
                        break; // break inner loop
                    }
                }
            }
            return Response.noContent().build();
        }catch(Exception e){
            log.error("Caught Exception in emptyTrashQueue: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not empty trash Queue: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
	
	//****************************************************************************************
	//Non-REST Functions
	//****************************************************************************************	

	private void updateCaseStatuses(Case dbCase) throws BPException {
		for(CaseService cs : dbCase.getCaseServices()) {
			if(cs.getStatus() == Status.WAITING) {
				if (cs.getServiceName().equals(GeneralParams.TRASH_QUEUE) && cs.getStatus() != Status.OVERRIDDEN) {
					cs.setStatus(Status.TRASH);
				}
				if (cs.getServiceName().equals(GeneralParams.ONHOLD_QUEUE) && cs.getStatus() != Status.OVERRIDDEN) {
					cs.setStatus(Status.ONHOLD);
				}
				if (cs.getServiceName().equals(GeneralParams.NOSHOW_QUEUE) && cs.getStatus() != Status.OVERRIDDEN) {
					cs.setStatus(Status.NOSHOW);
				}
				caseServiceDao.update(cs);
			}
		}
	}
	
	private List<CaseService> getCaseServicesOffline(Case dbCase, List<CaseService> caseServices) throws BPException {
		List<CaseService> offlineCaseServices = new ArrayList<CaseService>();
		Long caseId = dbCase.getId();
		for (CaseService cs : caseServices) {
			if (cs.getCaseId() == caseId) {
				offlineCaseServices.add(cs);
			}
		}
		return offlineCaseServices;
	}
	
	private void setEnterTime(Case dbCase) throws BPException {
		for(CaseService cs : dbCase.getCaseServices()){
			if(cs.getStatus() == Status.PENDING || cs.getStatus() == Status.WAITING || cs.getStatus() == Status.ONHOLD){
				cs.setCreated(new Date());
				caseServiceDao.update(cs);
			}
		}
	}
	
	private void cloneOverriddenService(CaseService clonedCaseService) throws BPException {
		Case dbCase = clonedCaseService.getParentCase();
		// Clone the Overridden service and add it to the end of Case Services
		CaseService caseService = new CaseService();
		caseService.setServiceId(clonedCaseService.getServiceId());
		caseService.setServiceName(clonedCaseService.getServiceName());
		caseService.setVip(clonedCaseService.getVip());
		caseService.setStatus(Status.PENDING);
		
		addCaseService(dbCase.getBranchId(), dbCase.getId(), caseService);
		caseService.setCreated(clonedCaseService.getCreated());
		caseServiceDao.update(clonedCaseService);
		moveCaseServiceToTop(caseService);
		/*
		Integer maxIdx = -1;
		for(CaseService cs : dbCase.getCaseServices()) {
			if(cs.getIdx() > maxIdx) {
				maxIdx = cs.getIdx();
				caseService = cs;
			}
		}
		
		if (maxIdx > -1) {
			moveCaseServiceToTop(caseService);
		}
		*/
	}
	
	private void moveCaseServiceToTop(CaseService dbCaseService) throws BPException {
		//Move the service up until it is after next WAITING
		Case dbCase = dbCaseService.getParentCase();
		CaseService nextDbCaseService = caseServiceDao.getNextChangeableCaseService(dbCase.getBranchId(), dbCase.getId());
		Integer nextDbCaseServiceIndex = nextDbCaseService.getIdx();
		while (dbCaseService.getIdx() > nextDbCaseServiceIndex) {
			Integer newIdx = dbCaseService.getIdx() - 1;
			for(CaseService cs : dbCase.getCaseServices()) {
				if(cs.getIdx() == newIdx) {
					cs.setIdx(dbCaseService.getIdx());
					dbCaseService.setIdx(newIdx);
					break;
				}
			}
		}
		updateUnservedServiceOrder(dbCase.getBranchId(), dbCase.getId());
		caseDao.update(dbCase);
	}
	
	private void swapCaseServices(Case dbCase, CaseService dbCaseService1, CaseService dbCaseService2) throws BPException {
		int cId = dbCaseService1.getServiceId();
		String cName = dbCaseService1.getServiceName();
		int nId = dbCaseService2.getServiceId();
		String nName = dbCaseService2.getServiceName();
		for (CaseService cs : dbCase.getCaseServices()) {
			if (cs.getId() == dbCaseService2.getId()) {
				cs.setServiceId(cId);
				cs.setServiceName(cName);
			}
			if (cs.getId() == dbCaseService1.getId()) {
				cs.setServiceId(nId);
				cs.setServiceName(nName);
			}
		}
		moveCaseServiceToTop(dbCaseService1);
	}
	
	private UserStatus callServiceFromQueue(UserStatus qpUserStatus, Integer branchId, Integer servicePointId, Long qpVisitServiceId) throws BPException {
		CaseService dbNewCaseService = caseServiceDao.findByQpVisitServiceId(qpVisitServiceId);
		Case dbNewCase = dbNewCaseService.getParentCase();
		
		// First, call visit in Orchestra
		try {
			qpUserStatus = servicePointClient.callVisit(branchId, servicePointId, dbNewCase.getQpVisitId().intValue());
		}
		catch (Exception e) {
			throw new BPException("Ticket " + dbNewCase.getTicketNumber() + " was already called by another user!", BPExceptionCode.VISIT_CALL_FAILED);
		}
		if(qpUserStatus.getVisit() == null) {
			throw new BPException("Call Visit failed on Orchestra side.", BPExceptionCode.VISIT_CALL_FAILED);
		}
		CaseService dbCalledCaseService = caseServiceDao.findByQpVisitServiceId(qpUserStatus.getVisit().getCurrentVisitService().getId());
		if(dbCalledCaseService == null) {
			log.warn("Could not find an ongoing case service for visit with id {}, has a visit been created outside of the Case workstation/reception?",qpUserStatus.getVisit().getId());
			throw new BPException("callServiceFromQueue could not find an ongoing case for the called visit", BPExceptionCode.INTERNAL_SERVER_ERROR);
		}
		//The called service was not the next to be called, so switch services Conductor
		if (dbNewCaseService.getId() != dbCalledCaseService.getId()) {
			swapCaseServices(dbNewCase, dbNewCaseService, dbCalledCaseService);
		}
		// Update our called caseService to serving
		setCaseServiceStatusToServing(dbCalledCaseService, qpUserStatus);
		transferCurrentNoteOntoCaseService(dbCalledCaseService);
		return qpUserStatus;
	}
	
    public UserStatus addDeliveredServiceByName(UserStatus qpUserStatus, String deliveredService) throws BPException{
        Integer configService = qpUserStatus.getVisit().getCurrentVisitService().getServiceId();
        List<DeliveredService> deliveredServices = servicePointClient.getDeliveredServices(qpUserStatus.getBranchId(), configService);
        Integer deliveredServiceId=-1;
        for(DeliveredService ds : deliveredServices){
            if(ds.getName().equals(deliveredService)){
                deliveredServiceId = ds.getId();
                break;
            }
        }
        if(deliveredServiceId==-1){
            throw new BPException("Could not complete this action, no " + deliveredService + " Transaction Type exists.", BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
        qpUserStatus = servicePointClient.addDeliveredService(qpUserStatus.getBranchId(), qpUserStatus.getVisit().getId(), qpUserStatus.getVisit().getCurrentVisitService().getId(), deliveredServiceId);
		
		if (deliveredService.equals(GeneralParams.QUEUE_CORRECTION_DELIVERED_SERVICE) || deliveredService.equals(GeneralParams.VISIT_REMOVED_DELIVERED_SERVICE) || deliveredService.equals(GeneralParams.NOSHOW_DELIVERED_SERVICE) || deliveredService.equals(GeneralParams.ONHOLD_DELIVERED_SERVICE)) {
			CaseService cs = caseServiceDao.findByQpVisitServiceId(qpUserStatus.getVisit().getCurrentVisitService().getId());
			// Add 'Mark' to our case...
			List<VisitDeliveredService> visitDeliveredServices = qpUserStatus.getVisit().getCurrentVisitService().getVisitDeliveredServices();
			VisitDeliveredService visitDeliveredService = visitDeliveredServices.get(visitDeliveredServices.size() - 1);

			Mark mark = markDao.findMarkByDeliveredServiceId(visitDeliveredService.getDeliveredServiceId());
			if(mark == null) {
				mark = new Mark();
				mark.setName(visitDeliveredService.getDeliveredServiceName());
				//mark.setCode(visitDeliveredService.getVisitOutcome().getOutcomeCode());
				mark.setQpDeliveredServiceId(visitDeliveredService.getDeliveredServiceId());
				mark = markDao.update(mark);
			}

			CaseServiceMark csm = new CaseServiceMark();
			csm.setMark(mark);
			csm.setQpVisitDeliveredServiceId(visitDeliveredService.getId());

			cs.getCaseServiceMarks().add(csm);
			caseServiceDao.update(cs);
		}
		
		return qpUserStatus;
    }

    private UserStatus updateUnservedServiceOrder(Integer branchId, Long caseId) throws BPException {
        Case dbCase = caseDao.getCase(branchId, caseId);

        UnservedServices services = new UnservedServices();
        services.setUnservedServiceIds(new ArrayList<Long>());

        List<CaseService> caseServices = dbCase.getCaseServices();
        Collections.sort(caseServices, new Comparator<CaseService>() {
            @Override
            public int compare(CaseService o1, CaseService o2) {
                return o1.getIdx().compareTo(o2.getIdx());
            }
        });

        // Due to the @OrderBy on the caseServices collection, they should always be returned to us in idx order ASC.
        for(CaseService cs : caseServices) {
            if(cs.getStatus() == Status.PENDING) {                 // || cs.getStatus() == Status.TRASH
                services.getUnservedServiceIds().add(cs.getQpVisitServiceId());
            }
        }

        // Finally. Get a fresh UserStatus so we know whether to use the servicePointClient or entryPointClient:
        // If there's no called visit, we must use entryPoint, the opposite applies to service points.
        // Note: We should perhaps NOT allow a user to modify an non-ongoing visit if the user has an other ongoing one.
        UserStatus qpUserStatus = servicePointClient.getUserStatus();

        if(qpUserStatus.getVisit() != null && qpUserStatus.getVisit().getId().equals(dbCase.getQpVisitId())) {
            return servicePointClient.sortUnservedService(dbCase.getBranchId(), dbCase.getQpVisitId(), services);
        } else {
            return entryPointClient.sortUnservedService(dbCase.getBranchId(), dbCase.getQpVisitId(), services);
        }
    }

    private Case moveCaseService(CaseService caseService, Boolean moveUp) throws BPException {

        Case dbCase = caseService.getParentCase();
        if(caseService.getStatus() != Status.PENDING) {
            throw new BPException("Could not move Service. Only services in state"  + Status.PENDING + " can be reordered.", BPExceptionCode.INTERNAL_SERVER_ERROR );
        }

        if(moveUp == true && caseService.getIdx() == 0) {
            throw new BPException("Could not move Service. Service is already at index 0.", BPExceptionCode.INTERNAL_SERVER_ERROR );
        }

        if(moveUp == false && caseService.getIdx() == caseService.getParentCase().getCaseServices().size() - 1) {
            throw new BPException("Could not move Service. Service is already at the last index.", BPExceptionCode.INTERNAL_SERVER_ERROR );
        }

        Integer newIdx = moveUp?caseService.getIdx()-1:caseService.getIdx()+1;

        // find the one with newIdx and set idx
        for(CaseService cs : dbCase.getCaseServices()) {
            if(cs.getIdx() == newIdx) {
                cs.setIdx(caseService.getIdx());
                break;
            }
        }
        // then, set new idx
        caseService.setIdx(newIdx);
        caseDao.update(dbCase);
        UserStatus qpUserStatus = updateUnservedServiceOrder(dbCase.getBranchId(), dbCase.getId());
        return dbCase;
    }

    private Long addServiceInQP(Case dbCase, CaseService caseService, UserStatus qpUserStatus)throws BPException{

        if(qpUserStatus.getVisit() != null && qpUserStatus.getVisit().getId().equals(dbCase.getQpVisitId())) {
            // Ongoing visit, use servicePointClient
            qpUserStatus = servicePointClient.addService(dbCase.getBranchId(), dbCase.getQpVisitId(), caseService.getServiceId());
        } else {
            qpUserStatus = entryPointClient.addService(dbCase.getBranchId(), dbCase.getQpVisitId(), caseService.getServiceId());
        }
        if(qpUserStatus.getVisit() == null){
            throw new BPException("Could not add Service: " + caseService.getServiceId() + " to visit: + " + dbCase.getQpVisitId() + " on branch: " + dbCase.getBranchId(),  BPExceptionCode.QP_ADD_SERVICE_FAILED);
        }

        // Get the last unserved visit service ID
        List<VisitService> unservedVisitServices = qpUserStatus.getVisit().getUnservedVisitServices();
        // get the last one
        VisitService vs = unservedVisitServices.get(unservedVisitServices.size() - 1);
        if(vs.getServiceId().equals(caseService.getServiceId())) {
            return vs.getId();
        }
        throw new BPException("Could not add Service: " + caseService.getServiceId() + " to visit: + " + dbCase.getQpVisitId() + " on branch: " + dbCase.getBranchId() + " We did not find serviceId on Visit.",  BPExceptionCode.QP_ADD_SERVICE_FAILED);
    }

    private UserStatus removeServiceInQP(CaseService caseService,UserStatus qpUserStatus) throws BPException{

        Case dbCase = caseService.getParentCase();
        // Important note: When removing a service from a Visit that isn't currently called, we must use the entryPointConnector.
        // So before calling the backend, check whether we're currently serving this visit or not.
        if(qpUserStatus.getVisit() == null || !qpUserStatus.getVisit().getId().equals(dbCase.getQpVisitId())) {
            qpUserStatus = entryPointClient.removeService(dbCase.getBranchId(),dbCase.getQpVisitId(), caseService.getQpVisitServiceId());
        } else {
            qpUserStatus = servicePointClient.removeService(dbCase.getBranchId(), dbCase.getQpVisitId(), caseService.getQpVisitServiceId());
        }
        if(qpUserStatus == null){
            throw new BPException("Could not remove Service: " + caseService.getQpVisitServiceId() + " from visit: + " + dbCase.getQpVisitId() + " on branch: " + dbCase.getBranchId(),  BPExceptionCode.QP_REMOVE_SERVICE_FAILED);
        }
        return qpUserStatus;
    }

    private Case addCaseServiceToCase(Case dbCase, CaseService caseService)throws BPException{
        dbCase.getCaseServices().add(caseService);
        caseService.setIdx(dbCase.getCaseServices().size() - 1);
        caseService.setRecyclesLeft(GeneralParams.MAX_RECYCLES);
        caseService.setParentCase(dbCase);
        caseService.getNotes().clear();
        caseServiceDao.create(caseService);
        caseDao.update(dbCase);
        return dbCase;
    }

    private Case removeCaseServiceFromCase(CaseService caseService)throws BPException{
        // Iterate over the collection. Remove the matching item, for all others, rewrite the idx
        Iterator<CaseService> i = caseService.getParentCase().getCaseServices().iterator();
        int idx = 0;
        while(i.hasNext()) {
            CaseService next = i.next();
            if(next.getId().equals(caseService.getId())) {
                i.remove();
            } else {
                next.setIdx(idx++);
            }
        }
        caseDao.update(caseService.getParentCase());
        return caseService.getParentCase();
    }

    private void setSSOCookie() {
        CookieHolder.set(headers.getCookies().get("SSOcookie"));
    }

    private UserStatus endCurrentCaseServiceVisit(CaseService dbCaseService, UserStatus qpUserStatus) throws BPException {

        if(qpUserStatus==null){
            throw new BPException("UserStatus is null in endCurrentCaseServiceVisit.", BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
		Case dbCase = dbCaseService.getParentCase();
        // Do we have an ongoing CaseService
        if(dbCaseService!=null && dbCaseService.getStatus() == Status.SERVING){
            Visit qpCurrentVisit = qpUserStatus.getVisit();

            // Do we have an ongoing Visit in Orchestra
            if(qpCurrentVisit!=null){
                // We need to set the next unserved (if applicable) dbCaseService to Status.WAITING if the endVisit was successful.
                // if this is the last service, we must add the trash service to make this visit end up in trash Queue.
                if (!hasUnservedServices(qpUserStatus)) {
                    CaseService trashService = buildCaseServiceFromName(dbCaseService.getParentCase(), GeneralParams.TRASH_QUEUE);
					trashService.setStatus(Status.TRASH);
					addCaseService(dbCase.getBranchId(), dbCase.getId(), trashService);
                }
                // Call Orchestra endVisit
                qpUserStatus = servicePointClient.endVisit(dbCaseService.getParentCase().getBranchId(), qpCurrentVisit.getId());
            }
            setCaseServiceStatusToFinished(dbCaseService);

            if(qpCurrentVisit!=null){
                Visit updatedQpVisit = servicePointClient.getVisit(qpUserStatus.getBranchId(), qpCurrentVisit.getId());

                // If there are more services to be served, we typically want to change the next one's state to WAITING
                for(CaseService cs : dbCaseService.getParentCase().getCaseServices()){
                    if(cs.getStatus() == Status.PENDING){

                        // Due to occasions when Orchestra didn't move a Visit to the next VisitService I have added
                        // an extra check to make sure we don't get a sync problem without our knowledge
                        if(cs.getQpVisitServiceId().compareTo(updatedQpVisit.getCurrentVisitService().getId())!=0){
                            log.error("Exception in moving visit to next Service. VisitId= " + updatedQpVisit.getId() + " QpVisitServiceId in CaseService= " + cs.getQpVisitServiceId() + " and VisitServiceId in QP= " + updatedQpVisit.getCurrentVisitService().getId() );
                            throw new BPException("Exception in moving visit to next Service. Current ServiceId in Orchestra is not the same as Case Management", BPExceptionCode.INTERNAL_SERVER_ERROR);
                        }
                        //cs.setCreated(new Date());
                        cs.setStatus(Status.WAITING);
                        caseServiceDao.update(cs);
                        break;
                    }
                }
            }
            caseDao.update(dbCaseService.getParentCase());
        }
        return qpUserStatus;
    }

    private boolean isOutcomeOrDeliveredServiceNeeded(UserStatus qpUserStatus) {
        if(qpUserStatus.getVisit()==null){
            return false;
        }
        return qpUserStatus.getVisitState() == VisitState.OUTCOME_NEEDED ||
                qpUserStatus.getVisitState() == VisitState.DELIVERED_SERVICE_NEEDED ||
                qpUserStatus.getVisitState() == VisitState.OUTCOME_OR_DELIVERED_SERVICE_NEEDED ||
                qpUserStatus.getVisitState() == VisitState.OUTCOME_FOR_DELIVERED_SERVICE_NEEDED;
    };
	
	private boolean isVisitInDisplayQueue(UserStatus qpUserStatus) {
        if(qpUserStatus.getVisit()==null || qpUserStatus.getVisitState()==null){
            return false;
        }
        return qpUserStatus.getVisitState() == VisitState.VISIT_IN_DISPLAY_QUEUE;
    };

    private Integer getQueueIdFromName(Integer branchId, String queueName){
        List<TinyQueue> queues = entryPointClient.getQueues(branchId);
        for(TinyQueue queue : queues) {
            if(queue.getName().equalsIgnoreCase(queueName)) {
                return queue.getId();
            }
        }
        return -1;
    }
	
	private Integer getServiceIdFromName(Integer branchId, String serviceName){
        Integer serviceId = null;
        List<Service> services = entryPointClient.getServices(branchId);
        for(Service s : services) {
            if(s.getInternalName().equalsIgnoreCase(serviceName)) {
                return s.getId();
            }
        }
        return -1;
    }

    private CaseService buildCaseServiceFromName(Case dbCase, String serviceName) throws BPException {
		Integer serviceId = getServiceIdFromName(dbCase.getBranchId(), serviceName);
		if(serviceId == -1) {
			return null;
		}
		CaseService service = new CaseService();
		service.setServiceId(serviceId);
		service.setServiceName(serviceName);
		service.setStatus(Status.PENDING);
		return service;
    }

    private void setCaseServiceStatusToFinished(CaseService dbCaseService ) throws BPException {
        if(dbCaseService.getStatus()==Status.SERVING){
            long diff = new Date().getTime() - dbCaseService.getUpdated().getTime();
			int servingTime = Math.round(diff/1000);
			if (dbCaseService.getServingTime() != null)
				servingTime += Math.round(dbCaseService.getServingTime());
			dbCaseService.setServingTime(servingTime);
        }else if(dbCaseService.getStatus()==Status.POST_PROCESSING){
            long diff = new Date().getTime() - dbCaseService.getPostProcessingStart().getTime();
            dbCaseService.setPostProcessingTime(Math.round(diff / 1000));
        }
        dbCaseService.setStatus(Status.FINISHED);
        caseServiceDao.update(dbCaseService);
    }
	/*
    private void setCaseServiceStatusToOnHold(CaseService dbCaseService ) throws BPException {
		long diff = new Date().getTime() - dbCaseService.getUpdated().getTime();
		int servingTime = Math.round(diff/1000);
		if (dbCaseService.getServingTime() != null)
			servingTime += Math.round(dbCaseService.getServingTime());
		dbCaseService.setServingTime(servingTime);
        dbCaseService.setStatus(Status.ONHOLD);
        caseServiceDao.update(dbCaseService);
    }
	*/
    private void setCaseServiceStatusToNoShow(CaseService dbCaseService ) throws BPException {

        dbCaseService.setStatus(Status.NOSHOW);
        caseServiceDao.update(dbCaseService);
    }

    private void setCaseServiceStatusToInPool(CaseService dbCaseService, UserStatus qpUserStatus ) throws BPException {

        dbCaseService.setServingTime(qpUserStatus.getVisit().getTimeSinceCalled());
        dbCaseService.setStatus(Status.IN_USER_POOL);
        caseServiceDao.update(dbCaseService);
    }

    private void setCaseServiceStatusToPostProcessing(CaseService dbCaseService ) throws BPException {

        dbCaseService.setPostProcessingStart(new Date());
        dbCaseService.setPostProcessingTime(0);
        dbCaseService.setStatus(Status.POST_PROCESSING);
        caseServiceDao.update(dbCaseService);
    }

    private void setCaseServiceStatusToServing(CaseService dbCaseService, UserStatus qpUserStatus) throws BPException {
		if (dbCaseService.getServiceId() != qpUserStatus.getVisit().getCurrentVisitService().getServiceId()) {
			//Service change to match Coductor Services
			qpUserStatus = servicePointClient.changeCurrentService(qpUserStatus.getBranchId(), qpUserStatus.getVisit().getId(), dbCaseService.getServiceId());
		}

        dbCaseService.setServedByCounterId(qpUserStatus.getServicePointId());
        dbCaseService.setServedByCounterName(qpUserStatus.getServicePointName());
		if (dbCaseService.getWaitingTime() == null) {
			Long waitingTime = (new Date().getTime() - dbCaseService.getCreated().getTime()) / 1000;
			dbCaseService.setWaitingTime(waitingTime.intValue());
		}
		else {
			Long waitingTime = (new Date().getTime() - dbCaseService.getUpdated().getTime()) / 1000;
			waitingTime += Math.round(dbCaseService.getWaitingTime());
			dbCaseService.setWaitingTime(waitingTime.intValue());
		}
		if (dbCaseService.getServingStart() == null) {
			dbCaseService.setServingStart(new Date());
		}
        dbCaseService.setServedByUserName(SecurityUtil.getCurrentFullName());
        dbCaseService.setServedByUserId(SecurityUtil.getCurrentUserName());
        dbCaseService.setStatus(Status.SERVING);
        caseServiceDao.update(dbCaseService);
    }

    private void setCaseServiceStatusToRecycled(CaseService dbCaseService ) throws BPException {

        dbCaseService.setRecyclesLeft(dbCaseService.getRecyclesLeft() - 1);
        dbCaseService.setStatus(Status.RECYCLED);
        caseServiceDao.update(dbCaseService);
    }

    private boolean hasUnservedServices(UserStatus qpUserStatus) {
        return qpUserStatus.getVisit().getUnservedVisitServices() != null && qpUserStatus.getVisit().getUnservedVisitServices().size() > 0;
    }	
	
    private boolean hasCurrentNote(Case newCase) {
        return newCase.getCurrentNote() != null && newCase.getCurrentNote().getText() != null && newCase.getCurrentNote().getText().trim().length() > 0;
    }

    private void setQpVisitServiceIdsForCase(Case dbCase, Visit visit) {
        //TODO review this function!
        // If case services length are same as unserved services length, we can trust the ordering and set qpVisitServiceId that way.
        if(dbCase.getCaseServices().size() == visit.getUnservedVisitServices().size()) {
            int index = 0;
            for(VisitService vs : visit.getUnservedVisitServices()) {
                CaseService caseService = dbCase.getCaseServices().get(index);
                if(caseService.getServiceId().equals(vs.getServiceId())) {
                    caseService.setQpVisitServiceId(vs.getId());
                }
                index++;
            }
        }
        // But if the unserved is one item shorter, we probably have a current one already OR more likely (if not a walk direct),
        // the first one has already been placed in a queue which sort of makes in "current" instead of "unserved".
        else if(dbCase.getCaseServices().size() == visit.getUnservedVisitServices().size()+1 && visit.getCurrentVisitService() != null) {
            CaseService current = dbCase.getCaseServices().get(0);
            current.setQpVisitServiceId(visit.getCurrentVisitService().getId());

            // TODO find some way to determine whether this is a normal placed-in-queue curerntVisit or a real walk direct.
            // Also, could it be called already by a storeNext?
            current.setStatus(Status.WAITING);

            int index = 1;
            for(VisitService vs : visit.getUnservedVisitServices()) {
                CaseService caseService = dbCase.getCaseServices().get(index);
                if(caseService.getServiceId().equals(vs.getServiceId())) {
                    caseService.setQpVisitServiceId(vs.getId());
                }
                index++;
            }
        } else {
            log.warn("Problem creating Case after issuing Visit to Orchestra, could not map VisitService ID's to CaseService instances in a meaningful way.");
            log.warn("Visit unserved: " + visit.getUnservedVisitServices().size() + ", ongoing: " + (visit.getCurrentVisitService() != null ? 1: 0) + ", served: " + visit.getServedVisitServices().size() + ". CaseService: " + dbCase.getCaseServices().size());
        }
    }
	
	private void transferCurrentNoteOntoCaseService(CaseService dbCaseService) throws BPException {
        // Transfer last note (if possible) from case#currentNote to CaseService
        CaseNote currentNote = dbCaseService.getParentCase().getCurrentNote();
        if(currentNote != null && currentNote.getText() != null && currentNote.getText().trim().length() > 0) {
            Note note = new Note();
            note.setText(currentNote.getText());
            note.setCreated(currentNote.getCreated());
            note.setCreatedBy(currentNote.getCreatedBy());
            dbCaseService.getNotes().add(note);
            caseServiceDao.update(dbCaseService);
        }
    }
	
	private void removeEmptyNotes(CaseService cs) {
        if(cs.getNotes() != null) {
            Iterator<Note> i = cs.getNotes().iterator();
            while(i.hasNext()) {
                Note note = i.next();
                if(note.getText() == null || note.getText().trim().length() == 0) {
                    i.remove();
                }
            }
        }
    }
	
	private Note getLastNoteOfWaitingService(List<CaseService> caseServices) {
        Note latest = null;
        for(CaseService cs : caseServices) {
            if(cs.getStatus() == Status.WAITING){
                for(Note n : cs.getNotes()) {
                    if(latest == null || latest.getCreated().compareTo(n.getCreated()) < 0) {
                        latest = n;
                    }
                }
            }
        }
        return latest;
    }

}