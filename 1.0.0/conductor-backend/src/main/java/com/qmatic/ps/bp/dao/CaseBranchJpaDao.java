package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.CaseBranch;
import com.qmatic.ps.bp.model.GeneralParams;
import com.qmatic.ps.bp.model.enumeration.Status;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class CaseBranchJpaDao extends JpaDao<CaseBranch> implements CaseBranchDao {

	@Override
    public CaseBranch getBranchInfo(Integer branchId) throws BPException {
		String queryString = "";
		/* queryString += "SELECT	:branchId AS id";
		queryString += "	,	ISNULL(COUNT(DISTINCT CASE WHEN cs.status = :finishedStatus THEN cs.parent_case_id ELSE NULL END), 0) AS customersServed";
		queryString += "	,	ISNULL(SUM(CASE WHEN cs.status = :finishedStatus THEN 1 ELSE 0 END), 0) AS servicesServed";
		queryString += "	,	ISNULL(SUM(CASE WHEN cs.status = :servingStatus OR cs.status = :postProcessingStatus THEN 1 ELSE 0 END), 0) AS customersBeingServed";
		queryString += "	,	ISNULL(COUNT(DISTINCT ws.parent_case_id), 0) AS customersWaiting";
		queryString += "	,	ISNULL(COUNT(ws.id), 0) AS servicesWaiting";
		queryString += "	,	ISNULL(MAX(maxWaitingTime), 0) AS maxWaitingTime";
		queryString += "	,	ISNULL(SUM(totalWaitingTime), 0) AS totalWaitingTime";
		queryString += "	,	0 AS averageWaitingTime";
		queryString += " FROM	case_services cs";
		queryString += " LEFT JOIN (";
		queryString += "	SELECT	parent_case_id";
		queryString += "		,	id";
		queryString += "		,	ISNULL(MAX(CASE WHEN cs.status = :recycledStatus THEN DATEDIFF(ss, cs.updated, GETDATE()) ELSE DATEDIFF(ss, cs.created, GETDATE())END), 0) AS maxWaitingTime";
		queryString += "		,	ISNULL(SUM(CASE WHEN cs.status = :recycledStatus THEN DATEDIFF(ss, cs.updated, GETDATE()) ELSE DATEDIFF(ss, cs.created, GETDATE())END), 0) AS totalWaitingTime";
		queryString += "	FROM	case_services cs";
		queryString += "	INNER JOIN (";
		queryString += "		SELECT	parent_case_id AS case_id,";
		queryString += "				COUNT(DISTINCT CASE WHEN ";
		queryString += "							cs.status = :servingStatus";
		queryString += "						OR	cs.status = :trashStatus";
		queryString += "						OR	cs.status = :noshowStatus";
		queryString += "						OR	cs.status = :onholdStatus";
		queryString += "						OR	(cs.status = :recycledStatus AND DATEDIFF(ss, cs.updated, GETDATE()) <= :recycleTime)";
		queryString += "					THEN parent_case_id ELSE NULL END) AS not_waiting";
		queryString += "		FROM case_services cs";
		queryString += "		INNER JOIN cases c ON cs.parent_case_id = c.id";
		queryString += "		WHERE	cs.created > :startOfToday";
		queryString += "		AND		branch_id = :branchId";
		queryString += "		GROUP BY branch_id, parent_case_id";
		queryString += "	) ws ON cs.parent_case_id = ws.case_id AND ws.not_waiting = 0";
		queryString += "	WHERE cs.status = :waitingStatus OR cs.status = :pendingStatus OR (cs.status = :recycledStatus AND DATEDIFF(ss, cs.updated, GETDATE()) > :recycleTime)";
		queryString += "	GROUP BY parent_case_id, id";
		queryString += " ) ws ON cs.id = ws.id";
		queryString += " INNER JOIN cases c ON cs.parent_case_id = c.id";
		queryString += " WHERE	cs.created > :startOfToday";
		queryString += " AND	c.branch_id = :branchId"; */
		
		queryString += "SELECT	:branchId AS id";
		queryString += "	,	MAX(customersServed) AS customersServed";
		queryString += "	,	MAX(servicesServed) AS servicesServed";
		queryString += "	,	MAX(customersBeingServed) AS customersBeingServed";
		queryString += "	,	MAX(customersWaiting) AS customersWaiting";
		queryString += "	,	MAX(servicesWaiting) AS servicesWaiting";
		queryString += "	,	MAX(maxWaitingTime) AS maxWaitingTime";
		queryString += "	,	MAX(totalWaitingTime) AS totalWaitingTime";
		queryString += "	,	MAX(averageWaitingTime) AS averageWaitingTime";
		queryString += " FROM	(	";
		queryString += "	SELECT	0 AS customersServed";
		queryString += "		,	0 AS servicesServed";
		queryString += "		,	0 AS customersBeingServed";
		queryString += "		,	ISNULL(COUNT(DISTINCT ws.case_id), 0) AS customersWaiting";
		queryString += "		,	ISNULL(COUNT(id), 0) AS servicesWaiting";
		queryString += "		,	ISNULL(MAX(CASE WHEN cs.status = :recycledStatus THEN DATEDIFF(ss, cs.updated, GETDATE()) ELSE DATEDIFF(ss, cs.created, GETDATE())END), 0) AS maxWaitingTime";
		queryString += "		,	ISNULL(SUM(CASE WHEN cs.status = :recycledStatus THEN DATEDIFF(ss, cs.updated, GETDATE()) ELSE DATEDIFF(ss, cs.created, GETDATE())END), 0) AS totalWaitingTime";
		queryString += "		,	0 AS averageWaitingTime";
		queryString += "	FROM	qp_conductor.case_services cs";
		queryString += "	INNER JOIN (";
		queryString += "		SELECT	parent_case_id AS case_id,";
		queryString += "				COUNT(DISTINCT CASE WHEN ";
		queryString += "							cs.status = :servingStatus ";
		queryString += "						OR	cs.status = :trashStatus";
		queryString += "						OR	cs.status = :noshowStatus";
		queryString += "						OR	cs.status = :onholdStatus";
		queryString += "						OR	(cs.status = :recycledStatus AND DATEDIFF(ss, cs.updated, GETDATE()) <= :recycleTime) ";
		queryString += "					THEN parent_case_id ELSE NULL END) AS not_waiting";
		queryString += "		FROM qp_conductor.case_services cs";
		queryString += "		INNER JOIN qp_conductor.cases c ON cs.parent_case_id = c.id";
		queryString += "		WHERE	c.created > :startOfToday";
		queryString += "		AND		branch_id = :branchId";
		queryString += "		GROUP BY branch_id, parent_case_id";
		queryString += "	) ws ON cs.parent_case_id = ws.case_id AND ws.not_waiting = 0";
		queryString += "	WHERE	cs.created > :startOfToday";
		queryString += "	AND		(cs.status = :waitingStatus OR cs.status = :pendingStatus OR (cs.status = :recycledStatus AND DATEDIFF(ss, cs.updated, GETDATE()) > :recycleTime))";
		queryString += " UNION ALL";
		queryString += "	SELECT	ISNULL(COUNT(DISTINCT case_id), 0) AS customersServed";
		queryString += "		,	ISNULL(COUNT(id), 0) AS servicesServed";
		queryString += "		,	0 AS customersBeingServed";
		queryString += "		,	0 AS customersWaiting";
		queryString += "		,	0 AS servicesWaiting";
		queryString += "		,	0 AS maxWaitingTime";
		queryString += "		,	0 AS totalWaitingTime";
		queryString += "		,	0 AS averageWaitingTime";
		queryString += "	FROM	qp_conductor.case_services cs";
		queryString += "	INNER JOIN (";
		queryString += "		SELECT	id AS case_id,";
		queryString += "				branch_id";
		queryString += "		FROM	qp_conductor.cases c";
		queryString += "		WHERE	created > :startOfToday";
		queryString += "		AND		branch_id = :branchId";
		queryString += "	) c ON cs.parent_case_id = c.case_id";
		queryString += "	WHERE	created > :startOfToday";
		queryString += "	AND		status = :finishedStatus";
		queryString += " UNION ALL";
		queryString += "	SELECT	0 AS customersServed";
		queryString += "		,	0 AS servicesServed";
		queryString += "		,	ISNULL(COUNT(id), 0) AS customersBeingServed";
		queryString += "		,	0 AS customersWaiting";
		queryString += "		,	0 AS servicesWaiting";
		queryString += "		,	0 AS maxWaitingTime";
		queryString += "		,	0 AS totalWaitingTime";
		queryString += "		,	0 AS averageWaitingTime";
		queryString += "	FROM	qp_conductor.case_services cs";
		queryString += "	INNER JOIN (";
		queryString += "		SELECT	id AS case_id,";
		queryString += "				branch_id";
		queryString += "		FROM	qp_conductor.cases c";
		queryString += "		WHERE	created > :startOfToday";
		queryString += "		AND		branch_id = :branchId";
		queryString += "	) c ON cs.parent_case_id = c.case_id";
		queryString += "	WHERE	created > :startOfToday";
		queryString += "	AND		(cs.status = :servingStatus OR cs.status = :postProcessingStatus)";
		queryString += ") data";
		
		Query query = entityManager.createNativeQuery(queryString);
		
		LocalDateTime ldt = LocalDateTime.now();
        ldt = ldt.withMillisOfDay(0);
		query.setParameter("branchId", branchId);
		query.setParameter("startOfToday", ldt.toDate());
		query.setParameter("finishedStatus", Status.FINISHED.toString());
		query.setParameter("trashStatus", Status.TRASH.toString());
		query.setParameter("noshowStatus", Status.NOSHOW.toString());
		query.setParameter("onholdStatus", Status.ONHOLD.toString());
		query.setParameter("servingStatus", Status.SERVING.toString());
		query.setParameter("postProcessingStatus", Status.POST_PROCESSING.toString());
		query.setParameter("waitingStatus", Status.WAITING.toString());
		query.setParameter("pendingStatus", Status.PENDING.toString());
		query.setParameter("recycledStatus", Status.RECYCLED.toString());
		query.setParameter("recycleTime", GeneralParams.SYSTEM_RECYCLE_DELAY);
		
		Object[] branch = (Object[])query.getSingleResult();
		
		return new CaseBranch(branch);
    }
	
}
