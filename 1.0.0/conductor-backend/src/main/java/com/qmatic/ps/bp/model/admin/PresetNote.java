package com.qmatic.ps.bp.model.admin;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Dejan Milojevic (dejmil), @created Nov 6, 2013
 */

@Entity
@Table(name = "preset_notes")
@Cacheable
@XmlRootElement
public class PresetNote implements Serializable {

	private Integer id;
	private String text;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "conductor_seq_table")
	@TableGenerator(name = "conductor_seq_table", table = "id_gen", pkColumnName = "id_name", valueColumnName = "id_val", pkColumnValue = "preset_notes", allocationSize = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "note_text", nullable = false)
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
