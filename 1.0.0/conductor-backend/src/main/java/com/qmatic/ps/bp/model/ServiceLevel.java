package com.qmatic.ps.bp.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * The Service Level entity.
 */
@Entity
@Table(name = "service_level")
@Cacheable
@XmlRootElement
public class ServiceLevel implements Serializable {

    private Long id;
    private Integer serviceId;
    private String serviceName;
    private Integer f2f = 0;
    private Integer pp = 0;
    private Integer tt = 0;
    private Integer wt = 0;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "conductor_seq_table")
    @TableGenerator(name = "conductor_seq_table", table = "id_gen", pkColumnName = "id_name", valueColumnName = "id_val", pkColumnValue = "service_levels", allocationSize = 1)

    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
	
	@Column(name="service_id", nullable = false)
    public Integer getServiceId() {
		return serviceId;
	}
	
	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}
	
	@Column(name="service_name", nullable = true)
	public String getServiceName() {
		return serviceName;
	}
	
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	@Column(name="f2f_lvl", nullable = false)
	public Integer getF2f() {
		return f2f;
	}
	
	public void setF2f(Integer f2f) {
		this.f2f = f2f;
	}
	
	@Column(name="pp_lvl", nullable = false)
	public Integer getPp() {
		return pp;
	}
	
	public void setPp(Integer pp) {
		this.pp = pp;
	}
	
	@Column(name="tt_lvl", nullable = false)
	public Integer getTt() {
		return tt;
	}
	
	public void setTt(Integer tt) {
		this.tt = tt;
	}
	
	@Column(name="wt_lvl", nullable = false)
	public Integer getWt() {
		return wt;
	}
	
	public void setWt(Integer wt) {
		this.wt = wt;
	}
}
