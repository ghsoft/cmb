package com.qmatic.ps.bp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.qmatic.ps.bp.model.Case;
import org.apache.log4j.BasicConfigurator;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.path.json.JsonPath.from;
import static org.hamcrest.Matchers.equalTo;


/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-02-21
 * Time: 15:58
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseRestTest {

    protected List<KeySet> keySetList = new ArrayList<>();


    @BeforeClass
    public void init() throws JsonProcessingException {
        BasicConfigurator.configure();

        RestAssured.basePath = "/conductor/rest/caseservicepoint";
        RestAssured.port = 7070;
        RestAssured.requestContentType("application/json");
        RestAssured.responseContentType("application/json");
      //  RestAssured.authentication = RestAssured.preemptive().basic("superadmin","secret"); // Tomcat

    }

    @BeforeTest
    public void reset() {
        keySetList.clear();
    }



    protected Long createCaseWithOneService() {
        Case aCase = ObjectFactory.newCase("Plumbing");
        Response resp = given().body(aCase).expect()
                .when().post("/case");

        Long id = from(resp.asString()).getLong("id");
        putId(id, "case");
         return id;
    }

    protected Long createCaseWithThreeServices() {
        Case aCase = ObjectFactory.newCase("Plumbing", "Electricity", "Ventilation");
        Response resp = given().body(aCase).expect()
                .when().post("/case");

        Long id = from(resp.asString()).getLong("id");
        putId(id, "case");
        return id;
    }

    protected Case findCaseById(Long caseId) {
        Response resp = given().pathParam("caseId", caseId)
                .expect()
                .body("payload.id", equalTo(caseId.intValue()))
                .body("meta.totalResults", equalTo(1))
                .when().get("/case/{caseId}");

        Case dbCase = resp.body().jsonPath().getObject("payload", Case.class);

        return dbCase;

    }

















    protected void deleteCreatedEntities() {
        // RestAssured.basePath = "/conductor/rest/caseservicepoint";
        //RestAssured.authentication = RestAssured.preemptive().basic("superadmin","secret");
        //clean("notes");
        //clean("marks");
        //clean("caseservices");
        clean("case");
    }

    private void clean(String endpoint) {
        for (KeySet entry : keySetList) {
            if (endpoint.equals(entry.endpoint)) {
                ArrayList<Long> ids = entry.keys;
                for (Long id : ids) {
                    given().pathParam("id", id)
                            .expect()
                            .contentType("")
                            .statusCode(204)
                            .when().delete("/" + endpoint + "/{id}");
                }
                break;
            }
        }
    }

    protected void putId(Long id, String endpoint) {
        boolean added = false;
        for (KeySet ks : keySetList) {
            if (ks.endpoint.equals(endpoint)) {
                ks.keys.add(id);
                added = true;
                break;
            }
        }
        if (!added) {
            KeySet ks = new KeySet();
            ks.endpoint = endpoint;
            ks.keys.add(id);
            keySetList.add(ks);
        }
    }


    class KeySet {
        public String endpoint;
        public ArrayList<Long> keys = new ArrayList<>();
    }
}
