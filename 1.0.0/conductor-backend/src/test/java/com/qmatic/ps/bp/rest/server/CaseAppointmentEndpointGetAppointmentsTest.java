package com.qmatic.ps.bp.rest.server;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.mockito.InOrder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.rest.client.AppointmentServiceClient;
import com.qmatic.qp.api.appointment.AppointmentStatus;
import com.qmatic.qp.api.connectors.dto.Appointment;

/**
 * @author Dejan Milojevic (dejmil), @created Nov 5, 2013
 */

@Test
public class CaseAppointmentEndpointGetAppointmentsTest {

	private CaseAppointmentEndpoint caseAppointmentEndpoint;
	private AppointmentServiceClient appointmentServiceClient;
	
	@BeforeMethod
	public void setUp() {
		caseAppointmentEndpoint = spy(new CaseAppointmentEndpoint());
		appointmentServiceClient = mock(AppointmentServiceClient.class);
		caseAppointmentEndpoint.setAppointmentServiceClient(appointmentServiceClient);
		caseAppointmentEndpoint.setHttpHeaders(mock(HttpHeaders.class));
	}
	
	public void ssoCookieShouldBeSetBeforeClientCall() throws BPException {
		caseAppointmentEndpoint.getAppointments(1);

		InOrder inOrder = inOrder(caseAppointmentEndpoint, appointmentServiceClient);
		inOrder.verify(caseAppointmentEndpoint).setSSOCookie();
		inOrder.verify(appointmentServiceClient).getAppointmentsForBranch(1);
	}
	
	public void emptyAppointmentListShouldBeReturnedWhenNoAppointmentsFound() throws BPException {
		doReturn(new ArrayList<Appointment>()).when(appointmentServiceClient).getAppointmentsForBranch(1);
		
		Response response = caseAppointmentEndpoint.getAppointments(1);
		List<Appointment> result = (List<Appointment>)response.getEntity();
		assertTrue(result.isEmpty());
	}
	
	public void onlyAppointmentWithStatusCreatedShouldBewReturned() throws BPException {
		List<Appointment> appointments = new ArrayList<>();
		Appointment appointment1 = new Appointment();
		appointment1.setStatus(AppointmentStatus.CREATED);
		appointments.add(appointment1);
		Appointment appointment2 = new Appointment();
		appointment2.setStatus(AppointmentStatus.ARRIVED);
		appointments.add(appointment2);
		
		doReturn(appointments).when(appointmentServiceClient).getAppointmentsForBranch(1);
		
		Response response = caseAppointmentEndpoint.getAppointments(1);
		List<Appointment> result = (List<Appointment>)response.getEntity();
		
		assertEquals(result.size(), 1);
		assertEquals(result.get(0).getStatus(), AppointmentStatus.CREATED);
	}
	
	@Test(expectedExceptions = BPException.class)
	public void bpExceptionShouldBeThrownWhenAnyExceptionCatched() throws BPException {
		doThrow(new RuntimeException("From mock")).when(appointmentServiceClient).getAppointmentsForBranch(1);
		caseAppointmentEndpoint.getAppointments(1);
	}

}
