var sourceCustomer = new function() {
	
	this.initView = function() {
		$('#admin_source_cust_search_input_fld').unbind().keyup(function(e) {
        	if(e.keyCode == 13) {
        		sourceCustomer.searchCustomers();
        	}
        });

        // Customer search button
        $('#admin_source_search_customer_btn').unbind();
        $('#admin_source_search_customer_btn').click(function() {
            sourceCustomer.searchCustomers();
        });
		
		$('#admin_source_create_customer_btn').unbind();
        $('#admin_source_create_customer_btn').click(function() {
            sourceCustomer.openCreateCustomerModal('Create customer', populateCustomerForm);
        });
	}
	
	var populateCustomerForm = function(cust) {
        $('#admin_source_customer_id_lbl').text(cust.id);
        $('#admin_source_customer_first_name_lbl').text(cust.firstName);
        $('#admin_source_customer_last_name_lbl').text(cust.lastName);
        $('#admin_source_customer_mobile_lbl').text(cust.phoneNumber);
        $('#admin_source_customer_email_lbl').text(cust.email);

        $('#admin_source_selected_customer_panel').removeClass('hidden');

        // Bind edit
        $('#admin_source_selected-customer-edit-btn').unbind().click(function() {
            sourceCustomer.openEditCustomerModal('Edit ' + cust.firstName + ' ' + cust.lastName, cust, populateCustomerForm);
        });
    };
	
	var populateEditCustomerForm = function(cust) {
        $('#customer_first_name_fld').val(cust.firstName);
        $('#customer_last_name_fld').val(cust.lastName);
        $('#customer_mobile_fld').val(cust.phoneNumber);
        $('#customer_email_fld').val(cust.email);
        $('#admin_source_selected_customer_panel').removeClass('hidden');
    };
	
	this.searchCustomers = function() {
		$('#admin_source_customer_id_lbl').text('');
        $('#admin_source_customer_first_name_lbl').text('');
        $('#admin_source_customer_last_name_lbl').text('');
        $('#admin_source_customer_mobile_lbl').text('');
        $('#admin_source_customer_email_lbl').text('');
		
        var val = $.trim($('#admin_source_cust_search_input_fld').val());
        if(val.length < 2) {
            util.showMessage("Type at least two characters");
            return;
        }
        $('#admin_source_selected_customer_panel').addClass('hidden');

        var results = {};
        var params = val.split(' ');
		var factor = "or";
		var searchParams = {};
		if (params.length == 1) {
			searchParams = {
				"branchId": 0,
				"id": params[0],
				"firstName": '%' + params[0] + '%',
				"lastName": '%' + params[0] + '%',
				"email": '%' + params[0] + '%',
				"phoneNumber": '%' + params[0] + '%'
			};
		}
		else {
			searchParams = {
				"branchId": 0,
				"firstName": '%' + params[0] + '%',
				"lastName": '%' + params[1] + '%'
			};
			factor = "and"
		}
		CaseServicePointService.searchCustomers(searchParams, factor)
			.done(function(searchResult){
				results = searchResult;
			})
			.fail(function(err){
				util.showError("Something when wrong when searching for the customer.", err);
			})

        $('#admin_source_customer_search_results').empty();

        for(var i = 0; i < results.length; i++) {
            var tr = $(document.createElement('tr'));
            
            $(tr).append('<td>' + results[i].id + '</td>');
            $(tr).append('<td>' + results[i].firstName + ' ' + results[i].lastName + '</td>');
            $(tr).append('<td>' + results[i].phoneNumber + '</td>');
            $(tr).append('<td>' + results[i].email + '</td>');
            // Add click handler for select customer
            $(tr).attr('style', 'cursor: pointer');
            (function(_id) {
                $(tr).click(function() {
                    CaseServicePointService.getCustomer(_id)
                        .done(function(cust){
                            populateCustomerForm(cust);
                        })
                        .fail(function(err){
                            util.showError("Could not get customer data.", err);
                        })

                    $('#admin_source_customer_search_results_container').addClass('hidden');

                });
            })(results[i].id);

            $('#admin_source_customer_search_results').append(tr);
        }

        $('#admin_source_customer_search_results_container').removeClass('hidden');
    };
	
	this.openCreateCustomerModal = function(title, onSaveCallback) {
        $('#customer_modal_title').text(title);

        $('#customer_first_name_fld').val('');
        $('#customer_last_name_fld').val('');
        $('#customer_mobile_fld').val('');
        $('#customer_email_fld').val('');

        // Bind save function to button
        $('#customer_save_btn').unbind();
        $('#customer_save_btn').click(function() {
			/*
			if ($('#customer_first_name_fld').val().trim() == '' || $('#customer_last_name_fld').val().trim() == '') {
				util.showError("Could not create customer, at least First Name and Last Name must be entered");
				return;
			}
			*/
            var cust = {
                "firstName" : $('#customer_first_name_fld').val().trim(),
                "lastName" : $('#customer_last_name_fld').val().trim(),
                "phoneNumber" : $('#customer_mobile_fld').val(),
                "email" : $('#customer_email_fld').val()
            };
            CaseServicePointService.createCustomer(cust)
                .done(function(createdCustomer){
                    onSaveCallback(createdCustomer);
                })
                .fail(function(err){
                    util.showError("Could not create customer..", err);
                })

            $('#customer_modal').modal('hide');

        });

        $('#customer_modal').modal('show');
    };
	
	this.openEditCustomerModal = function(title, cust, onSaveCallback) {
        $('#customer_modal_title').text(title);

        populateEditCustomerForm(cust);

        // Bind save function to button
        $('#customer_save_btn').unbind();
        $('#customer_save_btn').click(function() {
			/*
			if ($('#customer_first_name_fld').val().trim() == '' || $('#customer_last_name_fld').val().trim() == '') {
				util.showError("Could not save customer information, at least First Name and Last Name must be entered");
				return;
			}
			*/
            var updatedCust = {
                "id" : cust.id,
                "firstName" : $('#customer_first_name_fld').val().trim(),
                "lastName" : $('#customer_last_name_fld').val().trim(),
                "phoneNumber" : $('#customer_mobile_fld').val(),
                "email" : $('#customer_email_fld').val()
            };
            var params = {
                "customerId" : cust.id,
                "$entity" : updatedCust
            };

            CaseServicePointService.updateCustomer(cust.id, updatedCust)
                .done(function(nothing){
                    $('#customer_modal').modal('hide');
                    onSaveCallback(updatedCust);
                })
                .fail(function(err){
                    util.showError("Could not update customer..", err);
                    $('#customer_modal').modal('hide');
                })
        });

        $('#customer_modal').modal('show');
    };
	
}

