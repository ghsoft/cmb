/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-06-14
 * Time: 15:53
 * To change this template use File | Settings | File Templates.
 */
var noshow = new function() {


    this.noShow = function() {
        if(util.isNull(sessvars.state.visit)) {
            util.showError("No Show not possible, no ongoing visit.");
            return;
        }
        if(settings.hasValidSettings() &&
            sessvars.state.userState == servicePoint.userState.SERVING)
        {
            var recycleAllowed =  (sessvars.currentCase.ongoingService.recyclesLeft > 0 && sessvars.state.visit.recycleAllowed);
            var recallAllowed = sessvars.state.visit.recallAllowed;
            var noShowAllowed = sessvars.state.visit.noshowAllowed

            openNoShowDialog(recycleAllowed, recallAllowed, noShowAllowed);
        } else {
            $('#cst_noshow_btn').clickover();
        }
    };


    var openNoShowDialog = function(recycleAllowed, recallAllowed, noShowAllowed) {
		if (servicePoint.isVisitInDisplayQueue()) {
			return;
		}
		
        $('#noshow_recycle_btn').removeClass('hidden');
        $('#noshow_recall_btn').removeClass('hidden');
        $('#noshow_noshow_btn').removeClass('hidden');

        $('#noshow_recycle_btn').unbind().click(performRecycle);
        $('#noshow_recall_btn').unbind().click(performRecall);
        $('#noshow_noshow_btn').unbind().click(performNoShow);
		
		$('#noshow_modal_text').text('Please choose what you want to do for ticket: ' + sessvars.state.visit.ticketId);
        if(recycleAllowed) {
            $('#noshow_modalheader').text('Recycle / Recall / Noshow');
        } else {
            if(recallAllowed) {
                $('#noshow_modalheader').text('Recall / Noshow');
                $('#noshow_recycle_btn').addClass('hidden');
            } else if(noShowAllowed) {
                performNoShow();
                return;
            } else {
                util.showMessage('Recycle, Recall and No Show are not allowed');
                return;
            }
        }

        $('#noshow_modal').modal({
            "backdrop" : "static",
            "show" : true
        });
    };


    var performNoShow = function() {
        CaseServicePointService.noShowVisit()
			.done(function(userState) {
                // TODO do the recycle/recall thing
                sessvars.state = servicePoint.getState(userState);

                sessvars.statusUpdated = new Date();
                servicePoint.updateWorkstationStatus(false);
                sessvars.currentCustomer = null;
                servicePoint.clearOngoingVisit();
            }).fail(function(err){
                sessvars.state = servicePoint.getState();
                util.showError("Failed to No Show ticket.", err);
            });
    };

    var performRecycle = function() {
		CaseServicePointService.recycleVisit()
			.done(function(userState) {
                // TODO do the recycle/recall thing
                sessvars.state = servicePoint.getState(userState);
                sessvars.statusUpdated = new Date();
                servicePoint.clearOngoingVisit();
            }).fail(function(err){
                sessvars.state = servicePoint.getState();
                util.showError("Failed to Recycle ticket.", err);
            });
    };

    var performRecall = function() {
		CaseServicePointService.recallVisit()
			.done(function(userState) {
                // TODO do the recycle/recall thing
                sessvars.state = servicePoint.getState(userState);
                sessvars.statusUpdated = new Date();
            }).fail(function(err){
                sessvars.state = servicePoint.getState();
                util.showError("Failed to Recall ticket.", err);
            });
    }

};
