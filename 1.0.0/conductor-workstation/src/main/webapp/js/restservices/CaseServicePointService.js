
var CaseServicePointService = (function($) {

    var _baseRestPath = '/conductor-backend/rest/caseservicepoint';
	
	function _createCustomer(cust){
		if (cust.phoneNumber) {
			cust.phoneNumber = cust.phoneNumber.replace(/[^\d.]/g, '');
		}
		if (cust.branchId == null) {
			cust.branchId = sessvars.branchId;
		}
		cust.createdBy = sessvars.currentUser.userName;
		cust.updatedBy = sessvars.currentUser.userName;
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: _baseRestPath + '/customers/false',
            dataType: 'json',
            data: JSON.stringify(cust),
            async: false,
            cache: false
        });
    }
	
	function _searchCustomers(cust, andor){
		if (cust.id) {
			cust.id = cust.id.replace(/[^\d.]/g, '');
		}
		if (cust.branchId == null) {
			cust.branchId = sessvars.branchId;
		}
        return $.ajax({
            type: 'PUT',
			contentType: 'application/json',
            url: _baseRestPath + '/customers/search/' + andor + '/100',
			data: JSON.stringify(cust),
            async: false,
            cache: false
        });
    }
	
	function _getCustomer(customerId) {
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/customers/' + customerId,
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _updateCustomer(custId, custData){
		if (custData.phoneNumber) {
			custData.phoneNumber = custData.phoneNumber.replace(/[^\d.]/g, '');
		}
		custData.updatedBy = sessvars.currentUser.userName;
        return $.ajax({
            type: 'PUT',
            contentType: 'application/json',
            url: _baseRestPath + '/customers/' + custId,
            dataType: 'json',
            data: JSON.stringify(custData),
            async: false,
            cache: false
        });
    }
	
	function _mergeCustomersAndUpdateAllCases(currentCustomerId, newCustomerId) {
        return $.ajax({
            type: 'PUT',
            url: _baseRestPath + '/customers/' + currentCustomerId + '/merge/' + newCustomerId,
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _getBranches() {
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/branches',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _createCase(newCase) {
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/cases/entrypoint/' + sessvars.entryPointId,
			dataType: 'json',
            data: JSON.stringify(newCase),
			async: false,
            cache: false
        });
	}
	
	function _getCase(caseId) {
        return $.ajax({
            type: 'GET',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/cases/case/' + caseId,
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _getCaseByVisitId(visitId){
        return $.ajax({
            type: 'GET',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/cases/visit/' + visitId,
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _searchCase(searchstring){
        return $.ajax({
            type: 'GET',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/cases/search/' + searchstring,
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _findCasesHistoryByCustomerId(customerId){
        return $.ajax({
            type: 'GET',
            url: _baseRestPath + '/cases/history/customer/' + customerId,
            dataType: 'json',
            async: true,
            cache: true
        });
    }
	
	function _deleteCurrentCase(caseId){
        return $.ajax({
            type: 'DELETE',
            contentType: 'application/json',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/cases/case/' + caseId,
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _addCaseService(caseId, visit){
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/cases/case/' + caseId + '/caseservices',
            dataType: 'json',
            data: JSON.stringify(visit),
            async: false,
            cache: false
        });
    }
	
	function _getCaseServices(caseId) {
        return $.ajax({
            type: 'GET',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/cases/case/' + caseId + '/caseservices',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _deleteCaseService(caseId, caseServiceId){
        return $.ajax({
            type: 'DELETE',
            contentType: 'application/json',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/cases/case/' + caseId + '/caseservices/' + caseServiceId,
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function  _moveCaseService(caseServiceId, direction){
        return $.ajax({
            type: 'PUT',
            contentType: 'application/json',
            url: _baseRestPath + '/caseservices/' + caseServiceId + '/move/' + direction,
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function  _vipCaseService(caseServiceId, addremove){
        return $.ajax({
            type: 'PUT',
            contentType: 'application/json',
            url: _baseRestPath + '/caseservices/' + caseServiceId + '/vip/' + (addremove ? 'add' : 'remove'),
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _addDeliveredServices(deliveredServices){
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url:  _baseRestPath + '/deliveredServices',
            dataType: 'json',
            data: deliveredServices,
            async: false,
            cache: false
        });
    }
	
	function _getDeliveredServices(){
        return $.ajax({
            type: 'GET',
            url: _baseRestPath + '/deliveredServices',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _removeDeliveredService(dsId) {
		return $.ajax({
			type:'DELETE',
			url : _baseRestPath + '/deliveredServices/' + dsId,
			contentType: 'application/json',
            dataType: 'json',
            async: false,
            cache: false
		});
	}
	
	function _callNext(){
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/servicepoint/' + sessvars.servicePointId + '/next',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _walkDirect(visit){
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/servicepoint/' + sessvars.servicePointId + '/walkdirect',
            data: JSON.stringify(visit),
            async: false,
            cache: false
        });
    }
	
	function _callNextOnCase(caseId){
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/servicepoint/' + sessvars.servicePointId + '/call/case/' + caseId,
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _callService(qpVisitServiceId){
        return $.ajax({
            type: 'PUT',
            contentType: 'application/json',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/servicepoint/' + sessvars.servicePointId + '/call/service/' + qpVisitServiceId,
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _holdVisit(){
        return $.ajax({
            type: 'PUT',
            contentType: 'application/json',
			url : _baseRestPath + '/branches/' + sessvars.branchId + '/cases/visit/' + sessvars.state.visit.id + '/hold',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _noShowVisit() {
		return $.ajax({
            type : 'PUT',
			contentType: 'application/json',
            url : _baseRestPath + '/branches/' + sessvars.branchId + '/cases/visit/' + sessvars.state.visit.id + '/noshow',
            async: false,
            cache: false
        });
	}
	
	function _recallVisit() {
		return $.ajax({
            type : 'PUT',
			contentType: 'application/json',
            url : _baseRestPath + '/branches/' + sessvars.branchId + '/servicePoints/' + sessvars.servicePointId + '/visit/recall',
            async: false,
            cache: false
        });
	}
	
	function _recycleVisit() {
		return $.ajax({
            type : 'PUT',
			contentType: 'application/json',
            url : _baseRestPath + '/branches/' + sessvars.branchId + '/servicePoints/' + sessvars.servicePointId + '/visit/recycle',
            async: false,
            cache: false
        });
	}
	
	function _overrideService(caseId){
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/cases/case/' + caseId + '/overrideService',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _endFaceToFaceService(caseId, qpVisitServiceId){
        return $.ajax({
            type: 'PUT',
            contentType: 'application/json',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/cases/case/' + caseId + '/caseservice/' + qpVisitServiceId + '/end',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _startPostProcessing(caseId, qpVisitServiceId){
        return $.ajax({
            type: 'PUT',
            contentType: 'application/json',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/cases/case/' + caseId + '/caseservice/' + qpVisitServiceId + '/postProcess/start',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _endPostProcessing(caseId, qpVisitServiceId){
        return $.ajax({
            type: 'PUT',
            contentType: 'application/json',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/cases/case/' + caseId + '/caseservice/' + qpVisitServiceId + '/postProcess/end',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _transferVisitToOwnUserPool(){
		alert('_transferVisitToOwnUserPool');
        return $.ajax({
            type: 'PUT',
            contentType: 'application/json',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/cases/pool/user',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _getVisitsInUserPool(){
		alert('_getVisitsInUserPool');
        return $.ajax({
            type: 'GET',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/cases/pool/user/' + sessvars.currentUser.id,
            dataType: 'json',
            async: true,
            cache: false
        });
    }
	
	function _startUserServicePointSession(params) {
		return $.ajax({
			type : 'PUT',
			contentType: 'application/json',
			url : _baseRestPath + '/branches/' + sessvars.branchId + '/servicePoints/' + sessvars.servicePointId + '/start',
			dataType: 'json',
			async: false,
            cache: false
		})
	}
	
	function _closeWorkstation(){
        return $.ajax({
            type: 'DELETE',
            contentType: 'application/json',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/servicePoints/' + sessvars.servicePointId + '/users/' + sessvars.currentUser.userName,
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _getEntryPointsByDeviceType(){
        return $.ajax({
            type: 'GET',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/entryPoints/deviceTypes/TP311X', //SW_RECEPTION
            dataType: 'json',
            async: false,
            cache: true
        });
    }
	
	function _getServices(){
        return $.ajax({
            type: 'GET',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/services',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _getQueues(){
        return $.ajax({
            type: 'GET',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/queues',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _getCasesInQueue(queueId, page, limit){
        return $.ajax({
            type: 'GET',
            url: _baseRestPath + '/branches/' + sessvars.branchId + '/queues/' + queueId + '/cases' + (!isNaN(limit) && !isNaN(page) ? '?limit=' + limit + '&offset=' + ((page - 1) * limit) : ''),
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _createCaseNote(caseId, note) {
		return $.ajax({
			type: 'POST',
			contentType: 'application/json',
			url: _baseRestPath + '/notes/case/' + caseId,
			data: JSON.stringify(note),
			async: false,
            cache: false
		})
	}
	
	function _createCaseServiceNote(caseServiceId, note) {
		alert('_createCaseServiceNote');
		return $.ajax({
			type: 'POST',
			contentType: 'application/json',
			url: _baseRestPath + '/notes/caseservice/' + caseServiceId,
			data: JSON.stringify(note),
			async: false,
            cache: false
		});
	}
	
	function _updateCurrentNoteOnCase(caseId, note) {
		return $.ajax({
			type: 'PUT',
			contentType: 'application/json',
			url: _baseRestPath + '/notes/case/' + caseId,
			data: JSON.stringify(note),
			async: false,
            cache: false
		})
	}
	
	function _updateNote(note) {
		alert('_updateNote');
		return $.ajax({
			type: 'PUT',
			contentType: 'application/json',
			url: _baseRestPath + '/notes/note/' + note.id,
			data: JSON.stringify(note),
			async: false,
            cache: false
		});
	}

    function _getCaseServiceNotes(caseServiceId){
		alert('_getCaseServiceNotes');
        return $.ajax({
            type: 'GET',
            url: _baseRestPath + '/notes/caseservice/' + caseServiceId,
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
	function _deleteNote(noteId) {
		alert('_deleteNote');
		return $.ajax({
			type: 'DELETE',
			contentType: 'application/json',
			url: _baseRestPath + '/notes/note/' + noteId,
			async: false,
            cache: false
		});
	}

    return {
	
		createCustomer : function(cust){
            return _createCustomer(cust);
        },
		
		searchCustomers : function(searchParams, andor){
            return _searchCustomers(searchParams, andor);
        },
		
		getCustomer : function(customerId){
			return _getCustomer(customerId);
		},
		
		updateCustomer : function(custId, custData){
            return _updateCustomer(custId, custData);
        },
		
		mergeCustomersAndUpdateAllCases : function(currentCustomerId, newCustomerId) {
			return _mergeCustomersAndUpdateAllCases(currentCustomerId, newCustomerId);
		},
		
		getBranches : function() {
            return _getBranches();
        },
		
		createCase : function(newCase){
            return _createCase(newCase);
        },
	
		getCase : function(caseId){
            return _getCase(caseId);
        },
		
		getCaseByVisitId : function(visitId){
            return _getCaseByVisitId(visitId);
        },
		
		searchCase : function (searchstring){
            return _searchCase(searchstring);
        },

        findCasesHistoryByCustomerId : function(customerId){
            return _findCasesHistoryByCustomerId(customerId);
        },
		
		deleteCurrentCase : function(caseId){
            return _deleteCurrentCase(caseId);
        },
		
		addCaseService : function(caseId, visit){
            return _addCaseService(caseId, visit);
        },
		
		getCaseServices : function(caseId){
            return _getCaseServices(caseId);
        },
		
		deleteCaseService : function(caseId, caseServiceId){
            return _deleteCaseService(caseId, caseServiceId);
        },
		
		moveCaseService : function(caseServiceId, direction ){
            return _moveCaseService(caseServiceId, direction );
        },
		
		vipCaseService : function(caseId, caseServiceId, add ){
            return _vipCaseService(caseId, caseServiceId, add );
        },
		
		addDeliveredServices : function(deliveredServices){
            return _addDeliveredServices(deliveredServices);
        },
		
		getDeliveredServices : function(){
            return _getDeliveredServices();
        },
		
		removeDeliveredService : function (dsId) {
			return _removeDeliveredService(dsId);
		},
		
		callNext : function() {
            return _callNext();
        },
		
		walkDirect : function(visit){
            return _walkDirect(visit);
        },
		
		callNextOnCase : function(caseId) {
            return _callNextOnCase(caseId);
        },

		callService : function (qpVisitServiceId){
            return _callService(qpVisitServiceId);
        },
		
		noShowVisit : function() {
			return _noShowVisit();
		},
		
		recallVisit : function() {
			return _recallVisit();
		},
		
		recycleVisit : function() {
			return _recycleVisit();
		},
		
		holdVisit : function(){
            return _holdVisit();
        },
		
		overrideService : function(caseId) {
            return _overrideService(caseId);
        },
		
		endFaceToFaceService : function(caseId, qpVisitServiceId){
            return _endFaceToFaceService(caseId, qpVisitServiceId);
        },
		
		startPostProcessing : function(caseId, qpVisitServiceId){
            return _startPostProcessing(caseId, qpVisitServiceId);
        },
		
		endPostProcessing : function(caseId, qpVisitServiceId){
            return _endPostProcessing(caseId, qpVisitServiceId);
        },
		
		transferVisitToOwnUserPool : function(){
            return _transferVisitToOwnUserPool();
        },
		
		getVisitsInUserPool : function(){
            return _getVisitsInUserPool();
        },
		
		startUserServicePointSession : function(params){
            return _startUserServicePointSession(params);
        },
		
		closeWorkstation : function() {
            return _closeWorkstation();
        },
		
		getEntryPointsByDeviceType : function(){
            return _getEntryPointsByDeviceType();
        },
		
		getServices : function(){
            return _getServices();
        },
		
        getQueues : function(){
            return _getQueues();
        },

        getCasesInQueue : function(queueId, page, limit){
            return _getCasesInQueue(queueId, page, limit);
        },
		
		createCaseNote : function(caseId, note) {
			return _createCaseNote(caseId, note);
		},

        createCaseServiceNote : function(caseServiceId, note) {
			return _createCaseServiceNote(caseServiceId, note);
		},

        updateCurrentNoteOnCase : function(caseId, note) {
			return _updateCurrentNoteOnCase(caseId, note);
		},

        updateNote : function(note) {
			return _updateNote(note);
		},

        getCaseServiceNotes : function(caseServiceId){
            return _getCaseServiceNotes(caseServiceId);
        },

		deleteNote : function(noteId) {
			return _deleteNote(noteId);
		}
	};
})(jQuery);