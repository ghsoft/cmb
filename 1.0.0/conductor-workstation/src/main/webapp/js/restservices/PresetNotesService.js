var PresetNotesService = (function($) {

    var _baseRestPath = '/conductor-backend/rest/admin';

    function _getPresetNotes() {
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/presetNotes',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
    
    function _savePresetNote(id, text) {
        return $.ajax({
            type: 'POST',
            url: _baseRestPath +'/presetNotes/' + id + '/' + text,
            dataType: 'json',
            async: false,
            cache: false
        });
    }
    
    function _deletePresetNote(id) {
        return $.ajax({
            type: 'DELETE',
            url: _baseRestPath +'/presetNotes/' + id,
            dataType: 'json',
            async: false,
            cache: false
        });
    }

	return {
		getPresetNotes : function() {
            return _getPresetNotes();
        },
        
        savePresetNote : function(id, text) {
            return _savePresetNote(id, text);
        },
        
        deletePresetNote : function(id) {
            return _deletePresetNote(id);
        }
    };
    
})(jQuery);