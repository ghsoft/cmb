var events = new function() {

    this.initEvents = function() {
        if(util.isNull(sessvars.servicePointUnitId)) {
            return;
        }
        // INIT command to qevents
        var initCmd =  {"M":"C","C":{"CMD":"INIT","TGT":"CFM", "PRM":{"uid":"", "type":"60","encoding":"QP_JSON"}},"N":"0"};
        initCmd.C.PRM.uid = sessvars.servicePointUnitId;
        qevents.publish('/events/INIT', initCmd);

        // re-subscribe to events
        qevents.unsubscribe(util.asChannelStr(sessvars.servicePointUnitId));
        qevents.subscribe(util.asChannelStr(sessvars.servicePointUnitId), receiveEvent);
    };

    this.unsubscribeAndDisableQueues = function() {
        // end event subscription and clear queue timer
        if(settings.hasValidSettings(false)) {
            if(util.isNull(sessvars.servicePointUnitId)) {
                return;
            }
            qevents.unsubscribe(util.asChannelStr(sessvars.servicePointUnitId));
            clearTimeout(sessvars.queueTimer);
            sessvars.queueTimerOn = false;
			clearTimeout(sessvars.servicepointsTimer);
            sessvars.servicepointsTimerOn = false;
        }
    };


    /*
     * Receive events from Jiql CometD style.
     *
     * VISIT_CALL, in the case of store next
     * {"M":"E","E":{"evnt":"VISIT_CALL","type":"DEVICE","ts":"2012-09-06T16:12:54.449","did":120106000001,
     * 	"prm":{"queue":1,"servicePointName":"Web service point 1","ticket":"A001","servicePoint":1,"queueName":"Q1"}}}, on channel: /events/GBG/ServicePoint1
     *
     * USER_SERVICE_POINT_SESSION_END, in the case of session ending (being kicked off)
     * {"M":"E","E":{"evnt":"USER_SERVICE_POINT_SESSION_END","type":"DEVICE","ts":"2012-10-31T16:12:01.781+0100","did":220206000001,
     *  "prm":{"user":"test","servicePointId":"2"}}}, on channel: /events/BRA/ServicePoint2
     *
     *
     */
    var receiveEvent = function(event) {
        var processedEvent;

        try {
            processedEvent = JSON.parse(event);
        } catch (err) {
            return;
        }

        if(typeof processedEvent.E === "undefined" || typeof processedEvent.E.evnt === "undefined") {
            return;
        }
        switch(processedEvent.E.evnt) {
            case servicePoint.publicEvents.VISIT_CALL:
                if(sessvars.state.visitState == servicePoint.visitState.VISIT_IN_DISPLAY_QUEUE) {
                    //util.log('About to handle a visit call event when in state VISIT_IN_DISPLAY_QUEUE');
                    //window.clearTimeout(displayQueueTimeoutId);
                    //util.hideModal("displayQueueSpinnerWindow");
					$('#callingTicketWindow').modal('hide');
                    cServicePointService.getUserStatus()
                        .done(function(userStatus){
                            sessvars.state = userStatus;
                            sessvars.statusUpdated = new Date();
                        })
                        .fail(function(err){
                            util.showError("Could not get User status from server.", err);
                        })

                    //servicePoint.updateWorkstationStatus(false);
                    break;
                }
                // Update queues
                tables.refresh();
                break;
            case servicePoint.publicEvents.USER_SERVICE_POINT_SESSION_END:
				if (!settings.workstationOffline) {
					$('#forceLogoutWindow').modal(
						{"backdrop":"static",
							'show' : true,
							'keyboard' : false
						});
					$('#forceLogoutBtn').unbind().click(
						function() {
							window.location.replace("/logout.jsp");
						}
					);
					$('#forceLogoutWindow').modal('show');
				}
                // Someone or something has caused us to log off this servicepoint,
                // could either be someone else stealing it or the current user logging out somewhere else, or just a logout
                sessvars.$.clearMem();
                // we can't call the logout service by ourselves, as it might be us that have logged in somewhere else
				setTimeout(function() {
					window.location.replace("/logout.jsp");
				}, 30 * 1000);
                break;
            case servicePoint.publicEvents.VISIT_TRANSFER_TO_SERVICE_POINT_POOL:
                // servicePointPool.updateServicePointPool();
                break;
            case servicePoint.publicEvents.VISIT_TRANSFER_TO_USER_POOL:
                tables.populateUserPoolVisitsTable();
                // userPool.updateUserPool();
                break;
            case servicePoint.publicEvents.USER_SERVICE_POINT_WORK_PROFILE_SET:
                // If someone else (e.g. an administrator) sets the work profile for the user, do some updates.
                if (typeof processedEvent.E.prm !== 'undefined' && processedEvent.E.prm != null && typeof processedEvent.E.prm.workProfile !== 'undefined' && processedEvent.E.prm.workProfile != null) {
                    var workProfileId = processedEvent.E.prm.workProfile;
                    if (typeof sessvars.workProfileId !== "undefined") {
                        // If work profile has changed, update state, set the new workProfile
                        // on the sessvars, change in UI and show notification message
                        if (sessvars.workProfileId != workProfileId) {
                            cServicePointService.getUserStatus()
                                .done(function(userStatus){
                                    sessvars.state = userStatus;
                                    sessvars.statusUpdated = new Date();
									sessvars.workProfileId = userStatus.workProfileId;
									var work_profile_select = $("#work_profile_select");
									work_profile_select.prop('selectedIndex', $("#work_profile_select option[value=" + sessvars.workProfileId + "]").index());
									settings.updateWorkstationSettings();
									sessvars.profileName = $("option:selected", work_profile_select).text();
									util.showMessage('Profile changed to' + ': ' + sessvars.profileName);
                                })
                                .fail(function(err){
                                    util.showError("Could not get User status from server.", err);
                                })
                        }
                    }
                }
                break;
            default:
                break;
        }
    };


};