SELECT	c.ticket_number AS ticketId
	,	c.qp_visit_id AS visitId
	,	cs.waiting_time AS waitingTime
FROM	qp_conductor.cases c
INNER JOIN (
	SELECT	parent_case_id
		,	MAX(DATEDIFF(ss, cs.created, GETDATE())) AS waiting_time
	FROM	qp_conductor.case_services cs
	WHERE	cs.created > '2015-03-24'
	AND		cs.status = 'TRASH'
	AND		cs.service_name = 'TRASH'
	GROUP BY parent_case_id
) cs ON c.id = cs.parent_case_id
WHERE	c.created > '2015-03-24'
AND		c.branch_id = 1