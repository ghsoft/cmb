// start QP Java REST client API
package com.qmatic.ps.bp.rest.client;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.qmatic.ps.bp.util.CookieHolder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.qmatic.qp.api.connectors.dto.*;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import javax.annotation.PostConstruct;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.GenericType;
import org.springframework.stereotype.Component;

@Component
public class EntryPointServiceClient {
    protected Client c;
    protected ObjectMapper om = new ObjectMapper();

    @PostConstruct
    public void init() {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put("com.sun.jersey.api.json.POJOMappingFeature", Boolean.TRUE);
        c = Client.create(clientConfig);
        c.setFollowRedirects(true);
        c.setConnectTimeout(5000);

    }

    protected String getRestPath() {
        return getProtocol() + "://" + getIp() + ":" + getPort() + "/" + getBasePath();
    }

    protected String getProtocol() {
        return "http";
    }

    protected String getIp() {
        return "127.0.0.1";
    }

    protected String getPort() {
        //TODO change to config file.
        return "8080";
    }

    protected String getBasePath() {
        return "rest";
    }

    private Cookie getSSOCookie() {
        return CookieHolder.get();
    }

    // start JAX-RS API
    public java.util.List<com.qmatic.qp.api.connectors.dto.TinyBranch> getBranches() {
        java.util.List<com.qmatic.qp.api.connectors.dto.TinyBranch> result = c.resource(getRestPath() + "/entrypoint/branches")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.TinyBranch>>() {
                });
        return result;
    }

    public BranchInformation getBranch(Integer branchId) {
        BranchInformation result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<BranchInformation>() {
                });
        return result;
    }

    public GenericCustomer getCustomer(Integer customerId) {
        GenericCustomer result = c.resource(getRestPath() + "/entrypoint/customers/" + customerId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<GenericCustomer>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.TinyQueue> getQueues(Integer branchId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.TinyQueue> result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/queues")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.TinyQueue>>() {
                });
        return result;
    }

    public Visit createVisit(Integer branchId, Integer entryPointId, VisitParameters entity, TransferSortPolicy sortPolicy) {
        Visit result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/entryPoints/" + entryPointId + "/visits")
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .post(Visit.class);
        return result;
    }

    public UserStatus logout() {
        UserStatus result = c.resource(getRestPath() + "/entrypoint/logout")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public void removeVisit(Integer branchId, Integer entryPointId, Long visitId) {
        c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/entryPoints/" + entryPointId + "/visits/" + visitId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete();
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit> findVisits(Integer branchId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit> result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/visits")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit>>() {
                });
        return result;
    }

    public Visit getVisit(Integer branchId, Long visitId) {
        Visit result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/visits/" + visitId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<Visit>() {
                });
        return result;
    }

    public GenericCustomer createCustomer(GenericCustomer entity) {
        GenericCustomer result = c.resource(getRestPath() + "/entrypoint/customers")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .post(GenericCustomer.class);
        return result;
    }

    public void updateCustomer(Integer customerId, GenericCustomer entity) {
        c.resource(getRestPath() + "/entrypoint/customers/" + customerId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .put();
    }

    public void deleteCustomer(Integer customerId) {
        c.resource(getRestPath() + "/entrypoint/customers/" + customerId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete();
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.GenericCustomer> findCustomers() {
        java.util.List<com.qmatic.qp.api.connectors.dto.GenericCustomer> result = c.resource(getRestPath() + "/entrypoint/customers")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.GenericCustomer>>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.ServicePoint> getServicePointsByDeviceType(Integer branchId, String deviceType) {
        java.util.List<com.qmatic.qp.api.connectors.dto.ServicePoint> result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/servicePoints/deviceTypes/" + deviceType)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.ServicePoint>>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.EntryPoint> getEntryPointsByDeviceType(Integer branchId, String deviceType) {
        java.util.List<com.qmatic.qp.api.connectors.dto.EntryPoint> result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/entryPoints/deviceTypes/" + deviceType)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.EntryPoint>>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.Outcome> getOutcomesForService(Integer branchId, Integer serviceId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.Outcome> result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/services/" + serviceId + "/outcomes")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.Outcome>>() {
                });
        return result;
    }

    public EntryPoint getEntryPoint(Integer branchId, Integer entryPointId) {
        EntryPoint result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/entryPoints/" + entryPointId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<EntryPoint>() {
                });
        return result;
    }

    public TinyQueue getQueueForService(Integer branchId, Integer serviceId) {
        TinyQueue result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/services/" + serviceId + "/queue")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<TinyQueue>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.DeliveredService> getDeliveredServices(Integer branchId, Integer serviceId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.DeliveredService> result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/services/" + serviceId + "/deliverableServices")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.DeliveredService>>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit> getVisitsInQueue(Integer branchId, Integer queueId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit> result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/queues/" + queueId + "/visits")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit>>() {
                });
        return result;
    }

    public User getCurrentUser() {
        User result = c.resource(getRestPath() + "/entrypoint/user")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<User>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.User> getLoggedInUsers(Integer branchId, String onlyServicePoints) {
        java.util.List<com.qmatic.qp.api.connectors.dto.User> result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/users")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.User>>() {
                });
        return result;
    }

    public void setBranchVariable(Integer branchId, BranchVariable entity) {
        c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/variables")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .put();
    }

    public BranchVariable getBranchVariable(Integer branchId, String name) {
        BranchVariable result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/variables/" + name)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get(new GenericType<BranchVariable>() {
                });
        return result;
    }

    public void deleteBranchVariable(Integer branchId, String name) {
        c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/variables/" + name)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete();
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.BranchVariable> getBranchVariables(Integer branchId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.BranchVariable> result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/variables")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.BranchVariable>>() {
                });
        return result;
    }

    public void transferVisitToUserPool(Integer branchId, Integer userId, TransferParams entity) {
        c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/users/" + userId + "/visits")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .put();
    }

    public void transferVisitToQueue(Integer branchId, Integer queueId, TransferParams entity) {
        c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/queues/" + queueId + "/visits")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .put();
    }

    public GlobalVariable getGlobalVariable(String name) {
        GlobalVariable result = c.resource(getRestPath() + "/entrypoint/variables/" + name)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<GlobalVariable>() {
                });
        return result;
    }

    public void setGlobalVariable(GlobalVariable entity) {
        c.resource(getRestPath() + "/entrypoint/variables")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .put();
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.GlobalVariable> getGlobalVariables() {
        java.util.List<com.qmatic.qp.api.connectors.dto.GlobalVariable> result = c.resource(getRestPath() + "/entrypoint/variables")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.GlobalVariable>>() {
                });
        return result;
    }

    public void deleteGlobalVariable(String name) {
        c.resource(getRestPath() + "/entrypoint/variables/" + name)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete();
    }

    public void startUserSession(Integer branchId, String userName) {
        c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/users/" + userName)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put();
    }

    public void endUserSession(Integer branchId, String userName) {
        c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/users/" + userName)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete();
    }

    public SystemInformation getSystemInformation() {
        SystemInformation result = c.resource(getRestPath() + "/entrypoint/systemInformation")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<SystemInformation>() {
                });
        return result;
    }

    public TinyQueue getQueue(Integer branchId, Integer queueId) {
        TinyQueue result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/queues/" + queueId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<TinyQueue>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.Outcome> getOutcomesForDeliveredService(Integer branchId, Integer serviceId, Integer deliveredServiceId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.Outcome> result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/services/" + serviceId + "/deliveredServices/" + deliveredServiceId + "/outcomes")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.Outcome>>() {
                });
        return result;
    }

    public void transferVisitToServicePointPool(Integer branchId, Integer servicePointId, TransferParams entity) {
        c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/visits")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .put();
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.Service> getServices(Integer branchId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.Service> result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/services")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.Service>>() {
                });
        return result;
    }

    public Service getService(Integer branchId, Integer serviceId) {
        Service result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/services/" + serviceId)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .cookie(getSSOCookie())
                .get(new GenericType<Service>() {
                });
        return result;
    }

    public UserStatus removeService(Integer branchId, Long visitId, Long visitServiceId) {
        UserStatus result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/visits/" + visitId + "/services/" + visitServiceId)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .cookie(getSSOCookie())
                .delete(UserStatus.class);
        return result;
    }

    public UserStatus addService(Integer branchId, Long visitId, Integer serviceId) {
        UserStatus result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/visits/" + visitId + "/services/" +serviceId)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .cookie(getSSOCookie())
                .post(UserStatus.class);
        return result;
    }

    public UserStatus sortUnservedService(@PathParam("branchId") Integer branchId, @PathParam("visitId") Long visitId,
                                          UnservedServices unservedServices) {
        UserStatus result = c.resource(getRestPath() + "/entrypoint/branches/" + branchId + "/visits/" + visitId + "/services")
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .cookie(getSSOCookie())
                .entity(unservedServices, MediaType.APPLICATION_JSON_TYPE)
                .put(UserStatus.class);
        return result;
    }
}

