package com.qmatic.ps.bp.util;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-03-11
 * Time: 16:47
 * To change this template use File | Settings | File Templates.
 */
public class StringUtil {

    /**
     * Returns the string with the first character converted to upper-case, e.g: customerId becomes CustomerId.
     *
     * Very handy when invoking getters using reflection, e.g. field "fieldName" can easily be mapped to "getFieldName"
     * @param str
     * @return
     */
    public static String capitalize(String str) {
        if(str == null || str.length() < 2) {
            return str;
        }
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}
