package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.CaseServicePoint;
import com.qmatic.ps.bp.model.GeneralParams;
import com.qmatic.ps.bp.model.enumeration.Status;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class CaseServicePointJpaDao extends JpaDao<CaseServicePoint> implements CaseServicePointDao {

	@Override
    public List<CaseServicePoint> getServicePointsInfo() throws BPException {
		String queryString = "";
		queryString += "SELECT	10000+ROW_NUMBER() OVER(ORDER BY cs2.name) AS id";
		queryString += "	,	ISNULL(cs2.name, '') AS name";
		queryString += "	,	ISNULL(cs1.staffName, '') AS staffName";
		queryString += "	,	ISNULL(cs2.staffFullName, '') AS staffFullName";
		queryString += "	,	ISNULL(cs2.workProfileId, 0) AS workProfileId";
		queryString += "	,	ISNULL(cs2.workProfileName, '') AS workProfileName";
		queryString += "	,	ISNULL(cs2.status, '') AS status";
		queryString += "	,	ISNULL(cs2.serviceId, 0) AS serviceId";
		queryString += "	,	ISNULL(cs2.serviceName, '') AS serviceName";
		queryString += "	,	ISNULL(cs2.servicePointStatus, '') AS servicePointStatus";
		queryString += "	,	ISNULL(cs2.transactionTime, 0) AS transactionTime";
		queryString += "	,	ISNULL(cs2.customerId, 0) AS customerId";
		queryString += "	,	ISNULL(cs2.customerName, '') AS customerName";
		queryString += "	,	ISNULL(cs2.ticketNumber, '') AS ticketNumber";
		queryString += "	,	ISNULL(cs1.servedToday, 0) AS servedToday";
		queryString += " FROM";
		queryString += " (";
		queryString += "	SELECT	cs.served_by_user_id AS staffName";
		queryString += "		,	COUNT(*) AS servedToday";
		queryString += "	FROM	case_services cs";
		queryString += "	WHERE	cs.status = :finishedStatus";
		queryString += "	AND		cs.created > :startOfToday";
		queryString += "	GROUP BY cs.served_by_user_id";
		queryString += " ) cs1";
		queryString += " FULL JOIN";
		queryString += " (";
		queryString += "	SELECT	cs.served_by_counter_name AS name";
		queryString += "		,	cs.served_by_user_id AS staffName";
		queryString += "		,	cs.served_by_user_name AS staffFullName";
		queryString += "		,	0 AS workProfileId";
		queryString += "		,	'' AS workProfileName";
		queryString += "		,	'' AS status";
		queryString += "		,	cs.service_id AS serviceId";
		queryString += "		,	cs.service_name AS serviceName";
		queryString += "		,	cs.status AS servicePointStatus";
		queryString += "		,	DATEDIFF(ss, cs.updated, GETDATE()) AS transactionTime";
		queryString += "		,	c.customer_id AS customerId";
		queryString += "		,	cc.first_name + ' ' + cc.last_name AS customerName";
		queryString += "		,	c.ticket_number AS ticketNumber";
		queryString += "	FROM	case_services cs";
		queryString += "	INNER JOIN cases c ON c.id = cs.parent_case_id";
		queryString += "	INNER JOIN case_customers cc ON c.customer_id = cc.id";
		queryString += "	WHERE	cs.served_by_user_id IS NOT NULL";
		queryString += "	AND		(cs.status = :servingStatus OR cs.status = :postProcessingStatus)";
		queryString += "	AND		cs.created > :startOfToday";
		queryString += " ) cs2";
		queryString += " ON	cs1.staffName = cs2.staffName";
		
		Query query = entityManager.createNativeQuery(queryString, CaseServicePoint.class);
		
		LocalDateTime ldt = LocalDateTime.now();
        ldt = ldt.withMillisOfDay(0);
		query.setParameter("startOfToday", ldt.toDate());
		query.setParameter("finishedStatus", Status.FINISHED.toString());
		query.setParameter("servingStatus", Status.SERVING.toString());
		query.setParameter("postProcessingStatus", Status.POST_PROCESSING.toString());
		
		return (List<CaseServicePoint>) query.getResultList();
    }
	
}
