package com.qmatic.ps.bp.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 */
@Entity
@Table(name="notes")
@Cacheable
@XmlRootElement
public class Note implements Serializable {

    private Long id;
    private String createdBy;
    private String text;
    private Date created;

    /**
     * Internal primary key. Note that this key is never included in responses from the /public API.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "conductor_seq_table")
    @TableGenerator(name = "conductor_seq_table", table = "id_gen", pkColumnName = "id_name", valueColumnName = "id_val", pkColumnValue = "notes", allocationSize = 1)

    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Lob
    @Column(name="text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name="created")
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
	/*
    @PrePersist
    void onCreate() {
        this.setCreated(new Date());
    }
	*/
}
