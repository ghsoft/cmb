package com.qmatic.ps.bp.dao.admin;

import org.springframework.stereotype.Repository;

import com.qmatic.ps.bp.dao.JpaDao;
import com.qmatic.ps.bp.model.admin.PresetNote;

/**
 * @author Dejan Milojevic (dejmil), @created Nov 6, 2013
 */

@Repository
public class PresetNoteJpaDao extends JpaDao<PresetNote> implements PresetNoteDao {

}
