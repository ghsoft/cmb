package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.model.Note;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-23
 * Time: 13:50
 * To change this template use File | Settings | File Templates.
 */
public interface NoteDao extends Dao<Note> {
}
