package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.CaseService;
import com.qmatic.ps.bp.model.CaseServiceMark;
import com.qmatic.ps.bp.model.Note;
import com.qmatic.ps.bp.model.GeneralParams;
import com.qmatic.ps.bp.model.enumeration.Status;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.ws.rs.core.MultivaluedMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class CaseServiceJpaDao extends JpaDao<CaseService> implements CaseServiceDao {

	@Override
    public List<CaseService> getCaseServices(Integer branchId, Long caseId) throws BPException {

        LocalDateTime ldt = LocalDateTime.now();
        ldt = ldt.withMillisOfDay(0);

        Query q = entityManager.createQuery("SELECT cs FROM Case c join c.caseServices cs WHERE c.branchId = :branchId AND c.id = :caseId AND cs.created > :startOfToday ORDER BY cs.idx")
                .setParameter("branchId", branchId)
				.setParameter("caseId", caseId)
                .setParameter("startOfToday", ldt.toDate());
        return q.getResultList();
    }
	
	public CaseService getNextChangeableCaseService(Integer branchId, Long caseId) throws BPException {
		List<CaseService> caseServices = getCaseServices(branchId, caseId);
		
		Integer lowestIndex = 1000;
        for(CaseService cs : caseServices) {
			if (cs.getStatus() == Status.PENDING) {
				if (cs.getIdx() < lowestIndex) {
					lowestIndex = cs.getIdx();
				}
			}
        }
		for(CaseService cs : caseServices) {
            if (cs.getIdx() == lowestIndex) {
				return cs;
			}
        }
        return null;
	}
	
	public CaseService getNextCallableCaseService(Integer branchId, Long caseId) throws BPException {
		List<CaseService> caseServices = getCaseServices(branchId, caseId);
		
		Integer lowestIndex = 1000;
        for(CaseService cs : caseServices) {
			if (cs.getStatus() == Status.SERVING) {
				lowestIndex = cs.getIdx() + 1;
				break;
			}
            if (cs.getStatus() == Status.WAITING || cs.getStatus() == Status.ONHOLD || cs.getStatus() == Status.RECYCLED) {
				if (cs.getIdx() < lowestIndex) {
					lowestIndex = cs.getIdx();
				}
            }
        }
		for(CaseService cs : caseServices) {
            if (cs.getIdx() == lowestIndex) {
				return cs;
			}
        }
        return null;
	}

    @Override
    public List<Note> getNotesOfCaseService(Long caseServiceId, MultivaluedMap<String, String> params) {
        Query q = entityManager.createQuery("SELECT n FROM CaseService cs join cs.notes n WHERE cs.id = :id ORDER BY n.created DESC")
                .setParameter("id", caseServiceId);

        applyPaging(params, q);
        return q.getResultList();

    }

    @Override
    public CaseService findByQpVisitServiceId(Long qpVisitServiceId) throws BPException {

        List<CaseService> caseServices = entityManager.createQuery("SELECT cs FROM CaseService cs WHERE cs.qpVisitServiceId = :qpVisitServiceId ORDER BY cs.created DESC")
                    .setParameter("qpVisitServiceId", qpVisitServiceId)
                    .getResultList();

        if(caseServices.size() == 0) {
            return null;
        } else {
            return caseServices.get(0);
        }
    }

    @Override
    public CaseService findServingByServicePoint(Integer branchId, Integer servicePointId) throws BPException{
        List<CaseService> caseServices = entityManager.createQuery("SELECT cs FROM Case c join c.caseServices cs WHERE c.branchId = :branchId AND cs.servedByCounterId = :servicePointId AND (cs.status = :postProcessingStatus OR cs.status = :servingStatus)")
                .setParameter("branchId", branchId)
                .setParameter("servicePointId", servicePointId)
                .setParameter("postProcessingStatus", Status.POST_PROCESSING)
                .setParameter("servingStatus", Status.SERVING)
                .getResultList();

        if(caseServices.size() == 0) {
            return null;
        } else {
            return caseServices.get(0);
        }
    }

    @Override
    public void removeMark(Long id, Long visitDeliveredServiceId) throws BPException {
        try {
            CaseServiceMark caseServiceMark = (CaseServiceMark) entityManager.createQuery("SELECT csm FROM CaseService cs join cs.caseServiceMarks csm WHERE cs.id = :id AND csm.qpVisitDeliveredServiceId = :visitDeliveredServiceId")
                    .setParameter("id", id)
                    .setParameter("visitDeliveredServiceId", visitDeliveredServiceId)
                    .getSingleResult();
            entityManager.remove(caseServiceMark);
        } catch (NoResultException e) {
            log.warn("Could not remove mark from CaseService, no matching CaseServiceMark found.");
        }
    }

    @Override
    public List<CaseService> getOngoingCaseServices() throws BPException {
		
		LocalDateTime ldt = LocalDateTime.now();
        ldt = ldt.withMillisOfDay(0);
		
        List<CaseService> caseServices = entityManager.createQuery("SELECT cs FROM CaseService cs WHERE cs.created > :startOfToday AND (cs.status = :servingStatus OR cs.status = :postProcessingStatus)")
				.setParameter("startOfToday", ldt.toDate())
                .setParameter("servingStatus", Status.SERVING)
                .setParameter("postProcessingStatus", Status.POST_PROCESSING)
                .getResultList();

        return caseServices;
    }
	
	@Override
    public List<CaseService> getServicePointsInfo() throws BPException {
		
		LocalDateTime ldt = LocalDateTime.now();
        ldt = ldt.withMillisOfDay(0);
	
        List<CaseService> caseServices = entityManager.createQuery("SELECT cs FROM CaseService cs WHERE cs.created > :startOfToday AND (cs.status = :serving OR cs.status = :ps) ORDER BY cs.servedByCounterId DESC")
                .setParameter("startOfToday", ldt.toDate())
                .setParameter("serving", Status.SERVING)
                .setParameter("ps", Status.POST_PROCESSING)
                .getResultList();

        return caseServices;
    }
	
}
