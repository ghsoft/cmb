package com.qmatic.ps.bp.rest.client;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.qmatic.qp.api.connectors.dto.Appointment;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource.Builder;

@Component
public class AppointmentServiceClient extends BaseConnectorClient{
	
	private static final String SERVICE_PATH = "/appointment";
    
    @PostConstruct
    public void init() {
        super.init();
    }
    
	public List<Appointment> getAppointmentsForBranch(Integer branchId) {
		Builder resource = createResourceBuilder(SERVICE_PATH + "/branches/" + branchId + "/appointments");
		List<Appointment> result = resource.get(new GenericType<List<Appointment>>() {} );
	    return result;
	}
	
	public Appointment getAppointmentForBranch(Integer branchId, Integer appointmentId) {
		Builder resource = createResourceBuilder(SERVICE_PATH + "/branches/" + branchId + "/appointments/" + appointmentId);
		Appointment result = resource.get(new GenericType<Appointment>() {} );
	    return result;
	}

}