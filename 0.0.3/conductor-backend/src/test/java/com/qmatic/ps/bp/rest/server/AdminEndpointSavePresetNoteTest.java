package com.qmatic.ps.bp.rest.server;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.dao.admin.PresetNoteDao;
import com.qmatic.ps.bp.model.admin.PresetNote;

/**
 * @author Dejan Milojevic (dejmil), @created Nov 6, 2013
 */

@Test
public class AdminEndpointSavePresetNoteTest {

	private AdminEndpoint adminEndpoint;
	private PresetNoteDao presetNoteDao;

	@BeforeMethod
	public void setUp() {
		adminEndpoint = new AdminEndpoint();
		presetNoteDao = mock(PresetNoteDao.class);
		adminEndpoint.setPresetNoteDao(presetNoteDao);
	}
	
	public void noteShouldBeCreatedWhenPassedIdIsZero() throws BPException {
		adminEndpoint.savePresetNote(0, "This should be a new Note");
		verify(presetNoteDao).create(any(PresetNote.class));
	}
	
	public void noteShouldBeUpdatedWhenPassedIdIsOtherThanZero() throws BPException {
		doReturn(new PresetNote()).when(presetNoteDao).findById(1);
		adminEndpoint.savePresetNote(1, "This should be an existing Note");
		verify(presetNoteDao).update(any(PresetNote.class));
	}
}
