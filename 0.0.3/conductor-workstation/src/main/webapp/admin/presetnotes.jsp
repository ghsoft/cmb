<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Conductor Preset Notes</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../css/dataTable.css"/>
    <link rel="stylesheet" type="text/css" href="../css/conductor.css"/>
</head>

<body>
	<div class="navbar navbar-fixed-top">
	    <div class="navbar-inner">
	        <div class="container-fluid">
	            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </a>
	            <a class="brand" href="#">Conductor Preset Notes</a>
				<div class="nav-collapse collapse">
	                <p class="navbar-text pull-right">
                        <a class="icon-home-black" href="/"></a>
                    </p>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="container-fluid">
		<div class="row-fluid">
			<button type="button" id="preset-note-add-btn" class="btn btn-small btn-primary">Create Preset Note</button>
			<table id="preset-notes-table" class="table table-striped" style="max-width: 600px;">
				<thead>
					<tr>
						<th>Preset Notes</th>
						<th style="width: 34px;"><!-- Edit/Delete --></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>

	<footer class="navbar-inner" id="footer-admin"></footer>
	
	<!-- Modals -->
	<jsp:include page="dialogs/edit-preset-note.html" />
	<jsp:include page="dialogs/delete-confirm.html" />

	<!-- Scripts -->
	<script type="text/javascript" src="../js/jquery/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../js/bootstrap/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap/bootstrapx-clickover.js"></script>
	<script type="text/javascript" src="../js/util.js"></script>
	<script type="text/javascript" src="../js/restservices/PresetNotesService.js"></script>
	<script type="text/javascript" src="../js/admin.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			admin.initView();
		});
	</script>

</body>

</html>