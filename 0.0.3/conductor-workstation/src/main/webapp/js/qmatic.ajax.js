/**
* Qmatic AJAX functions.
*
* Provides wrapper functions for making AJAX calls using jQuery.
*
* GET - data is never sent, data is always returned
* PUT - sometimes data is sent, sometimes data is returned
* POST - data always sent, sometimes data is returned
* DELETE - data is never sent, data is never returned
**/
var qmatic = qmatic || {};
qmatic.ajax = function() {
	
	var _timeout;
	var _path;
	var _error;
	
	/* Asynchronous GET */
	var _getReq = function(uri, callback) {
		$.ajax({
			url: _path + uri,
			type: 'GET',
			aync: true,
			cache: false,
			timeout: _timeout,
			success: function(data) {
				callback(data);
			},
			error: function(xhr, status, e) {
				_error(xhr.status, xhr.statusText);
			}
		});
	}
	
	/* Asynchronous PUT - send no data, return no data */
	var _putReq = function(uri) {
		$.ajax({
			url: _path + uri,
			type: 'PUT',
			aync: true,
			cache: false,
			timeout: _timeout,
			success: function(data) {
				
			},
			error: function(xhr, status, e) {
				_error(xhr.status, xhr.statusText);
			}
		});
	}
	
	/* Asynchronous PUT - send no data, return data */
	var _putReqCallback = function(uri, callback) {
		$.ajax({
			url: _path + uri,
			type: 'PUT',
			aync: true,
			cache: false,
			timeout: _timeout,
			success: function(data) {
				callback(data);
			},
			error: function(xhr, status, e) {
				_error(xhr.status, xhr.statusText);
			}
		});
	}
	
	/* Asynchronous PUT - send data, return no data */
	var _putReqData = function(uri, data) {
		$.ajax({
			url: _path + uri,
			type: 'PUT',
			contentType: 'application/json',
			data: data,
			aync: true,
			cache: false,
			timeout: _timeout,
			success: function(data) {
				
			},
			error: function(xhr, status, e) {
				_error(xhr.status, xhr.statusText);
			}
		});
	}
	
	/* Asynchronous PUT - send data, return data */
	var _putReqDataCallback = function(uri, data, callback) {
		$.ajax({
			url: _path + uri,
			type: 'PUT',
			contentType: 'application/json',
			data: data,
			aync: true,
			cache: false,
			timeout: _timeout,
			success: function(data) {
				callback(data);
			},
			error: function(xhr, status, e) {
				_error(xhr.status, xhr.statusText);
			}
		});
	}
	
	/* Asynchronous POST - send data, return no data */
	var _postReq = function(uri, data) {
		$.ajax({
			url: _path + uri,
			type: 'POST',
			contentType: 'application/json',
			data: data,
			aync: true,
			cache: false,
			timeout: _timeout,
			success: function(data) {
				
			},
			error: function(xhr, status, e) {
				_error(xhr.status, xhr.statusText);
			}
		});
	}
	
	/* Asynchronous POST - send data, return data */
	var _postReqCallback = function(uri, data, callback) {
		$.ajax({
			url: _path + uri,
			type: 'POST',
			contentType: 'application/json',
			data: data,
			aync: true,
			cache: false,
			timeout: _timeout,
			success: function(data) {
				callback(data);
			},
			error: function(xhr, status, e) {
				_error(xhr.status, xhr.statusText);
			}
		});
	}
	
	/* Asynchronous DELETE */
	var _deleteReq = function(uri) {
		$.ajax({
			url: _path + uri,
			type: 'DELETE',
			aync: true,
			cache: false,
			timeout: _timeout,
			success: function(data) {
				
			},
			error: function(xhr, status, e) {
				_error(xhr.status, xhr.statusText);
			}
		});
	}
	
	return {
		init: function(path, timeout, errorFunc) {
			_timeout = timeout;
			_path = path;
			_error = errorFunc;
		},
		
		get: function(uri, callback) {
			_getReq(uri, callback);
		},
		
		put: function(uri, data, callback) {
			if(data === undefined) {
				if(callback === undefined) {
					return _putReq(uri);
				} else {
					_putReqCallback(uri, callback);
				}
			} else {
				if(callback === undefined) {
					_putReqData(uri, data);
				} else {
					_putReqDataCallback(uri, data, callback);
				}
			}
		},
		
		post: function(uri, data, callback) {
			if(callback === undefined) {
				_postReq(uri, data);
			} else {
				_postReqCallback(uri, data, callback);
			}
		},
		
		delete: function(uri) {
			_deleteReq(uri);
		}
	};
}();