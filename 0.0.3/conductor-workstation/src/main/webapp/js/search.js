var search = new function() {

    this.initGUI = function() {
        // Bind search buttons
        $('#search_btn').unbind().bind('click', search.findCase);
        $('#search_fld').keyup(function(e) {
        	if(e.keyCode == 13) {
        		search.findCase();
        	}
        });
        $('#search_clear_btn').unbind().click(search.clearResults);
    };

    // Searching...
    this.findCase = function() {
        var searchArg = $.trim($('#search_fld').val());
        if(searchArg.length < 2) {
            util.showMessage('Please enter at least two character in the search field.');
            return;
        }

        $('#search_results_div').dataTable().fnClearTable();

        CaseServicePointService.searchCase(searchArg)
            .done(function(cases){
                panels.showSearchResultsPanel();

                if(cases.length > 0) {

                    for(var i = 0; i < cases.length; i++) {
                        var services = common.buildServicesString(cases[i].caseServices);
                        var statuses = common.buildServicesStatuses(cases[i].caseServices);

                        $('#search_results_div').dataTable().fnAddData(
                            [  {
                                "customerName" : cases[i].caseCustomer.fullName,
                                "ticketNumber" : cases[i].ticketNumber,
                                "created" : cases[i].created,
                                "ongoingServiceName" : services,
                                "ongoingServiceStatus" : statuses,
                                "customerId" : cases[i].caseCustomer.id,
                                "qpVisitId" : cases[i].qpVisitId
                            }
                            ]
                        );
                    }
                }
            })
            .fail(function(err){
				util.showError(err.responseText);
              //TODO errormessage?...
            })
    };


    this.clearResults = function() {
        $('#search_fld').val('');
        $('#search_results').addClass('hidden');

        common.updateQueuesTable('queues_table');
    };
};