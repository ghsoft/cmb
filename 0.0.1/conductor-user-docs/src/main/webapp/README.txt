Customize the GUI:

1. Copy the PDF files into /docs folder
2. In /js/settings.js, add the files in JSON format, put the name, description and path, example:
	var documents = [
		{"name" : "Document 1", "desc" : "Click here to view the file", "path" : "docs/doc1.pdf"}
	,	{"name" : "Document 2", "desc" : "Click here to view the file", "path" : "docs/doc2.pdf"}
	,	{"name" : "Document 3", "desc" : "Click here to view the file", "path" : "docs/doc3.pdf"}
	,	{"name" : "Document 4", "desc" : "Click here to view the file", "path" : "docs/doc4.pdf"}
	];