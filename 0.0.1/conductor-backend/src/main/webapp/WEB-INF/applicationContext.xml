<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:task="http://www.springframework.org/schema/task"
       xmlns:cache="http://www.springframework.org/schema/cache"
       xmlns:p="http://www.springframework.org/schema/p"
       xmlns:jee="http://www.springframework.org/schema/jee"
       xmlns:jms="http://www.springframework.org/schema/jms"
       xsi:schemaLocation="
		http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.1.xsd
		http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-3.1.xsd
		http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-3.0.xsd
		http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-3.1.xsd
        http://www.springframework.org/schema/task http://www.springframework.org/schema/task/spring-task-3.1.xsd
        http://www.springframework.org/schema/cache http://www.springframework.org/schema/cache/spring-cache-3.1.xsd
        http://www.springframework.org/schema/jee http://www.springframework.org/schema/jee/spring-jee-3.0.xsd
        http://www.springframework.org/schema/jms http://www.springframework.org/schema/jms/spring-jms.xsd">

    <context:component-scan base-package="com.qmatic.ps.bp"/>

    <!-- JBoss 7: java:/jdbc/conductorDS -->
    <!-- Tomcat: java:/comp/env/jdbc/conductorDS -->
    <!-- GlassFish: jdbc/conductorDS -->
    <jee:jndi-lookup id="dataSource" jndi-name="jdbc/conductorDS" expected-type="javax.sql.DataSource"/>

    <task:annotation-driven executor="myExecutor" scheduler="myScheduler"></task:annotation-driven>

    <task:executor id="myExecutor" pool-size="5"/>
    <task:scheduler id="myScheduler" pool-size="10"/>

    <cache:annotation-driven />

    <!-- Ehcache library setup -->
    <bean id="cacheManager" class="org.springframework.cache.ehcache.EhCacheCacheManager" p:cacheManager-ref="ehcache" />
    <bean id="ehcache" class="org.springframework.cache.ehcache.EhCacheManagerFactoryBean" p:shared="true" p:configLocation="classpath:ehcache.xml"/>

    <bean id="transactionManager" class="org.springframework.orm.jpa.JpaTransactionManager">
        <property name="entityManagerFactory" ref="entityManagerFactory" />
        <property name="dataSource" ref="dataSource" />
    </bean>

    <bean id="jpaAdapter" class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter">
        <!-- <property name="databasePlatform" value="org.hibernate.dialect.H2Dialect" /> -->
        <property name="databasePlatform" value="org.hibernate.dialect.SQLServer2008Dialect" />
        <property name="showSql" value="true" />
        <property name="generateDdl" value="true" />
    </bean>

    <bean id="entityManagerFactory" class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
        <property name="dataSource" ref="dataSource" />
        <property name="jpaVendorAdapter" ref="jpaAdapter" />
        <property name="persistenceUnitName" value="conductorPU" />
        <!--
        <property name="persistenceUnitManager">
            <bean class="org.springframework.orm.jpa.persistenceunit.DefaultPersistenceUnitManager">
                <property name="defaultDataSource" ref="dataSource" />
            </bean>
        </property>
        -->
        <property name="jpaPropertyMap">
            <map>
                <!-- <entry key="hibernate.dialect" value="org.hibernate.dialect.H2Dialect" /> -->
                <entry key="hibernate.dialect" value="org.hibernate.dialect.SQLServer2008Dialect" />
            </map>
        </property>
    </bean>



    <!-- The Transactional advice (i.e. what 'happens' ; see the <aop:advisor/>
       bean below) -->
    <tx:advice id="txAdviceDao" transaction-manager="transactionManager">
        <tx:attributes>
            <!-- all methods starting with 'get' and 'find' are read-only -->
            <tx:method name="find*" read-only="true" />
            <tx:method name="get*" read-only="true" />
            <tx:method name="*Exists" read-only="true" />
            <tx:method name="create*" rollback-for="com.qmatic.qp.bp.common.BPException"/>
            <!-- other methods use the default transaction settings (see below) -->
            <tx:method name="*" rollback-for="com.qmatic.qp.bp.common.BPException" propagation="MANDATORY" />
        </tx:attributes>
    </tx:advice>

    <tx:advice id="txAdvice" transaction-manager="transactionManager">
        <tx:attributes>
            <!-- all methods starting with 'get' and 'find' are read-only -->
            <tx:method name="find*" read-only="true" />
            <tx:method name="get*" read-only="true" />
            <!-- other methods use the default transaction settings (see below) -->
            <tx:method name="*" rollback-for="com.qmatic.qp.bp.common.BPException" />
        </tx:attributes>
    </tx:advice>

    <tx:advice id="bootstrapTxAdvice" transaction-manager="transactionManager">
        <tx:attributes>
            <tx:method name="init" rollback-for="com.qmatic.qp.bp.common.BPException" propagation="REQUIRES_NEW"/>
        </tx:attributes>
    </tx:advice>

    <tx:advice id="mdbTxAdvice" transaction-manager="transactionManager">
        <tx:attributes>
            <tx:method name="onMessage" rollback-for="com.qmatic.qp.bp.common.BPException" propagation="REQUIRES_NEW"/>
        </tx:attributes>
    </tx:advice>

	<aop:aspectj-autoproxy/>

    <!-- ensure that the above transactional advice runs for any execution of
        an operation defined by the FooService interface -->
    <aop:config>
        <aop:pointcut id="daoTransaction" expression="execution(* com.qmatic.ps.bp.dao..*Dao.*(..))" />
        <aop:pointcut id="endpointTransaction" expression="execution(* com.qmatic.ps.bp.rest.*.*Endpoint.*(..))" />
        <aop:pointcut id="bootstrapTransaction" expression="execution(* com.qmatic.ps.bp.startup.Boot*.*(..))" />
        <aop:pointcut id="syncTransaction" expression="execution(* com.qmatic.ps.bp.sync.*.*(..))" />
        <aop:pointcut id="taskTransaction" expression="execution(* com.qmatic.ps.bp.sync.task.*Task.*(..))" />
        <aop:pointcut id="mdbTransaction" expression="execution(* com.qmatic.ps.bp.mdb.*.*(..))" />

        <aop:advisor advice-ref="txAdviceDao" pointcut-ref="daoTransaction" />
        <aop:advisor advice-ref="txAdvice" pointcut-ref="endpointTransaction" />
        <aop:advisor advice-ref="bootstrapTxAdvice" pointcut-ref="bootstrapTransaction" />
        <aop:advisor advice-ref="txAdvice" pointcut-ref="syncTransaction" />
        <aop:advisor advice-ref="txAdvice" pointcut-ref="taskTransaction" />
        <aop:advisor advice-ref="mdbTxAdvice" pointcut-ref="mdbTransaction" />

    </aop:config>


    <!-- Makes it possible to use annotation PersistenceContext in DAOs -->
    <bean id="persistenceAnnotation" class="org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor"/>


    <!-- this is the Message Driven POJO (MDP)
    <bean id="messageListener" class="com.qmatic.ps.bp.mdb.ResetListener" init-method="init"/>

    <jee:jndi-lookup id="connectionFactory" jndi-name="QPInternalConnectionFactory"/>

    <bean id="destination" class="org.springframework.jndi.JndiObjectFactoryBean">  -->
        <!-- <property name="jndiTemplate" ref="jndiTemplate"/>       -->   <!--
        <property name="jndiName" value="topic/qpEventTopic"/>
    </bean>

    <bean id="jmsContainer" class="org.springframework.jms.listener.DefaultMessageListenerContainer">
        <property name="connectionFactory" ref="connectionFactory"/>
        <property name="destination" ref="destination"/>
        <property name="messageListener" ref="messageListener" />
    </bean>
          -->



    <!-- JMS Topic Connection Factory -->
    <bean id="jndiJmsTopicConnectionFactory" class="org.springframework.jndi.JndiObjectFactoryBean">
        <property name="jndiName" value="QPInternalConnectionFactory" />
        <property name="lookupOnStartup" value="false"/>
        <property name="cache" value="true" />
        <property name="proxyInterface"  value="javax.jms.TopicConnectionFactory"/>
    </bean>

    <!--Spring JMS Topic Connection Factory-->
    <bean id="jmsTopicConnectionFactory" class="org.springframework.jms.connection.SingleConnectionFactory">
        <property name="targetConnectionFactory" ref="jndiJmsTopicConnectionFactory" />
    </bean>

    <!--Message Driven POJO-->
    <bean id="messageListener" class="com.qmatic.ps.bp.mdb.ResetListener" autowire="byName"/>

    <bean id="destination" class="org.springframework.jndi.JndiObjectFactoryBean">
        <property name="jndiName" value="topic/qpEventTopic" />
    </bean>
    <!--and this is the message listener container-->
    <bean id="jmsContainer" class="org.springframework.jms.listener.DefaultMessageListenerContainer">
        <property name="connectionFactory" ref="jmsTopicConnectionFactory"/>
        <property name="destination" ref="destination"/>
        <property name="messageListener" ref="messageListener" />
    </bean>
</beans>