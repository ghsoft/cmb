// start QP Java REST client API
package com.qmatic.ps.bp.rest.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qmatic.ps.bp.util.CookieHolder;
import com.qmatic.qp.api.connectors.dto.SystemInformation;
import com.qmatic.qp.api.connectors.dto.WorkProfile;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;

@Component
public class ManagementInformationServiceClient {

    private final Logger log = LoggerFactory.getLogger(ManagementInformationServiceClient.class);

    protected Client c;
    protected ObjectMapper om = new ObjectMapper();

    @PostConstruct
    public void init() {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put("com.sun.jersey.api.json.POJOMappingFeature", Boolean.TRUE);
        c = Client.create(clientConfig);
        c.setFollowRedirects(true);
        c.setConnectTimeout(5000);
    }

    protected String getRestPath() {
        return getProtocol() + "://" + getIp() + ":" + getPort() + "/" +  getBasePath();
    }

    protected String getProtocol() {
        return "http";
    }

    protected String getIp() {
        return "127.0.0.1";
    }

    protected String getPort() {
        return "8080";
    }

    protected String getBasePath() {
        return "rest";    }

    private Cookie getSSOCookie() {
        return CookieHolder.get();
    }

// start JAX-RS API
public java.util.List<com.qmatic.qp.api.connectors.dto.Queue> getQueueData(Integer branch) {
    java.util.List<com.qmatic.qp.api.connectors.dto.Queue> result = c.resource(getRestPath() + "/managementinformation/branches/" + branch + "/queues")
    .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
    .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.Queue>>() {} );
    return result;
}

public java.util.List<com.qmatic.qp.api.connectors.dto.VisitData> getVisitData(Integer branch,Integer queue) {
    long start = System.currentTimeMillis();
    java.util.List<com.qmatic.qp.api.connectors.dto.VisitData> result = c.resource(getRestPath() + "/managementinformation/branches/" + branch + "/queues/" + queue + "/visits")
    .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
    .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.VisitData>>() {} );
    log.info("Getting visits took " + (System.currentTimeMillis() - start) + " ms");

    return result;
}

public WorkProfile getProfile(Integer branch,String username) {
    WorkProfile result = c.resource(getRestPath() + "/managementinformation/branches/" + branch + "/users/" + username + "/profile")
    .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
    .get(new GenericType<WorkProfile>() {} );
    return result;
}

public java.util.List<com.qmatic.qp.api.connectors.dto.Branch> getBranchData() {
    java.util.List<com.qmatic.qp.api.connectors.dto.Branch> result = c.resource(getRestPath() + "/managementinformation/branches")
    .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
    .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.Branch>>() {} );
    return result;
}

public java.util.List<com.qmatic.qp.api.connectors.dto.ServicePointData> getServicePointData(Integer branch) {
    java.util.List<com.qmatic.qp.api.connectors.dto.ServicePointData> result = c.resource(getRestPath() + "/managementinformation/v2/branches/" + branch + "/servicePoints")
    .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
    .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.ServicePointData>>() {} );
    return result;
}

public java.util.List<com.qmatic.qp.api.connectors.dto.WorkProfile> getWorkProfiles(Integer branch) {
    java.util.List<com.qmatic.qp.api.connectors.dto.WorkProfile> result = c.resource(getRestPath() + "/managementinformation/branches/" + branch + "/profiles")
    .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
    .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.WorkProfile>>() {} );
    return result;
}

public void setWorkProfile(Integer branchId,String username,Integer workProfileId) {
    c.resource(getRestPath() + "/managementinformation/branches/" + branchId + "/user/" + username + "/profile/" + workProfileId)
    .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
    .put();
}

public java.util.List<com.qmatic.qp.api.connectors.dto.User> getCurrentLoggedInUsers(Integer branch,Long servicepoint) {
    java.util.List<com.qmatic.qp.api.connectors.dto.User> result = c.resource(getRestPath() + "/managementinformation/branches/" + branch + "/servicePoints/" + servicepoint)
    .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
    .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.User>>() {} );
    return result;
}

public SystemInformation getSystemInformation() {
    SystemInformation result = c.resource(getRestPath() + "/managementinformation/systemInformation")
    .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
    .get(new GenericType<SystemInformation>() {} );
    return result;
}

}

