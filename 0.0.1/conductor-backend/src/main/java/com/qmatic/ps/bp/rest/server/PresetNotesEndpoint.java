package com.qmatic.ps.bp.rest.server;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.common.BPExceptionCode;
import com.qmatic.ps.bp.dao.admin.PresetNoteDao;
import com.qmatic.ps.bp.model.admin.PresetNote;

/**
 * @author Dejan Milojevic (dejmil), @created Nov 6, 2013
 */

@Component
@Scope(value = "request")
@Path("/admin")
public class PresetNotesEndpoint {
	
	private PresetNoteDao presetNoteDao;
	
	@POST
	@Path("/presetNotes/{id}/{text}")
	@Produces(value = MediaType.APPLICATION_JSON)
	public Response savePresetNote(@PathParam("id") Integer id, @PathParam("text") String text) throws BPException {
		try {
			PresetNote presetNote;
			if (id != 0) {
				presetNote = presetNoteDao.findById(id);
				presetNote.setText(text);
				presetNoteDao.update(presetNote);
			} else {
				presetNote = new PresetNote();
				presetNote.setText(text);
				presetNoteDao.create(presetNote);
			}
			return Response.ok(presetNote).build();
		} catch(Exception e){
            throw new BPException("Failed to save Preset Note: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
	}

	@GET
	@Path("/presetNotes")
	@Produces(value = MediaType.APPLICATION_JSON)
	public Response getPresetNotes() throws BPException {
		try {
			List<PresetNote> presetNotes = presetNoteDao.getAll();
			return Response.ok(presetNotes).build();
		} catch (Exception e) {
			throw new BPException("Failed to retrieve Preset Notes: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DELETE
	@Path("/presetNotes/{id}")
	@Produces(value = MediaType.APPLICATION_JSON)
	public Response deletePresetNote(@PathParam("id") Integer id) throws BPException {
		try {
			PresetNote presetNote = presetNoteDao.findById(id);
			presetNoteDao.delete(presetNote);
			return Response.ok(new PresetNote()).build();
		} catch(Exception e){
            throw new BPException("Failed to delete Preset Note: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
	}

	@Autowired
	public void setPresetNoteDao(PresetNoteDao presetNoteDao) {
		this.presetNoteDao = presetNoteDao;
	}

}
