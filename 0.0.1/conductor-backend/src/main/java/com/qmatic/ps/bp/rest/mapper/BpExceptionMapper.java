package com.qmatic.ps.bp.rest.mapper;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.common.BPExceptionCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionSystemException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class BpExceptionMapper implements ExceptionMapper<Exception> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public Response toResponse(Exception x) {

        Response.ResponseBuilder rb = null;

        if (x instanceof BPException) {
            rb = handleCalException((BPException) x);
        } else if (x instanceof TransactionSystemException) {
            if (((TransactionSystemException) x).getOriginalException() instanceof BPException) {
                BPException BPException = (BPException) ((TransactionSystemException) x).getOriginalException();
                rb = handleCalException(BPException);
            } else {
                rb = Response.serverError();
                rb.header("ERROR_CODE", 0);
                rb.header("ERROR_MESSAGE", x.getMessage());
            }
            // TODO add some mechanism that can handle JAX-RS NotFoundExceptions in a vendor-neutral way.
        } else {
            rb = Response.serverError();
            rb.header("ERROR_CODE", 0);
            rb.header("ERROR_MESSAGE", x.getMessage());
        }
        return rb.build();
    }

    private Response.ResponseBuilder handleCalException(BPException BPException) {
        Response.ResponseBuilder rb = null;
        switch (BPException.getCode()) {
            case BPExceptionCode.BRANCH_NOT_FOUND:
                rb = Response.status(Response.Status.BAD_REQUEST);
                rb.entity(errorMsgAsEntity(BPException));
                break;
            case BPExceptionCode.SERVICE_NOT_FOUND:
                rb = Response.status(Response.Status.BAD_REQUEST);
                rb.entity(errorMsgAsEntity(BPException));
                break;
            case BPExceptionCode.INVALID_DATE_SELECTION:
                rb = Response.status(Response.Status.BAD_REQUEST);
                rb.entity(errorMsgAsEntity(BPException));
                break;
            case BPExceptionCode.INVALID_TIME_SELECTION:
                rb = Response.status(Response.Status.BAD_REQUEST);
                rb.entity(errorMsgAsEntity(BPException));
                break;
            case BPExceptionCode.COULD_NOT_IDENTIFY_CUSTOMER:
                break;
            case BPExceptionCode.DESERIALIZATION_ERROR:
            case BPExceptionCode.SERIALIZATION_ERROR:
                rb = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
                rb.entity(errorMsgAsEntity(BPException));
                break;
            case BPExceptionCode.NOT_FOUND:
            case BPExceptionCode.NOT_FOUND_QP_ID:
            case BPExceptionCode.NOT_FOUND_PUBLIC_ID:
            case BPExceptionCode.READ_ERROR:
                rb = Response.status(Response.Status.NOT_FOUND);
                rb.entity(errorMsgAsEntity(BPException));
                break;

            case BPExceptionCode.NO_SUCH_FIELD:
                rb = Response.serverError();
                rb.entity(errorMsgAsEntity(BPException));
                break;
            default:
                rb = Response.serverError();
                rb.entity(BPException.getMessage());
                break;
        }

        rb.header("ERROR_CODE", BPException.getCode());
        rb.header("ERROR_MESSAGE", BPException.getMessage());

        if (log.isTraceEnabled())
            log.trace(rb.toString(), BPException);
        return rb;
    }

    private String errorMsgAsEntity(BPException BPException) {
        return "{\"msg\":\"" + BPException.getMessage() + "\"}";
    }
}
