package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.ServiceLevel;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.ws.rs.core.MultivaluedMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class ServiceLevelJpaDao extends JpaDao<ServiceLevel> implements ServiceLevelDao {

    @Override
    public List<ServiceLevel> getServiceLevels() throws BPException {
		
        List<ServiceLevel> serviceLevels = entityManager.createQuery("SELECT sl FROM ServiceLevel sl")
                .getResultList();

        return serviceLevels;
    }
	
	@Override
    public ServiceLevel getServiceLevel(Integer serviceId) throws BPException {
		
        List<ServiceLevel> serviceLevels = entityManager.createQuery("SELECT sl FROM ServiceLevel sl WHERE sl.serviceId = :serviceId")
				.setParameter("serviceId", serviceId)
                .getResultList();
				
		if(serviceLevels.size() == 0) {
            return null;
        } else {
            return serviceLevels.get(0);
        }
    }

}
