package com.qmatic.ps.bp.rest.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qmatic.ps.bp.util.CookieHolder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-05-20
 * Time: 17:03
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseConnectorClient {

    protected Client client;
    protected ObjectMapper om = new ObjectMapper();

    public void init() {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put("com.sun.jersey.api.json.POJOMappingFeature", Boolean.TRUE);
        client = Client.create(clientConfig);
        client.setFollowRedirects(true);
        client.setConnectTimeout(5000);
    }

    protected String getRestPath() {
        return getProtocol() + "://" + getIp() + ":" + getPort() + "/" +  getBasePath();
    }
    
    protected Cookie getSSOCookie() {
        return CookieHolder.get();
    }
    
    protected Builder createResourceBuilder(String path) {
    	Builder resourceBuilder = client.resource(getRestPath() + path)
    			.accept(MediaType.APPLICATION_JSON_TYPE)
    			.cookie(getSSOCookie());
		return resourceBuilder;
    }

    protected String getProtocol() {
        return "http";
    }

    protected String getIp() {
        return "127.0.0.1";
    }

    protected String getPort() {
        return "8080";
    }

    protected String getBasePath() {
        return "rest";
    }

}
