package com.qmatic.ps.bp.rest.server;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.common.BPExceptionCode;
import com.qmatic.ps.bp.model.enumeration.Status;
import com.qmatic.ps.bp.dao.CaseDao;
import com.qmatic.ps.bp.dao.CaseServicePointDao;
import com.qmatic.ps.bp.model.GeneralParams;
import com.qmatic.ps.bp.model.CaseBranch;
import com.qmatic.ps.bp.model.Case;
import com.qmatic.ps.bp.model.CaseService;
import com.qmatic.ps.bp.model.CaseServicePoint;
import com.qmatic.ps.bp.rest.client.ManagementInformationServiceClient;
import com.qmatic.ps.bp.rest.client.EntryPointServiceClient;
import com.qmatic.ps.bp.util.CookieHolder;
import com.qmatic.ps.bp.util.SettingsUtil;
import com.qmatic.qp.api.connectors.dto.ServicePointData;
import com.qmatic.qp.api.connectors.dto.ServicePoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import java.util.*;

import javax.ws.rs.GET;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-06-17
 * Time: 10:52
 * To change this template use File | Settings | File Templates.
 */
@Component
@Scope(value = "request")
@Path("/opspanel")
public class OpsPanelEndpoint {

    private final Logger log = LoggerFactory.getLogger(OpsPanelEndpoint.class);

    @Context
    UriInfo uriInfo;

    @Autowired(required = true)
    CaseDao caseDao;
	
	@Autowired(required = true)
    CaseServicePointDao caseServicePointDao;
	
	@Autowired
    EntryPointServiceClient entryPointClient;
	
	@Autowired
    CaseServicePointEndpoint caseServicePointEndpoint;

    @Autowired
    SettingsUtil settingsUtil;

    @Context
    protected HttpHeaders headers;

    @Autowired
    ManagementInformationServiceClient managementInformationClient;

    private void setSSOCookie() {
        CookieHolder.set(headers.getCookies().get("SSOcookie"));
    }

    @GET
    @Path("/branches/{branchId}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getBranch(@PathParam("branchId") Integer branchId) throws BPException {
		try {
			setSSOCookie();
			/*
			CaseBranch caseBranch = caseBranchDao.getBranchInfo(branchId);
			caseBranch.calculateAverageWaitingTime();
			*/
			
			CaseBranch caseBranch = new CaseBranch();
			caseBranch.setId(branchId);
			List<Case> todaysCases = caseDao.getTodaysCases(branchId);
			List<Case> waitingCases = caseServicePointEndpoint.getWaitingCases(branchId, todaysCases);
			
			Boolean caseServedCounted = false;
			Boolean caseWaitingCounted = false;
			Integer customersServed = 0;
			Integer servicesServed = 0;
			Integer customersBeingServed = 0;
			Integer customersWaiting = 0;
			Integer servicesWaiting = 0;
			Integer totalWaitingTime = 0;
			Integer maxWaitingTime = 0;
			
			for(Case c : todaysCases) {
				caseServedCounted = false;
				for(CaseService cs : c.getCaseServices()) {
					if(cs.getStatus() == Status.FINISHED) {
						if (!caseServedCounted) {
							customersServed++;
							caseServedCounted = true;
						}
						servicesServed++;
					}
					if(cs.getStatus() == Status.SERVING || cs.getStatus() == Status.POST_PROCESSING) {
						customersBeingServed++;
					}
				}
			}
			for(Case c : waitingCases) {
				caseWaitingCounted = false;
				for(CaseService cs : c.getCaseServices()) {
					if(cs.getStatus() == Status.WAITING || cs.getStatus() == Status.PENDING) {
						if (!caseWaitingCounted) {
							customersWaiting++;
							caseWaitingCounted = true;
						}
						servicesWaiting++;
						Long sWt = (new Date().getTime() - cs.getCreated().getTime()) / 1000;
						totalWaitingTime += sWt.intValue();
						if (sWt > maxWaitingTime) {
							maxWaitingTime = sWt.intValue();
						}
					}
					else if(cs.getStatus() == Status.RECYCLED) {
						Long rWt = (new Date().getTime() - cs.getUpdated().getTime()) / 1000;
						if (rWt > GeneralParams.SYSTEM_RECYCLE_DELAY) {
							if (!caseWaitingCounted) {
								customersWaiting++;
								caseWaitingCounted = true;
							}
							servicesWaiting++;
							totalWaitingTime += rWt.intValue();
							if (rWt > maxWaitingTime) {
								maxWaitingTime = rWt.intValue();
							}
						}
					}
				}
			}
			
			caseBranch.setCustomersServed(customersServed);
			caseBranch.setServicesServed(servicesServed);
			caseBranch.setCustomersBeingServed(customersBeingServed);
			caseBranch.setCustomersWaiting(customersWaiting);
			caseBranch.setServicesWaiting(servicesWaiting);
			caseBranch.setTotalWaitingTime(totalWaitingTime);
			caseBranch.setMaxWaitingTime(maxWaitingTime);
			caseBranch.calculateAverageWaitingTime();
			
			return Response.ok(caseBranch).build();
		}
		catch(Exception e){
            log.error("Caught Exception getBranch : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not get Branch: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }

    @GET
    @Path("/branches/{branchId}/servicepoints")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getServicePoints(@PathParam("branchId") Integer id) throws BPException {
		try {
			setSSOCookie();
			
			List<ServicePoint> servicePoints = entryPointClient.getServicePointsByDeviceType(id, "SW_SERVICE_POINT");
			List<ServicePointData> servicePointDatas = managementInformationClient.getServicePointData(id);
			List<CaseServicePoint> caseServicePoints = caseServicePointDao.getServicePointsInfo();
		
			for(ServicePointData servicePointData: servicePointDatas){
				boolean spFound = false;
				if (servicePointData.getStatus() != null) {
					//if (servicePointData.getStatus().toString().equals("OPEN")) {
						for(CaseServicePoint caseServicePoint: caseServicePoints){
							if(caseServicePoint.getName() != null && servicePointData.getName() != null) {
								if(caseServicePoint.getName().equals(servicePointData.getName())){
									caseServicePoint.setStaffName(servicePointData.getStaffName());
									caseServicePoint.setWorkProfileId(servicePointData.getWorkProfileId());
									caseServicePoint.setWorkProfileName(servicePointData.getWorkProfileName());
									caseServicePoint.setStatus(servicePointData.getStatus().toString());
									spFound = true;
									break;
								}
								else if(caseServicePoint.getName().equals("")){
									if(caseServicePoint.getStaffName() != null && servicePointData.getStaffName() != null) {
										if(caseServicePoint.getStaffName().equals(servicePointData.getStaffName())){
											caseServicePoint.setName(servicePointData.getName());
											caseServicePoint.setStaffFullName(servicePointData.getStaffFullName());
											caseServicePoint.setWorkProfileId(servicePointData.getWorkProfileId());
											caseServicePoint.setWorkProfileName(servicePointData.getWorkProfileName());
											caseServicePoint.setStatus(servicePointData.getStatus().toString());
											spFound = true;
											break;
										}
									}
								}
							}
						}
					//}
				}
				if (!spFound) {
					if(servicePointData.getId() != null && servicePointData.getName() != null) {
						CaseServicePoint newCaseServicePoint = new CaseServicePoint(servicePointData);
						caseServicePoints.add(newCaseServicePoint);
					}
				}
			}
			
			for(CaseServicePoint caseServicePoint: caseServicePoints){
				for(ServicePoint servicePoint: servicePoints){
					if (caseServicePoint.getName() != null && servicePoint.getName() != null) {
						if(caseServicePoint.getName().equals(servicePoint.getName())){
							if (servicePoint.getUnitId() != null) {
								String unitId = servicePoint.getUnitId().replaceAll("\\D+","");
								caseServicePoint.setId(Long.parseLong(unitId));
								break;
							}
						}
					}
				}
			}
			
			return Response.ok(caseServicePoints).build();
		}
		catch(Exception e){
            log.error("Caught Exception getServicePoints : {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
            throw new BPException("Could not get Servicepoints: " + e.getMessage(), BPExceptionCode.INTERNAL_SERVER_ERROR);
        }
    }
}
