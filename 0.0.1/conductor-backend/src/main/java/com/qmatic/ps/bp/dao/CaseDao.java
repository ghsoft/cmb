package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.Case;

import java.util.List;
import java.util.Map;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
public interface CaseDao extends Dao<Case> {

	Case getCase(Integer branchId, Long caseId) throws BPException;
	
    Case getCaseByVisitId(Integer branchId, Long qpVisitId) throws BPException;
	
	List<Case> getTodaysCases(Integer branchId) throws BPException;
	
    List<Case> searchCases(Integer branchId, String searchArg, Map<String, List<String>> params) throws BPException;
	
	List<Case> findCasesByCustomerId(Integer branchId, Long customerId) throws BPException;
	
	List<Case> findCasesByTicketNumber(Integer branchId, String ticketNumber) throws BPException;
	
	List<Case> findCasesByDate(Integer branchId, Date date) throws BPException;

    List<Case> findCasesHistoryByCustomerId(Long customerId) throws BPException;

    List<Case> findAllCasesByCustomerId(Long customerId) throws BPException;

    List<Case> findOnGoingCasesForBranchAndServicePoint(Integer branchId, Integer servicePointId) throws BPException;

    List<Case> findVisitsInTrashQueue(Integer branchId) throws BPException;

    List<Case> findCasesInTrashQueues(Integer branchId) throws BPException;

	boolean isCaseBeingServed(Integer branchId, Long caseId) throws BPException;
	
	void deleteOngoingCasesForBranch(Integer branchId) throws BPException;
}
