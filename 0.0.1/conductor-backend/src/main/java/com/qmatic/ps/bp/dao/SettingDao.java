package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.Setting;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-01-31
 * Time: 16:39
 * To change this template use File | Settings | File Templates.
 */
public interface SettingDao extends Dao<Setting> {

    Setting findByGroup(String group) throws BPException;

    Setting update(Setting setting) throws BPException;
    void create(Setting setting) throws BPException;
    void delete(Setting setting) throws BPException;
    void deleteById(Long id) throws BPException;
}
