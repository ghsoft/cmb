package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.CaseService;
import com.qmatic.ps.bp.model.Note;

import javax.ws.rs.core.MultivaluedMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
public interface CaseServiceDao extends Dao<CaseService> {

	List<CaseService> getCaseServices(Integer branchId, Long caseId) throws BPException;
	
    List<Note> getNotesOfCaseService(Long caseServiceId, MultivaluedMap<String, String> params);

    // CaseService findByServiceIdOnCase(Long caseId, Integer serviceId, Status[] statuses) throws BPException;

    CaseService findByQpVisitServiceId(Long qpVisitServiceId) throws BPException;

    CaseService findServingByServicePoint(Integer branchId, Integer ServicPointId) throws BPException;
	
    CaseService getNextChangeableCaseService(Integer branchId, Long caseId) throws BPException;
	
    CaseService getNextCallableCaseService(Integer branchId, Long caseId) throws BPException;

    void removeMark(Long id, Long visitDeliveredServiceId) throws BPException;

    List<CaseService> getOngoingCaseServices() throws BPException;
	
    List<CaseService> getServicePointsInfo() throws BPException;
	
}
