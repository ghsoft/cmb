package com.qmatic.ps.bp.mdb;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.dao.CaseDao;
import com.qmatic.ps.bp.dao.CaseServiceDao;
import com.qmatic.qp.core.common.QPEvent;
import com.qmatic.qp.constants.QPConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-06-19
 * Time: 15:46
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ResetListener implements MessageListener {

    private static Logger log = LoggerFactory.getLogger(ResetListener.class);

    @Autowired
    CaseServiceDao caseServiceDao;

    @Autowired
    CaseDao caseDao;

    public void init() {
        log.info("Conductor - init method of ResetListener");
    }

    @Override
    public void onMessage(Message message) {
        log.info("Conductor - onMessage");
        if(message instanceof ObjectMessage) {
			try {
				ObjectMessage oMsg = (ObjectMessage) message;
                if(oMsg.getObject() instanceof QPEvent) {
                    QPEvent evt = (QPEvent) oMsg.getObject();
                    log.info("Event: {} received", evt.getEventName() );

                    if(evt.getEventName().equals("BRANCH_PUBLISH")) {
                        resetConductorBranch(evt);
                    }
                }
            } catch (JMSException e) {
                log.error("JMS Exception handling message: {}", e.getMessage());
            }
        }

    }

    // When a Branch is published Orchestra-side, it's queues are emptied. Thus, we need to make sure the Conductor side
    // of things stay in synch.
    private void resetConductorBranch(QPEvent evt) {
        Integer branchId = (Integer) evt.getParameter(QPConstants.PRM_BRANCH_ID);
        log.info("START - Reset branch with ID {}", branchId);

        // Get all Cases for this branch that is in some kind of ongoing state, has visits waiting, being served etc.
        try {
            caseDao.deleteOngoingCasesForBranch(branchId);
            log.info("STOP - Reset branch successful");

        } catch (BPException e) {
            log.error(e.getMessage());
        }

    }
}
