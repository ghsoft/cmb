package com.qmatic.ps.bp.rest.server;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.dao.admin.PresetNoteDao;
import com.qmatic.ps.bp.model.admin.PresetNote;

/**
 * @author Dejan Milojevic (dejmil), @created Nov 6, 2013
 */

@Test
public class AdminEndpointGetPresetNotesTest {

	private AdminEndpoint adminEndpoint;
	private PresetNoteDao presetNoteDao;

	@BeforeMethod
	public void setUp() {
		adminEndpoint = new AdminEndpoint();
		presetNoteDao = mock(PresetNoteDao.class);
		adminEndpoint.setPresetNoteDao(presetNoteDao);
	}

	public void emptyListShouldBeReturnedWhenNoNotesFound() throws BPException {
		doReturn(new ArrayList<PresetNote>()).when(presetNoteDao).getAll();
		Response response = adminEndpoint.getPresetNotes();
		List<PresetNote> result = (List<PresetNote>) response.getEntity();
		assertTrue(result.isEmpty());
	}

}
