/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-05-07
 * Time: 10:45
 * To change this template use File | Settings | File Templates.
 */

var notes = new function() {


    var noteHtml =
        '<div class="row-fluid"><div class="span1"></div><div id="note_headline" class="span3 headline"></div><div class="span7 note"><div id="note_text" class="span7 name"></div>'+
            '<div id="note_author" class="span3 user"></div>'+
            '<div class="span2 text-right action">'+
            '    <i id="edit_icon" class="icon-pencil"></i>'+
            '    <i id="trash_icon" class="icon-delete-note"></i>'+
            '</div></div>'+
            '</div>';


    this.reloadNotes = function(caseServiceId) {
        CaseServicePointService.getCaseServiceNotes(caseServiceId)
            .done(function(notesList){
                notes.populateNotesOfCurrentTransaction(caseServiceId, notesList);
            })
            .fail(function(err){
                util.showError("Could not get notes for Case Service", err);
            })
    };
    
    this.populatePresetNotes = function(note) {
    	PresetNotesService.getPresetNotes()
	    .done(function(result){
	    	$('#preset_notes').empty();
            for (var i = 0; i < result.length; i++) {
                var item = result[i];
                $('#preset_notes').append($("<option />").val(item.text).text(item.text));
            }
	    })
	    .fail(function(error){
	        util.showMessage("Failed to retrieve Preset Notes.", error);
	    });

        if(util.notNull(note) && util.notNull(note.text)) {
            $('#note_textarea').val(note.text);
        }

        $('#select_preset_note').unbind();
        $('#select_preset_note').click(function() {
            var preset = $('#preset_notes').val();
            $('#note_textarea').val($('#note_textarea').val() + " " + preset);
        });
    };

    this.openNoteDialog = function(note, caseServiceId, isEdit, onCompleteCallback) {
    	notes.populatePresetNotes(note);
        
        $('#note_save_btn').unbind();
        $('#note_save_btn').click(function() {

            note.text = $('#note_textarea').val();
            note.createdBy = sessvars.currentUser.fullName; //sessvars.currentUser.userName;
			note.created = new Date();

            // An edit actually just updates the currentNote on the case now, since 2014-02-24
            if(isEdit) {
                CaseServicePointService.updateNote(note)
                    .done(function(msg) {
                        $('#note_textarea').val('');
                        $('#noteModal').modal('hide');
                        if(typeof onCompleteCallback !== 'undefined' && onCompleteCallback != null) {
                            onCompleteCallback(msg);
                        }
                    });
            } else {
                CaseServicePointService.createCaseServiceNote(caseServiceId, note)
					.done(function(msg) {
                        $('#note_textarea').val('');
                        $('#noteModal').modal('hide');
                        if(typeof onCompleteCallback !== 'undefined' && onCompleteCallback != null) {
                            onCompleteCallback(msg);
                        }
                    });
            }

        });
        
        $('#noteModal').modal('show');
    };

    this.openNoteDialogV2 = function(note, caseId, isEdit, onCompleteCallback) {
        notes.populatePresetNotes(note);

        $('#note_save_btn').unbind();
        $('#note_save_btn').click(function() {
            // Create new instance of not already existing.
            if(util.isNull(note)) {
                note = {};
            }
            note.text = $('#note_textarea').val();
            note.createdBy = sessvars.currentUser.fullName; //sessvars.currentUser.userName;
			note.created = new Date();

            // An edit actually just updates the currentNote on the case now, since 2014-02-24
            if(isEdit) {
                CaseServicePointService.updateCurrentNoteOnCase(caseId, note)
                    .done(function(msg) {
                        $('#note_textarea').val('');
                        $('#noteModal').modal('hide');
                        //sessvars.currentCase.currentNote = msg;
                        if(typeof onCompleteCallback !== 'undefined' && onCompleteCallback != null) {
                            onCompleteCallback(msg);
                        }
                    });
            } 
			else {
                CaseServicePointService.createCaseNote(caseId, note)
					.done(function(msg) {
                        $('#noteModal').modal('hide');
                        $('#note_textarea').val('');
						//sessvars.currentCase.currentNote = msg;
                        if(typeof onCompleteCallback !== 'undefined' && onCompleteCallback != null) {
                            onCompleteCallback(msg);
                        }
                    });
            }

        });

        $('#noteModal').modal('show');
    };

    this.populateNoteOfCurrentTransaction = function(pCase) {
        $('#notes-container').empty();
        $('#notes-container').append('<div class="row-fluid"><div class="span11 headline line">Notes</div></div>');
		var container = $('<div class="row-fluid"></div>');
        if(util.notNull(pCase.currentNote)) {
			var content = $('<div class="span11"></div>');
            var markup = markupBuilder.buildNoteMarkupV2(pCase.id, pCase.currentNote, true, false, function() {
                 notes.populateNoteOfCurrentTransaction(sessvars.currentCase);
            });
            content.append(markup);
			container.append(content);
        }
		else {
			container.append('<div class="span8"></div>');
			container.append('<div class="span3" style="text-align:right;"><span id="cst_add_note_btn" style="cursor:pointer;"><i class="icon-add-note"></i> Add Note</span></div>');
		}
		$('#notes-container').append(container);
    };

    this.populateNotesOfCurrentTransaction = function(caseServiceId, notesList) {
        $('#notes-container').empty();
        $('#notes-container').append('<div class="row-fluid"><div class="span1"></div><div class="span10 line"></div></div>');
        $.map(notesList, function(note, index) {
            var headline = "";
            if(index == 0) {
            	headline = "Notes";
            }
            var markup = markupBuilder.buildNoteMarkupV2(sessvars.currentCase.id, note, true, false, (function(_serviceId) {
        		return function() {          
            		servicePoint.populateCurrentTransaction(sessvars.currentCase);
            		//notes.reloadNotes(_serviceId);
        		}
        	})(caseServiceId));
            var container = $('<div class="row-fluid">' +
								'<div class="span1"></div>' +
								'<div class="span3 headline">' + headline + '</div>' +
							'</div>');
            var content = $('<div class="span7"></div>');
            content.append(markup);
            container.append(content);
            $('#notes-container').append(container);
        });
    };

    this.deleteNote = function(noteId, onCompleteCallback) {
        util.openConfirm('Delete note','Are you sure you want to delete this note?', function() {
			CaseServicePointService.deleteNote(noteId)
				.done(function( msg ) {
                    $('#confirm_modal').modal('hide');
                    if(typeof onCompleteCallback !== 'undefined' && onCompleteCallback != null) {
                        onCompleteCallback();
                    }
                });
        });
    };
};
