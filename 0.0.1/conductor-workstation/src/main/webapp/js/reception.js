/**
 * JS functionality specific to the reception part should go in here.
 */

var reception = new function() {

    var workstationOffline = false;
    var _active = false;

    this.setActive = function(active){
        _active = active;
    }

    this.isActive = function (){
        return _active
    }

    var newVisit = {
        "caseServices" : new Array(),
        "ticketNumber" : "",
        "caseCustomer" : {
			"id": ""
		}
    };

    var clearNewVisit = function() {
        newVisit = {
            "caseServices" : new Array(),
            "ticketNumber" : "",
			"caseCustomer" : {
				"id": ""
			}
        };
        sessvars.currentCustomer = null;
        $('#new_visit_add_service_select').val(0);
		$('#new_visit_add_service_btn').addClass('disabled');
        $('#customer_first_name_lbl').text('');
        $('#customer_last_name_lbl').text('');
        $('#customer_mobile_lbl').text('');
        $('#customer_email_lbl').text('');
        $('#cust_search_input_fld').val('');
        $('#new_visit_btn').addClass('disabled');
		$('#new_visit_btn').prop('disabled', false);
        $('#selected_customer_panel').addClass('hidden');
        $('#customer_search_results_container').addClass('hidden');

        $('#new_visit_note').val('');
        updateVisitServicesTable();
    };

    var initEntryPointSelector = function() {

        $('#entrypoint_select').empty();

        CaseServicePointService.getEntryPointsByDeviceType()
            .done(function(entrypoints){
                $.map(entrypoints, function(entryPoint) {
                    $('#entrypoint_select').append($("<option />").val(entryPoint.id).text(entryPoint.name));
                });
            });
    };

    this.initReceptionElements = function() {
        if(reception.isActive()==false) return;

        $('#new_visit_btn').unbind().click(createNewVisit);
        $('#new_visit_clear_all_btn').unbind().click(clearNewVisit);

        initEntryPointSelector();

        customer.initCustomerReceptionElements();
		
        // init the service(s) section for creating new visits
//        $('#create_visit_add_service').unbind();
//        $('#create_visit_add_service').click(function() {
//            common.openAddServiceDialog(newVisit, function(caseId) {
//                // This add service only does things client-side
//                var serviceId = $('#add_service_select').val();
//                var order = 0; // HARD-CODED to FIRST (add on top of existing services)
//
//                var caseService = {
//                    "serviceId": $('#add_service_select').val(),
//                    "serviceName": $('#add_service_select option:selected').text(),
//                    "status" : "PENDING",
//                    "notes" : new Array()
//                };
//
//                newVisit.caseServices.push(caseService);
//                $('#addServiceModal').modal('hide');
//
//                // Also, update the listing
//                updateVisitServicesTable();
//                reception.updateNewVisitButtonStatus();
//            });
//        });

        var services = {};
        CaseServicePointService.getServices()
            .done(function(_services){
                services = _services;
            })
            .fail(function(err){
                util.showError("Could not retrieve the list of Service from the server.", err);
            });
        $('#new_visit_add_service_select').empty();
		$('#new_visit_add_service_btn').addClass('disabled');
		$('#new_visit_add_service_select').append($("<option />").val(0).text(''));
        $.map(services, function(item) {
            $('#new_visit_add_service_select').append($("<option />").val(item.id).text(item.externalName));
        });
		
		$('#new_visit_add_service_select').change(function(){
            if($('#new_visit_add_service_select').val() == 0){
                $('#new_visit_add_service_btn').addClass('disabled');
            }else{
                $('#new_visit_add_service_btn').removeClass('disabled');
            }
        });

        $('#new_visit_add_service_btn').unbind().click(function() {
            // This add service only does things client-side
            var serviceId = $('#new_visit_add_service_select').val();
			if (serviceId == 0) return;
            var order = 0; // HARD-CODED to FIRST (add on top of existing services)

            var caseService = {
                "serviceId": $('#new_visit_add_service_select').val(),
                "serviceName": $('#new_visit_add_service_select option:selected').text(),
                "status" : "PENDING",
                "notes" : new Array(),
				"vip" : $('#checkbox_new_visit_service_vip').prop('checked')
            };

            newVisit.caseServices.push(caseService);
           //$('#addServiceModal').modal('hide');

            // Also, update the listing
            updateVisitServicesTable();
            $('#new_visit_add_service_select').val(0);
			$('#new_visit_add_service_btn').addClass('disabled');
			$('#checkbox_new_visit_service_vip').prop('checked', false);
            reception.updateNewVisitButtonStatus();
        });


        PresetNotesService.getPresetNotes()
            .done(function(result){
                $('#new_visit_preset_notes').empty();
                for (var i = 0; i < result.length; i++) {
                    var item = result[i];
                    var opt = $('#new_visit_preset_notes').append($("<option />").val(item.text).text(item.text));
                }
                $('#new_visit_preset_notes_add').unbind().click(function() {
                    var preset = $('#new_visit_preset_notes').val();
                    $('#new_visit_note').val($('#new_visit_note').val() + " " + preset);
                });
            })
    };



    var addIconsForServiceRow = function(cs, i) {
        var icons = $(document.createElement('td'));

        // Up/down icons

        // If only one service, no up/down icons
        if(i == 0 && newVisit.caseServices.length == 1) {

        }
        // If first service (and there's more than one)
        else if(i == 0 && newVisit.caseServices.length > 1) {
            var downIcon = $(document.createElement('i'));
            $(downIcon).addClass('icon-chevron-down').click(function() {
                util.swapElement(newVisit.caseServices, i, i+1);
                updateVisitServicesTable();
            }).attr('title','Move down');
            $(icons).append(downIcon);
        }
        // If not first, and not last element
        if(i > 0 && newVisit.caseServices.length > i+1) {
            var upIcon = $(document.createElement('i'));
            $(upIcon).addClass('icon-chevron-up').click(function() {
                util.swapElement(newVisit.caseServices, i, i-1);
                updateVisitServicesTable();
            }).attr('title','Move up');
            $(icons).append(upIcon);

            var downIcon = $(document.createElement('i'));
            $(downIcon).addClass('icon-chevron-down').click(function() {
                util.swapElement(newVisit.caseServices, i, i+1);
                updateVisitServicesTable();
            }).attr('title','Move down');
            $(icons).append(downIcon);
        }
        // Last one, more than one item
        else if(newVisit.caseServices.length > 1 && i == newVisit.caseServices.length-1) {
            var upIcon = $(document.createElement('i'));
            $(upIcon).addClass('icon-chevron-up').click(function() {
                util.swapElement(newVisit.caseServices, i, i-1);
                updateVisitServicesTable();
            }).attr('title','Move up');
            $(icons).append(upIcon);
        }

        return icons;
    };
	
	this.changeVipStatus = function(_index) {
		newVisit.caseServices[_index].vip = !newVisit.caseServices[_index].vip
	};

    var updateVisitServicesTable = function() {
        $('#create_visit_services_tbody').empty();

        for(var i = 0; i < newVisit.caseServices.length; i++) {
            var cs = newVisit.caseServices[i];

            var tr = $(document.createElement('tr'));

            $(tr).append(addIconsForServiceRow(cs, i));
            $(tr).append('<td class="service-name">' + cs.serviceName + '</td>');
			
			var vipCheckbox = '<input id="vip_service_new_visit_checkbox' + cs.id + '" ';
			vipCheckbox += 'name="vip_service_new_visit_checkbox' + cs.id + '" ';
			vipCheckbox += 'value="vip_service_new_visit_checkbox' + cs.id + '" ';
			vipCheckbox += 'type="checkbox" ';
			vipCheckbox += (cs.vip == true ? 'checked="checked" ' : '');
			vipCheckbox += 'onclick="javascript:reception.changeVipStatus(' + i + ');">';

            $(tr).append('<td>' + vipCheckbox + '</td>');
            var notesTD = $(document.createElement('td'));


            for(var j = 0; j < cs.notes.length; j++) {
                var noteMarkup = reception.buildReceptionNoteMarkup(i, j, cs.notes[j], updateVisitServicesTable);
                $(notesTD).append(noteMarkup);
            }

//            var addButton = $('<span title="Add Note" style="cursor: pointer; float: right;"></span>');
//            var addButtonIcon = $('<i class="icon-add-note"></i>');
//            $(addButton).append(addButtonIcon);
//            $(addButton).append(' Add Note');
//            (function(rowId) {
//            	$(addButton).click(function(i) {
//            		reception.openReceptionNoteDialog({}, rowId, function() {
//            			updateVisitServicesTable();
//            		});
//            	});
//            })(i);
//            $(notesTD).append(addButton);

            $(tr).append(notesTD);
            
            var deleteServiceColumn = $('<td class="delete-row-column"></td>');
            var deleteIcon = $(document.createElement('i'));
            $(deleteIcon).addClass('icon-trash');
            $(deleteIcon).attr('title','Remove service');
            (function(rowId){
            	$(deleteIcon).click(function() {
            		newVisit.caseServices.splice(rowId, 1);
            		updateVisitServicesTable();
            	});
            })(i);
            $(deleteServiceColumn).append(deleteIcon);
            $(tr).append(deleteServiceColumn);
            
            $('#create_visit_services_tbody').append(tr);
        }
    };

//    this.buildReceptionNoteMarkup = function(caseServiceIndex, noteIndex, dbNote, callback) {
//        var note = $(document.createElement('div'));
//        $(note).addClass('note');
//        $(note).append('<div class="name"><i>' + dbNote.text + '</i></div>');
//
//        var action = $('<div class="text-right action"></div>');
//        $(note).append(action);
//
//        var editIcon = $('<i class="icon-edit-note" style="margin-left: 6px;" title="Edit note"></i>');
//        $(action).append(editIcon);
//        (function(_dbNote) {
//            $(editIcon).click(function() {
//                reception.openReceptionNoteDialog(_dbNote, null, callback);
//            });
//        })(dbNote);
//
//        var deleteIcon = $('<i class="icon-delete-note"  title="Remove note"></i>');
//        $(action).append(deleteIcon);
//        (function(_dbNote) {
//            $(deleteIcon).click(function() {
//                newVisit.caseServices[caseServiceIndex].notes.splice(noteIndex, 1);
//                updateVisitServicesTable();
//            });
//        })(dbNote);
//
//        $(note).append('<div class="user"><i>' + util.timeOfDate(dbNote.created) + ' ' + dbNote.createdBy + '</i></div>');
//
//        return note;
//    };
//
//    this.openReceptionNoteDialog = function(note,  addToCaseServiceId, onCompleteCallback ) {
//    	notes.populatePresetNotes(note);
//
//        $('#note_save_btn').unbind();
//        $('#note_save_btn').click(function() {
//
//            note.text = $('#note_textarea').val();
//            note.createdBy = sessvars.currentUser.userName;
//            note.created = new Date();
//            if(typeof addToCaseServiceId !== 'undefined' && addToCaseServiceId != null){
//                newVisit.caseServices[addToCaseServiceId].notes.push(note);
//            }
//            $('#noteModal').modal('hide');
//            if(typeof onCompleteCallback !== 'undefined' && onCompleteCallback != null) {
//                onCompleteCallback();
//            }
//        });
//
//        $('#noteModal').modal('show');
//    };
   /* unused code??
    this.deleteNote = function(noteId, onCompleteCallback) {

        util.openConfirm('Delete note','Are you sure you want to delete this note?', function() {
            CaseServicePointService.deleteNote(noteId)
				.done(function( msg ) {
                    $('#confirm_modal').modal('hide');
                    if(typeof onCompleteCallback !== 'undefined' && onCompleteCallback != null) {
                        onCompleteCallback();
                    }
                });
        });
    };
*/
    this.updateNewVisitButtonStatus = function() {
        if(reception.isActive()==false) return;

        if(newVisit.caseServices.length > 0 && typeof sessvars.currentCustomer !== 'undefined' && sessvars.currentCustomer != null) {
            $('#new_visit_btn').removeClass('disabled');
			$('#new_visit_btn').prop('disabled', false);
        } else {
            $('#new_visit_btn').addClass('disabled');
			$('#new_visit_btn').prop('disabled', true);
        }
    };

    var createNewVisit = function() {
        if(util.isNull( sessvars.currentCustomer)) {
            util.showError('You must add at least one customer to the visit');
            return;
        }
        // Set up customer object
        newVisit.branchId = sessvars.branchId;
        newVisit.branchName = sessvars.branchName;
        newVisit.caseCustomer.id = sessvars.currentCustomer.id;
        //newVisit.customerName = sessvars.currentCustomer.firstName + " " + sessvars.currentCustomer.lastName;
        //newVisit.vip = sessvars.currentCustomer.vip;

        if(newVisit.caseServices.length == 0) {
            util.showError('You must add at least one service');
            return;
        }

        if(sessvars.branchId == null) {
            util.showError('You must select a Branch before creating a Visit');
            return;
        }

        sessvars.entryPointId = $('#entrypoint_select').val();
		
		newVisit.entryPointId = sessvars.entryPointId;
		newVisit.entryPointName = $('#entrypoint_select option:selected').text();

        if(sessvars.entryPointId == null) {
            util.showError('Error, no Entry Point selected');
            return;
        }

        if(util.notNull($('#new_visit_note').val())) {
            var note = {
                'text': $('#new_visit_note').val(),
                'createdBy' : sessvars.currentUser.fullName, //sessvars.currentUser.userName,
                'created' : new Date()
            };
            newVisit.currentNote = note;
        }
		
		$('#new_visit_btn').prop('disabled', true);
        CaseServicePointService.createCase(newVisit)
			.done(function(visit) {
                $('#last_ticket_number').text(visit.ticketId);
                clearNewVisit();
                common.updateQueuesTable('queues_table');
            })
            .fail(function(err){
                util.showError('Could not create visit.', err);
				$('#new_visit_btn').prop('disabled', false);
            }
        );

    };
};