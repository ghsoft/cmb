/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-05-06
 * Time: 11:39
 * To change this template use File | Settings | File Templates.
 */

var servicePoint = new function() {

    // TODO move this to UNIT TYPE Settings
    this.DETAILED_ERRORS = true;
    this.USE_HOLDING_QUEUE = true;
    this.ALLOW_ONLY_CREATOR_TO_EDIT_NOTE = false;

    var logoffTimer = null;
    var autoClose = 0;
    var currentF2FTime = 0;
    var txTimeIntervalHandle = 0;
    var currentPPTime = 0;
    var _active = false;

    /** This is a hack to prevent double ajax events being sent on some instances of double clicks */
    var ongoingAjax = false;

    this.setActive = function(active){
        _active = active;
    }

    this.isActive = function(){
        return _active;
    };

    // Service point states
    this.servicePointState = {
        OPEN: 'OPEN',
        CLOSED: 'CLOSED'
    };

    this.userState = {
        /** User has not started a session */
        NO_STARTED_USER_SESSION: "NO_STARTED_USER_SESSION",
        /** User has not started a session on any Service Point */
        NO_STARTED_SERVICE_POINT_SESSION: "NO_STARTED_SERVICE_POINT_SESSION",
        /** User has not set a work profile but is logged in */
        NO_PROFILE_SET: "NO_PROFILE_SET",
        /** User has started a service point session but not called any customer */
        INACTIVE: "INACTIVE",
        /** User called a customer but no customers were waiting. The next created visit will be called to the user automatically */
        IN_STORE_NEXT: "IN_STORE_NEXT",
        /** User has called a customer and the customer is NOT confirmed, i.e. has NOT reached the ServicePoint */
        CALLED: "CALLED",
        /** User has a confirmed customer and is currently serving that customer */
        SERVING: 'SERVING',
        /** User has served the customer and is doing some kind of wrap up activity
         * NOT APPLICABLE FOR Conductor!!!! */
        WRAPUP: "WRAPUP",
        /** User is doing Conductor Post-Processing. This state CAN NOT be returned from Qmatic Platform */
        IN_POST_PROCESSING: "IN_POST_PROCESSING",
        /** The current visit is not called. Most probably because of display throttling */
        VISIT_NOT_CALLED: "VISIT_NOT_CALLED"
    };

    this.visitState = {
        /** Normal state for a visit. This visit can be ended */
        OK : "OK",
        /** There is no visit that can be returned by a next visit request */
        NO_CALLABLE_VISITS: "NO_CALLABLE_VISITS",
        /** The visit needs to be confirmed. */
        CONFIRM_NEEDED: "CONFIRM_NEEDED",
        /** The visit has a service that requires a delivered service */
        DELIVERED_SERVICE_NEEDED: "DELIVERED_SERVICE_NEEDED",
        /** The visit has a service or delivered service that requires an outcome */
        OUTCOME_NEEDED: "OUTCOME_NEEDED",
        /** The visit has a delivered service that requires an outcome */
        OUTCOME_FOR_DELIVERED_SERVICE_NEEDED: "OUTCOME_FOR_DELIVERED_SERVICE_NEEDED",
        /** The visit has a service that requires a delivered service or an outcome */
        OUTCOME_OR_DELIVERED_SERVICE_NEEDED: "OUTCOME_OR_DELIVERED_SERVICE_NEEDED",
        /** User has served the customer and is doing some kind of wrap up activity */
        WRAPUP: "WRAPUP",
        /** The current visit is not called. Most probably because of display throttling */
        VISIT_IN_DISPLAY_QUEUE: "VISIT_IN_DISPLAY_QUEUE",
        /** Visit, from Conductor PoV, is in Post-Processing. This state CAN NOT be returned from Qmatic Platform */
        IN_POST_PROCESSING: "IN_POST_PROCESSING"
    };

    this.publicEvents = {
        VISIT_REMOVE: "VISIT_REMOVE",
        VISIT_CREATE: "VISIT_CREATE",
        VISIT_CALL: "VISIT_CALL",
        SERVICE_POINT_CLOSE: "SERVICE_POINT_CLOSE",
        RESET: "RESET",
        VISIT_CONFIRM: "VISIT_CONFIRM",
        VISIT_END: "VISIT_END",
        VISIT_NOSHOW: "VISIT_NOSHOW",
        USER_SESSION_START: "USER_SESSION_START",
        USER_SESSION_END: "USER_SESSION_END",
        VISIT_NEXT: "VISIT_NEXT",
        USER_SERVICE_POINT_SESSION_START: "USER_SERVICE_POINT_SESSION_START",
        USER_SERVICE_POINT_SESSION_END: "USER_SERVICE_POINT_SESSION_END",
        SERVICE_POINT_OPEN: "SERVICE_POINT_OPEN",
        USER_SERVICE_POINT_WORK_PROFILE_SET: "USER_SERVICE_POINT_WORK_PROFILE_SET",
        VISIT_TRANSFER_TO_QUEUE: "VISIT_TRANSFER_TO_QUEUE",
        VISIT_TRANSFER_TO_SERVICE_POINT_POOL: "VISIT_TRANSFER_TO_SERVICE_POINT_POOL",
        VISIT_TRANSFER_TO_USER_POOL: "VISIT_TRANSFER_TO_USER_POOL",
        VISIT_RECYCLE: "VISIT_RECYCLE",
        UNSUPPORTED: "UNSUPPORTED"
    };

    this.setProfile = function(params) {
        cServicePointService.setWorkProfile(params)
            .done(function(userState){
                sessvars.state = userState;
            })
            .fail(function(err){
                sessvars.state = servicePoint.getState('undefined');
                util.showError("Couldn't set workprofile. ", err);
            })

        sessvars.statusUpdated = new Date();
        $('#work_profile_select').val(sessvars.workProfileId);
    };

    this.closeWorkstation = function(closeWorkstationCallBack) {
	
		if (servicePoint.isVisitInDisplayQueue()) {
			return;
		}

        if(typeof(closeWorkstationCallBack) != typeof(Function))   closeWorkstationCallBack = servicePoint.closeWorkstation;

        servicePoint.getState(sessvars.state);
		
        // Make sure the ongoing visit (if any) does not require
        if(deliveredservices.isOutcomeOrDeliveredServiceNeeded()) {
            if(!deliveredservices.isOutcomeOrDeliveredServiceAdded()) {
                deliveredservices.openAddDeliveredServiceDialog(closeWorkstationCallBack);
                return false;
            }
            if(!deliveredservices.hasDefinedOutcomeOrDeliveredService()) {
                deliveredservices.openAddDeliveredServiceDialog(closeWorkstationCallBack);
                return false;
            }
            deliveredservices.openAddDeliveredServiceDialog(closeWorkstationCallBack);

            return false;
        }

        CaseServicePointService.closeWorkstation()
            .done(function(userState){
                sessvars.state = userState;
                servicePoint.clearOngoingVisit();
            })
            .fail(function(err){
                util.showError('Could not do Close workstation.', err);
            })

        //if(callback) callback();
        return true;
    };
    this.canEndVisit = function (callback) {
		if(!settings.hasValidSettings()) {
            util.showError('Settings are invalid. Check the settings');
            return false;
        } else if (deliveredservices.isOutcomeOrDeliveredServiceNeeded()) {
            deliveredservices.openAddDeliveredServiceDialog(callback);
            return false;
        }
        return true;
    }

    this.callNext = function() {
		if (servicePoint.isVisitInDisplayQueue()) {
			return;
		}
		
        if (deliveredservices.isOutcomeOrDeliveredServiceNeeded()) {

            if(!deliveredservices.openAddDeliveredServiceDialog(servicePoint.callNext)){
                return;
            }
        }
        buttons.sleepCallNextBtn();
        var currentCaseVisitId = -1;
        if (util.notNull(sessvars.currentCase) ) {
            currentCaseVisitId = sessvars.currentCase.qpVisitId;
        }

        CaseServicePointService.callNext()
            .done(function(userState){
                sessvars.state = userState;
                panels.resetRightPanel();

                if(sessvars.state.visitState == servicePoint.visitState.NO_CALLABLE_VISITS || util.isNull(sessvars.state.visitState) ) {
                    //no tickets left in the queue(s) for the selected prio
                    util.showMessage("No waiting customers");
                    servicePoint.clearOngoingVisit();
                } else {
                    // servicePoint.updateWorkstationStatus(true);
                    sessvars.currentCustomer = null;
                    // If call next to R5 successful, we should be in state "SERVING" and have a nice little visit in the sessvars
                    if(sessvars.state.userState == servicePoint.userState.SERVING && typeof sessvars.state.visit.ticketId !== 'undefined') {
                        // Now, load the case based on qpVisitId or ticket number
                        CaseServicePointService.getCaseByVisitId(sessvars.state.visit.id)
                            .done(function(dbCase){
                                servicePoint.populateCurrentTransaction(dbCase);
                                servicePoint.updateServingTimeClock();
                            })
                            .fail(function(err){
                                util.showMessage('No Case attached to \'' + sessvars.state.visit.ticketId + '\', Call Next or Close Workstation when serving is complete.');
                            })
                    }
                }
            })
            .fail(function(err){
                sessvars.state = servicePoint.getState(sessvars.state);
                util.showError("Failed to call next.", err);
            })
    };

    /**
     * Updates the workstation status in case of user refresh (F5 pressed) or programmatic refresh e.g. call next ticket.
     * The object sessvars.state corresponds the object QLWorkstationStatus, see api docs.
     *
     * @param isRefresh if the call is a user refresh (F5), the queues shall only be updated and no timer shall be created
     *
     */
    this.updateWorkstationStatus = function(isRefresh) {
       // clearOngoingVisit();
        if(sessvars.state.servicePointState == servicePoint.servicePointState.CLOSED) {

        } else if(sessvars.state.servicePointState == servicePoint.servicePointState.OPEN &&
            sessvars.state.userState == servicePoint.userState.IN_STORE_NEXT) {

        } else if(sessvars.state.servicePointState == servicePoint.servicePointState.OPEN &&
            sessvars.state.userState == servicePoint.userState.INACTIVE) {

        } else if(sessvars.state.servicePointState == servicePoint.servicePointState.OPEN &&
            sessvars.state.visitState == servicePoint.visitState.NO_CALLABLE_VISITS) {

        } else if(sessvars.state.servicePointState == servicePoint.servicePointState.OPEN &&
            sessvars.state.userState == servicePoint.userState.SERVING &&
            deliveredservices.isOutcomeOrDeliveredServiceNeeded()) {
			if(sessvars.state.visitState == servicePoint.visitState.OUTCOME_NEEDED) {
                util.showMessage('No outcome specified');
            } else if(sessvars.state.visitState == servicePoint.visitState.DELIVERED_SERVICE_NEEDED) {
                util.showMessage('Transaction Type is needed');
            } else if(sessvars.state.visitState == servicePoint.visitState.OUTCOME_FOR_DELIVERED_SERVICE_NEEDED) {
                util.showMessage('No outcome specified for the Transaction Type');
            } else if(sessvars.state.visitState == servicePoint.visitState.OUTCOME_OR_DELIVERED_SERVICE_NEEDED) {
                util.showMessage('No outcome or Transaction Type');
            }

        } else {
			
        }

        resetLogoffCounter();
    };
	
	this.isVisitInDisplayQueue = function() {
		if (sessvars.state.visitState == servicePoint.visitState.VISIT_IN_DISPLAY_QUEUE) {
			sessvars.state = servicePoint.getState();
		}
		if (sessvars.state.visitState == servicePoint.visitState.VISIT_IN_DISPLAY_QUEUE) {
			util.showWaitingMessage('Your ticket is in queue to be called. Please wait for it to be announced.');
			return true;
		}
		return false;
	}

    //clean the GUI on refresh or when a button is pressed
    this.clearOngoingVisit = function() {

        $('#cst_ticketnumber_lbl').empty();
        $('#cst_customerid_lbl').empty();
        $('#cst_customername_lbl').empty();

        $('#cst_service_lbl').empty();
        $('#cst_waited_lbl').empty();
        $('#cst_served_lbl').empty();
        $('#cst_postprocessing_lbl').empty();

        //$('#cst_vip').prop('checked', false);

        $('#cst-state').text(sessvars.state.servicePointState);

        if(util.notNull(txTimeIntervalHandle)) {
            window.clearInterval(txTimeIntervalHandle);
            txTimeIntervalHandle = null;
        }
        panels.hideCurrentTransactionPanel();
        panels.showQueuesTablePanel();
        common.updateQueuesTable('queues_table');
        buttons.updateAccordingToServicePointStatus();
        sessvars.currentCase = null;
    };

    this.storeSettingsInSession = function(sessvarsInfo) {
        sessvars.branchId = sessvarsInfo.branchId;

        if(typeof sessvarsInfo.branchName === 'undefined' || sessvarsInfo.branchName == null) {

            cServicePointService.getBranch(sessvarsInfo.branchId)
                .done(function(branch){
                    sessvars.branchName = branch.name;
                })
                .fail(function(err){
                    util.showError('Could not get Branch.');
                })

        } else {
            sessvars.branchName = sessvarsInfo.branchName;
        }
        
        //sessvars.branchName = "";
		if (servicePoint.isActive() == true) {
			sessvars.servicePointId = sessvarsInfo.servicePointId;
			var params = {};
			params.branchId = sessvars.branchId;
			params.servicePointId = sessvars.servicePointId;
			var userServicePoint = 'undefined';
			cServicePointService.getServicePoint(params)
				.done(function(servicePoint){
					userServicePoint = servicePoint;
				})
				.fail(function(err){
					util.showError('Could not get ServicePoint data from server.', err);
				})

			sessvars.servicePointUnitId = typeof userServicePoint !== 'undefined' || userServicePoint != null ? userServicePoint.unitId : "";
			sessvars.servicePointName = sessvarsInfo.servicePointName;

			sessvars.workProfileId = sessvarsInfo.workProfileId;
			sessvars.profileName = sessvarsInfo.profileName;
		}
    };

//    this.ings = function() {
//        sessvars.branchId = null;
//        sessvars.branchName = null;
//        sessvars.servicePointId = null;
//        sessvars.servicePointUnitId = null;
//        sessvars.servicePointName = null;
//        sessvars.workProfileId = null;
//        sessvars.profileName = null;
//        sessvars.singleSettingsOnly = null;
//    };


    // Resets the logout counter
    var resetLogoffCounter = function() {
        if (logoffTimer != null) {
            window.clearTimeout(logoffTimer);
        }
        if (typeof autoClose === 'undefined' || autoClose == null || autoClose == 0) {
            return;
        }
        var lastUpdate = new Date();
        var now = new Date();
        if (typeof sessvars.statusUpdated !== 'undefined' && sessvars.statusUpdated != null) {
            lastUpdate = sessvars.statusUpdated;
        }
        var timeSinceLastUpdate = now.getTime() - lastUpdate.getTime();
        if (timeSinceLastUpdate > 0) {
            timeSinceLastUpdate = 0;
        } else {
            timeSinceLastUpdate = timeSinceLastUpdate / 1000;
        }
        var timeUntilLogoff = (autoClose - timeSinceLastUpdate) * 1000;
        logoffTimer = window.setTimeout(function() {
            servicePoint.handleLogoutQES(true);
            window.location.href = "/logout.jsp";
        }, timeUntilLogoff);

    };


    this.handleLogoutQES = function(warn) {
        var isLogout = false;
        //close workstation if settings have been applied
        if(!settings.workstationOffline && settings.hasValidSettings() && !servicePoint.isOutcomeOrDeliveredServiceNeeded()) {
            var displayDialog = false;
            if(warn) {
                var warningMessage = "";
//                var isServicePointPoolEmpty = servicePointPool.isEmpty();
//                var isUserPoolEmpty = userPool.isEmpty();
//                if(!isServicePointPoolEmpty && !isUserPoolEmpty) {
//                    displayDialog = true;
//                    warningMessage = translate.msg("info.confirm.logout.visits.still.in.both.pools",
//                        [translate.msg("info.confirm.logout.service.point.pool"),
//                            translate.msg("info.confirm.logout.user.pool")]);
//                } else if(!isServicePointPoolEmpty) {
//                    displayDialog = true;
//                    warningMessage = translate.msg("info.confirm.logout.visits.still.in.pool",
//                        [translate.msg("info.confirm.logout.service.point.pool")]);
//                } else if(!isUserPoolEmpty) {
//                    displayDialog = true;
//                    warningMessage = translate.msg("info.confirm.logout.visits.still.in.pool",
//                        [translate.msg("info.confirm.logout.user.pool")]);
//                }
            }
            if(displayDialog) {
                util.showModal("logoutWindow");
                $("#confirmLogoutMessage").html(warningMessage);
            } else {
                //check if workstation is open inside endUserServicePointSession() call
                servicePoint.endUserServicePointSession();

                //check if the close resulted in a matter code needed result, in that case, don't logout
                if(servicePoint.isOutcomeOrDeliveredServiceNeeded()) {
                    servicePoint.updateWorkstationStatus(false);
                } else {
                    servicePoint.cleanAndLogout();
                    isLogout = true;
                }
            }
        }
        return isLogout;
    };

    this.cleanAndLogout = function() {
		if (servicePoint.isVisitInDisplayQueue()) {
			return;
		}
        if(sessvars.state.servicePointState != servicePoint.servicePointState.OPEN || servicePoint.closeWorkstation(servicePoint.cleanAndLogout)) {
			sessvars.$.clearMem();
			settings.workstationOffline = true;
			cServicePointService.logout();
			window.location.replace("/logout.jsp");
        }
    };


    this.endUserServicePointSession = function() {
		if (servicePoint.isVisitInDisplayQueue()) {
			return;
		}
        if(settings.hasValidSettings() && sessvars.state.servicePointState == servicePoint.servicePointState.OPEN &&
            !(servicePoint.isOutcomeOrDeliveredServiceNeeded())/*&& !( sessvars.state.userState == this.SERVING && sessvars.forceMark && !hasMark() )*/
			) {
            cServicePointService.endUserServicePointSession(servicePoint.createParams())
                .done(function(userState){
                    sessvars.state = userState;
                })
                .fail(function(err){
                    util.showError(jQuery.i18n.prop("error.close") + " ", err);
                    return false;
                })
        }
    };


    this.createParams = function() {
        var params = {};
        params.branchId = parseInt(sessvars.branchId);
        params.branchName = sessvars.branchName;

        params.servicePointId = parseInt(sessvars.servicePointId);
        params.servicePointName = sessvars.servicePointName;

        params.workProfileId = parseInt(sessvars.workProfileId);
        params.profileName = sessvars.profileName;

        if(typeof sessvars.currentUser === 'undefined' || sessvars.currentUser == null) {
            cServicePointService.getCurrentUser()
                .done(function(user){
                    sessvars.currentUser = user;
                })
                .fail(function(err){
                    util.showError('Could not get current User from server.', err);
                })

        }
        if(typeof sessvars.currentUser !== 'undefined' && sessvars.currentUser.hasOwnProperty("userName")) {
            params.userName = sessvars.currentUser.userName;
        }
        return params;
    };

    /**
     * Wrap any call to callService that returns state with this method to avoid ending up in an undefined state.
     * @param returnValue
     */
    this.getState = function(returnValue) {
        if(typeof returnValue === 'undefined') {
            cServicePointService.getUserStatus()
                .done(function(userStatus){
                    returnValue = userStatus;
                })
                .fail(function(err){
                    util.showError('Could not get User status from server.', err);
                })
        }
        if(typeof returnValue === 'undefined') {
            returnValue = sessvars.state;
        }
        return returnValue;
    };

    // START Conductor SPECIFICS

    this.moveServiceUp = function(caseId, caseServiceId) {
        moveService(caseId, caseServiceId, 'up');
    };

    this.moveServiceDown = function(caseId, caseServiceId) {
        moveService(caseId, caseServiceId, 'down');
    };

    var moveService = function(caseId, caseServiceId, direction) {
        CaseServicePointService.moveCaseService(caseServiceId, direction)
            .done(function(dbCase){
                //loadServicesToday(dbCase);
            })
            .fail(function(err){
                util.showError('Could not move service in service list.', err);
            })
    };

    var createServicesTodayIcons = function(pCase, services, i) {
        var service = services[i];
        var icons = $(document.createElement('td'));

        // Simply put, only services in state "PENDING" can be moved or deleted.
        if(service.status != 'PENDING') {
            return icons;
        }

        // Delete and up/down icons are only rendered if we have more than one service on the visit that is in state PENDING.
        if(services.length > 1) {
            // Up/down icons
            if(pCase.numberOfPendingServices > 1) {
                var downIcon = $(document.createElement('i'));
                $(downIcon).addClass('icon-chevron-down').click(function() {	
					if (servicePoint.isVisitInDisplayQueue()) {
						return;
					}
                    servicePoint.moveServiceDown(pCase.id, service.id);
                    util.swapElement(services, i, i+1);
                    populateServicesToday(pCase, services);
                }).attr('title','Move down');

                var upIcon = $(document.createElement('i'));
                $(upIcon).addClass('icon-chevron-up').click(function() {
					if (servicePoint.isVisitInDisplayQueue()) {
						return;
					}
                    servicePoint.moveServiceUp(pCase.id, service.id);
                    util.swapElement(services, i, i-1);
                    populateServicesToday(pCase, services);
                }).attr('title','Move up');

                // If only one service, no up/down icons

                // If first service (and there's more than one)
                if(i == 0 && services.length > 1) {
                    $(icons).append(downIcon);
                }
                // If not first, and not last element and previous element is in state PENDING
                if(i > 0 && services.length > i+1 && services[i-1].status == 'PENDING') {
                    $(icons).append(upIcon);
                }
                // If not first, and not last element and next element is in state PENDING
                if(i > 0 && services.length > i+1 && services[i+1].status == 'PENDING') {
                    $(icons).append(downIcon);
                }
                // Last one, more than one item, only if previous is in state PENDING
                if(services.length > 1 && i == services.length-1 && services[i-1].status == 'PENDING') {
                    $(icons).append(upIcon);
                }
            }
        }
        return icons;
    };

    this.performRemoveService = function(caseId, caseServiceId, onCompleteCallback) {
        if(util.notNull(caseId) && util.notNull(caseServiceId)) {
            CaseServicePointService.deleteCaseService(caseId, caseServiceId)
                .done(function(userStatus){
                    sessvars.state = userStatus;
                    if(util.notNull(onCompleteCallback)) {
                        onCompleteCallback();
                    }
                    $('#confirm_modal').modal('hide');
                })
                .fail(function(err){
                    util.showError('Failed to delete Case Service.', err);
                })
        }
    };

    this.callServiceWhenInPostProcessing = function(serviceName) {
		if (servicePoint.isVisitInDisplayQueue()) {
			return;
		}
		
        if(deliveredservices.isOutcomeOrDeliveredServiceNeeded()) {
            deliveredservices.openAddDeliveredServiceDialog();
            return;
        }

        var caseId = sessvars.currentCase.id;

        util.openConfirm('Call service?', 'Are you sure you want to end the ongoing post-processing and call \'' + serviceName + '\' directly?',
            function() {
                CaseServicePointService.endPostProcessing( sessvars.currentCase.id, sessvars.currentCase.ongoingService.qpVisitServiceId)
                    .done(function(userState){
                        $('#confirm_modal').modal('hide');
                        sessvars.state = userState;
                        CaseServicePointService.getCase(caseId)
                            .done(function(pCase) {
                                servicePoint.callVisit(pCase);

                            });
                    })
                    .fail(function(err){
                        $('#confirm_modal').modal('hide');
                        util.showError('Failed to end Post Processing. ', err);
                    });
            }
        );
    };

    this.callServiceWhileServing = function(qpVisitServiceId, serviceName) {
		if (servicePoint.isVisitInDisplayQueue()) {
			return;
		}
        if(deliveredservices.isOutcomeOrDeliveredServiceNeeded()) {
            deliveredservices.openAddDeliveredServiceDialog();
            return;
        }

        util.openConfirm('Call service?', 'Are you sure you want to end the ongoing visit and call \'' + serviceName + '\' directly?',
            function() {
                CaseServicePointService.callService(qpVisitServiceId)
                    .done(function(userStatus){
                        sessvars.state = userStatus;
                        CaseServicePointService.getCaseByVisitId(sessvars.state.visit.id)
                            .done(function(dbCase){
                                servicePoint.populateCurrentTransaction(dbCase);
                                servicePoint.updateServingTimeClock();
                                $('#confirm_modal').modal('hide');
                            })
                            .fail(function(err){
                                util.showError('Error to get case by visit id. ', err);
                                $('#confirm_modal').modal('hide');
                            });
                    })
                    .fail(function(err){
                        util.showError('Error calling service. ', err);
                        $('#confirm_modal').modal('hide');
                    });
            },
        function() {
            $('#confirm_modal').modal('hide');
        });

    };
	
	this.callServiceFromQueueList = function(qpVisitServiceId) {
		if (servicePoint.isVisitInDisplayQueue()) {
			return;
		}
        if(deliveredservices.isOutcomeOrDeliveredServiceNeeded()) {
            deliveredservices.openAddDeliveredServiceDialog();
            return;
        }

        CaseServicePointService.callService(qpVisitServiceId)
			.done(function(userStatus){
				sessvars.state = userStatus;
				CaseServicePointService.getCaseByVisitId(sessvars.state.visit.id)
					.done(function(dbCase){
						servicePoint.populateCurrentTransaction(dbCase);
						servicePoint.updateServingTimeClock();
						panels.resetRightPanel();
					})
					.fail(function(err){
						util.showError('Error to get case by visit id. ', err);
					});
			})
			.fail(function(err){
				util.showError('Error calling service. ', err);
				panels.resetRightPanel();
			});

    };

    var populateServicesToday = function(pCase, services) {
        $('#services_today_tbody').empty();

        if(services.length == 0) {
            return;
        }
		var count = 0;
        for(var i = 0; i < services.length;i++) {
            var service = services[i];
			if (service.status != settings.overriddenStatus) {
				count++;
				var tr = $('<tr valign="top"></tr>');
				$('#services_today_tbody').append(tr);
				$(tr).append('<td>' + (count) + '</td>');
				//$(tr).append(createServicesTodayIcons(pCase, services, i));
				$(tr).append('<td class="service-name">' + service.serviceName + '</td>');
				
				//Add VIP column
				var vipServiceColumn = $('<td></td>');
				if(service.status == 'PENDING' && services.length > 1) {
					var vipCheckbox = $(document.createElement('input')).attr({
						id:		'vip_service_checkbox' + service.id,
						name:  	'vip_service_checkbox' + service.id,
						value: 	'vip_service_checkbox' + service.id,
						type:  	'checkbox'
					});
					$(vipCheckbox).prop('checked', service.vip);
					(function(_caseId, _serviceId) {
						$(vipCheckbox).click(function() {
							CaseServicePointService.vipCaseService(_serviceId, $(this).prop('checked'))
							.done(function(service){

							})
							.fail(function(err){
								util.showError('Error assigning VIP status to the service. ', err);
							})
						});
					})(pCase.id, service.id);

					$(vipServiceColumn).append(vipCheckbox);
				}
				else {
					$(vipServiceColumn).append(service.vip == true ? '<i title="This is a VIP service" class="icon-vip-customer"></i>' : '<i title="This is a VIP service" class="icon-vip-customer hidden"></i>');
				}
				$(tr).append(vipServiceColumn);
				
				$(tr).append('<td>' + (service.status == 'PENDING' ? 'WAITING' : service.status) + '</td>');

				var td = $(document.createElement('td'));
				if(sessvars.currentCase.ongoingService.id == service.id) {
					$(td).append('<span style="font-style: italic;">You are serving this Service in Current Transaction.</span>');
				} else {
					var notesList = service.notes;
					var marksList = service.caseServiceMarks;
					for(var j = 0, k=0; j < notesList.length || k < marksList.length;) {
						var dbNote = notesList[j];
						var caseServiceMark = marksList[k];
						if(caseServiceMark == undefined || (dbNote != undefined && dbNote.created < caseServiceMark.created)) {
							var editAllowed = service.status=="FINISHED"?false:true;
							var note = markupBuilder.buildNoteMarkup(dbNote, service.id, false, false, (function(_serviceId) {
								return function() {          
									loadServicesToday(pCase);
								}
							})(service.id));
							$(td).append(note);
							j++;
						} else {
							var mark = markupBuilder.buildMarkMarkup(caseServiceMark, false);
							$(td).append(mark);
							k++;
						}
					}
				}

				$(tr).append(td);

				var deleteServiceColumn = $('<td class="delete-row-column"></td>');
				if(service.status == 'PENDING' && services.length > 1) {
					var deleteIcon = $(document.createElement('i'));
					$(deleteIcon).addClass('icon-trash').attr('title','Remove service');
					(function(_service) {
						$(deleteIcon).click(function() {
							if (servicePoint.isVisitInDisplayQueue()) {
								return;
							}
							util.openConfirm("Remove service?", "Are you sure you wish to remove service '" + _service.serviceName + "' from the ongoing visit?",
								function() {
									servicePoint.performRemoveService(pCase.id, _service.id, function() {
									loadServicesToday(pCase);
								});
							});
						});
					})(service);
					$(deleteServiceColumn).append(deleteIcon);
				}
				$(tr).append(deleteServiceColumn);
				if(service.status == 'PENDING') {
					$(tr).append('<td style="text-align: center; vertical-align: middle;"><i class="icon-play-green" title="Call service, ends the currently ongoing service without post-processing" onclick="javascript:servicePoint.callServiceWhileServing(' + service.qpVisitServiceId + ',\'' + function(){return service.serviceName}() + '\');"></i></td>');
				} else if(service.status == 'WAITING') {
					$(tr).append('<td style="text-align: center; vertical-align: middle;"><i class="icon-play-green" title="Call service, ends the currently ongoing post-processing" onclick="javascript:servicePoint.callServiceWhenInPostProcessing(\'' + function(){return service.serviceName}() + '\');"></i></td>');
				} else {
					$(tr).append('<td></td>');
				}
			}
        }
    };

    var loadServicesToday = function(pCase) {
        if(typeof pCase !== 'undefined') {
            CaseServicePointService.getCaseServices(pCase.id)
                .done(function(services){
                    populateServicesToday(pCase, services);
                })
                .fail(function(err){
                    util.showError('Error fetching case services. ', err);
                })
        }
    };

    var addServiceToPersistedCaseCallback = function(caseId) {
        var serviceId = $('#add_service_select').val();
        var order = 0; // HARD-CODED to FIRST (add on top of existing services)

        var visit = {
            "serviceId": serviceId,
            "serviceName": $('#add_service_select option:selected').text(),
            "status" : "PENDING",
            "notes" : [],
			"vip" : $('#checkbox_service_vip').prop('checked')
        };

        // Only add notes if there's a note to add.
//        if($('#add_service_note').val() !== '') {
//            visit.notes = [
//                {
//                    "text":$('#add_service_note').val(),
//                    "createdBy":sessvars.currentUser.userName
//                }
//            ];
//        }

        CaseServicePointService.addCaseService(caseId, visit)
            .done(function(dbCase){	
				//sessvars.state = servicePoint.getState();
                //servicePoint.populateCurrentTransaction(dbCase);
				loadServicesToday(dbCase);
                $('#addServiceModal').modal('hide');
            })
            .fail(function(err){
                util.showError('Error adding service.', err);
            })
    };

    this.callVisit = function(pCase) {
		if (servicePoint.isVisitInDisplayQueue()) {
			return;
		}
        if(typeof pCase.qpVisitId === 'undefined' || pCase.qpVisitId == null) {
            util.showError('Error, no QP visitId on Visit.');
            return;
        }
        if(servicePoint.canEndVisit(util.partial(servicePoint.callVisit,pCase))) callConductorVisit(pCase);
    };

    var callConductorVisit = function(pCase) {
        // Call Conductor backend
        CaseServicePointService.callNextOnCase(pCase.id)
            .done(function(userState){
				sessvars.state = userState;
                CaseServicePointService.getCase(pCase.id)
                    .done(function(caseData){
						panels.resetRightPanel();
                        servicePoint.populateCurrentTransaction(caseData);
                        servicePoint.updateServingTimeClock();
                    })
            })
            .fail(function(err){
                util.showError('Could not call Visit \'' + pCase.ticketNumber + '\'. ', err);
                panels.resetRightPanel();
            })
    };

    var addServiceToNonOngoingVisit = function(caseId) {
        var serviceId = $('#add_service_select').val();
        var visit = {
            "serviceId": serviceId,
            "serviceName": $('#add_service_select option:selected').text(),
            "status" : "PENDING",
            "notes" : [],
			"vip" : $('#checkbox_service_vip').prop('checked')
        };
        // Only add notes if there's a note to add.
		/*
        if($('#add_service_note').val() !== '') {
            visit.notes = [
                {
                    "text":$('#add_service_note').val(),
                    "createdBy":sessvars.currentUser.fullName //sessvars.currentUser.userName
                }
            ];
        }
		*/

        CaseServicePointService.addCaseService(caseId, visit)
            .done(function(caseData){
                tables.populateVisitDetailsServicesTable(caseData);
                $('#addServiceModal').modal('hide');
            })
            .fail(function(err){
                util.showError('Error adding service: ', err);
            })
    };

    var hideBreadCrumb = function(){
        $('#visit-details-queues').next().addClass('hidden');
        $('#visit-details-queue-name').addClass('hidden');
        $('#visit-details-queue-name').next().addClass('hidden');
        $('#visit-details-customer-name').addClass('hidden');
        $('#visit-details-customer-name').next().addClass('hidden');
        $('#visit-details-ticket-number').addClass('hidden');
    }

    var showBreadCrumb = function(){
        $('#visit-details-queues').next().removeClass('hidden');
        $('#visit-details-queue-name').removeClass('hidden');
        $('#visit-details-queue-name').next().removeClass('hidden');
        $('#visit-details-customer-name').removeClass('hidden');
        $('#visit-details-customer-name').next().removeClass('hidden');
        $('#visit-details-ticket-number').removeClass('hidden');
    }

    this.showCustomerDetails = function(customerId) {
        customer.populateCustomerDetails(customerId, false);
        tables.loadCustomerHistory(customerId, 'customer-details-history-table');

        // Back links..
        $('#visit-details-home').unbind().click(function() {
            panels.resetRightPanel();
        });
        $('#visit-details-queues').unbind().click(function() {
            panels.resetRightPanel();
        });
        $('#visit-details-ticketNumber').addClass('hidden');
        //$('#visit-details-call-btn').addClass('hidden');
        $('#visit-details-remove-btn').addClass('hidden');
        $('#visit-details-created').addClass('hidden');
        hideBreadCrumb();

        panels.showCustomerDetailsPanel();
    };

    this.populateVisitDetails = function(pCase, queueId, queueName) {
        customer.populateCustomerDetails(pCase.caseCustomer.id, true);
        showBreadCrumb();

        // Back links..
        $('#visit-details-home').unbind().click(function() {
            panels.resetRightPanel();
        });
        $('#visit-details-queues').unbind().click(function() {
            panels.resetRightPanel();
        });

        // Add service button...
        $('#visit_details_add_service').unbind().click(function() {
            common.openAddServiceDialog(pCase.id, function(caseId) {
                addServiceToNonOngoingVisit(caseId);
            });
        });

        // Only allow Call and add service if there are remaining services unserved or in trash.
        //$('#visit-details-call-btn').unbind().click(function() {
		//	servicePoint.callVisit(pCase);
        //});
		
		// Remove visit button
        $('#visit-details-remove-btn').unbind().click(function() {
			if (sessvars.state.visitState != null || sessvars.isPostProcessing == true) {
				util.showMessage('Cannot Remove this Visit. Another ticket is already called, End serving the called ticket and try again');
				return;
			}
            util.openConfirm('Remove Visit: ' + pCase.ticketNumber + '?', 'Are you sure you want to move this visit to the Trash queue?', function() {

                CaseServicePointService.deleteCurrentCase(pCase.id)
                    .done(function(){
                        panels.resetRightPanel();
                        util.showMessage("Visit has been moved to the Trash queue.")
						$('#visit-details').addClass('hidden');
                        // no response
                    })
                    .fail(function(err){
                        util.showError('Failed to delete Visit. ', err);
                    })

                $('#confirm_modal').modal('hide');
            })
        });
		
        if(pCase.inTrash || pCase.inHold || pCase.inNoShow) {
            //$('#visit-details-call-btn').addClass('hidden').addClass("disabled").attr('disabled','disabled');
			$('#visit-details-remove-btn').addClass('hidden').addClass("disabled").attr('disabled','disabled').unbind();
            //$('#visit_details_add_service').parent().css('display', 'none');
        } else {
            if(!$('#counter-container').hasClass('hidden')) {
                //$('#visit-details-call-btn').removeClass("hidden").removeClass("disabled").removeAttr('disabled');
				$('#visit-details-remove-btn').removeClass("hidden").removeClass("disabled").removeAttr('disabled');
            }
        }
		$('#visit_details_add_service').parent().css('display', 'block');
		
        if(util.notNull(queueName)) {
            $('#visit-details-queue-name').html(queueName);
            $('#visit-details-queue-name').unbind().click(function() {
                panels.showQueueTablePanel();
                tables.showQueueTable(queueId, queueName);
            });
        }

        $('#visit-details-customer-name').html(pCase.caseCustomer.fullName);
        $('#visit-details-customer-name').unbind().click(function() {
            customer.populateCustomerDetails(pCase.caseCustomer.id, true);
        });
        $('#visit-details-ticketNumber').removeClass('hidden');
        $('#visit-details-ticketNumber').html(pCase.ticketNumber);
        $('#visit-details-ticket-number').html(pCase.ticketNumber);
        $('#visit-details-customerName-lbl').html(pCase.caseCustomer.fullName);
        $('#visit-details-created').removeClass('hidden');
        $('#visit-details-created-lbl').html(util.toDateTime(pCase.created));

        $('#visit-details-current-note-text').text('');
        if(util.notNull(pCase.currentNote)) {
            $('#visit-details-current-note-add-btn').addClass('hidden');
            $('#visit-details-current-note-edit-btn').removeClass('hidden');
            $('#visit-details-current-note-author').removeClass('hidden');
            $('#visit-details-current-note-edit-btn').unbind().click(function() {
                notes.openNoteDialogV2(pCase.currentNote, pCase.id, true, function(note) {
                    $('#visit-details-current-note-text').text(note.text);
                    $('#visit-details-current-note-author').text(note.createdBy + ' ' + util.timeOfDate(note.created));
                });
            });
            $('#visit-details-current-note-text').text(pCase.currentNote.text);
            $('#visit-details-current-note-author').text(pCase.currentNote.createdBy + ' ' + util.timeOfDate(pCase.currentNote.created));
        } else {
            $('#visit-details-current-note-edit-btn').addClass('hidden');
            $('#visit-details-current-note-author').addClass('hidden');
            $('#visit-details-current-note-add-btn').removeClass('hidden');
            $('#visit-details-current-note-add-btn').unbind().click(function() {
                notes.openNoteDialogV2(null, pCase.id, false, function(note) {
                    $('#visit-details-current-note-text').text(note.text);
                    $('#visit-details-current-note-author').text(note.createdBy + ' ' + util.timeOfDate(note.created));
                    $('#visit-details-current-note-add-btn').addClass('hidden');
                    $('#visit-details-current-note-edit-btn').removeClass('hidden');
                    $('#visit-details-current-note-author').removeClass('hidden');
                });
            });
        }

        $('#customer_details_history_div').hide();
        $('#vd_show_customer_history_btn').show();
        $('#vd_hide_customer_history_btn').hide();
        $('#vd_customer_history_header_div').unbind();
        $('#vd_customer_history_header_div').bind('click', function() {
            toggleCustomerHistoryInVisitDetails(pCase.caseCustomer.id);
        });
        tables.populateVisitDetailsServicesTable(pCase);
        panels.showVisitDetailsPanel();
    };

    var customerIdLoadedInVisitDetailsHistory = -1;
    var toggleCustomerHistoryInVisitDetails = function(customerId){
        if(customerIdLoadedInVisitDetailsHistory!=customerId){
            tables.loadCustomerHistory(customerId, 'customer-details-history-table');
            customerIdLoadedInVisitDetailsHistory=customerId;
        }
        $('#customer_details_history_div').toggle();
        $('#vd_show_customer_history_btn').toggle();
        $('#vd_hide_customer_history_btn').toggle();

    };

    this.setOngoingCaseService = function(dbCase){
        if(util.isNull(dbCase) || util.isNull(dbCase.caseServices) || dbCase.caseServices.length == 0) {
            return;
        }
        for(var a = 0; a < dbCase.caseServices.length; a++) {
            if( dbCase.caseServices[a].servedByCounterId == sessvars.servicePointId &&
                (dbCase.caseServices[a].status == 'POST_PROCESSING' || dbCase.caseServices[a].status == 'SERVING')){
                sessvars.currentCase.ongoingService = dbCase.caseServices[a];
                return;
            }
        }
    };

    this.populateCurrentTransaction = function(pCase) {

        sessvars.currentCase = pCase;

        if(util.notNull(sessvars.currentCase)) {
            $('#cst_ticketnumber_lbl').html(sessvars.currentCase.ticketNumber);
            $('#cst_customerid_lbl').html(sessvars.currentCase.caseCustomer.id);
            $('#cst_customername_lbl').html(sessvars.currentCase.caseCustomer.fullName);

            servicePoint.setOngoingCaseService(pCase);
            if(util.notNull(sessvars.currentCase.ongoingService)){
                if(util.notNull(sessvars.currentCase.ongoingService) && sessvars.currentCase.ongoingService.recycleCount > 0) {
                    $('#recycled_times_lbl').removeClass('hidden').text(' (Recycled: ' + sessvars.currentCase.ongoingService.recycleCount + ')');
                } else {

                    $('#recycled_times_lbl').addClass('hidden').text('');
                }
                $('#cst_service_lbl').html(sessvars.currentCase.ongoingService.serviceName);
                $('#cst_waited_lbl').html(util.formatIntoHHMMSS(sessvars.currentCase.ongoingService.waitingTime));
                $('#cst_served_lbl').html(util.formatIntoHHMMSS(sessvars.currentCase.ongoingService.servingTime));
                $('#cst_postprocessing_lbl').html(util.formatIntoHHMMSS(sessvars.currentCase.ongoingService.postProcessingTime));

                // notes
                //notes.populateNotesOfCurrentTransaction(sessvars.currentCase.ongoingService.id, sessvars.currentCase.ongoingService.notes);
                notes.populateNoteOfCurrentTransaction(sessvars.currentCase);

                 // current state heading
                $('#cst-state').text(sessvars.state.userState);

                // Delivered services required etc.
                if(deliveredservices.isOutcomeOrDeliveredServiceNeeded() || (util.notNull(sessvars.state.visit) && sessvars.state.visit.currentVisitService.deliveredServiceExists)) {
                    $('#add_delivered_service_btn').removeClass('disabled').unbind().click(function() {

                      //  deliveredservices.openAddDeliveredServiceDialog(function() {});
                    });
                } else {
                    $('#add_delivered_service_btn').addClass('disabled').unbind();
                }

                if( sessvars.currentCase.ongoingService.vip == true) {
                    $('#srv_vip_lbl').removeClass('hidden');
                } else {
                    $('#srv_vip_lbl').addClass('hidden');
                }
				
            }
        }
        deliveredservices.loadMarks();
        loadServicesToday(sessvars.currentCase);

        $('#btn_show_customer_history').show();
        $('#btn_hide_customer_history').hide();
        $('#customer_history_div').hide();
        $('#customer_history_header_div').unbind();
        $('#customer_history_header_div').bind('click', function() {
            toggleCustomerHistory();
        });

        // Bind noshow button
        $('#cst_noshow_btn').unbind().click(function() {
            noshow.noShow();
        });

        // Bind click listener for add note. Only show if there is no currentNote on the ongoing case
        if(util.isNull(sessvars.currentCase.currentNote)) {
            $('#cst_add_note_btn').unbind();
            $('#cst_add_note_btn').bind('click', function() {
                notes.openNoteDialogV2({}, sessvars.currentCase.id, false, function(note) {
                    //notes.reloadNotes(sessvars.currentCase.ongoingService.id);
					sessvars.currentCase.currentNote = note;
                    notes.populateNoteOfCurrentTransaction(sessvars.currentCase);
                    loadServicesToday(sessvars.currentCase);
                    $('#cst_add_note_btn').addClass('hidden');
                });
            });
            $('#cst_add_note_btn').removeClass('hidden');
        } else {
            $('#cst_add_note_btn').addClass('hidden');
        }


        // Bind click listener for add service
        $('#services_today_add_service').unbind();
        $('#services_today_add_service').bind('click', function() {
			if (servicePoint.isVisitInDisplayQueue()) {
				return;
			}
            common.openAddServiceDialog(sessvars.currentCase.id, addServiceToPersistedCaseCallback);
        });

        // Add click handler for the End button. This button shall endVisit vs Orchestra,
        // then put the Conductor part into POST PROCESSING (e.g. the faked clone)
        $('#cst_end_f2f_btn').unbind().click(servicePoint.onEndFaceToFaceServing);
        $('#cst_post_processing_start_btn').unbind().click(servicePoint.onStartPostProcessing);

        $('#cst_put_in_pool_btn').unbind().click(onPark);

        buttons.updateAccordingToServicePointStatus();
        panels.showCurrentTransactionPanel();
    };

    var customerIdLoadedInHistory = -1;
    var toggleCustomerHistory = function(){
        if(customerIdLoadedInHistory!=sessvars.currentCase.caseCustomer.id){
            tables.loadCustomerHistory(sessvars.currentCase.caseCustomer.id, 'cst_customer_history_table');
            customerIdLoadedInHistory=sessvars.currentCase.caseCustomer.id;
        }
        $('#customer_history_div').toggle();
        $('#btn_show_customer_history').toggle();
        $('#btn_hide_customer_history').toggle();

    };

    var onPark = function() {
		if (servicePoint.isVisitInDisplayQueue()) {
			return;
		}
        if(servicePoint.USE_HOLDING_QUEUE == true){
            CaseServicePointService.holdVisit()
                .done(function(userState){
                    sessvars.state = userState;
                    servicePoint.clearOngoingVisit();
                    util.showMessage("Visit parked in Holding Queue");
                })
                .fail(function(err){
                    util.showError('Failed to transfer Visit to Holding Queue. ', err);
                })
        }else{
            CaseServicePointService.transferVisitToOwnUserPool(sessvars.branchId)
                .done(function(userState){
                    sessvars.state = userState;
                    servicePoint.clearOngoingVisit();
                    util.showMessage("Visit parked in User Pool");
                    tables.populateUserPoolVisitsTable();
                })
                .fail(function(err){
                    util.showError('Failed to transfer Visit to Pool. ', err);
                })
        }
    };

    /**
     * Puts an ongoing visit into Post-processing mode, effectively ending the ongoing Visit in Orchestra,
     * but keeping the Transaction going in regard to the Conductor backend and workstation.
     */
    this.onStartPostProcessing = function() {
		if (servicePoint.isVisitInDisplayQueue()) {
			return;
		}

        // Make sure we have an ongoing tx
        servicePoint.getState(sessvars.state);

        if(util.notNull(txTimeIntervalHandle)) {
            window.clearInterval(txTimeIntervalHandle);
            txTimeIntervalHandle = null;
        }

        if (util.isNull(sessvars.currentCase.ongoingService) || sessvars.state.servicePointState == servicePoint.servicePointState.CLOSED ||
            sessvars.state.userState != servicePoint.userState.SERVING) {
            util.showError('Cannot start Post-Processing, Service Point and/or Visit is not in an applicable state.');
            return;
        }
        if(servicePoint.canEndVisit(servicePoint.onStartPostProcessing)) {

        // Then, mark stuff in Conductor db and start the post-processing step
        CaseServicePointService.startPostProcessing(sessvars.currentCase.id, sessvars.currentCase.ongoingService.qpVisitServiceId)
            .done(function(userState){
                sessvars.state = userState;

                // Manual hack for Conductor - set UserState in session to POST_PROCESSING
				sessvars.isPostProcessing = true;

                buttons.setCSTStateForPostProcessing();

                // Reload the case object from the backend, populate CST panel.
                CaseServicePointService.getCase(sessvars.currentCase.id)
                    .done(function(dbCase){
                        servicePoint.populateCurrentTransaction(dbCase);
                        servicePoint.updateServingTimeClock();
                    })
                    .fail(function(err){
                        util.showError('Failed to fetch Case. ', err);
                    })
            })
            .fail(function(err){
                util.showError('Error starting Post-Processing. ', err);
            })
        }
    };

    this.onEndPostProcessing = function() {

        if(sessvars.isPostProcessing != true) {
            util.showError('Cannot end Post-Processing, Service Point is not in Post Processing state. Actual: ' + sessvars.state.userState);
            return;
        }
        CaseServicePointService.endPostProcessing( sessvars.currentCase.id, sessvars.currentCase.ongoingService.qpVisitServiceId)
            .done(function(userState){
                sessvars.state = userState;
				sessvars.isPostProcessing = false;
                buttons.setCSTStateForNormalOperation();
                servicePoint.clearOngoingVisit();

            })
            .fail(function(err){
                util.showError('Failed to end Post Processing. ', err);
            })

    };

    this.onEndFaceToFaceServing = function(callBack) {
		if (servicePoint.isVisitInDisplayQueue()) {
			return;
		}
        // Make sure we have an ongoing tx
        servicePoint.getState(sessvars.state);
        // Also, make sure we don't need to add any outstanding Delivered Services, Outcomes, Marks etc.
        if (deliveredservices.isOutcomeOrDeliveredServiceNeeded()) {
           // util.showError('Please add required Delivered Service(s) or Marks before ending the visit.');
		   
			deliveredservices.openAddDeliveredServiceDialog(util.partial(servicePoint.onEndFaceToFaceServing,callBack));
            return;
        }

        // Then, mark stuff in Conductor db and start the post-processing step     /case/{caseId}/service/{serviceId}/end
        CaseServicePointService.endFaceToFaceService(sessvars.currentCase.id, sessvars.state.visit.currentVisitService.id)
            .done(function(userState){
                sessvars.state = userState;
                buttons.setCSTStateForNormalOperation();
                // TODO What does end F2F mean here? Start post-processing or just a regular endVisit?
                servicePoint.clearOngoingVisit();

                if(typeof(callBack) == typeof(Function)) {callBack();}

            })
            .fail(function(err){
                util.showError('Failed to end Serving. ', err);
            })

    };


    //F5 pressed
    this.updateUI = function() {

        if(settings.hasValidSettings(true)) {
            cServicePointService.getUserStatus()
                .done(function(userState){
                    sessvars.state = userState;
                })
                .fail(function(err){
                    util.showError('Could not get User status from server.', err);
                })

            sessvars.statusUpdated = new Date();
			if (servicePoint.isActive() == true) {
				if(typeof sessvars.state.servicePointState === 'undefined' || sessvars.state.servicePointState == null) {
					var servicePointStatus = null;
					cServicePointService.getServicePoint(servicePoint.createParams())
						.done(function(status){
							servicePointStatus = status;
						})
						.fail(function(err){
							util.showError('Failed to get ServicePoint data from server.', err);
						})

					if(typeof servicePointStatus !== 'undefined' && servicePointStatus != null &&
						typeof servicePointStatus.state !== 'undefined' && servicePointStatus.state != null) {
						sessvars.state.servicePointState = servicePointStatus.state;
					}
				}
			}
            //readServicePointSettingsFromWorkstation();
            // queues will not be updated here since it is a refresh,
            servicePoint.updateWorkstationStatus(true);
            buttons.updateAccordingToServicePointStatus();
            //updateWorkstationSettings();

        } else {
            clearTimeout(sessvars.queueTimer);
            sessvars.queueTimerOn = false;
			clearTimeout(sessvars.servicepointsTimer);
            sessvars.servicepointsTimerOn = false;
            cServicePointService.getCurrentUser()
                .done(function(user){
                    sessvars.currentUser = user;
                })
                .fail(function(err){
                    util.showError('Could not get current User from server.', err);
                })

            $("#userName").html(sessvars.currentUser.fullName); //sessvars.currentUser.userName);
			$("#branch-name-lbl").text(sessvars.branchName);
            $("#servicepoint-name-lbl").text(sessvars.state.servicePointName);
           // settings.showSettingsWindow();
        }
    };

    // DWR status handler, needs to be overwritten since we dont use the same
    // html elements for error boxes as other modules
    this.cometDWSPollStatusHandler = function(status, message) {
        if (status) {
            try {
                //settings.setWorkstationOnline();
            } catch (e) {
                //util.showError('Connection to server lost: ' + e);
            }
        } else {
            //util.showError('Connection to server lost... please wait.');
            //settings.setWorkstationOffline();
        }
    };

    this.initGUI = function() {
        // We may only INIT the GUI once, otherwise we'll get many errors when dataTables are reinitialized.
        if(init.guiInitialized) {
            return;
        }

        if(util.isNull(sessvars.branchId)) {
            return;
        }
		
		$('#opspanel-tab').hide();
		$('#reception-tab').hide();
		$('#counter-tab').hide();
		var modules = getParameterByName('modules');
		
		if(opspanel.isActive()){
            $('#opspanel-tab').unbind();
            $('#opspanel-tab').click(function() {
                panels.showOpspanelContainer();
                tables.refresh();
                tables.reloadQueue();
                //if(util.notNull(sessvars.currentCase)) {
                    tables.reloadVisitDetailsServicesTable(sessvars.currentCase);
                //}
                $(this).parent().parent().find('li').removeClass('active');
                $(this).parent().addClass('active');
				opspanel.updateServicepointsTable('servicepoints_table');
            });
			if (modules.length > 1) {
				$('#opspanel-tab').show();
			}
        }
		
        if(reception.isActive()){
            $('#reception-tab').unbind();
            $('#reception-tab').click(function() {
                panels.showReceptionContainer();
                tables.refresh();
                tables.reloadQueue();
                //if(util.notNull(sessvars.currentCase)) {
                    tables.reloadVisitDetailsServicesTable(sessvars.currentCase);
                //}
                $(this).parent().parent().find('li').removeClass('active');
                $(this).parent().addClass('active');
            });
			if (modules.length > 1) {
				$('#reception-tab').show();
			}
        }	

        if(servicePoint.isActive()){
            panels.showPoolContainer();
            $('#counter-tab').unbind();
            $('#counter-tab').click(function() {
                panels.showWorkstationContainer();
                tables.refresh();
                tables.reloadQueue();
               // if(util.notNull(sessvars.currentCase)) {
                    tables.reloadVisitDetailsServicesTable(sessvars.currentCase);
               // }

                if(sessvars.currentCase == null){
                    panels.expandQueueView(true);
                }

                $(this).parent().parent().find('li').removeClass('active');
                $(this).parent().addClass('active');
            });
			if (modules.length > 1) {
				$('#counter-tab').show();
			}
        }
		
		if(modules.indexOf('c') != -1) {
			panels.showWorkstationContainer();
		}
		else {
			if(modules.indexOf('r') != -1) {
				panels.showReceptionContainer();
			}
			else if(modules.indexOf('p') != -1) {
				panels.showOpspanelContainer();
			}
			else {
				panels.showWorkstationContainer();
			}
		}

        // Settings link
        $('#settings').unbind();
        $('#settings').click(function() {
            settings.showSettingsWindow();
        });
		
		$('#branch-name-lbl').unbind();
        $('#branch-name-lbl').click(function() {
            settings.showSettingsWindow();
        });
		
		$('#servicepoint-name-lbl').unbind();
        $('#servicepoint-name-lbl').click(function() {
            settings.showSettingsWindow();
        });

        // Logout link
        $('#logOut').unbind();
        $('#logOut').click(function() {
            servicePoint.cleanAndLogout();
        });
		
		if(typeof sessvars.isPostProcessing === 'undefined' || sessvars.isPostProcessing == null) {
			sessvars.isPostProcessing = false;
		}

        search.initGUI();
        buttons.initGUI();

        // Populate work profiles btn
        $('#work_profile_select').empty();
        var workProfiles = {};
        cServicePointService.getWorkProfiles(sessvars.branchId)
            .done(function(workprofiles){
                workProfiles = workprofiles;
            })
            .fail(function(err){
                util.showError('Failed to fetch WorkProfiles from server.', err);
            })

        $.map(workProfiles, function(workProfile) {
            var opt = $("<option />").val(workProfile.id).text(workProfile.name);
            if(workProfile.id == sessvars.workProfileId) {
                $(opt).attr('selected', 'selected');
            }
            $('#work_profile_select').append(opt);
        });
        $('#work_profile_select').unbind().change(function() {
            sessvars.workProfileId = $('#work_profile_select').val();
            servicePoint.setProfile(servicePoint.createParams());
        });

        // Bind delete visit button
        $('#visit_delete_btn').unbind();
        $('#visit_delete_btn').click(deleteCurrentCase);
		
        tables.initGUI();
        if(reception.isActive()){
            reception.initReceptionElements();
        }
		if(opspanel.isActive()){
            opspanel.initOpspanelElements();
        }
        walkDirect.initGUI();

        // Get user state, if we have an ongoing TX, populate
        servicePoint.getState(sessvars.state);

        if(util.notNull(sessvars.state) && util.notNull(sessvars.state.visit) && sessvars.state.userState == servicePoint.userState.SERVING) { //typeof sessvars.state !== 'undefined' && sessvars.state != null && sessvars.state.visit sessvars.currentCase.qpVisitId != null) {
            CaseServicePointService.getCaseByVisitId(sessvars.state.visit.id)
                .done(function(dbCase){
                    sessvars.currentCase = dbCase;
                    servicePoint.populateCurrentTransaction(dbCase);
                    servicePoint.updateServingTimeClock();
                })
                .fail(function(err){
                    util.showError('Cannot view ongoing Visit, no case is attached to the visit.', err);
                })
        }
        buttons.updateAccordingToServicePointStatus();
        init.guiInitialized = true;
    };

    var deleteCurrentCase = function() {
        if(sessvars.currentCase != null && sessvars.currentCase.id != null) {
            CaseServicePointService.deleteCurrentCase(sessvars.currentCase.id)
                .done(function(something){
                    //no return??
                })
                .fail(function(err){
                    util.showError('Failed to delete current Case.', err);
                })
            sessvars.currentCase = null;
            servicePoint.clearOngoingVisit();
        }
    };

    this.updateServingTimeClock = function(){
        // start js based time counters depending on service state
        if(sessvars.currentCase.ongoingService.status == 'SERVING') {

            // START SERVED timer
            //currentF2FTime = sessvars.state.visit.timeSinceCalled;
            //currentF2FTime = sessvarscurrentCase.ongoingService.servingTime;
			currentF2FTime = moment(new Date()).diff(sessvars.currentCase.ongoingService.updated)/1000;
            $('#cst_served_lbl').text(util.secondsToHms(currentF2FTime));
            if(util.notNull(txTimeIntervalHandle)) {
                window.clearTimeout(txTimeIntervalHandle);
            }
            txTimeIntervalHandle = setInterval(function() {
                $('#cst_served_lbl').text(util.secondsToHms(++currentF2FTime))
            }, 1000);
            $('#cst_postprocessing_lbl').countdown('destroy');
            $('#cst_postprocessing_lbl').html(util.formatIntoHHMMSS(0));
        }
        if(sessvars.currentCase.ongoingService.status == 'POST_PROCESSING') {
            // START POST_PROCESSING timer
            updatePostProcessingTime();
        }
    };

    var updateTransactionTime = function() {
        var timeRelativeToCallNext = -1;
        if(sessvars.state.userState == servicePoint.userState.SERVING) {
            if(sessvars.state.visitState == servicePoint.visitState.VISIT_IN_DISPLAY_QUEUE) {
                $("#cst_served_lbl").empty().text("info.visit.not.called.yet");
            } else {
                // Use the fields in the UserState to find out how long since the ticket was called
                var now;
                if (sessvars.state.visit.timeSinceCalled != null && sessvars.state.visit.waitingTime != null &&
                    sessvars.statusUpdated != null) {
                    now = new Date();
                    timeRelativeToCallNext = sessvars.statusUpdated.getTime() - now.getTime();
                    //Has the ticket has been called in the future?
                    if(timeRelativeToCallNext > 0) {
                        timeRelativeToCallNext = 0;
                    } else {
                        timeRelativeToCallNext = timeRelativeToCallNext/1000;
                    }
                    // add (or subtract, since we're counting up) no of seconds given from the server to the time spent in the client since the status was retrieved
                    timeRelativeToCallNext -= sessvars.state.visit.timeSinceCalled;
                }
            }
        } else {
            timeRelativeToCallNext = -1;
        }
        if(timeRelativeToCallNext != -1) {
            if(util.notNull(txTimeIntervalHandle)) {
                window.cancelTimeout(txTimeIntervalHandle);
            }
            $('#cst_served_lbl').countdown({since: timeRelativeToCallNext, compact: true, format: 'HMS'});
        }
    };


    var updatePostProcessingTime = function() {
        var timeRelativeToCallNext = -1;
        if(sessvars.isPostProcessing == true) {
            $('#cst_postprocessing_lbl').countdown({since: 0, compact: true, format: 'HMS'});
        }
    };
};
