package com.qmatic.ps.bp.rest.server;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import javax.ws.rs.core.HttpHeaders;

import org.mockito.InOrder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.dao.CaseDao;
import com.qmatic.ps.bp.rest.client.EntryPointServiceClient;
import com.qmatic.ps.bp.rest.mapper.CaseBuilder;
import com.qmatic.qp.api.connectors.dto.TransferSortPolicy;
import com.qmatic.qp.api.connectors.dto.VisitParameters;

/**
 * @author Dejan Milojevic (dejmil), @created Nov 5, 2013
 */

@Test
public class CaseAppointmentEndpointUpdateAppointmentToArrivedTest {

	private CaseAppointmentEndpoint caseAppointmentEndpoint;
	private EntryPointServiceClient entryPointServiceClient;
	
	@BeforeMethod
	public void setUp() {
		caseAppointmentEndpoint = spy(new CaseAppointmentEndpoint());
		entryPointServiceClient = mock(EntryPointServiceClient.class);
		caseAppointmentEndpoint.setEntryPointServiceClient(entryPointServiceClient);
		caseAppointmentEndpoint.setHttpHeaders(mock(HttpHeaders.class));
		caseAppointmentEndpoint.setCaseBuilder(mock(CaseBuilder.class));
		caseAppointmentEndpoint.setCaseDao(mock(CaseDao.class));
	}
	
	public void ssoCookieShouldBeSetBeforeClientCall() throws BPException {
		caseAppointmentEndpoint.updateAppointmentToArrived(1, 2, 3, 0, "");
		
		InOrder inOrder = inOrder(caseAppointmentEndpoint, entryPointServiceClient);
		inOrder.verify(caseAppointmentEndpoint).setSSOCookie();
		inOrder.verify(entryPointServiceClient).createVisit(anyInt(), anyInt(), any(VisitParameters.class), any(TransferSortPolicy.class));
	}
	
}
