package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.CaseCustomer;

import java.util.List;
import java.util.Map;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
public interface CaseCustomerDao extends Dao<CaseCustomer> {

    List<CaseCustomer> search(CaseCustomer searchCustomer, String andor, Integer limit) throws BPException;
	
	CaseCustomer searchById(Long customerId) throws BPException;

	boolean isCustomerExist(CaseCustomer searchCustomer) throws BPException;
}
