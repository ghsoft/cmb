package com.qmatic.ps.bp.rest.mapper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.*;
import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.common.BPExceptionCode;
import com.qmatic.ps.bp.common.Constants;
import com.qmatic.ps.bp.rest.dto.QMetadata;
import com.qmatic.ps.bp.rest.dto.QResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 08:40
 * To change this template use File | Settings | File Templates.
 */
public class QResponseBuilder {

    private static final Logger log = LoggerFactory.getLogger(QResponseBuilder.class);

    private static ObjectMapper om = new ObjectMapper();

    public static QResponse fromCollection(Collection<? extends Serializable> entities, Map<String, List<String>> params) {
        List<Serializable> payload = new ArrayList<>();
        List<String> fieldsList = readFieldsFromQueryParams(params);

        QResponse response = new QResponse();
        response.setMeta(buildMeta(entities.size(), params));

        if (entities != null) {
            for (Serializable entity : entities) {

                try {
                    payload.add(genericEntityToFieldFilteredHashMap(fieldsList, om, entity));
                } catch (IOException e) {
                    log.error(e.getMessage());
                    throw new RuntimeException(e.getMessage());
                }
            }
        }
        response.setPayload((Serializable) payload);
        return response;
    }

    public static QResponse fromEntity(Serializable entity, Map<String, List<String>> params) {
        List<String> fieldsList = readFieldsFromQueryParams(params);

        QResponse response = new QResponse();
        response.setMeta(buildMeta(1, params));

        if (entity != null) {

            try {
                response.setPayload(genericEntityToFieldFilteredHashMap(fieldsList, om, entity));
            } catch (IOException e) {
                log.error(e.getMessage());
                throw new RuntimeException(e.getMessage());
            }
        }

        return response;
    }

    public static QResponse fromRawEntity(Serializable entity, Map<String, List<String>> params) {
        QResponse response = new QResponse();
        response.setMeta(buildMeta(1, params));
        response.setPayload(entity);
        return response;
    }


    private static List<String> readFieldsFromQueryParams(Map<String, List<String>> params) {
        String[] fields = null;
        if (params.containsKey(Constants.PRM_FIELDS)) {
            fields = params.get(Constants.PRM_FIELDS).get(0).split(",");
        } else {
            fields = new String[]{};
        }
        return Arrays.asList(fields);
    }

    private static String getNullSafeValue(Map<String, List<String>> params, String key) {
        String val = params.get(key).get(0);

        if (val == null || "null".equalsIgnoreCase(val)) {
            return null;
        } else {
            return val;
        }
    }


    private static HashMap<String, Serializable> genericEntityToFieldFilteredHashMap(List<String> fieldsList, ObjectMapper om, Serializable entity) throws IOException {
        // First, convert the JPA entity to a JsonNode
        JsonNode pn = om.readTree(om.writeValueAsString(entity));

        // Then, iterate over each field, adding only those included in the fields list
        HashMap<String, Serializable> on = new HashMap<>();
        Iterator<Map.Entry<String, JsonNode>> i = pn.fields();
        while (i.hasNext()) {
            Map.Entry<String, JsonNode> next = i.next();
            if (fieldsList.size() == 0 || fieldsList.contains(next.getKey())) {
                writeFilteredValue(on, next);
            }
        }
        return on;
    }

    /**
     * Transforms the supplied entity into a hashmap where the sub-objects and sub-arrays have
     * been reduced to id+name.
     *
     * @param entity
     * @return
     * @throws IOException
     */
    private static HashMap<String, Serializable> buildFilteredObject(Serializable entity) throws BPException {
        try {
            JsonNode pn = om.readTree(om.writeValueAsString(entity));
            HashMap<String, Serializable> on = new HashMap<>();
            Iterator<Map.Entry<String, JsonNode>> i = pn.fields();
            while (i.hasNext()) {
                Map.Entry<String, JsonNode> next = i.next();
                writeFilteredValue(on, next);
            }
            return on;
        } catch (IOException e) {
            throw new BPException(e.getMessage(), BPExceptionCode.SERIALIZATION_ERROR);
        }
    }

    private static void writeFilteredValue(HashMap<String, Serializable> on, Map.Entry<String, JsonNode> next) {
        JsonNode value = next.getValue();

        // Handle sub-arrays
        if (value instanceof ArrayNode) {
            ArrayList innerList = new ArrayList();
            serializeArrayNode(innerList, next.getKey(), value);
            on.put(next.getKey(), innerList);
        }
        // Handle sub-objects
        else if (value instanceof ObjectNode) {
            on.put(next.getKey(), serializeSubObject(next.getKey(), (ObjectNode) value));
        }

        // And handle "primitive" types
        else {
            if (next.getValue() == null || next.getValue().asText().equalsIgnoreCase("null")) {
                on.put(next.getKey(), null);
            } else {
                // TODO store stuff as they are
                JsonNode value1 = next.getValue();
                if (value1 instanceof DoubleNode) {
                    on.put(next.getKey(), next.getValue().asDouble());
                } else if (value1 instanceof NumericNode) {
                    on.put(next.getKey(), next.getValue().asInt());
                } else if (value1 instanceof BooleanNode) {
                    on.put(next.getKey(), next.getValue().asBoolean());
                } else {
                    on.put(next.getKey(), next.getValue().asText());
                }
            }

        }
    }

    private static void serializeArrayNode(ArrayList innerList, String key, JsonNode value) {

        ArrayNode arr = (ArrayNode) value;
        Iterator<JsonNode> elements = arr.elements();
        while (elements.hasNext()) {
            JsonNode innerNode = elements.next();

            if (innerNode instanceof ObjectNode) {
                innerList.add(serializeSubObject(key, (ObjectNode) innerNode));
            }
        }
    }

    /**
     * For sub-objects, we only add the following fields (if present)
     * <li>id</li>
     * <li>name</li>
     * <li>title</li>
     * <li>firstName</li>
     * <li>lastName</li>
     *
     * @param key
     * @param value
     * @return
     */
    private static HashMap<String, Serializable> serializeSubObject(String key, ObjectNode value) {
        HashMap<String, Serializable> subObject = new HashMap<>();


        // Sub-object. Add ID and NAME or TITLE to the sub-object and then set the sub-object on the response map
        JsonNode id = value.get(Constants.FIELD_ID);
        if (id != null) {
            subObject.put(Constants.FIELD_ID, id.asLong());
        }

        JsonNode name = value.get(Constants.FIELD_NAME);
        if (name != null) {
            subObject.put(Constants.FIELD_NAME, name.asText());
        }

        JsonNode title = value.get(Constants.FIELD_TITLE);
        if (title != null) {
            subObject.put(Constants.FIELD_TITLE, title.asText());
        }

        JsonNode firstName = value.get(Constants.FIELD_FIRST_NAME);
        if (firstName != null) {
            subObject.put(Constants.FIELD_FIRST_NAME, firstName.asText());
        }

        JsonNode lastName = value.get(Constants.FIELD_LAST_NAME);
        if (lastName != null) {
            subObject.put(Constants.FIELD_LAST_NAME, lastName.asText());
        }
        return subObject;
    }

    private static QMetadata buildMeta(int totalResults, Map<String, List<String>> params) {
        QMetadata meta = new QMetadata();
        meta.setTotalResults(totalResults);
        for (String key : params.keySet()) {
            switch (key) {
                case Constants.PRM_START:
                    meta.setStart(getNullSafeValue(params, key));
                    break;
                case Constants.PRM_END:
                    meta.setEnd(getNullSafeValue(params, key));
                    break;
                case Constants.PRM_FIELDS:
                    meta.setFields(getNullSafeValue(params, key));
                    break;
                case Constants.PRM_OFFSET:
                    meta.setOffset(Integer.parseInt(params.get(key).get(0)));
                    break;
                case Constants.PRM_LIMIT:
                    meta.setLimit(Integer.parseInt(params.get(key).get(0)));
                    break;
                default:
                    // For all other stuff, just add them "as-is" to the params map
                    meta.getArguments().put(key, getNullSafeValue(params, key));
                    break;
            }
        }
        return meta;
    }

}
