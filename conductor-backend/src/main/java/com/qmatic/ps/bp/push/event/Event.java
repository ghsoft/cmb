package com.qmatic.ps.bp.push.event;

import java.io.Serializable;

/**
 * Base class for our CometD-based events.
 */
public abstract class Event implements Serializable {

    /** ISO 8601 datetime string */
    private String timeStamp;

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public abstract String getEventType();
}
