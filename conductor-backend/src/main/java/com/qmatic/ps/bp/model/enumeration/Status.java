package com.qmatic.ps.bp.model.enumeration;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-23
 * Time: 15:54
 * To change this template use File | Settings | File Templates.
 */
public enum Status {
    PENDING,
	WAITING,
	RECYCLED,
	ONHOLD,
	SERVING,
	POST_PROCESSING,
	FINISHED,
	OVERRIDDEN,
	TRASH,
	NOSHOW,
	REMOVED, //NOT USED
	IN_USER_POOL //NOT USED
}