package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;

import java.util.List;

/**
 * Copyright 2012 Q-MATIC AB. All rights reserved
 * 
 * Base interface for all Data Access Objects in the QMatic Platform.
 * 
 * The interface provides basic CRUD operations.
 * 
 * @author fregus
 * @created 2012-01-23
 */
public interface Dao<E> {
    /**
     * Creates and persists the entity
     * 
     * @param entity
     * @throws Exception
     *             if the entity already exists
     */
    public <E> E create(E entity) throws BPException;

    /**
     * The standard read/get-method of the entity, e.g. {@code get<Entity>(id) }
     * 
     * @param id
     * @return null if not found
     */
    public <E> E findById(Object id) throws BPException;

    /**
     * Retrieves all entities of the specific type. <b>NB:</b> It is performance wise potentially expensive to execute this generic method.
     * 
     * @return an empty list if there are no elements.
     */
    public List<E> getAll() throws BPException;

    /**
     * Updates an already existing entity.
     * 
     * @param entity
     */
    public <E> E update(E entity) throws BPException;

    /**
     * Deletes an existing entity
     * 
     * @param entity
     */
    public void delete(E entity) throws BPException;

    /**
     * Convenience method to deletes an existing entity, identified by its primary key
     * 
     * @param id
     */
    public void deleteById(Object id) throws BPException;

    List<E> getAll(java.util.Map<String,java.util.List<String>> params) throws BPException;

    List<E> search(java.util.Map<String,java.util.List<String>> params) throws BPException;


}
