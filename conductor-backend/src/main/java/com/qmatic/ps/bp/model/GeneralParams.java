package com.qmatic.ps.bp.model;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-23
 * Time: 15:54
 * To change this template use File | Settings | File Templates.
 */
public class GeneralParams {

	public static final String TRASH_QUEUE = "TRASH";
    public static final String NOSHOW_QUEUE = "NOSHOW";
    public static final String ONHOLD_QUEUE = "ONHOLD";
	public static final String SILENTCALL_QUEUE = "SILENTCALL";
	public static final String NEVERCALL_QUEUE = "NEVERCALL";
	
	public static final String TRASH_DELIVERED_SERVICE = "TRASH";
	public static final String NOSHOW_DELIVERED_SERVICE = "NOSHOW";
	public static final String ONHOLD_DELIVERED_SERVICE = "ONHOLD";
	public static final String QUEUE_CORRECTION_DELIVERED_SERVICE = "Queue Correction";
	public static final String VISIT_REMOVED_DELIVERED_SERVICE = "Visit Removed";
	public static final String SERVICE_OVERRIDDEN_DELIVERED_SERVICE = "Service Overridden";
    public static final Integer MAX_RECYCLES = 3;
    public static final Integer SYSTEM_RECYCLE_DELAY = 60; //seconds

}
