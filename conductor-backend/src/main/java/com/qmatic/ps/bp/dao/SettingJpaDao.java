package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.Setting;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-01-31
 * Time: 16:38
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class SettingJpaDao extends JpaDao<Setting> implements SettingDao {


    @Cacheable("settingJpaDaoMethodCache")
    public Setting findByGroup(String group) throws BPException {
        try {
            return (Setting) entityManager.createQuery("SELECT s FROM Setting s WHERE s.group = :group")
                    .setParameter("group", group)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }


    @Cacheable("settingJpaDaoMethodCache")
    public List<Setting> getAll(java.util.Map<String, List<String>> params) throws BPException {
         return super.getAll(params);
    }

    @Override
    @CacheEvict(value="settingJpaDaoMethodCache", allEntries=true)
    public Setting update(Setting setting) throws BPException {
        return super.update(setting);
    }

    @Override
    @CacheEvict(value="settingJpaDaoMethodCache", allEntries=true)
    public void create(Setting setting) throws BPException {
        super.create(setting);
    }

    @Override
    @CacheEvict(value="settingJpaDaoMethodCache", allEntries=true)
    public void delete(Setting setting) throws BPException {
        super.delete(setting);
    }

    @Override
    @CacheEvict(value="settingJpaDaoMethodCache", allEntries=true)
    public void deleteById(Long id) throws BPException {
        super.deleteById(id);
    }
}
