package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.model.Note;
import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-01-31
 * Time: 16:27
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class NoteJpaDao extends JpaDao<Note> implements NoteDao {
}
