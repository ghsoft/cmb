// start QP Java REST client API
package com.qmatic.ps.bp.rest.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qmatic.ps.bp.util.CookieHolder;
import com.qmatic.qp.api.connectors.dto.*;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Component
public class ServicePointServiceClient {

    protected Client c;
    protected ObjectMapper om = new ObjectMapper();
    private final Logger log = LoggerFactory.getLogger(ServicePointServiceClient.class);

    @PostConstruct
    public void init() {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put("com.sun.jersey.api.json.POJOMappingFeature", Boolean.TRUE);
        c = Client.create(clientConfig);
        c.setFollowRedirects(true);
        c.setConnectTimeout(5000);
//        String currentUserName = SecurityUtil.getCurrentUserName();
//        Subject subject = SecurityUtils.getSubject();
//        SecurityUtils.
//        c.addFilter(new HTTPBasicAuthFilter(currentUserName, credentials.get("password")));
    }

    protected String getRestPath() {
        return getProtocol() + "://" + getIp() + ":" + getPort() + "/" + getBasePath();
    }

    protected String getProtocol() {
        return "http";
    }

    protected String getIp() {
        return "127.0.0.1";
    }

    protected String getPort() {
        return "8080";
    }

    protected String getBasePath() {
        return "rest";
    }


    // start JAX-RS API
    public java.util.List<com.qmatic.qp.api.connectors.dto.TinyBranch> getBranches() {

        java.util.List<com.qmatic.qp.api.connectors.dto.TinyBranch> result = c.resource(getRestPath() + "/servicepoint/branches")
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.TinyBranch>>() {
                });
        return result;
    }

    private Cookie getSSOCookie() {
        return CookieHolder.get();
    }

    public BranchInformation getBranch(Integer branchId) {
        BranchInformation result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .cookie(getSSOCookie())
                .get(new GenericType<BranchInformation>() {
                });
        return result;
    }

    public GenericCustomer getCustomer(Integer customerId) {
        GenericCustomer result = c.resource(getRestPath() + "/servicepoint/customers/" + customerId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<GenericCustomer>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.TinyQueue> getQueues(Integer branchId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.TinyQueue> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/queues")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.TinyQueue>>() {
                });
        return result;
    }

    public UserStatus logout() {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/logout")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public void removeVisit(Integer branchId, Integer servicePointId, Long visitId) {
        c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/visits/" + visitId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete();
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit> findVisits(Integer branchId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit>>() {
                });
        return result;
    }

    public Visit getVisit(Integer branchId, Long visitId) {
        Visit result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<Visit>() {
                });
        return result;
    }

    public UserStatus nextVisit(Integer branchId, Integer servicePointId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/visits/next")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .post(UserStatus.class);
        return result;
    }

    public UserStatus addMark(Integer branchId, Long visitId, Integer markId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/marks/" + markId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .post(UserStatus.class);
        return result;
    }

    public UserStatus confirmVisit(Integer branchId, Long visitId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/confirm")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public UserStatus recycleVisit(Integer branchId, Integer servicePointId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/visit/recycle")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public UserStatus recallVisit(Integer branchId, Integer servicePointId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/visit/recall")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public UserStatus noShowVisit(Integer branchId, Long visitId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/noshow")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public UserStatus endVisit(Integer branchId, Long visitId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/end")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public UserStatus callVisit(Integer branchId, Integer servicePointId, Integer visitId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/visits/" + visitId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .post(UserStatus.class);
        return result;
    }

    public UserStatus removeMark(Integer branchId, Long visitId, Long visitMarkId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/marks/" + visitMarkId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete(UserStatus.class);
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.Mark> getMarks(Integer branchId, Integer markTypeId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.Mark> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/markTypes/" + markTypeId + "/marks")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.Mark>>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.MarkType> getMarkTypes(Integer branchId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.MarkType> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/markTypes")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.MarkType>>() {
                });
        return result;
    }

    public UserStatus startWrapup(Integer branchId, Long visitId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/wrapup")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public UserStatus addService(Integer branchId, Long visitId, Integer serviceId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/services/" + serviceId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .post(UserStatus.class);
        return result;
    }
	
	public UserStatus changeCurrentService(Integer branchId, Long visitId, Integer serviceId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/services/" + serviceId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public UserStatus serveService(Integer branchId, Long visitId, Long visitServiceId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/services/" + visitServiceId + "/serve")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public UserStatus endService(Integer branchId, Long visitId, Long visitServiceId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/services/" + visitServiceId + "/end")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public UserStatus removeService(Integer branchId, Long visitId, Long visitServiceId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/services/" + visitServiceId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete(UserStatus.class);
        return result;
    }

    public GenericCustomer createCustomer(/*GenericCustomer*/ String entity) {
        GenericCustomer result = c.resource(getRestPath() + "/servicepoint/customers")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .post(GenericCustomer.class);
        return result;
    }

    public void updateCustomer(Integer customerId, /*GenericCustomer*/ String entity) {
        c.resource(getRestPath() + "/servicepoint/customers/" + customerId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .put();
    }

    public void deleteCustomer(Integer customerId) {
        c.resource(getRestPath() + "/servicepoint/customers/" + customerId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete();
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.GenericCustomer> findCustomers() {
        java.util.List<com.qmatic.qp.api.connectors.dto.GenericCustomer> result = c.resource(getRestPath() + "/servicepoint/customers")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.GenericCustomer>>() {
                });
        return result;
    }

    public java.util.List<GenericCustomer> findCustomers(String searchParam) {
        java.util.List<GenericCustomer> result = c.resource(getRestPath() + "/servicepoint/customers;firstName=" + searchParam + ";lastName=" + searchParam + ";cardNumber=" + searchParam)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<GenericCustomer>>() {});
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.ServicePoint> getServicePointsByDeviceType(Integer branchId, String deviceType) {
        java.util.List<com.qmatic.qp.api.connectors.dto.ServicePoint> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/deviceTypes/" + deviceType)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.ServicePoint>>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.Outcome> getOutcomesForService(Integer branchId, Integer serviceId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.Outcome> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/services/" + serviceId + "/outcomes")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.Outcome>>() {
                });
        return result;
    }



    public java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit> getVisitsInQueue(Integer branchId, Integer queueId) {
        long start = System.currentTimeMillis();
        java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/queues/" + queueId + "/visits")
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit>>() {
                });
        log.info("Reading visits over connector took {} ms", (System.currentTimeMillis() - start));
        return result;
    }

    public User getCurrentUser() {
        User result = c.resource(getRestPath() + "/servicepoint/user")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<User>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.User> getLoggedInUsers(Integer branchId, String onlyServicePoints) {
        java.util.List<com.qmatic.qp.api.connectors.dto.User> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/users")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.User>>() {
                });
        return result;
    }

    public void setBranchVariable(Integer branchId, BranchVariable entity) {
        c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/variables")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .put();
    }

    public BranchVariable getBranchVariable(Integer branchId, String name) {
        BranchVariable result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/variables/" + name)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<BranchVariable>() {
                });
        return result;
    }

    public void deleteBranchVariable(Integer branchId, String name) {
        c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/variables/" + name)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete();
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.BranchVariable> getBranchVariables(Integer branchId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.BranchVariable> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/variables")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.BranchVariable>>() {
                });
        return result;
    }

    public void transferVisitToUserPool(Integer branchId, Integer userId, TransferParams entity) {
        c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/users/" + userId + "/visits")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .put();
    }

    public void transferVisitToQueue(Integer branchId, Integer queueId, TransferParams entity) {
        c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/queues/" + queueId + "/visits")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .put();
    }

    public GlobalVariable getGlobalVariable(String name) {
        GlobalVariable result = c.resource(getRestPath() + "/servicepoint/variables/" + name)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<GlobalVariable>() {
                });
        return result;
    }

    public void setGlobalVariable(GlobalVariable entity) {
        c.resource(getRestPath() + "/servicepoint/variables")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .put();
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.GlobalVariable> getGlobalVariables() {
        java.util.List<com.qmatic.qp.api.connectors.dto.GlobalVariable> result = c.resource(getRestPath() + "/servicepoint/variables")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.GlobalVariable>>() {
                });
        return result;
    }

    public void deleteGlobalVariable(String name) {
        c.resource(getRestPath() + "/servicepoint/variables/" + name)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete();
    }

    public UserStatus startUserSession(Integer branchId, String userName) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/users/" + userName)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public UserStatus endUserSession(Integer branchId, String userName) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/users/" + userName)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete(UserStatus.class);
        return result;
    }

    public SystemInformation getSystemInformation() {
        SystemInformation result = c.resource(getRestPath() + "/servicepoint/systemInformation")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<SystemInformation>() {
                });
        return result;
    }

    public Journey getJourneyForService(Integer branchId, Integer serviceId) {
        Journey result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/services/" + serviceId + "/journey")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<Journey>() {
                });
        return result;
    }

    public WorkProfile getWorkProfile(Integer branchId, String userName) {
        WorkProfile result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/users/" + userName + "/workProfile")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<WorkProfile>() {
                });
        return result;
    }

    public UserStatus setWorkProfile(Integer branchId, String userName, Integer workProfileId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/users/" + userName + "/workProfile/" + workProfileId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.WorkProfile> getWorkProfiles(Integer branchId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.WorkProfile> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/workProfiles")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.WorkProfile>>() {
                });
        return result;
    }

    public ServicePoint getServicePoint(Integer branchId, Integer servicePointId) {
        ServicePoint result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<ServicePoint>() {
                });
        return result;
    }

    public Visit getCurrentVisit(Integer branchId) {
        Visit result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visit")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<Visit>() {
                });
        return result;
    }

    public Visit updateVisitParameters(Integer branchId, Long visitId, Map entity) {
        Visit result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/parameters")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .put(Visit.class);
        return result;
    }

    public UserStatus startUserServicePointSession(Integer branchId, Integer servicePointId, String userName) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/users/" + userName)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public UserStatus endUserServicePointSession(Integer branchId, Integer servicePointId, String userName) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/users/" + userName)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete(UserStatus.class);
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.User> getUsersOnServicePoint(Integer branchId, Integer servicePointId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.User> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/users")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.User>>() {
                });
        return result;
    }

    public UserStatus callAppointment(Integer branchId, Integer servicePointId, Integer appointmentId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/appointments/" + appointmentId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .post(UserStatus.class);
        return result;
    }

    public UserStatus createVisitAtServicePoint(Integer branchId, Integer servicePointId, VisitParameters entity) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/visits")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .post(UserStatus.class);
        return result;
    }

    public UserStatus cancelWaitForVisit(Integer branchId, Integer servicePointId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/visit/cancelWait")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public UserStatus addCustomerToVisit(Integer branchId, Long visitId, Long customerId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/customers/" + customerId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public UserStatus removeCustomerFromVisit(Integer branchId, Long visitId, Long customerId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/customers/" + customerId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete(UserStatus.class);
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.VisitEvent> getVisitHistory(Integer branchId, Long visitId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.VisitEvent> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/events")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.VisitEvent>>() {
                });
        return result;
    }

    public UserStatus getUserStatus() {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/user/status")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<UserStatus>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit> getVisitsInServicePointPool(Integer branchId, Integer servicePointId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/pool/visits")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit>>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit> getVisitsInUserPool(Integer branchId, Integer userId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/users/" + userId + "/pool/visits")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.TinyVisit>>() {
                });
        return result;
    }

    public UserStatus callVisitFromUserPool(Integer branchId, Integer servicePointId, Integer userId, Long visitId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/users/" + userId + "/pool/" + visitId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.DeliveredService> getDeliveredServices(Integer branchId, Integer serviceId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.DeliveredService> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/services/" + serviceId + "/deliverableServices")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.DeliveredService>>() {
                });
        return result;
    }

    public UserStatus addDeliveredService(Integer branchId, Long visitId, Long visitServiceId, Integer deliveredServiceId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/services/" + visitServiceId + "/deliveredServices/" + deliveredServiceId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .post(UserStatus.class);
        return result;
    }

    public UserStatus removeDeliveredService(Integer branchId, Long visitId, Long visitDeliveredServiceId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/deliveredServices/" + visitDeliveredServiceId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .delete(UserStatus.class);
        return result;
    }

    public UserStatus setOutcomeForService(Integer branchId, Long visitId, Long visitServiceId, Integer outcomeCode) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/services/" + visitServiceId + "/outcome/" + outcomeCode)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public TinyQueue getQueue(Integer branchId, Integer queueId) {
        TinyQueue result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/queues/" + queueId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<TinyQueue>() {
                });
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.Outcome> getOutcomesForDeliveredService(Integer branchId, Integer serviceId, Integer deliveredServiceId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.Outcome> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/services/" + serviceId + "/deliveredServices/" + deliveredServiceId + "/outcomes")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.Outcome>>() {
                });
        return result;
    }

    public void transferVisitToServicePointPool(Integer branchId, Integer servicePointId, TransferParams entity) {
        c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/visits")
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .entity(entity, MediaType.APPLICATION_JSON_TYPE)
                .put();
    }

    public UserStatus callVisitFromServicePointPool(Integer branchId, Integer servicePointId, Long visitId) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/servicePoints/" + servicePointId + "/pool/" + visitId)
                .accept(MediaType.APPLICATION_JSON_TYPE).cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public UserStatus setOutcomeForDeliveredService(Integer branchId, Long visitId, Long visitDeliveredServiceId, Integer outcomeCode) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/deliveredServices/" + visitDeliveredServiceId + "/outcome/" + outcomeCode)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .cookie(getSSOCookie())
                .put(UserStatus.class);
        return result;
    }

    public java.util.List<com.qmatic.qp.api.connectors.dto.Service> getServices(Integer branchId) {
        java.util.List<com.qmatic.qp.api.connectors.dto.Service> result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/services")
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .cookie(getSSOCookie())
                .get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.Service>>() {
                });
        return result;
    }

    public Service getService(Integer branchId, Integer serviceId) {
        Service result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/services/" + serviceId)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .cookie(getSSOCookie())
                .get(new GenericType<Service>() {
                });
        return result;
    }

    public UserStatus sortUnservedService(Integer branchId, Long visitId, UnservedServices unservedServices) {
        UserStatus result = c.resource(getRestPath() + "/servicepoint/branches/" + branchId + "/visits/" + visitId + "/services")
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .cookie(getSSOCookie())
                .entity(unservedServices, MediaType.APPLICATION_JSON_TYPE)
                .put(UserStatus.class);
        return result;
    }

}

