package com.qmatic.ps.bp.rest.dto;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-02-04
 * Time: 09:24
 * To change this template use File | Settings | File Templates.
 */
public class QNotification {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
