package com.qmatic.ps.bp.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 08:38
 * To change this template use File | Settings | File Templates.
 */
public class QResponse implements Serializable {

    private QMetadata meta;
    private Set<QNotification> notifications = new HashSet<>();
    private Serializable payload;

    public QMetadata getMeta() {
        return meta;
    }

    public void setMeta(QMetadata meta) {
        this.meta = meta;
    }

    public Set<QNotification> getNotifications() {
        return notifications;
    }

    public void setNotifications(Set<QNotification> notifications) {
        this.notifications = notifications;
    }

    public Serializable getPayload() {
        return payload;
    }

    public void setPayload(Serializable payload) {
        this.payload = payload;
    }
}
