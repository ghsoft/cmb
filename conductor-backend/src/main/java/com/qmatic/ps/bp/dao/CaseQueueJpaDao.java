package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.CaseQueue;
import com.qmatic.ps.bp.model.GeneralParams;
import com.qmatic.ps.bp.model.enumeration.Status;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class CaseQueueJpaDao extends JpaDao<CaseQueue> implements CaseQueueDao {

	@Override
    public List<CaseQueue> getQueuesInfo(Integer branchId) throws BPException {
		String queryString = "";
		queryString += "SELECT	id";
		queryString += "	,	name";
		queryString += "	,	MAX(servicesWaiting) AS servicesWaiting";
		queryString += "	,	MAX(waitingTime) AS waitingTime";
		queryString += "	,	MAX(servicesServed) AS servicesServed";
		queryString += " FROM	(	";
		queryString += "	SELECT	service_id AS id";
		queryString += "		,	service_name AS name";
		queryString += "		,	ISNULL(COUNT(id), 0) AS servicesWaiting";
		queryString += "		,	ISNULL(MAX(CASE WHEN cs.status = :recycledStatus THEN DATEDIFF(ss, cs.updated, GETDATE()) ELSE DATEDIFF(ss, cs.created, GETDATE())END), 0) AS waitingTime";
		queryString += "		,	0 AS servicesServed";
		queryString += "	FROM	case_services cs";
		queryString += "	INNER JOIN (";
		queryString += "		SELECT	parent_case_id AS case_id,";
		queryString += "				COUNT(DISTINCT CASE WHEN ";
		queryString += "							cs.status = :servingStatus";
		queryString += "						OR	cs.status = :trashStatus";
		queryString += "						OR	cs.status = :noshowStatus";
		queryString += "						OR	cs.status = :onholdStatus";
		queryString += "						OR	(cs.status = :recycledStatus AND DATEDIFF(ss, cs.updated, GETDATE()) <= :recycleTime) ";
		queryString += "					THEN parent_case_id ELSE NULL END) AS not_waiting";
		queryString += "		FROM case_services cs";
		queryString += "		INNER JOIN cases c ON cs.parent_case_id = c.id";
		queryString += "		WHERE	c.created > :startOfToday";
		queryString += "		AND		branch_id = :branchId";
		queryString += "		GROUP BY branch_id, parent_case_id";
		queryString += "	) ws ON cs.parent_case_id = ws.case_id AND ws.not_waiting = 0";
		queryString += "	WHERE	cs.created > :startOfToday";
		queryString += "	AND		(cs.status = :waitingStatus OR cs.status = :pendingStatus OR (cs.status = :recycledStatus AND DATEDIFF(ss, cs.updated, GETDATE()) > :recycleTime))";
		queryString += "	GROUP BY service_id, service_name";
		queryString += " UNION ALL";
		queryString += "	SELECT	service_id AS id";
		queryString += "		,	service_name AS name";
		queryString += "		,	0 AS servicesWaiting";
		queryString += "		,	0 AS waitingTime";
		queryString += "		,	ISNULL(COUNT(id), 0) AS servicesServed";
		queryString += "	FROM	case_services cs";
		queryString += "	INNER JOIN (";
		queryString += "		SELECT	id AS case_id,";
		queryString += "				branch_id";
		queryString += "		FROM	cases c";
		queryString += "		WHERE	c.created > :startOfToday";
		queryString += "		AND		branch_id = :branchId";
		queryString += "	) c ON cs.parent_case_id = c.case_id";
		queryString += "	WHERE	cs.created > :startOfToday";
		queryString += "	AND		status = :finishedStatus";
		queryString += "	GROUP BY service_id, service_name";
		queryString += " UNION ALL";
		queryString += "	SELECT	service_id AS id";
		queryString += "		,	service_name AS name";
		queryString += "		,	ISNULL(COUNT(id), 0) AS servicesWaiting";
		queryString += "		,	ISNULL(MAX(DATEDIFF(ss, cs.created, GETDATE())), 0) AS waitingTime";
		queryString += "		,	0 AS servicesServed";
		queryString += "	FROM	case_services cs";
		queryString += "	INNER JOIN (";
		queryString += "		SELECT	id AS case_id,";
		queryString += "				branch_id";
		queryString += "		FROM	cases c";
		queryString += "		WHERE	c.created > :startOfToday";
		queryString += "		AND		branch_id = :branchId";
		queryString += "	) c ON cs.parent_case_id = c.case_id";
		queryString += "	WHERE	cs.created > :startOfToday";
		queryString += "	AND		(";
		queryString += "				(status = :trashStatus AND service_name = :trashQueue)";
		queryString += "			OR	(status = :onholdStatus AND service_name = :onholdQueue)";
		queryString += "			OR	(status = :noshowStatus AND service_name = :noshowQueue)";
		queryString += "			)";
		queryString += "	GROUP BY service_id, service_name";
		queryString += ") data";
		queryString += " GROUP BY id, name";
		
		Query query = entityManager.createNativeQuery(queryString);
		
		LocalDateTime ldt = LocalDateTime.now();
        ldt = ldt.withMillisOfDay(0);
		query.setParameter("branchId", branchId);
		query.setParameter("startOfToday", ldt.toDate());
		query.setParameter("finishedStatus", Status.FINISHED.toString());
		query.setParameter("trashStatus", Status.TRASH.toString());
		query.setParameter("noshowStatus", Status.NOSHOW.toString());
		query.setParameter("onholdStatus", Status.ONHOLD.toString());
		query.setParameter("servingStatus", Status.SERVING.toString());
		query.setParameter("waitingStatus", Status.WAITING.toString());
		query.setParameter("pendingStatus", Status.PENDING.toString());
		query.setParameter("recycledStatus", Status.RECYCLED.toString());
		query.setParameter("trashQueue", GeneralParams.TRASH_QUEUE);
		query.setParameter("noshowQueue", GeneralParams.NOSHOW_QUEUE);
		query.setParameter("onholdQueue", GeneralParams.ONHOLD_QUEUE);
		query.setParameter("recycleTime", GeneralParams.SYSTEM_RECYCLE_DELAY);
		
		List<Object[]> dbQueues = query.getResultList();
		List<CaseQueue> queues = new ArrayList<CaseQueue>();
		
		for(Object[] dbQueue: dbQueues){
			queues.add(new CaseQueue(dbQueue));
		}
		
		return queues;
    }
	
}
