package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.model.Mark;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-06-14
 * Time: 11:06
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class MarkJpaDao extends JpaDao<Mark> implements MarkDao {

    @Override
    public Mark findMarkByDeliveredServiceId(Integer deliveredServiceId) throws BPException {
        try {
            return (Mark) entityManager.createQuery("SELECT m FROM Mark m WHERE m.qpDeliveredServiceId = :deliveredServiceId")
                    .setParameter("deliveredServiceId", deliveredServiceId)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
