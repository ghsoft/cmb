package com.qmatic.ps.bp.model;

/**
 * Created with IntelliJ IDEA.
 * User: Roger
 * Date: 2013-06-22
 * Time: 00:12
 * To change this template use File | Settings | File Templates.
 */
public class RemoteHost {

    String remoteHostName;
    String remoteIPAddress;

    public String getRemoteHostName() {
        return remoteHostName;
    }

    public void setRemoteHostName(String remoteHostName) {
        this.remoteHostName = remoteHostName;
    }

    public String getRemoteIPAddress() {
        return remoteIPAddress;
    }

    public void setRemoteIPAddress(String remoteIPAddress) {
        this.remoteIPAddress = remoteIPAddress;
    }

}
