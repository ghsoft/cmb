Conductor package:

Installation:
1. Install QP_Central_2.3.1.504_win64 (standard installation)
2. Run SQL scripts in folder conf/db
3. Modify shiro.ini with content of shiro.ini in conf/
4. Restart Orchestra
5. Modify commonMessages.properties with the content of commonMessages.properties in conf/lang
6. Copy install/deploy/image.war to Orchestra's Deploy folder and then deploy the image.war folder in GlassFish admin console
7. Set up datasource in GlassFish admin for qp_conductor (jdbc/conductorDS) database, similar to how qp_central is done
8. Deploy conductor-backend, conductor-workstation and conductor-user-docs.
9. Restart Orchestra
10. Add the following User Roles and add the similar named application to each of them: 
	- ConductorReceptionUser
	- ConductorCounterUser
	- ConductorOperationsPanelUser
	- ConductorCounterReceptionUser
	- ConductorCounterOperationsPanelUser
	- ConductorReceptionOperationsPanelUser
	- ConductorAllInOneUser
	- ConductorDocumentsUser
	- ConductorPresetNotesAdmin
	- ConductorCustomersAdmin
11. Add a new user with username: "conductor" and password "ulan", add this user to "CounterUser" role to all branches.

Configuration:
1. Add/Change branch description to be 'conductor' without quotes.
2. Add a Service/Queue named (if not added) 'SILENTCALL' (CaSe SeNsEtIvE). Don't include this in any display/voice announcement, nor in any calling profile, this is basically used internally in the customization
3. Add Services/Queues named (if not added) (rename 'HOLD to 'ONHOLD'), 'TRASH', 'NOSHOW', 'NEVERCALL' (CaSe SeNsEtIvE). Include those in ALL displays/voice announcements.
4. Add the following Delivered Services (CaSe SeNsEtIvE) to all services:
	- [SERVICE_NAME] a delivered service with the same name as its parent service. Add to ALL services except (NEVERCALL, SILENTCALL, NOSHOW, ONHOLD).
	- 'ONHOLD'
	- 'NOSHOW'
	- 'Queue Correction'
	- 'Visit Removed'
	- 'Service Overridden'
5. Read README.txt file inside OAS_installation\app\glassfish3\glassfish\domains\domain1\applications\conductor-user-docs.war to add/change the documents used in User Documentation module!

Reports:
1. Deploy Score Reports
2. Overwrite the files in "Score Files" to "\pentaho-solutions\system\reporting\reportviewer"