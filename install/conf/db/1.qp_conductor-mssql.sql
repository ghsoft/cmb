-- Case database for San Diego LUEG
IF DB_ID (N'qp_conductor') IS NOT NULL
DROP DATABASE qp_conductor;

IF (SELECT name FROM sys.sql_logins WHERE name = 'qp_conductor') IS NOT NULL
BEGIN
  drop login qp_conductor;
END
GO
IF NOT EXISTS(SELECT name FROM sys.sql_logins WHERE name = 'qp_conductor')
BEGIN
  CREATE LOGIN qp_conductor WITH PASSWORD = 'qp_conductor', CHECK_POLICY = OFF;
END
GO

USE master;
GO

IF DB_ID (N'qp_conductor') IS NOT NULL
DROP DATABASE qp_conductor;
GO

CREATE DATABASE qp_conductor collate latin1_general_cs_as;
GO

USE [qp_conductor];
GO

EXEC sp_adduser 'qp_conductor', 'qp_conductor', 'db_owner';
GO

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

SET ANSI_PADDING ON;
GO

ALTER DATABASE qp_conductor MODIFY FILE (NAME=qp_conductor, SIZE = 100MB )
GO

ALTER DATABASE qp_conductor MODIFY FILE (NAME=qp_conductor, FILEGROWTH = 10% )
GO

ALTER USER qp_conductor WITH DEFAULT_SCHEMA = qp_conductor;
GO

ALTER DATABASE qp_conductor SET READ_COMMITTED_SNAPSHOT ON WITH ROLLBACK IMMEDIATE;
GO