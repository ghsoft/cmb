use qp_central;

INSERT INTO qp_central.applications 
VALUES ('conductor-c','False','False',1,'notinuse','conductor-workstation?modules=c',1, 10);

INSERT INTO qp_central.application_modules 
VALUES ('conductor-c',0,1,null,20,null,110,'conductor-c');

INSERT INTO qp_central.applications 
VALUES ('conductor-r','False','False',1,'notinuse','conductor-workstation?modules=r',1, 11);

INSERT INTO qp_central.application_modules 
VALUES ('conductor-r',0,1,null,20,null,111,'conductor-r');

INSERT INTO qp_central.applications 
VALUES ('conductor-p','False','False',1,'notinuse','conductor-workstation?modules=p',1, 12);

INSERT INTO qp_central.application_modules 
VALUES ('conductor-p',0,1,null,20,null,112,'conductor-p');

INSERT INTO qp_central.applications 
VALUES ('conductor-cr','False','False',1,'notinuse','conductor-workstation?modules=cr',1, 13);

INSERT INTO qp_central.application_modules 
VALUES ('conductor-cr',0,1,null,20,null,113,'conductor-cr');

INSERT INTO qp_central.applications 
VALUES ('conductor-cp','False','False',1,'notinuse','conductor-workstation?modules=cp',1, 14);

INSERT INTO qp_central.application_modules 
VALUES ('conductor-cp',0,1,null,20,null,114,'conductor-cp');

INSERT INTO qp_central.applications 
VALUES ('conductor-rp','False','False',1,'notinuse','conductor-workstation?modules=rp',1, 15);

INSERT INTO qp_central.application_modules 
VALUES ('conductor-rp',0,1,null,20,null,115,'conductor-rp');

INSERT INTO qp_central.applications 
VALUES ('conductor-crp','False','False',1,'notinuse','conductor-workstation?modules=crp',1, 16);

INSERT INTO qp_central.application_modules 
VALUES ('conductor-crp',0,1,null,20,null,116,'conductor-crp');

INSERT INTO qp_central.applications 
VALUES ('conductor-presetnotes','False','False',1,'notinuse','conductor-workstation/admin/presetnotes.jsp',1, 17);

INSERT INTO qp_central.application_modules 
VALUES ('conductor-presetnotes',0,1,null,20,null,117,'conductor-presetnotes');

INSERT INTO qp_central.applications 
VALUES ('conductor-customers','False','False',1,'notinuse','conductor-workstation/admin/customers.jsp',1, 18);

INSERT INTO qp_central.application_modules 
VALUES ('conductor-customers',0,1,null,20,null,118,'conductor-customers');

INSERT INTO qp_central.applications 
VALUES ('conductor-user-docs','False','False',1,'notinuse','conductor-user-docs',1, 19);

INSERT INTO qp_central.application_modules 
VALUES ('conductor-user-docs',0,1,null,20,null,119,'conductor-user-docs');

INSERT INTO qp_central.applications 
VALUES ('score','True','False',1,'images/icons/application.png','score',1, 20);

INSERT INTO qp_central.application_modules 
VALUES ('score',0,1,null,20,null,120,'score');