function goBack() {
	window.history.back();
}

function loadGUI() {
	var html = '<table>';
	var rowEnd = true;
	for (i = 0; i < documents.length; i++) {
		if (rowEnd) {
			rowEnd = false;
			html += '<tr>';
		}
		else {
			rowEnd = true;
		}
		html += '	<td>';
		html += '		<a href=' + documents[i].path + '>';
		html +=	'			<span style="background: url(images/userdocs.png) no-repeat;">';
		html += '				<span class="areatitle">' + documents[i].name + '</span>'
		html += 				documents[i].desc
		html += '			</span>';
		html += '		</a>';
		html += '	</td>';
		if (rowEnd) {
			html += '</tr>';
		}
	}
	if (!rowEnd) {
		html += '</tr>';
	}

	html += '</table>';

	document.getElementById("documents_table").innerHTML = html;
}
