package com.qmatic.ps.bp.rest.dto;

import java.io.Serializable;

/**
 * DTO for transferring some relevant visit data from the QP UserStatus object returned on callVisit etc.
 * to Conductor connectors.
 */
public class QPVisitData implements Serializable {

    private Long visitId;
    private String ticketNumber;

    private Integer waitingTime;
    private Integer totalWaitingTime;
    private Integer timeSinceCalled;
    private Integer servingTime;

    public Long getVisitId() {
        return visitId;
    }

    public void setVisitId(Long visitId) {
        this.visitId = visitId;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public Integer getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(Integer waitingTime) {
        this.waitingTime = waitingTime;
    }

    public Integer getTotalWaitingTime() {
        return totalWaitingTime;
    }

    public void setTotalWaitingTime(Integer totalWaitingTime) {
        this.totalWaitingTime = totalWaitingTime;
    }

    public Integer getTimeSinceCalled() {
        return timeSinceCalled;
    }

    public void setTimeSinceCalled(Integer timeSinceCalled) {
        this.timeSinceCalled = timeSinceCalled;
    }

    public Integer getServingTime() {
        return servingTime;
    }

    public void setServingTime(Integer servingTime) {
        this.servingTime = servingTime;
    }
}
