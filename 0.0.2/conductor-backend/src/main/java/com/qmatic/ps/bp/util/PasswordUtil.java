package com.qmatic.ps.bp.util;

import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.authc.credential.PasswordService;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-02-28
 * Time: 12:48
 * To change this template use File | Settings | File Templates.
 */
@Component
public class PasswordUtil {

    private DefaultPasswordService passwordService;

    public PasswordService getPasswordService() {
        if (this.passwordService == null) {
            this.passwordService = new DefaultPasswordService();
        }
        return this.passwordService;
    }
}
