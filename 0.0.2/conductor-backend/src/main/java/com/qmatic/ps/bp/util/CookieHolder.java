package com.qmatic.ps.bp.util;

import javax.ws.rs.core.Cookie;

/**
 * Calls to our REST service that needs to call Orchestra over the REST clients must
 * put their SSO cookie (from @Context HttpHeaders) into this ThreadLocal.
 */
public class CookieHolder {

    private static final ThreadLocal<Cookie> cookieHolder = new ThreadLocal<>();

    public static void set(Cookie cookie) {
        cookieHolder.set(cookie);
    }

    public static Cookie get() {
        return cookieHolder.get();
    }

    public static void remove() {
        cookieHolder.remove();
    }
}
