package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.common.BPExceptionCode;
import com.qmatic.ps.bp.common.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import javax.persistence.metamodel.SingularAttribute;
import java.lang.reflect.ParameterizedType;
import java.util.*;

/**
 * Copyright 2012 Q-MATIC AB. All rights reserved
 * <p/>
 * Base abstract class for all entities in the QMatic Platform. It provides the standard implementation of the basic CRUD-methods for the Java
 * Persistence API towards RDBMS.
 *
 * @author fregus
 * @created 2012-01-23
 */
@Repository
public abstract class JpaDao<E> implements Dao<E> {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @PersistenceContext(unitName = "conductorPU")
    protected EntityManager entityManager;

    protected Class entityClass;
    protected String getAllQuery;
    protected String getByPublicIdQuery;
    protected String getByQpIdQuery;
    protected String deleteQuery;


    public JpaDao() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class) genericSuperclass.getActualTypeArguments()[0];
        this.getAllQuery = new StringBuilder("select e from ").append(this.entityClass.getSimpleName()).append(" e").toString();
        this.deleteQuery = new StringBuilder("delete ").append(this.entityClass.getSimpleName()).append(" e where e.id = :id").toString();
        this.getByPublicIdQuery = new StringBuilder("select e from ").append(this.entityClass.getSimpleName()).append(" e WHERE e.publicId = :publicId").toString();
        this.getByQpIdQuery = new StringBuilder("select e from ").append(this.entityClass.getSimpleName()).append(" e WHERE e.qpId = :qpId").toString();
    }

    /**
     * @Inheritdoc
     */
    @Override

    public <E> E create(E entity) throws BPException {
        try {
            entityManager.persist(entity);
            entityManager.flush();
        } catch (RuntimeException rte) {
            if (log.isTraceEnabled()) log.trace("Error creating entity: ", rte);
            throw new BPException(rte.getMessage(), BPExceptionCode.CREATE_ERROR);
        }
        return entity;
    }

    /**
     * @Inheritdoc
     */
    @Override

    public <E> E update(E entity) throws BPException {
        try {
            entity = entityManager.merge(entity);
            entityManager.flush();
        } catch (RuntimeException rte) {
            rte.printStackTrace();
            if (log.isTraceEnabled()) log.trace("Error updating entity: ", rte);
            throw new BPException(rte.getMessage(), BPExceptionCode.UPDATE_ERROR);
        }
        return entity;
    }

    /**
     * @Inheritdoc
     */
    @Override

    public void delete(E entity) throws BPException {
        try {
            entityManager.remove(entity);
            entityManager.flush();
        } catch (RuntimeException rte) {
            if (log.isTraceEnabled()) log.trace("Error deleting entity: ", rte);
            throw new BPException(rte.getMessage(), BPExceptionCode.DELETE_ERROR);
        }
    }

    /**
     * @Inheritdoc
     */
    @Override

    public void deleteById(Object id) throws BPException {
        try {
            //entityManager.createQuery(this.deleteQuery).setParameter("id", id).executeUpdate();
            entityManager.remove(entityManager.find(entityClass, id));
            entityManager.flush();
        } catch (RuntimeException rte) {
            if (log.isTraceEnabled()) log.trace("Error deleting by id entity: ", rte);
            throw new BPException(rte.getMessage(), BPExceptionCode.DELETE_ERROR);
        }
    }

    /**
     * @Inheritdoc
     */
    @Override
    public <E> E findById(Object id) throws BPException {

        try {
            return (E) entityManager.find(this.entityClass, id);
        } catch (RuntimeException rte) {
            if (log.isTraceEnabled()) log.trace("Error finding entity by id: ", rte);
            throw new BPException(rte.getMessage(), BPExceptionCode.READ_ERROR);
        }
    }

    protected <E> E findByPublicId(String publicId) throws BPException {

        try {
            // TODO perhaps do as Criteria Query instead
            return (E) entityManager.createQuery(this.getByPublicIdQuery).setParameter("publicId", publicId).getSingleResult();
        } catch (RuntimeException rte) {
            if (log.isTraceEnabled()) log.trace("Error finding entity by publicId: ", rte);
            throw new BPException(entityClass.getSimpleName() + " not found", BPExceptionCode.NOT_FOUND_PUBLIC_ID);
        }
    }

    protected <E> E findByQpId(Integer qpId) throws BPException {
        try {
            // TODO perhaps do as Criteria Query instead
            return (E) entityManager.createQuery(this.getByQpIdQuery).setParameter("qpId", qpId).getSingleResult();
        } catch (RuntimeException rte) {
            if (log.isTraceEnabled()) log.trace("Error finding entity by qpId: ", rte);
            throw new BPException(entityClass.getSimpleName() + " not found by qpId", BPExceptionCode.NOT_FOUND_QP_ID);
        }
    }

    /**
     * @Inheritdoc
     */
    @Override
    public List<E> getAll() throws BPException {
        return entityManager.createQuery(this.getAllQuery).getResultList();
    }

    /**
     * @Inheritdoc
     */
    @Override
    public List<E> getAll(java.util.Map<String, java.util.List<String>> params) throws BPException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> query = cb.createQuery(entityClass);

        Root<E> root = query.from(entityClass);
        applyDateFilter(params, query, root, cb);
        applyRelationFilters(params, query, root, cb);

        TypedQuery<E> q = entityManager.createQuery(query);
        applyPaging(params, q);
        return q.getResultList();
    }

    /**
     * Adds "relation" filters, e.g:   the 'relation.id' below
     *
     * (single)
     * SELECT a FROM Note a WHERE a.relation.id = :id
     *
     * or
     *
     * (multi)
     * SELECT a FROM Note a WHERE a.relation.id IN (:idList)
     *
     * @param params
     * @param query
     * @param root
     * @param cb
     * @throw BPException
     */
    protected void applyRelationFilters(Map<String, List<String>> params, CriteriaQuery<E> query, Root<E> root, CriteriaBuilder cb) throws BPException {
        for(String key : params.keySet()) {
            switch(key) {
                case Constants.PRM_START:
                case Constants.PRM_END:
                case Constants.PRM_FIELDS:
                case Constants.PRM_OFFSET:
                case Constants.PRM_LIMIT:
                    break;
                default:
                    // For all other stuff, add to query
                    String values = params.get(key).get(0);
                    if (values != null && values.indexOf(",") > 0) {
                        applyMultiValueFiltering(query, root, key, values);
                    } else {
                        applySingleValueFiltering(query, root, cb, key, values);
                    }
                    break;
            }
        }
    }

    private void applySingleValueFiltering(CriteriaQuery<E> query, Root<E> root, CriteriaBuilder cb, String key, String values) throws BPException {
        try {
            Expression<Long> ex = root.get(key).get("id");
            query.where(cb.equal(ex, values));
        } catch (IllegalArgumentException e) {
            throw new BPException("Cannot query " + entityClass.getSimpleName() + " on field [" + key + "], no such field. Available fields are: " + getAllFields(), BPExceptionCode.NO_SUCH_FIELD);
        }
    }

    private void applyMultiValueFiltering(CriteriaQuery<E> query, Root<E> root, String key, String values) throws BPException {
        String[] args = values.split(",");
        Collection<Long> ids = new HashSet<>();
        for(String arg : args) {
            ids.add(Long.parseLong(arg));
        }
        try {
            Expression<Long> ex = root.get(key).get("id");
            query.where(ex.in(ids));
        } catch (IllegalArgumentException e) {
            throw new BPException("Cannot query " + entityClass.getSimpleName() + " on field [" + key + "], no such field. Available fields are: " + getAllFields(), BPExceptionCode.NO_SUCH_FIELD);
        }
    }

    protected void applyDateFilter(Map<String, List<String>> params, CriteriaQuery<E> query, Root<E> root, CriteriaBuilder cb) throws BPException {

        boolean hasStart = params.containsKey(Constants.PRM_START);
        boolean hasEnd = params.containsKey(Constants.PRM_END);

        try {
            if(hasStart && !hasEnd) {
                applyDateFilterStartDateOnly(params.get(Constants.PRM_START).get(0), query, root, cb);
            }
            if(!hasStart && hasEnd) {
                applyDateFilterEndDateOnly(params.get(Constants.PRM_END).get(0), query, root, cb);
            }
            if(hasStart && hasEnd) {
                applyDateFilterStartAndEnd(params.get(Constants.PRM_START).get(0),params.get(Constants.PRM_END).get(0), query, root, cb);
            }
        } catch (IllegalArgumentException e) {
            throw new BPException("Could query " + entityClass.getSimpleName() + " on field [start] or [end], no such field", BPExceptionCode.NO_SUCH_FIELD);
        }

    }

    private void applyDateFilterStartDateOnly(String start, CriteriaQuery<E> query, Root<E> root, CriteriaBuilder cb) {
        Expression<String> startExpr = root.get(Constants.PRM_START);
        query.where(cb.greaterThanOrEqualTo(startExpr, start));
    }

    private void applyDateFilterEndDateOnly(String end, CriteriaQuery<E> query, Root<E> root, CriteriaBuilder cb) {
        Expression<String> endExpr = root.get(Constants.PRM_END);
        query.where(cb.lessThanOrEqualTo(endExpr, end));
    }

    /**
     * uses this predicate: (a.start <= :end AND a.end >= :start)
     */
    private void applyDateFilterStartAndEnd(String start, String end, CriteriaQuery<E> query, Root<E> root, CriteriaBuilder cb) {
        Expression<String> startExpr = root.get(Constants.PRM_START);
        Expression<String> endExpr = root.get(Constants.PRM_END);
        Predicate dateOverlappingPredicate = cb.and(cb.lessThanOrEqualTo(startExpr, end), cb.greaterThanOrEqualTo(endExpr, start));

        query.where(dateOverlappingPredicate);
    }

    protected void applyPaging(Map<String, List<String>> params, Query q) {
        if (params.containsKey(Constants.PRM_LIMIT)) {
            int offset = params.containsKey(Constants.PRM_OFFSET) ? Integer.parseInt(params.get(Constants.PRM_OFFSET).get(0)) : 0;
            int limit = params.containsKey(Constants.PRM_LIMIT) ? Integer.parseInt(params.get(Constants.PRM_LIMIT).get(0)) : 0;
            q.setFirstResult(offset);
            q.setMaxResults(limit);
        }
    }


    protected EntityManager getEntityManager() {
        return entityManager;
    }


    public String getAllFields() {
        StringBuilder buf = new StringBuilder();
        Metamodel m = entityManager.getMetamodel();
        EntityType et = m.entity(this.entityClass);
        Set attributes = et.getDeclaredSingularAttributes();
        for(Object o : attributes) {
            SingularAttribute sa = (SingularAttribute) o;
            if(sa.getType() instanceof EntityType) {
                buf.append(sa.getName()).append(",");
            }
        }
        if(buf.length() > 0) {
            buf.setLength(buf.length() - 1);
        }

        return buf.toString();
    }


    public List<E> search(Map<String, List<String>> params) throws BPException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> query = cb.createQuery(entityClass);

        Root<E> root = query.from(entityClass);

        applyFieldFilter(params, query, root, cb);

        TypedQuery<E> q = entityManager.createQuery(query);
        applyPaging(params, q);
        return q.getResultList();
    }

    private void applyFieldFilter(Map<String, List<String>> params, CriteriaQuery<E> query, Root<E> root, CriteriaBuilder cb) {
         Set<Predicate> predicates = new HashSet<>();
        for(String key : params.keySet()) {
            switch(key) {
                // fields, offset and limit are still handled by other mechanisms and are considered "non-searchable".
                // So a hint from the coach may be to not name any entity fields as these fellas.
                case Constants.PRM_FIELDS:
                case Constants.PRM_OFFSET:
                case Constants.PRM_LIMIT:
                    break;
                default:
                    // For all other stuff, add equals Predicate to predicates list
                    String value = params.get(key).get(0);
                    Expression expr = root.get(key);
                    if (value != null && value.indexOf(',') == -1) {
                        predicates.add(cb.equal(expr, value));
                    } else {
                        if(value == null) {
                            throw new IllegalArgumentException("Cannot apply field filter, missing value");
                        }
                        String[] args = value.split(",");
                        Collection<String> ids = new HashSet<>();
                        for(String arg : args) {
                            ids.add(arg);
                        }
                        Predicate in = expr.in(ids);
                        predicates.add(in);
                    }
                    break;
            }
        }
        // Finally, if we had any predicates added, apply those to the query
        if(predicates.size() > 0) {
            query.where(cb.and(predicates.toArray(new Predicate[]{})));
        }
    }
}
