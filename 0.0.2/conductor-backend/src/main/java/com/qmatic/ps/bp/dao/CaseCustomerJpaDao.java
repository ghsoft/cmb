package com.qmatic.ps.bp.dao;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.common.BPExceptionCode;
import com.qmatic.ps.bp.model.CaseCustomer;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.sql.Timestamp;
import java.util.List;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class CaseCustomerJpaDao extends JpaDao<CaseCustomer> implements CaseCustomerDao {

    @Override
    public List<CaseCustomer> search(CaseCustomer searchCustomer, String andor, Integer limit) throws BPException {
		Integer branchId = 0;
		if (searchCustomer.getBranchId() != null) {
			branchId = searchCustomer.getBranchId();
		}
		String queryString = "SELECT c FROM CaseCustomer c ";
		queryString += " WHERE " + (branchId > 0 ? "(c.branchId = " + branchId + ")" : "(1=1)");
		
		String whereString = " AND (";
		try {
			for (java.lang.reflect.Field field : searchCustomer.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				if (field.get(searchCustomer) != null) {
					if (!field.getName().equals("branchId")) {
						if (!whereString.equals(" AND (")) {
							whereString += (andor.toLowerCase().equals("or") ? "OR" : "AND");
						}
						if (field.getType() == Long.class) {
							if ((long)field.get(searchCustomer) > 0) {
								whereString += " c." + field.getName() + " = '" + field.get(searchCustomer) + "' ";
							}
						}
						else if (field.getType() == Date.class) {
							DateFormat formatter = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy");
							SimpleDateFormat simpleFormatter = new SimpleDateFormat("yyyy-MM-dd");
							Date date = formatter.parse(field.get(searchCustomer).toString());
							whereString += " c." + field.getName() + " >= '" + simpleFormatter.format(date) + "' ";
						}
						else {
							whereString += " LOWER(ISNULL(c." + field.getName() + ", '')) LIKE '" + field.get(searchCustomer) + "' ";
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Caught exception in searchCustomer: {}, msg: {}", e.getClass().getSimpleName(), e.getMessage());
		}
		/* if (searchCustomer.getId() != null) {
			whereString += "c.id = " + searchCustomer.getId() + " ";
		}
		if (searchCustomer.getFirstName() != null) {
			if (!whereString.equals(" AND (")) {
				whereString += (andor.toLowerCase().equals("or") ? "OR" : "AND");
			}
			whereString += " LOWER(ISNULL(c.firstName, '')) LIKE '" + searchCustomer.getFirstName() + "' ";
		}
		if (searchCustomer.getLastName() != null) {
			if (!whereString.equals(" AND (")) {
				whereString += (andor.toLowerCase().equals("or") ? "OR" : "AND");
			}
			whereString += " LOWER(ISNULL(c.lastName, '')) LIKE '" + searchCustomer.getLastName() + "' ";
		}
		if (searchCustomer.getEmail() != null) {
			if (!whereString.equals(" AND (")) {
				whereString += (andor.toLowerCase().equals("or") ? "OR" : "AND");
			}
			whereString += " LOWER(ISNULL(c.email, '')) LIKE '" + searchCustomer.getEmail() + "' ";
		}
		if (searchCustomer.getPhoneNumber() != null) {
			if (!whereString.equals(" AND (")) {
				whereString += (andor.toLowerCase().equals("or") ? "OR" : "AND");
			}
			whereString += " LOWER(ISNULL(c.phoneNumber, '')) LIKE '" + searchCustomer.getPhoneNumber() + "' ";
		}
		if (searchCustomer.getCreatedBy() != null) {
			if (!whereString.equals(" AND (")) {
				whereString += (andor.toLowerCase().equals("or") ? "OR" : "AND");
			}
			whereString += " LOWER(ISNULL(c.createdBy, '')) LIKE '" + searchCustomer.getCreatedBy() + "' ";
		}
		if (searchCustomer.getUpdatedBy() != null) {
			if (!whereString.equals(" AND (")) {
				whereString += (andor.toLowerCase().equals("or") ? "OR" : "AND");
			}
			whereString += " LOWER(ISNULL(c.updatedBy, '')) LIKE '" + searchCustomer.getUpdatedBy() + "' ";
		}
		if (searchCustomer.getCreated() != null) {
			if (!whereString.equals(" AND (")) {
				whereString += (andor.toLowerCase().equals("or") ? "OR" : "AND");
			}
			whereString += " c.created >= '" + searchCustomer.getCreated() + "' ";
		}
		if (searchCustomer.getUpdated() != null) {
			if (!whereString.equals(" AND (")) {
				whereString += (andor.toLowerCase().equals("or") ? "OR" : "AND");
			}
			whereString += " c.updated >= '" + searchCustomer.getUpdated() + "' ";
		} */
		whereString += ")";
		
		if (whereString.equals(" AND ()")) {
			whereString = "";
			limit = 100;
		}
		
		log.info("Customers Select Where: " + whereString);
		
		TypedQuery <CaseCustomer> query = entityManager.createQuery(queryString + whereString, CaseCustomer.class);
		if (limit > 0) {
			query.setMaxResults(limit);
		}
		return query.getResultList();
    }
	
	@Override
    public CaseCustomer searchById(Long customerId) throws BPException {
		List<CaseCustomer> customers = entityManager.createQuery("SELECT c FROM CaseCustomer c WHERE c.id = :id")
			.setParameter("id", customerId)
			.getResultList();
		if(customers.size() == 0) {
			return null;
		} else {
			return customers.get(0);
		}
    }
	
	@Override
    public boolean isCustomerExist(CaseCustomer searchCustomer) throws BPException {
        return search(searchCustomer, "AND", 1).size() == 1;
    }

}
