package com.qmatic.ps.bp.common;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-02-06
 * Time: 08:58
 * To change this template use File | Settings | File Templates.
 */
public interface BPExceptionCode {

    /** 100-199 range is used for generic errors */
    public static final int CREATE_ERROR = 100;
    public static final int UPDATE_ERROR = 110;
    public static final int DELETE_ERROR = 120;
    public static final int READ_ERROR = 130;

    public static final int NOT_FOUND = 140;
    public static final int NOT_FOUND_QP_ID = 141;
    public static final int NOT_FOUND_PUBLIC_ID = 142;
    public static final int NO_SUCH_FIELD = 150;

    public static final int SERIALIZATION_ERROR = 170;
    public static final int DESERIALIZATION_ERROR = 171;

    /** 400-499 are public workflow booking errors */
    public static final int BRANCH_NOT_FOUND = 410;
    public static final int SERVICE_NOT_FOUND = 420;
    public static final int INVALID_DATE_SELECTION = 430;
    public static final int INVALID_TIME_SELECTION = 440;
   // public static final int NO_AVAILABLE_TIMESLOT = 441;

    public static final int COULD_NOT_IDENTIFY_CUSTOMER = 450;
    public static final int MULTIPLE_CUSTOMERS_FOUND = 451;

    /** 500-599 range is business logic errors */

    public static final int INVALID_TIME_FORMAT = 510;
    public static final int SYNC_ERROR = 520;
    public static final int MISSING_FIELD = 530;

    public static final int CUSTOMER_MAPPING_CONFIGURATION_ERROR = 532;
    public static final int REQUIRED_OBJECT_NOT_FOUND = 540;

    public static final int MULTIPLE_OBJECTS_FOUND = 541;

    public static final int VISIT_ID_NOT_FOUND = 550;
    public static final int VISIT_CALL_FAILED = 551;

    /** 600-700 is when external comms fails */
    public static final int REST_CALL_FAILED = 600;
    public static final int ORCHESTRA_UNAVAILABLE = 610;

    /** 700-800 is when rest commands failed */
    public static final int QP_ADD_SERVICE_FAILED = 700;
    public static final int QP_REMOVE_SERVICE_FAILED = 710;

    public static final int INTERNAL_SERVER_ERROR = 999;

}
