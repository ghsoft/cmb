package com.qmatic.ps.bp.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-23
 * Time: 15:56
 * To change this template use File | Settings | File Templates.
 */
@Table(name = "marks")
@Entity
@Cacheable
@XmlRootElement
public class Mark {

    private Long id;
    private String name;
    private Integer code;
    private Integer qpDeliveredServiceId;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "conductor_seq_table")
    @TableGenerator(name = "conductor_seq_table", table = "id_gen", pkColumnName = "id_name", valueColumnName = "id_val", pkColumnValue = "marks", allocationSize = 1)

    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="code")
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * ID from Orchestra
     * @return
     */
    @Column(name="qp_delivered_service_id")
    public Integer getQpDeliveredServiceId() {
        return qpDeliveredServiceId;
    }

    public void setQpDeliveredServiceId(Integer qpDeliveredServiceId) {
        this.qpDeliveredServiceId = qpDeliveredServiceId;
    }
}
