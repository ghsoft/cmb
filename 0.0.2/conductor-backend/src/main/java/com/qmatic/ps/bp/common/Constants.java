package com.qmatic.ps.bp.common;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-02-06
 * Time: 10:36
 * To change this template use File | Settings | File Templates.
 */
public interface Constants {

    public static final String OBJ_META = "meta";
    public static final String OBJ_NOTIFICATIONS = "notifications";

    public static final String PRM_START = "start";
    public static final String PRM_END = "end";

    public static final String PRM_LIMIT = "limit";
    public static final String PRM_OFFSET = "offset";

    public static final String PRM_FIELDS = "fields";


    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_FIRST_NAME = "firstName";
    public static final String FIELD_LAST_NAME = "lastName";

    /** Customer object property fields */
    public static final String CUSTOMER_FIELD_ADDRESS1 = "address1";
    public static final String CUSTOMER_FIELD_ADDRESS2 = "address2";
    public static final String CUSTOMER_FIELD_POSTAL_CODE = "postalCode";
    public static final String CUSTOMER_FIELD_CITY = "city";
    public static final String CUSTOMER_FIELD_COUNTRY = "country";
    public static final String CUSTOMER_FIELD_EMAIL = "email";
    public static final String CUSTOMER_FIELD_PHONE = "phoneNumber";


     /** Some constant values used for Branch Parameters from Orchestra R5 */
    public static final String QP_ADDRESS1 = "label.address1";
    public static final String QP_ADDRESS2 = "label.address2";
    public static final String QP_ADDRESS3 = "label.address3";
    public static final String QP_POSTCODE = "label.postcode";
    public static final String QP_CITY = "label.city";
    public static final String QP_COUNTRY = "label.country";
    public static final String QP_LATITUDE = "label.latitude";
    public static final String QP_LONGITUDE = "label.longitude";

    /** Settings values */
    public static final String ORCHESTRA_URL_KEY = "url";
    public static final String ORCHESTRA_USERNAME_KEY = "username";
    public static final String ORCHESTRA_PASSWORD_KEY = "password";

      /** Settings groups */
    public static final String ORCHESTRA_SETTINGS_GROUP = "system_settings";
    public static final String CUSTOMER_MAPPING_SETTINGS_GROUP = "customer_mapping_settings";
    public static final String BOOKING_SETTINGS = "booking_settings";


}
