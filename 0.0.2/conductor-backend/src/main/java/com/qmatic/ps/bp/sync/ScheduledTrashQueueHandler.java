package com.qmatic.ps.bp.sync;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.dao.CaseDao;
import com.qmatic.ps.bp.dao.CaseServiceDao;
import com.qmatic.ps.bp.model.Case;
import com.qmatic.ps.bp.model.CaseService;
import com.qmatic.ps.bp.model.enumeration.Status;
import com.qmatic.ps.bp.model.GeneralParams;
import com.qmatic.ps.bp.rest.client.ServicePointServiceClient;
import com.qmatic.ps.bp.util.CookieHolder;
import com.qmatic.qp.api.connectors.dto.ServicePoint;
import com.qmatic.qp.api.connectors.dto.TinyQueue;
import com.qmatic.qp.api.connectors.dto.TinyBranch;
import com.qmatic.qp.api.connectors.dto.UserStatus;
import com.qmatic.qp.api.connectors.dto.WorkProfile;
import com.qmatic.qp.api.connectors.dto.DeliveredService;
import com.qmatic.qp.api.connectors.dto.TransferParams;
import com.qmatic.qp.api.connectors.dto.TransferSortPolicy;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.util.List;


@Component
public class ScheduledTrashQueueHandler {
    private static final Logger log = LoggerFactory.getLogger(ScheduledTrashQueueHandler.class);

    @Autowired
    CaseDao caseDao;

    @Autowired
    private CaseServiceDao caseServiceDao;

    @Autowired
    ServicePointServiceClient servicePointServiceClient;

    private static final String BATCH_USERNAME = "conductor";

    private static final String BATCH_PASSWORD = "ulan";

    private Client c;


    //@Scheduled(cron = "*/60 * * * * MON-FRI")
    @Scheduled(cron = "0 0 19 * * *")
	//@Scheduled(cron = "0 0 * * * *") //Every Hour
    //@Scheduled(cron = "0 */2 * * * *") //Rogers 2 min interval test
	
    public void doSomethingScheduled() {
        log.info("Cleaning of {} Queue - STARTED", GeneralParams.TRASH_QUEUE);

        try {
            List<TinyBranch> branchList = connectToBackend();
            for (TinyBranch tb : branchList) {
				Integer branchId = tb.getId();
                List<Case> caseList = caseDao.findCasesInTrashQueues(branchId);
                if (caseList.size() > 0) {
                    List<ServicePoint> servicePoints = servicePointServiceClient.getServicePointsByDeviceType(branchId, "SW_SERVICE_POINT");

                    Integer servicePointId = autoPickServicePointId(tb, servicePoints);
                    Integer workProfileId = autoPickWorkProfileId(tb);

                    servicePointServiceClient.startUserServicePointSession(branchId, servicePointId, BATCH_USERNAME);
                    servicePointServiceClient.setWorkProfile(branchId, BATCH_USERNAME, workProfileId);
                    for (Case c : caseList) {
                        callAndEndTrashVisit(servicePointId, c);
                    }
                    servicePointServiceClient.endUserServicePointSession(branchId, Integer.valueOf(1), BATCH_USERNAME);
                }
            }
            servicePointServiceClient.logout();
			log.info("Cleaning of {} Queue - ENDED", GeneralParams.TRASH_QUEUE);
			
        } catch (BPException e) {
			log.error("Cleaning of {} Queue - ERROR", GeneralParams.TRASH_QUEUE);
            log.error(e.getMessage());
        } catch (Exception e) {
			log.error("Cleaning of {} Queue - ERROR", GeneralParams.TRASH_QUEUE);
            log.error(e.getMessage());
        }

    }

    private Integer autoPickWorkProfileId(TinyBranch tb) {
        List<WorkProfile> workProfiles = servicePointServiceClient.getWorkProfiles(tb.getId());
        Integer workProfileId = ((WorkProfile) workProfiles.get(0)).getId();
        log.info("Auto-picked WorkProfile {} for batch processing on Branch {}", ((WorkProfile) workProfiles.get(0)).getName(), tb.getName());
        return workProfileId;
    }

    private Integer autoPickServicePointId(TinyBranch tb, List<ServicePoint> servicePoints) {
        Integer servicePointId = ((ServicePoint) servicePoints.get(servicePoints.size() - 1)).getId();
        log.info("Auto-picked service point {} for batch processing on Branch {}", ((ServicePoint) servicePoints.get(servicePoints.size() - 1)).getName(), tb.getName());
        return servicePointId;
    }
	
	private Integer getQueueIdFromName(Integer branchId, String queueName){
        List<TinyQueue> queues = servicePointServiceClient.getQueues(branchId);
        for(TinyQueue queue : queues) {
            if(queue.getName().equalsIgnoreCase(queueName)) {
                return queue.getId();
            }
        }
        return -1;
    }
	
	private Integer getDeliveredServiceIdFromName(Integer branchId, Integer ServiceId, String dsName) {
		List<DeliveredService> deliveredServices = servicePointServiceClient.getDeliveredServices(branchId, ServiceId);
        Integer deliveredServiceId=-1;
        for(DeliveredService ds : deliveredServices){
            if(ds.getName().equals(dsName)){
                return ds.getId();
            }
        }
        return -1;
	}

    private void callAndEndTrashVisit(Integer servicePointId, Case c) throws BPException {
        UserStatus userStatus = null;
        try{
			Integer branchId = c.getBranchId();
			// First, transfer the visit to the SILENTCALL queue
			TransferParams parameters = new TransferParams();
			parameters.setSortPolicy(TransferSortPolicy.SORTED);
			parameters.setFromBranchId(branchId);
			parameters.setVisitId(c.getQpVisitId());
			parameters.setFromId(servicePointId);
			Integer queueId = getQueueIdFromName(branchId, GeneralParams.SILENTCALL_QUEUE);
			// if queue is not found, they forgot to set one up
			if(queueId != -1){
				servicePointServiceClient.transferVisitToQueue(branchId, queueId, parameters);
			}else{
				log.error("Could not transfer to {} queue for Visit: {}", GeneralParams.SILENTCALL_QUEUE, c.getTicketNumber());
			}
			// Now Call the visit
            userStatus = servicePointServiceClient.callVisit(branchId, servicePointId, Integer.valueOf(c.getQpVisitId().intValue()));
			// Add TRASH Delivered Service
            if (userStatus.getVisit() != null) {
				Integer deliveredServiceId = getDeliveredServiceIdFromName(branchId, userStatus.getVisit().getCurrentVisitService().getServiceId(), GeneralParams.TRASH_DELIVERED_SERVICE);
				if (deliveredServiceId != -1) {
					userStatus = servicePointServiceClient.addDeliveredService(branchId, userStatus.getVisit().getId(), userStatus.getVisit().getCurrentVisitService().getId(), deliveredServiceId);
					servicePointServiceClient.endVisit(branchId, c.getQpVisitId());
				}
				else {
					log.error("Could not add Delivered Service to Visit: {}", c.getTicketNumber());
				}
            }else{
                log.error("Could not end Visit as UserStatus visit is null for ticket no {}", c.getTicketNumber());
            }
        }catch(Exception e){
            log.error("Could not call and end Visit {} in Orchestra, to end trash queue visit. Visit may have been removed from Orchestra", c.getTicketNumber());
			log.error(e.getMessage());
        }

        for (CaseService cs : c.getCaseServices()) {
            if (cs.getStatus() == Status.TRASH) {
                cs.setStatus(Status.FINISHED);
                caseServiceDao.update(cs);
            }
        }
        log.info("Successfully set the state to Finised for visit {}", c.getTicketNumber());
    }

    private List<TinyBranch> connectToBackend() throws BPException {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put("com.sun.jersey.api.json.POJOMappingFeature", Boolean.TRUE);
        c = Client.create(clientConfig);
        c.setFollowRedirects(Boolean.valueOf(true));
        c.setConnectTimeout(Integer.valueOf(5000));

        c.addFilter(new HTTPBasicAuthFilter(BATCH_USERNAME, BATCH_PASSWORD));

        WebResource resource = c.resource(getRestPath() + "/servicepoint/branches");
        List<TinyBranch> result = (List) resource.accept(MediaType.APPLICATION_JSON).get(new GenericType<java.util.List<com.qmatic.qp.api.connectors.dto.TinyBranch>>() {
        });

        parseSSOCookie(resource);

        return result;
    }

    private void parseSSOCookie(WebResource resource) throws BPException {
        ClientResponse head = resource.accept(MediaType.APPLICATION_JSON).head();
        MultivaluedMap<String, String> headers = head.getHeaders();
        List<String> strings = (List) headers.get("Set-Cookie");
        for (String header : strings) {
            int ssOcookieIndex = header.indexOf("SSOcookie");
            if (ssOcookieIndex > -1) {
                String cookieValue = header.substring(ssOcookieIndex + "SSOcookie".length() + 1, header.indexOf(";"));
                Cookie cookie = new Cookie("SSOcookie", cookieValue);
                CookieHolder.set(cookie);
                break;
            }
        }
    }


    private Cookie getSSOCookie() {
        return CookieHolder.get();
    }

    protected String getRestPath() {
        return getProtocol() + "://" + getIp() + ":" + getPort() + "/" + getBasePath();
    }

    protected String getProtocol() {
        return "http";
    }

    protected String getIp() {
        return "127.0.0.1";
    }

    protected String getPort() {
        return "8080";
    }

    protected String getBasePath() {
        return "rest";
    }
}