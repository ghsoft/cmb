package com.qmatic.ps.bp.model;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-06-14
 * Time: 13:31
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "case_service_marks")
public class CaseServiceMark {

    private Long id;
    private Long qpVisitDeliveredServiceId;
    private Mark mark;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "conductor_seq_table")
    @TableGenerator(name = "conductor_seq_table", table = "id_gen", pkColumnName = "id_name", valueColumnName = "id_val", pkColumnValue = "case_marks", allocationSize = 1)

    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="qp_visit_delivered_service_id")
    public Long getQpVisitDeliveredServiceId() {
        return qpVisitDeliveredServiceId;
    }

    public void setQpVisitDeliveredServiceId(Long qpVisitDeliveredServiceId) {
        this.qpVisitDeliveredServiceId = qpVisitDeliveredServiceId;
    }

    @ManyToOne
    public Mark getMark() {
        return mark;
    }

    public void setMark(Mark mark) {
        this.mark = mark;
    }
}
