package com.qmatic.ps.bp.rest.dto;

import com.qmatic.ps.bp.model.CaseNote;
import com.qmatic.ps.bp.model.Note;
import com.qmatic.qp.api.connectors.dto.TinyVisit;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-05-21
 * Time: 13:59
 * To change this template use File | Settings | File Templates.
 */
public class ExtendedVisitDataDTO extends TinyVisit {

    private CaseNote lastNote;
    private String customerId;
    private String customerName;
    private String serviceName;
    private String services;
    private Long qpVisitId;
    private String vip;

    private Integer numberOfUnservedServices;
    private Integer numberOfServices;

    public ExtendedVisitDataDTO() {}

    public ExtendedVisitDataDTO(TinyVisit visitData) {
        super.setVisitId(visitData.getVisitId());
        super.setTicketId(visitData.getTicketId());
        super.setWaitingTime(visitData.getWaitingTime());

    }

    public CaseNote getLastNote() {
        return lastNote;
    }

    public void setLastNote(CaseNote lastNote) {
        this.lastNote = lastNote;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
	
    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }
	
    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public void setQpVisitId(Long qpVisitId) {
        this.qpVisitId = qpVisitId;
    }

    public Long getQpVisitId() {
        return qpVisitId;
    }

    public Integer getNumberOfUnservedServices() {
        return numberOfUnservedServices;
    }

    public void setNumberOfUnservedServices(Integer numberOfUnservedServices) {
        this.numberOfUnservedServices = numberOfUnservedServices;
    }

    public Integer getNumberOfServices() {
        return numberOfServices;
    }

    public void setNumberOfServices(Integer numberOfServices) {
        this.numberOfServices = numberOfServices;
    }
}
