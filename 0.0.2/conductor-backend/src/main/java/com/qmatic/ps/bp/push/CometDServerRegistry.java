package com.qmatic.ps.bp.push;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * This Servlet-based Initializer gets hold of the CometD Server instance from the ServletContext and
 * then puts a reference to the CometD server into this singleton.
 */
@Component(value = "cometDServerRegistry")
@Scope(value = "singleton")
public class CometDServerRegistry {

    private CometDServer server;

    public void setCometDServer(CometDServer server) {
        this.server = server;
    }

    public CometDServer getCometDServer() {
        return server;
    }
}
