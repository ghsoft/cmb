package com.qmatic.ps.bp.util;

import com.qmatic.qp.constants.QPConstants;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-05-22
 * Time: 14:20
 * To change this template use File | Settings | File Templates.
 */
public class SecurityUtil {

    private static Logger log = LoggerFactory.getLogger(SecurityUtil.class);

    public static final String getCurrentUserName() {
        String username = QPConstants.ANONYMOUS_USER;
        try {
            Subject subject = SecurityUtils.getSubject();
            username = getUserName(subject);
        } catch (Exception e) {
            if (log.isDebugEnabled())
                log.debug("Anonymous user intercepted");
        }
        return username;
    }

    private static final String getUserName(final Subject subject) {
        Object principal = subject.getPrincipal();
        if (principal instanceof PrincipalCollection) {
            return ((PrincipalCollection) principal).oneByType(String.class);
        }
        if ( principal instanceof String)
            return (String) principal;

        return QPConstants.ANONYMOUS_USER;
    }
	
	public static final String getCurrentFullName() {
        String fullname = QPConstants.ANONYMOUS_USER;
        try {
			Subject subject = SecurityUtils.getSubject();
            String firstname = (String)subject.getSession().getAttribute(QPConstants.SESSION_PRM_FIRST_NAME);
			String lastname = (String)subject.getSession().getAttribute(QPConstants.SESSION_PRM_LAST_NAME);
			fullname = firstname + " " + lastname;
        } catch (Exception e) {
            if (log.isDebugEnabled())
                log.debug("Anonymous user intercepted");
        }
        return fullname;
    }
}
