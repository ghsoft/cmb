package com.qmatic.ps.bp.startup;

import com.qmatic.ps.bp.common.BPException;
import com.qmatic.ps.bp.util.PasswordUtil;
import com.qmatic.ps.bp.util.SettingsUtil;
import org.h2.tools.Console;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.SQLException;

/**
 * Performs some startup tasks:
 *
 * <li>Creates default placeholder settings for Orchestra R5 url, username and password</li>
 */
@Component
public class Bootstrap {

    private static final Logger log = LoggerFactory.getLogger(Bootstrap.class);

    @Autowired
    SettingsUtil settingsUtil;

    @Autowired
    PasswordUtil passwordUtil;

    @Autowired
    SettingsFactory settingsFactory;


    @PostConstruct
    public void init() throws BPException {
       // h2databaseConsole();
        seedInitData();
    }

    /** Starts the h2 database console if running h2 **/
    private void h2databaseConsole() {

        String h2args[] = { "-web", "-webPort", "11111" };
        try {
            new Console().runTool(h2args);
            log.info("Starting H2 database console on port {}", 11111);
        } catch (SQLException e) {
            log.error("Could not start H2 Console [{}]", e.getMessage());
        }
    }

    private void seedInitData() throws BPException {
        settingsFactory.createOrchestraSettings();
    }



}
