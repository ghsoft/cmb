<?xml version="1.0"?>
<web-app xmlns="http://java.sun.com/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
         version="3.0" metadata-complete="true">
    <display-name>San Diego Building and Planning REST interface</display-name>

    <!-- The definition of the Root Spring Container shared by all Servlets and Filters -->
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>/WEB-INF/applicationContext.xml</param-value>
    </context-param>

    <context-param>
        <param-name>log4jConfigLocation</param-name>
        <param-value>/WEB-INF/log4j.properties</param-value>
    </context-param>

    <context-param>
        <param-name>webAppRootKey</param-name>
        <param-value>conductor-backend</param-value>
    </context-param>

    <!-- Explicitly point to our own shiro ini file if we want security to be handled by the calendar-backend.
        If running alongside Orchestra R5, we can comment out the context-param below which will include this
        module in Orchestra's shiro context. Note that that will require some kind of qp-aaa-jar.jar on the classpath.

    <context-param>
        <param-name>shiroConfigLocations</param-name>
        <param-value>classpath:calendar-shiro.ini</param-value>
    </context-param>
      -->
    <listener>
        <listener-class>org.springframework.web.util.Log4jConfigListener</listener-class>
    </listener>

    <!-- Creates the Spring Container shared by all Servlets and Filters -->
    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>
    <listener>
        <listener-class>org.springframework.web.context.request.RequestContextListener</listener-class>
    </listener>



    <!-- Processes API application requests -->
    <servlet>
        <servlet-name>jersey</servlet-name>
        <servlet-class>com.sun.jersey.spi.spring.container.servlet.SpringServlet</servlet-class>
        <init-param>
            <param-name>com.sun.jersey.config.property.packages</param-name>
            <param-value>com.qmatic.ps.bp.rest.server;com.qmatic.ps.bp.rest.mapper;com.qmatic.ps.bp.util;com.fasterxml.jackson.jaxrs</param-value> <!-- org.codehaus.jackson.jaxrs -->
        </init-param>
        <init-param>
            <param-name>com.sun.jersey.api.json.POJOMappingFeature</param-name>
            <param-value>true</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>

    <servlet-mapping>
        <servlet-name>jersey</servlet-name>
        <url-pattern>/rest/*</url-pattern>
    </servlet-mapping>


     <!-- START COMETD SERVLET -->
    <servlet>
        <servlet-name>cometd</servlet-name>
        <servlet-class>org.cometd.annotation.AnnotationCometdServlet</servlet-class>
        <init-param>
            <param-name>transports</param-name>
            <param-value>org.cometd.websocket.server.WebSocketTransport</param-value>
        </init-param>
        <init-param>
            <param-name>services</param-name>
            <param-value>com.qmatic.ps.bp.push.CometDServer</param-value>
        </init-param>
        <init-param>
            <param-name>maxLazyTimeout</param-name>
            <param-value>5000</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
        <async-supported>true</async-supported>
    </servlet>
    <servlet-mapping>
        <servlet-name>cometd</servlet-name>
        <url-pattern>/cometd/*</url-pattern>
    </servlet-mapping>

    <servlet>
        <servlet-name>initializer</servlet-name>
        <servlet-class>com.qmatic.ps.bp.push.Initializer</servlet-class>
        <load-on-startup>2</load-on-startup>
        <async-supported>true</async-supported>
    </servlet>

    <filter>
        <filter-name>cross-origin</filter-name>
        <filter-class>org.eclipse.jetty.servlets.CrossOriginFilter</filter-class>
        <async-supported>true</async-supported>
    </filter>
    <filter-mapping>
        <filter-name>cross-origin</filter-name>
        <url-pattern>/cometd/*</url-pattern>
    </filter-mapping>




    <!-- Experimental, our very own js-api servlet! (currently unused) -->
    <!--
    <servlet>
        <servlet-name>qp-rest-jsapi-servlet</servlet-name>
        <servlet-class>com.qmatic.qp.common.restjs.QPJSAPIServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>

    <servlet-mapping>
        <servlet-name>qp-rest-jsapi-servlet</servlet-name>
        <url-pattern>/qp-rest-js</url-pattern>
    </servlet-mapping>

    <context-param>
        <param-name>qp.rest.servlet.mapping.prefix</param-name>
        <param-value>/rest</param-value>
    </context-param>

    <context-param>
        <param-name>qp.rest.endpoints</param-name>
        <param-value>com.qmatic.ps.bp.rest.server.CaseServicePointEndpoint</param-value>
    </context-param>
    -->

    <!-- See http://stackoverflow.com/questions/631695/why-am-i-getting-a-hibernate-lazyinitializationexception-in-this-spring-mvc-web
    This filter does away with those extremely irritating LazyInitExceptions when Jackson is serializing a POJO with sub-objects
    not fully loaded. -->
    <filter>
        <filter-name>entitymanager-filter</filter-name>
        <filter-class>org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>entitymanager-filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>




    <!-- Security setup listener  -->
    <listener>
        <listener-class>org.apache.shiro.web.env.EnvironmentLoaderListener</listener-class>
    </listener>

    <!-- Security filter  to use when running on standalone Tomcat -->
    <!--
    <filter>
        <filter-name>shiroFilter</filter-name>
        <filter-class>org.apache.shiro.web.servlet.ShiroFilter</filter-class>
        <async-supported>true</async-supported>
    </filter>
     -->

    <!-- Security filter to use if running on GlassFish and we want to integrate with the Qmatic Platform shiro context -->
    <filter>
        <filter-name>shiroFilter</filter-name>
        <filter-class>com.qmatic.qp.core.aaa.shiro.QPShiroFilter</filter-class>
        <async-supported>true</async-supported>
    </filter>


    <!-- Default security filter that maps ALL    -->
    <filter-mapping>
        <filter-name>shiroFilter</filter-name>
        <url-pattern>/*</url-pattern>
        <dispatcher>REQUEST</dispatcher>
        <dispatcher>FORWARD</dispatcher>
        <dispatcher>INCLUDE</dispatcher>
        <dispatcher>ERROR</dispatcher>
    </filter-mapping>

</web-app>
