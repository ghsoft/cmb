package com.qmatic.ps.bp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qmatic.ps.bp.model.Case;
import com.qmatic.ps.bp.model.CaseService;
import com.qmatic.ps.bp.model.Note;
import com.qmatic.ps.bp.model.enumeration.Status;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-02-21
 * Time: 10:47
 * To change this template use File | Settings | File Templates.
 */
public class ObjectFactory {

    private static ObjectMapper om = new ObjectMapper();

    public static Case newCase(String ... caseServiceNames) {
        Case c = new Case();
        c.setCustomerId("CustomerId1234");
        c.setCustomerName("John Doe");
        c.setPermitId("PermitId1234");
        c.setTicketNumber("1234");

        for(String caseServiceName : caseServiceNames) {
            CaseService cs = newCaseService(caseServiceName);
            c.getCaseServices().add(cs);
        }

        return c;
    }

    public static CaseService newCaseService(String caseServiceName) {
        CaseService cs = new CaseService();
        cs.setServiceName(caseServiceName);
        cs.setStatus(Status.PENDING);
        return cs;
    }

    public static Note newNote(Integer index) {
        Note r = new Note();
        r.setText("Note" + index);
        return r;
    }
}
