package com.qmatic.ps.bp.crud;

import com.jayway.restassured.response.Response;
import com.qmatic.ps.bp.BaseRestTest;
import com.qmatic.ps.bp.ObjectFactory;
import com.qmatic.ps.bp.model.Case;
import com.qmatic.ps.bp.model.CaseService;
import com.qmatic.ps.bp.model.Note;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.path.json.JsonPath.from;
import static org.hamcrest.Matchers.equalTo;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-04-24
 * Time: 10:49
 * To change this template use File | Settings | File Templates.
 */
@Test
public class CaseCrudTest extends BaseRestTest {

    private static Logger log = LoggerFactory.getLogger(CaseCrudTest.class);

    @Test
    public void createCaseWithCaseServicesAndNotes() {
        keySetList.clear();
        Long caseId = createCaseWithOneService();
        try {
            Case aCase = findCaseById(caseId);
            // Try to add a note now
            Note note = ObjectFactory.newNote(1);
            CaseService caseService = aCase.getCaseServices().get(0);

            Response resp = given().body(note).pathParam("caseServiceId",caseService.getId()).expect()
                    .body("text", equalTo("Note1"))
                    .when().post("/caseservice/{caseServiceId}/note");
            Note dbNote = resp.body().jsonPath().getObject("", Note.class);
            Assert.assertEquals(dbNote.getText(), "Note1");

            // Now, let's add an extra service! Whee!!
            CaseService permits = ObjectFactory.newCaseService("Permits");

            resp = given().body(permits).pathParam("caseId",caseId).expect()
                    .body("serviceName", equalTo("Permits"))
                    .when().post("/case/{caseId}/caseservice");

            aCase = findCaseById(caseId);
            Assert.assertEquals(aCase.getCaseServices().size(), 2);

        } catch(Exception e) {
            log.error(e.getMessage());
            Assert.assertTrue(false, "We should never get here!!");
        } finally {
            deleteCreatedEntities();
        }
    }




}
