<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Qmatic Conductor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <!-- TODO: make sure bootstrap.min.css points to BootTheme generated file
    -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/dataTable.css"/>
    <!-- TODO: make sure bootstrap-responsive.min.css points to BootTheme
    generated file http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/ -->
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/conductor.css"/>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
</head>

<body>
	<div class="navbar navbar-fixed-top">
	    <div class="navbar-inner">
	        <div class="container-fluid">
	            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </a>
	            <a class="brand" href="#">Qmatic Conductor</a>
				<a id="branch-name-lbl" class="info" href="#"></a>
	            <a id="servicepoint-name-lbl" class="info" href="#"></a>
	            <div class="nav-collapse collapse">
	                <p class="navbar-text pull-right">

	                    <img src="images/ajax-loader.gif" id="loading-indicator" style="display:none" />
	                    Logged in as
	                    <a id="userName" href="#" class="navbar-link">Username</a> |
	                    <a id="settings" href="#" class="navbar-link">Settings</a> |
	                    <a id="logOut" href="#" class="navbar-link">Logout</a> |
                        <a class="icon-home-black" href="/"></a>
                    </p>

	                <ul class="nav">
	                    <li class="active">
	                        <a id="counter-tab" href="#" class="">Counter</a>
	                    </li>
	                    <li>
	                        <a id="reception-tab" href="#" class="">Reception</a>
	                    </li>
						<li>
	                        <a id="opspanel-tab" href="#" class="">Ops Panel</a>
	                    </li>
	                    <!-- remove until implementation time
	                   <li>
	                       <a href="#appointments" class="">Appointments</a>
	
	                   </li>
	                   -->
	                </ul>
	            </div>
	        </div>
	    </div>
	</div>
	
	<div id="opspanel-summary-area" class="container-fluid hidden">
		
			<!-- Top panel -->
			<div>
				<div id="summary-container">
				    <jsp:include page="snippets/opspanel/branch-summary.html" />
				</div>
			</div>
		
	</div>
	
	<!-- COUNTER and RECEPTION START -->
	<div class="container-fluid">
		<div class="row-fluid">
			<!-- Left panel -->
			<div id="counter-reception-area" class="span3 main-panel">
				<!-- Left panel for Counter -->
				<div id="counter-container">
				    <jsp:include page="snippets/counter/action-bar.html" />
				    <jsp:include page="snippets/counter/current-service-transaction.html" />
				</div>
				<!-- Left panel for Reception -->
				<div id="reception-container" class="hidden">
		            <jsp:include page="snippets/reception/action-bar.html" />
		            <jsp:include page="snippets/reception/new-visit.html" />
				</div>
				<!-- Left panel for OpsPanel -->
				<div id="opspanel-container" class="hidden">
					<jsp:include page="snippets/opspanel/action-bar.html" />
		            <jsp:include page="snippets/opspanel/service-points.html" />
				</div>
			</div>
		
			<!-- Right panel common for Counter and Reception (except for Visit Pool) -->
			<div id="queue-appointment-area" class="span9 main-panel">
				<jsp:include page="snippets/common/search-bar.html" />
				<jsp:include page="snippets/common/customer-search-result.html" />
				<jsp:include page="snippets/common/queue-list.html" />
				<jsp:include page="snippets/common/queue-details.html" />
				<jsp:include page="snippets/common/visit-details.html" />
				
				<!-- Counter only -->
				<jsp:include page="snippets/counter/visits-in-your-pool.html" />
				
				<!-- Reception only -->
				<jsp:include page="snippets/reception/appointments-today.html" />
			</div>
		</div>
	</div>
	<!-- COUNTER and RECEPTION END -->

	<footer class="navbar-inner" id="footer-index"></footer>

	<!-- Modal START -->
	<jsp:include page="dialogs/settings.html" />
	<jsp:include page="dialogs/calling-ticket.html" />
	<jsp:include page="dialogs/change-profile.html" />
	<jsp:include page="dialogs/hijack.html" />
	<jsp:include page="dialogs/add-service.html" />
	<jsp:include page="dialogs/edit-note.html" />
	<jsp:include page="dialogs/delete-confirm.html" />
	<jsp:include page="dialogs/no-show.html" />
	<jsp:include page="dialogs/add-delivered-services.html" />
	<jsp:include page="dialogs/edit-customer-dialog.html" />
	<jsp:include page="dialogs/walk-direct.html" />
	<!-- Modal END -->

	<div id="message-main" class="message-main fade right in" style="width:400px; display: none;">
	    <div class="arrow"></div>
	    <div id="message-title-heading" class="message-title heading"><i id="message-dismiss-btn" class="icon-remove"></i><span id="message-title">Title here</span></div>
	    <div id="message-content" class="message-content">Text here</div>
	</div>
	
	<div id="message"></div>
	<div id="error"></div>

<!-- Le javascript================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery.countdown.min.js"></script>
<!-- http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/ -->
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/bootstrap/bootstrapx-clickover.js"></script>

<script src="js/json2.js"></script>

<script type="text/javascript" src="/events/scripts/org/cometd.js"></script>
<script type="text/javascript" src="/events/scripts/jquery/jquery.cometd.js"></script>
<script type="text/javascript" src="/events/qevents_cometd.js"></script>

<!-- Rest-js functions has been replaced
<!--<script src="/rest-js"></script> <!-- Load the ServicePointService REST Client script -->
<!-- <script src="qp-rest-js"></script>  Load the "Case" REST Client script -->

<script type="text/javascript" src="js/jquery/jquery.dataTables.js"></script>
<script src="js/restservices/CaseServicePointService.js"></script>
<script src="js/restservices/ServicePointService.js"></script>
<script src="js/restservices/OpsPanelService.js"></script>
<script src="js/qmatic.ajax.js"></script>
<script type="text/javascript" src="js/markupBuilder.js"></script>
<script src="js/panels.js"></script>
<script src="js/buttons.js"></script>
<script src="js/common.js"></script>
<script src="js/customer.js"></script>
<script src="js/notes.js"></script>
<script src="js/tables.js"></script>
<script src="js/search.js"></script>
<script src="js/deliveredservices.js"></script>
<script src="js/noshow.js"></script>
<script src="js/reception.js"></script>
<script src="js/workstation.js"></script>
<script src="js/opspanel.js"></script>
<script src="js/settings.js"></script>
<script src="js/events.js"></script>
<script src="js/init.js"></script>
<script src="js/moment/moment.min.js"></script>
<script type="text/javascript" src="js/restservices/CaseAppointmentService.js"></script>
<script type="text/javascript" src="js/appointments.js"></script>
<script type="text/javascript" src="js/restservices/PresetNotesService.js"></script>


<script src="js/walkdirect.js"></script>

<script src="js/util.js"></script>
<script src="js/sessvars.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        var modules = getParameterByName('modules');
        if(modules.indexOf('r') >= 0){
            reception.setActive(true);
        }
        if(modules.indexOf('c') >= 0){
            servicePoint.setActive(true);
        }
		if(modules.indexOf('p') >= 0){
            opspanel.setActive(true);
        }
        moment().format();
        init.init();
    });

    function getParameterByName(name){
        var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if(results==null){
            return "cr";
        }
        return results[1] || 0;
    }

    $(document).ajaxSend(function(event, request, settings) {
        $('#loading-indicator').show();
    });

    $(document).ajaxComplete(function(event, request, settings) {
        $('#loading-indicator').hide();
    });
</script>
</body>

</html>