/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 2013-05-06
 * Time: 12:22
 * To change this template use File | Settings | File Templates.
 */

var init = new function () {

    this.guiInitialized = false;

    var useAdvancedFailoverDetection = true;

    this.init = function () {
        util.setDetailedErrors(servicePoint.DETAILED_ERRORS);

        if(typeof sessvars.currentUser === "undefined" || null == sessvars.currentUser || sessvars.currentUser == "" ) {
            cServicePointService.getCurrentUser()
                .done(function(user){
                    sessvars.currentUser = user;
                })
                .fail(function(err){
                    util.showError("Could not get current User from server.", err);
                })
        }

        if(typeof sessvars.systemInformation == "undefined" || sessvars.systemInformation== "" || null == sessvars.systemInformation) {
            cServicePointService.getSystemInformation()
                .done(function(info){
                    sessvars.systemInformation = info;
                })
                .fail(function(err){
                    util.showError("Could not retrieve system information from server.", err);
                })
        }

        initSessvars();
		
		sessvars.keepSessionAlive = setInterval(function() {
			var random_value = Math.floor((Math.random() * 10000) + 1);
			$.get("css/rtl.css?" + random_value, function(data) {
				console.log('Session timer reset at: ' + new Date());
			});
		}, 20 * 60 * 1000);

        qevents.init(false, typeof useAdvancedFailoverDetection !== 'undefined' && useAdvancedFailoverDetection ? servicePoint.cometDWSPollStatusHandler : "");

        settings.initSettings();
        //servicePoint.initGUI();
    };

    var initSessvars = function () {

        cServicePointService.getUserStatus()
            .done(function(userState){
                sessvars.state = userState;
            })
            .fail(function(err){
                util.showError("Could not get User status from server.", err);
            })
		
        sessvars.statusUpdated = new Date();
		if (servicePoint.isActive() == true) {
			if (typeof sessvars.state !== 'undefined' && sessvars.state != null &&
				typeof sessvars.state.branchId !== "undefined" && sessvars.state.branchId != null &&
				typeof sessvars.state.servicePointId !== "undefined" && sessvars.state.servicePointId != null &&
				typeof sessvars.state.workProfileId !== "undefined" && sessvars.state.workProfileId != null) {
				servicePoint.storeSettingsInSession(sessvars.state);
			} else {
				// something is not valid, everything is invalid. Scenario: New configuration has been published
				resetSettings();
			}
		}
		else {
			if (typeof sessvars.state !== 'undefined' && sessvars.state != null &&
				typeof sessvars.state.branchId !== "undefined" && sessvars.state.branchId != null) {
				servicePoint.storeSettingsInSession(sessvars.state);
			} else {
				// something is not valid, everything is invalid. Scenario: New configuration has been published
				resetSettings();
			}
		}

    }

    var resetSettings = function() {
        sessvars.branchId = null;
        sessvars.branchName = null;
        sessvars.servicePointId = null;
        sessvars.servicePointUnitId = null;
        sessvars.servicePointName = null;
        sessvars.workProfileId = null;
        sessvars.profileName = null;
        sessvars.singleSettingsOnly = null;
    };
};