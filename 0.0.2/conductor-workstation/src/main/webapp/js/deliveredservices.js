var deliveredservices = new function() {

    this.openAddDeliveredServiceDialog = function(okCallback) {

        //$('#delivered_services_none_available').addClass('hidden');

        if(util.isNull(sessvars.state.visit)) {
            util.showMessage("Can't open Transaction Types dialog, no ongoing visit.");
            return;
        }

        // Populate modal
        CaseServicePointService.getDeliveredServices(sessvars.branchId, sessvars.state.visit.currentVisitService.serviceId)
            .done(function(deliveredServices){
				if (deliveredServices.length == 1) {
					addDeliveredService(deliveredServices[0].id + "", okCallback);
				}
				else {
					$('#delivered_services').empty();
					$('#delivered_services_modal_header').text('Add Transaction Types to Ticket ' + sessvars.state.visit.ticketId);
					
					//var addedItems = 0;
					for(var a = 0; a < deliveredServices.length; a++) {
						var item = deliveredServices[a];
						//var disable = isDeliveredServiceAdded(item.id);

						var btn = document.createElement('button');
						$(btn).addClass('btn').addClass('ds-btn').text(item.name).attr('id', item.id).attr('style', 'width: 100%; padding: 10px;');
						$('#delivered_services').append(btn);
						/*
						if(disable) {
							$('#' + item.id).addClass('disabled');
							$('#' + item.id).attr('disabled', 'disabled');
						} else {
							addedItems++;
						}
						*/
					}
					/*
					if(addedItems == 0) {
						$('#delivered_services_none_available').removeClass('hidden');
					}
					*/
					
					// Bind the OK function
					$('#delivered_services_ok_btn').unbind().click(function() {

						var dsList = $('#delivered_services').find('.active');
						if(dsList.length == 0) {
							$('#delivered_services_modal').modal('hide');
						}

						var deliveredServiceList = "";
						for(var i=0; i<dsList.length; i++){
							deliveredServiceList += $(dsList[i]).attr('id');
							if(i!=dsList.length-1){
								deliveredServiceList += ",";
							}
						}
						addDeliveredService(deliveredServiceList, okCallback);
					});
					 $('#delivered_services_modal').modal({
						 "backdrop" : "static",
						 "show" : true
					 });
				} 
            }
        );
    };
	
	var addDeliveredService = function(deliveredServiceList, okCallback) {
		CaseServicePointService.addDeliveredServices(deliveredServiceList)
                .done(function(userStatus){
                    sessvars.state = userStatus;
                    if(okCallback) {
                        $('#delivered_services_modal').modal('hide');
                        okCallback();
                    } else {
                        common.getCaseByVisitId(sessvars.state.visit.id, function(dbCase) {
                            servicePoint.populateCurrentTransaction(dbCase);
                        });
                        $('#delivered_services_modal').modal('hide');
                    }
                })
                .fail(function(err){
                    util.showMessage("Transaction Types couldn't be added.", err);
                })
	}

    var isDeliveredServiceAdded = function(dsId) {
        // Check so this delivered service isn't already added
        var addedDSList = sessvars.state.visit.currentVisitService.visitDeliveredServices;
        if(util.notNull(addedDSList)) {
            for(var a = 0; a < addedDSList.length; a++) {
                if(addedDSList[a].deliveredServiceId == dsId) {
                    return true;
                }
            }
        }
        return false;
    };

    this.removeDeliveredService = function(qpVisitServiceDeliveredServiceId) {

        util.openConfirm('Remove Transaction Type?','Are you sure you wish to remove this Transaction Type?', function() {
            CaseServicePointService.removeDeliveredService(qpVisitServiceDeliveredServiceId)
				.done(function(userStatus) {
                    sessvars.state = userStatus;
                    common.getCaseByVisitId(sessvars.state.visit.id, function(dbCase) {
                        servicePoint.populateCurrentTransaction(dbCase);
                        $('#confirm_modal').modal('hide');
                    });
                }).fail(function(msg) {
                    util.showMessage('Error removing Transaction Type: ' + msg);
                    $('#confirm_modal').modal('hide');
                });
        });

    };


    this.loadMarks = function() {

        if(util.isNull(sessvars.currentCase.ongoingService)) {
            util.showError('Cannot load marks, no ongoing service.');
            return;
        }

        var dsList = sessvars.currentCase.ongoingService.caseServiceMarks;
        $('#marks-container').empty();
        $('#marks-container').append('<div class="row-fluid"><div class="span11 headline line">Transaction Types</div></div>');

        var editAllowed = sessvars.currentCase.ongoingService.status == 'SERVING';
        //if(dsList.length > 0) {
	        var container = $('<div class="row-fluid"></div>');
	        //$(container).append('<div class="span1"></div>');
	        //var headlineColumn = $('<div class="span3 headline"></div>');
	        //$(container).append(headlineColumn);
	        var deliveredServicesColumn = $('<div class="span8"></div>');
	        $(container).append(deliveredServicesColumn);
	        
	        $.map(dsList, function(ds, index) {
	            var markup = markupBuilder.buildMarkMarkup(ds, editAllowed);
	            $(deliveredServicesColumn).append(markup);
	        });
			$(container).append('<div class="span3" style="text-align:right;"><span id="cst-add-mark-btn" style="cursor: pointer;"><i class="icon-add"></i> Add Transaction Type</span></div>');
	        $('#marks-container').append(container);
        //}

        if(editAllowed){
            $('#cst-add-mark-btn').unbind().click(function() {deliveredservices.openAddDeliveredServiceDialog()});
            $('#cst-add-mark-btn').show();
        }else{
            $('#cst-add-mark-btn').hide();
        }
    };
	
	this.isVirtualDeliveredServiceAdded = function() {
        // Check if Virtual Delivered Services added ("NOSHOW", "ONHOLD", "Queue Correction")
		var virtualTT = false;
		if (sessvars.state.visit == null) 
			return false;
		if (sessvars.state.visit.currentVisitService == null) 
			return false;
        var addedDSList = sessvars.state.visit.currentVisitService.visitDeliveredServices;
		
        if(util.notNull(addedDSList)) {
            for(var a = 0; a < addedDSList.length; a++) {
                if( addedDSList[a].deliveredServiceName == 'NOSHOW' ||
					addedDSList[a].deliveredServiceName == 'ONHOLD' ||
					addedDSList[a].deliveredServiceName == 'Queue Correction') {
                    virtualTT = true;
                }
            }
			if (virtualTT) {
				for(var a = 0; a < addedDSList.length; a++) {
					if( addedDSList[a].deliveredServiceName != 'NOSHOW' &
						addedDSList[a].deliveredServiceName != 'ONHOLD' &
						addedDSList[a].deliveredServiceName != 'Queue Correction') {
						virtualTT = false;
					}
				}
			}
        }
        return virtualTT;
    };

    this.isOutcomeOrDeliveredServiceNeeded = function() {
        return sessvars.state.visitState == servicePoint.visitState.OUTCOME_NEEDED ||
            sessvars.state.visitState == servicePoint.visitState.DELIVERED_SERVICE_NEEDED ||
            sessvars.state.visitState == servicePoint.visitState.OUTCOME_OR_DELIVERED_SERVICE_NEEDED ||
            sessvars.state.visitState == servicePoint.visitState.OUTCOME_FOR_DELIVERED_SERVICE_NEEDED ||
			deliveredservices.isVirtualDeliveredServiceAdded();
    };

    this.isOutcomeOrDeliveredServiceAdded = function() {
        return sessvars.state.visit != null && sessvars.state.visit.currentVisitService != null &&
            (sessvars.state.visit.currentVisitService.visitDeliveredServices != null ||
                sessvars.state.visit.currentVisitService.visitOutcome != null);
    };

    this.hasDefinedOutcomeOrDeliveredService = function() {
        return sessvars.state.visit.currentVisitService.outcomeExists ||
            sessvars.state.visit.currentVisitService.deliveredServiceExists;
    };
};