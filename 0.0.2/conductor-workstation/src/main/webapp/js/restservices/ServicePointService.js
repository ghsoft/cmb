
var cServicePointService = (function($) {

    var _baseRestPath = '/rest/servicepoint';

    function _getBranch(branchId) {
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/branches/' + branchId,
            dataType: 'json',
            async: false,
            cache: false
        });
    }

    function _getCurrentUser() {
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/user',
            dataType: 'json',
            async: false,
            cache: false
        });
    }
	
    function _getQueues(branchId){
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/branches/' + branchId + '/queues',
            dataType: 'json',
            async: false,
            cache: false
        });
    }

    function _getServicePoint(params) {
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/branches/' + params.branchId + '/servicePoints/' + params.servicePointId,
            dataType: 'json',
            async: false,
            cache: false
        });
    }

    function _getServicePointsByDeviceType(branchId, deviceType){
        return $.ajax({
            type: 'GET',
            url: _baseRestPath + '/branches/' + branchId + '/servicePoints/deviceTypes/' + deviceType,
            dataType: 'json',
            async: false,
            cache: false
        });
    }

    function _getServices(branchId) {
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/branches/' + branchId + '/services',
            dataType: 'json',
            async: false,
            cache: false
        });
    }

    function _getSystemInformation(){
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/systemInformation',
            dataType: 'json',
            async: false,
            cache: true
        });
    }

    function _getUsersOnServicePoint(branchId, servicePointId){
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/branches/' + branchId + '/servicePoints/' + servicePointId + '/users',
            dataType: 'json',
            async: false,
            cache: false
        });
    }

    function _getUserStatus() {
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/user/status',
            dataType: 'json',
            async: false,
            cache: false
        });
    }

    function _getWorkProfiles(branchId) {
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/branches/' + branchId + '/workProfiles',
            dataType: 'json',
            async: false,
            cache: false
        });
    }

    function _logout() {
        return $.ajax({
            type: 'PUT',
            url: _baseRestPath +'/logout',
            dataType: 'json',
            async: false,
            cache: false
        });
    }

    function _removeVisit(params) {
        return $.ajax({
            type: 'DELETE',
            url: _baseRestPath +'/branches/' + params.branchId + '/servicePoints/' + params.servicePointId + '/visits/' + params.visitId,
            dataType: 'json',
            async: false,
            cache: false
        });
    }

    function _setWorkProfile(params) {
        return $.ajax({
            type: 'PUT',
            url: _baseRestPath +'/branches/' + params.branchId + '/users/' + params.userName + '/workProfile/' + params.workProfileId,
            dataType: 'json',
            async: false,
            cache: false
        });
    }

    function _endUserServicePointSession(params) {
        return $.ajax({
            type: 'DELETE',
            url: _baseRestPath +'/branches/' + params.branchId + '/servicePoints/' + params.servicePointId + '/users/' + params.userName,
            dataType: 'json',
            async: false,
            cache: false
        });
    }

	return {

		endUserServicePointSession : function(params) {
            return _endUserServicePointSession(params);
        },

        getBranch : function(branchId) {
            return _getBranch(branchId);
        },

        getCurrentUser : function() {
            return _getCurrentUser();
        },

        getQueues : function(branchId){
            return _getQueues(branchId);
        },

        getServicePoint: function(params){
            return _getServicePoint(params);
        },

        getServicePointsByDeviceType : function(branchId, deviceType){
            return _getServicePointsByDeviceType(branchId, deviceType);
        },

        getServices : function(branchId) {
            return _getServices(branchId);
        },

        getSystemInformation : function(){
            return _getSystemInformation();
        },

        getUsersOnServicePoint : function(branchId, servicePointId){
            return _getUsersOnServicePoint(branchId, servicePointId);
        },

        getUserStatus : function(){
            return _getUserStatus();
        },

        getWorkProfiles : function(branchId){
            return _getWorkProfiles(branchId);
        },

        logout : function(){
            return _logout();
        },

        removeVisit : function(params){
            return _removeVisit(params);
        },

        setWorkProfile : function(params) {
            return _setWorkProfile(params);
        }

    };
})(jQuery);