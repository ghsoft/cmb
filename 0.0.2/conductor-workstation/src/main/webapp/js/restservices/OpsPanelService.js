
var OpsPanelService = (function($) {

    var _baseRestPath = '/conductor-backend/rest/opspanel';
	var _miMountingPoint = '/rest/managementinformation/v2'

    function _getServicePoints(branchId) {
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/branches/' + branchId + "/servicepoints",
            dataType: 'json',
            async: true,
            cache: false
        });
    }
	
	function _getBranchInfo(branchId) {
        return $.ajax({
            type: 'GET',
            url: _baseRestPath +'/branches/' + branchId,
            dataType: 'json',
            async: true,
            cache: false
        });
    }
	
	function _setProfile(params) {
		$.ajax({
			type: 'PUT',
			url: _miMountingPoint + '/branches/' + params.branchId + '/user/' + params.staffName + '/profile/' + params.profileNumber,
			dataType: 'json',
			async: false,
			success: function(suc) {

			},
			error: function(xhr, type) {

			}
		});
	}

    return {
	
        getServicePoints : function(branchId){
            return _getServicePoints(branchId);
        },
		
		getBranchInfo : function(branchId){
            return _getBranchInfo(branchId);
        },
		
		setProfile : function(params){
            return _setProfile(params);
        }
		
	};
})(jQuery);