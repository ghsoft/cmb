/**
 * Stuff needed by both reception.js and workstation.js can go in here.
 */
var common = new function() {



    this.buildServicesString = function(caseServices) {
        if(util.isNull(caseServices) || caseServices.length == 0) {
            return "";
        }
        var str = "";
        for(var a = 0; a < caseServices.length; a++) {
			if (caseServices[a].status != 'FINISHED' && caseServices[a].status != settings.overriddenStatus) {
				str += caseServices[a].serviceName + '<br />';
			}
        }
        str = str.substring(0, str.length - 2);
        return str;
    };
	
	this.buildServicesStatuses = function(caseServices) {
        if(util.isNull(caseServices) || caseServices.length == 0) {
            return "";
        }
        var str = "";
        for(var a = 0; a < caseServices.length; a++) {
			if (caseServices[a].status != 'FINISHED' && caseServices[a].status != settings.overriddenStatus) {
				str += (caseServices[a].status == 'PENDING' ? 'WAITING' : caseServices[a].status) + '<br />';
			}
        }
        str = str.substring(0, str.length - 2);
        return str;
    };

    this.openAddServiceDialog = function(pCaseId, onSaveCallback) {
		CaseServicePointService.getCase(pCaseId)
			.done(function(pCase){
				if(typeof pCase === 'undefined' || pCase == null) {
					util.showError("Cannot add service, no Case is active.", err);
					return;
				}

				$('#add_service_select').empty();
				$('#checkbox_service_vip').prop('checked', false);
				$('#add_service_note').val('');
				$('#add_service_ticket_number').text(pCase.ticketNumber);

				// Add listing of current services
				var currentServices = $('#add_service_current_services');
				$(currentServices).empty();
				if(pCase.caseServices.length == 0) {
					$(currentServices).append('<span style="font-size: 12px; font-style: italic;">No other Services added.</span>');
				}
				for(var i = 0; i < pCase.caseServices.length; i++) {
					if (pCase.caseServices[i].status != settings.overriddenStatus) {
						$(currentServices).append(
							'<div class="row-fluid"><div class="span6 service-name">' + pCase.caseServices[i].serviceName + '</div>' +
								'<div class="span6">' + pCase.caseServices[i].status + '</div></div>'
						);
					}
				}

				var params = {
					"branchId":sessvars.branchId
				};
				var services = {};
				CaseServicePointService.getServices()
					.done(function(_services){
						services = _services;
					})
					.fail(function(err){
						util.showError("Could not retrieve the list of Service from the server.", err);
					})
				$.map(services, function(item) {
					$('#add_service_select').append($("<option />").val(item.id).text(item.externalName));
				});

				// Add click handler for the "add service" button

				// @Path("/branches/{branchId}/visits/{visitId}/services/{serviceId}")

				$('#add_service_save_btn').unbind();
				$('#add_service_save_btn').click(function() {
					onSaveCallback(pCase.id);
				});


				$('#addServiceModal').modal('show');
			})
			.fail(function(err){
				util.showError('Failed to fetch Case. ', err);
			})
    };

    // Strips any html elements within a string
    // inspired from http://stackoverflow.com/questions/822452/strip-html-from-text-javascript:
    this.stripHtml = function(htmlString) {
        // put the string in a div and get the raw text
        var tmp = document.createElement("DIV");
        tmp.innerHTML = htmlString;
        var text = tmp.textContent||tmp.innerText;
        // remove any nested html comments
        return text.replace(/<!--*[^<>]*-->/ig, "");
    };

    this.updateQueuesTable = function(queueTableElementId) {

        $('#' + queueTableElementId).dataTable().fnClearTable();

        CaseServicePointService.getQueues()
            .done(function(results){
                $.map(results, function(queue) {
					if (queue.name != 'SILENTCALL' && queue.name != 'NEVERCALL') {
						$('#' + queueTableElementId).dataTable().fnAddData([queue]);
					}
                });
            })
		opspanel.updateBranchInfo();
    };

    this.initQueuesTable = function(elementId, rowClickedCallback) {

        var columns = [
            {"bSearchable": false,
                "bVisible": true,
                "mDataProp": "id"
            },
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "name",
                "iDataSort": 0
            },
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "servicesWaiting"
			},
            {"bSearchable": true,
                "bVisible": true,
                "mDataProp": "waitingTime"
			},
			{"bSearchable": false,
                "bVisible": true,
                "mDataProp": "servicesServed"
			}
        ];

        $('#' + elementId).dataTable(
            {
                "aoColumns":columns,
                "bPaginate": false,
                "bLengthChange": false,
                "iDisplayLength ": false,
                "bFilter": false,
                "bInfo": false,
                "bProcessing": true,
                // Removed the ajax sourcing and replaced that with programmatic load through the API to not risk
                // concurrent loads of data which results in duplicate rows.
//                "sAjaxSource": url,
//                "fnServerData": function(sSource, aoData, fnCallback) {
//                    $.getJSON(sSource, aoData, function(json) {
//                        fnCallback({"iTotalRecords":json.length,"iTotalDisplayRecords":json.length, "aaData":json});
//                    });
//                },
                "fnRowCallback": rowClickedCallback
            });
    };

    this.getCaseByVisitId = function(visitId, callback, failCallback) {
        CaseServicePointService.getCaseByVisitId(visitId)
            .done(callback)
            .fail(failCallback)
    };
};