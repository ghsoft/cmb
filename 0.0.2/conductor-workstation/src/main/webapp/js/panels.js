/**
 * Put stupid add/remove hidden class in this js "object" for managing which panels to show/hide.
 */
var panels = new function() {
    this.showCustomerDetailsPanel = function() {
        $('#visit-details').removeClass('hidden');
        $('#search_results').addClass('hidden');
        $('#queues-container').addClass('hidden');
        $('#selected_queue').addClass('hidden');
        $('#visit-details-services-today').addClass('hidden');
    };

    this.showVisitDetailsPanel = function() {
        $('#visit-details').removeClass('hidden');
        $('#search_results').addClass('hidden');
        $('#queues-container').addClass('hidden');
        $('#selected_queue').addClass('hidden');
        $('#visit-details-services-today').removeClass('hidden');
    };

    this.showQueueTablePanel = function() {
        $('#queues-container').addClass('hidden');
        $('#selected_queue').removeClass('hidden');
        $('#visit-details').addClass('hidden');
    };

    this.showQueuesTablePanel = function() {
        $('#queues-container').removeClass('hidden');
        $('#selected_queue').addClass('hidden');
        panels.showPoolContainer();
        $('#visit-details').addClass('hidden');
    };

    this.showSearchResultsPanel = function() {
        $('#search_results').removeClass('hidden');
        $('#queues-container').removeClass('hidden');
        $('#visit-details').addClass('hidden');
    };

    this.showPoolContainer = function() {
        if(servicePoint.USE_HOLDING_QUEUE == false){
            $('#visits-users-pool-container').removeClass('hidden');
        }else{
            $('#visits-users-pool-container').addClass('hidden');
        }
    };

    this.showWorkstationContainer = function() {
        $('#counter-container').removeClass('hidden');
        panels.showPoolContainer();
        //$('#visit-details-call-btn').removeClass('hidden');
        $('#visit-details-remove-btn').removeClass('hidden');
        $('#queue_table_wrapper .call-btn').removeClass('hidden');
        $('#reception-container').addClass('hidden');
        $('#appointments-today').addClass('hidden');
		$('#opspanel-container').addClass('hidden');
		$('#opspanel-summary-area').addClass('hidden');
    };

    this.showReceptionContainer = function() {
        $('#counter-container').addClass('hidden');
        $('#visits-users-pool-container').addClass('hidden');
        //$('#visit-details-call-btn').addClass('hidden');
        $('#visit-details-remove-btn').addClass('hidden');
        $('#queue_table_wrapper .call-btn').addClass('hidden');
        $('#reception-container').removeClass('hidden');
        $('#appointments-today').addClass('hidden');
		$('#opspanel-container').addClass('hidden');
		$('#opspanel-summary-area').addClass('hidden');
        panels.expandQueueView(false);
    };
	
	this.showOpspanelContainer = function() {
		$('#opspanel-container').removeClass('hidden');
		$('#opspanel-summary-area').removeClass('hidden');
        $('#counter-container').addClass('hidden');
		$('#visits-users-pool-container').addClass('hidden');
        //$('#visit-details-call-btn').addClass('hidden');
        $('#visit-details-remove-btn').addClass('hidden');
        $('#queue_table_wrapper .call-btn').addClass('hidden');
        $('#reception-container').addClass('hidden');
        $('#appointments-today').addClass('hidden');
		panels.expandQueueView(false);
    };

    this.showCurrentTransactionPanel = function() {
        $('#cst-notes-container').removeClass('hidden');
        $('#cst-main-container').removeClass('hidden');
		try {
			if (sessvars.state.visit.currentVisitService.serviceExternalName == 'ONHOLD') {
				$('#cst-marks-container').removeClass('hidden');
			}
			else {
				$('#cst-marks-container').addClass('hidden');
			}
		}
		catch (e) {
			$('#cst-marks-container').addClass('hidden');
		}
        panels.expandQueueView(false);
    };

    this.hideCurrentTransactionPanel = function() {
        $('#cst-notes-container').addClass('hidden');
        $('#cst-marks-container').addClass('hidden');
        $('#cst-main-container').addClass('hidden');
        panels.expandQueueView(true);
    };

    this.resetRightPanel = function() {
        $('#visit-details').addClass('hidden');
        $('#search_results').addClass('hidden');
        $('#queues-container').removeClass('hidden');
        $('#selected_queue').addClass('hidden');
    };

    this.showCustomerSearchResultsPanel = function() {
        $('#customer_search_results_container').removeClass('hidden');
    };

    this.hideCustomerSearchResultsPanel = function() {
        $('#customer_search_results_container').addClass('hidden');
    };

    this.expandQueueView = function(expand){
        if(expand){
            $('#counter-reception-area').removeClass('span5');
            $('#counter-reception-area').addClass('span3');
            $('#queue-appointment-area').removeClass('span7');
            $('#queue-appointment-area').addClass('span9');
            $('#counter-action-bar').addClass('btn-group-vertical');
            $('#counter-action-bar').removeClass('row-fluid');
        }else{
            $('#counter-reception-area').removeClass('span3');
            $('#counter-reception-area').addClass('span5');
            $('#queue-appointment-area').removeClass('span9');
            $('#queue-appointment-area').addClass('span7');
            $('#counter-action-bar').addClass('row-fluid');
            $('#counter-action-bar').removeClass('btn-group-vertical');
        }
    }
};
