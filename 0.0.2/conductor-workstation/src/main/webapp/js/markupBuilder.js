/**
 * Dejan Milojevic (dejmil)
 */

var markupBuilder = new function() {

    var noteHtml = "";

    this.buildNoteMarkupV2 = function(caseId, dbNote, editAllowed, deleteAllowed, callback) {
        var note = $(document.createElement('div'));
        $(note).addClass('note');
        $(note).append('<div class="name"><i>' + dbNote.text + '</i></div>');

        if(editAllowed && (servicePoint.ALLOW_ONLY_CREATOR_TO_EDIT_NOTE == false || dbNote.createdBy == sessvars.currentUser.fullName)){
            var action = $('<div class="text-right action"></div>');
            $(note).append(action);

            var editIcon = $('<i class="icon-edit-note" style="margin-left: 6px;" title="Edit note"></i>');
            $(action).append(editIcon);
            (function(_dbNote) {
                $(editIcon).click(function() {
                    //Note: CaseServiceId (2nd argument) is not needed/used when edit (3rd argument) is true.
                    notes.openNoteDialogV2(_dbNote, caseId, true, callback);
                });
            })(dbNote);

            if(deleteAllowed) {
                var deleteIcon = $('<i class="icon-delete-note"  title="Remove note"></i>');
                $(action).append(deleteIcon);
                (function(_dbNote) {
                    $(deleteIcon).click(function() {
                        notes.deleteNote(_dbNote.id, callback);
                    });
                })(dbNote);
            }

        }
        $(note).append('<div class="user"><i>' + util.timeOfDate(dbNote.created) + ' ' + dbNote.createdBy + '</i></div>');

        return note;
    };

    this.buildNoteMarkup = function(dbNote, serviceId, editAllowed, deleteAllowed, callback) {
        var note = $(document.createElement('div'));
        $(note).addClass('note');
        $(note).append('<div class="name"><i>' + dbNote.text + '</i></div>');

        if(editAllowed && (servicePoint.ALLOW_ONLY_CREATOR_TO_EDIT_NOTE == false || dbNote.createdBy == sessvars.currentUser.fullName)){
        	var action = $('<div class="text-right action"></div>');
        	$(note).append(action);
        	
        	var editIcon = $('<i class="icon-edit-note" style="margin-left: 6px;" title="Edit note"></i>');
        	$(action).append(editIcon);
        	(function(_dbNote, _serviceId) {
        		$(editIcon).click(function() {
        			//Note: CaseServiceId (2nd argument) is not needed/used when edit (3rd argument) is true.
        			notes.openNoteDialog(_dbNote, serviceId, true, callback);
        		});
        	})(dbNote, serviceId);

            if(deleteAllowed) {
                var deleteIcon = $('<i class="icon-delete-note"  title="Remove note"></i>');
                $(action).append(deleteIcon);
                (function(_dbNote) {
                    $(deleteIcon).click(function() {
                        notes.deleteNote(_dbNote.id, callback);
                    });
                })(dbNote);
            }

        }
        $(note).append('<div class="user"><i>' + util.timeOfDate(dbNote.created) + ' ' + dbNote.createdBy + '</i></div>');

        return note;
    };


    this.buildMarkMarkup = function(caseServiceMark, editAllowed) {
    	var markName = '<div class="name">' + caseServiceMark.mark.name + '</div>';
    	var markAction = $('<div class="text-right action"></div>');
    	if(editAllowed) {
    		markAction.append('<i class="icon-trash" onclick="javascript:deliveredservices.removeDeliveredService(' + caseServiceMark.qpVisitDeliveredServiceId + ');" title="Remove delivered service"></i>');
    	}
    	var mark = $('<div></div>');
    	mark.addClass('mark');
    	mark.append(markName);
    	mark.append(markAction);
        return mark.wrap('<div></div>').parent().html();
    };

};
